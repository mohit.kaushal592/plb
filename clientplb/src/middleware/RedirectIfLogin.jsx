import React from "react";
import { Navigate, Outlet } from "react-router-dom";

const ProtectedAdmin = () => {
    var authenticate = localStorage.getItem('accessToken');
    if(authenticate){
        return <Navigate to="/clientDashboard" replace/> 
    }else{
        return <Outlet /> 
    } 
}

export default ProtectedAdmin;