import React, { useState, useEffect } from 'react';
import { useForm } from "react-hook-form";
import ReactDOM from 'react-dom';
import { Link, NavLink, useLocation, useSearchParams } from 'react-router-dom';
import { useNavigate } from 'react-router-dom';
import '../Auth/Auth.css';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import Top from '../includes/Top';
import Header from '../includes/Header';
import Loader from '../Common/Loader/Loader';
import axios from 'axios';

function AddUser() {
    const [searchParams, setSearchParams] = useSearchParams()
    const [user, userData] = useState('');
    const [role, setRole] = useState(localStorage.getItem("role").toLowerCase());
    const [department, setDepartment] = useState(localStorage.getItem("department").toLowerCase());

    const navigate = useNavigate();
    const { register, handleSubmit, formState: { errors }, setValue } = useForm();

    var url;

    if (!process.env.NODE_ENV || process.env.NODE_ENV === 'development') {
        // Use the development URL or a default value if it's not defined
        url = process.env.REACT_APP_PRODUCTION_URL;
    } else {
        // Use the production URL from your environment variables
        url = process.env.REACT_APP_PRODUCTION_URL;
    }
    const [accessToken, setAccessToken] = useState(localStorage.getItem('accessToken'));
    const [loading, setLoading] = useState(false);

    const headerData = {
        headers: {
            Authorization: `Bearer ${accessToken}`
        }
    }
    var userID = searchParams.get('edit')


    console.log("userID", userID)
    useEffect(() => {
        async function getData() {
            if (userID != null) {
                const userRes = await axios.get(`${url}/user_edit/${userID}/`, headerData);
                setValue("first_name", userRes.data.first_name);
                setValue("last_name", userRes.data.last_name);
                setValue("email", userRes.data.email);
                setValue("address", userRes.data.address);
                // setValue("gender", userRes.data.gender);
                setValue("department", userRes.data.department);
                setValue("role", userRes.data.role);
                setValue("phone_no", userRes.data.phone_no);
            } else {
                setValue("first_name", '');
                setValue("last_name", '');
                setValue("email", '');
                setValue("address", '');
                // setValue("gender", '');
                setValue("department", 'client');
                setValue("role", 'team');
                setValue("phone_no", '');
            }
        }
        getData();
    }, [userID]);


    const onSubmit = data => {
        console.log("data",data)
        setLoading(true)
        if (userID == null) {
            axios.post(`${url}/signup/`, data, headerData).then((response) => {
                setLoading(false)
                if (response.status == 201) {
                    toast.success(`${response.data.message}`)
                    //  navigate(`/manageUser`);
                    navigate(-1);
                }
            }).catch((error) => {
                if (error) {
                    toast.error("Some tehnical issue.")
                }
            });
        } else {
            //var result = Object.keys(data).map(key => key + '=' + data[key]).join('&');
            //console.log(`${url}/users/${userID}/?first_name=Aman`)
            axios.put(`${url}/users/${userID}/`, data, headerData).then((response) => {
                setLoading(false)
                if (response.status == 200) {
                    toast.success(`Record updated successfully.`)
                }
            }).catch((error) => {
                if (error) {
                    toast.error("Some tehnical issue.")
                }
            });
        }
    }

    return (
        <>
            <div className="container-fluid d-flex align-items-center justify-content-center vh-100  login_card">
                <ToastContainer />
                <div className="row">
                {/* <Top /> */}
                <div className="col-lg p-0" id="Center">
                    {/* <Header /> */}
                    <div className="main card project_card">
                    {/* <div className="row mt-3">
                        <div className="col-xl-12 d-flex justify-content-between align-items-center">
                        <div className="breadcrumbs">
                            <nav aria-label="breadcrumb">
                            <ol className="breadcrumb">
                                <li className="breadcrumb-item">
                                <a href="#" className="text-decoration-none">User Management</a>
                                </li>
                                <li className="breadcrumb-item active" aria-current="page">Add Team Member</li>
                            </ol>
                            </nav>
                        </div>
                        </div>
                    </div> */}
                    <form className="mt-3" onSubmit={handleSubmit(onSubmit)}>
                        <div className="d-flex justify-content-between gap-4 flex-column flex-sm-row align-items-sm-center align-items-start p-2 border-bottom">
                        <h5 className="main_heading">Add Client</h5>
                        <div className="buttons d-flex gap-3 justify-content-between align-items-sm-center  align-items-center">
                            <button className="btn_outlined">Edit</button>
                            <button className="main_btn">
                            <img src="/assets/Icons/Plus.svg" className="me-3" alt="" />{loading ? <Loader />:'Save Details' }</button>
                        </div>
                        </div> 
                        <div className=" p-2 "> 
                        <div className="col-lg-12">
                            <div className="p-2">
                            <div className="row g-3">
                                <div className="col-lg-7 bg-white p-4 border rounded-4">
                                <div className="row">
                                    <div className="col-lg-6">
                                    <div className="mb-3 ">
                                        <label for="FullName" className="form-label text-start">First Name</label>
                                        <input type="text" className="form-control " id="FullName" {...register('first_name', { required: true })} />
                                    </div> {errors.first_name && <p className="text-danger error-custom">First name is required.</p>}
                                    </div>
                                    <div className="col-lg-6">
                                    <div className="mb-3 ">
                                        <label for="FullName" className="form-label text-start">Last Name</label>
                                        <input type="text" className="form-control " id="FullName" {...register('last_name', { required: false })} />
                                    </div> {errors.last_name && <p className="text-danger error-custom">Last name is required.</p>}
                                    </div>
                                    {/* <div className="col-lg-6">
                                    <div className="mb-3 ">
                                        <label for="MobileNo" className="form-label text-start">Gender</label>
                                        <select className="form-control" {...register('gender', { required: true })}>
                                        <option value="">Select Gender</option>
                                        <option value="male">Male</option>
                                        <option value="female">Female</option>
                                        <option value="other">Other</option>
                                        </select>
                                    </div> {errors.gender && <p className="text-danger error-custom">Gender is required.</p>}
                                    </div> */}
                                    <div className="col-lg-6">
                                    <div className="mb-3">
                                        <label for="email1" className="form-label text-start">Email</label>
                                        <input type="email" className="form-control " id="email1" {...register('email', { required: true })} />
                                    </div> {errors.email && <p className="text-danger error-custom">Email is required.</p>}
                                    </div>
                                    <div className="col-lg-6">
                                    <div className="mb-3 ">
                                        <label for="phone" className="form-label text-start">Mobile Number</label>
                                        <input type="tel" className="form-control " id="phone" {...register('phone_no', { required: false })} />
                                    </div> {errors.phone_no && <p className="text-danger error-custom">Phone is required.</p>}
                                    </div>
                                    {/* <div className="col-lg-6">
                                    <div className="mb-3 ">
                                        <label for="ID" className="form-label text-start">Employee ID</label>
                                        <input type="text" className="form-control " id="ID" />
                                    </div> {errors.password && <p className="text-danger error-custom">Password is required.</p>}
                                    </div> */}
                                    <div className="col-lg-6">
                                    <div className="mb-3 ">
                                        <label for="Password" className="form-label text-start">Password</label>
                                        <input type="password" className="form-control " id="Password" {...register('password', { required: true })} />
                                    </div> {errors.password && <p className="text-danger error-custom">Password is required.</p>}
                                    </div>
                                    <div className="col-lg-6 d-none">
                                    <div className="mb-3 ">
                                        <label for="report" className="form-label text-start">Role</label>
                                        <div className="dropdown ">
                                        <button className="form-control w-100 text-start" type="button" data-bs-toggle="dropdown" aria-expanded="false"> Actions </button>
                                        <ul className="dropdown-menu w-100">
                                            <li>
                                            <a className="dropdown-item">
                                                <input className="form-check-input me-2" type="checkbox" value="" id="flexCheckDefault" />Lead </a>
                                            </li>
                                            <li>
                                            <a className="dropdown-item">
                                                <input className="form-check-input me-2" type="checkbox" value="" id="flexCheckDefault" />Team </a>
                                            </li>
                                            <li className="border-top text-end p-2">
                                            <button className="main_btn">Apply</button>
                                            </li>
                                        </ul>
                                        </div>
                                    </div>
                                    </div>
                                    <div className="col-lg-6  d-none">
                                    <div className="mb-3 ">
                                        <label for="report" className="form-label text-start">Role</label>
                                        <div className="dropdown">
                                        <button className="form-control w-100 text-start" type="button" data-bs-toggle="dropdown" aria-expanded="false"> Actions </button>
                                        <ul className="dropdown-menu w-100">
                                            <li>
                                            <a className="dropdown-item">
                                                <input className="form-check-input me-2" type="checkbox" value="" id="flexCheckDefault" />Lead </a>
                                            </li>
                                            <li>
                                            <a className="dropdown-item">
                                                <input className="form-check-input me-2" type="checkbox" value="" id="flexCheckDefault" />Team </a>
                                            </li>
                                            <li className="border-top text-end p-2">
                                            <button className="main_btn">Apply</button>
                                            </li>
                                        </ul>
                                        </div>
                                    </div>
                                    </div> {/* <div className="col-lg-6">
                                    <div className="mb-3 ">
                                        <label for="MobileNo" className="form-label text-start">Department</label>
                                        <select className="form-control" {...register('department', { required: false })}>
                                        <option value="seo">Seo</option>
                                        <option value="content_writing">Content Writing</option>
                                        <option value="content_marketing">Content Marketing</option>
                                        <option value="client">Client</option>
                                        <option value="client_service">Client Service</option>
                                        </select>
                                    </div> {errors.department && <p className="text-danger error-custom">Department is required.</p>}
                                    </div> */} 


                                    {/* {(role == 'manager') ? ( <>
                                    <div className="col-lg-6">
                                        <div className="mb-3 ">
                                        <label for="MobileNo" className="form-label text-start">Department</label>
                                        <select className="form-control" {...register('department', { required: false })}>
                                            <option value="seo">Seo</option>
                                            <option value="content_writing">Content Writing</option>
                                            <option value="content_marketing">Content Marketing</option>
                                            <option value="client">Client</option>
                                            <option value="client_service">Client Service</option>
                                        </select>
                                        </div> {errors.department && <p className="text-danger error-custom">Department is required.</p>}
                                    </div>
                                    <div className="col-lg-6">
                                        <div className="mb-3 ">
                                        <label for="MobileNo" className="form-label text-start">Role</label>
                                        <select className="form-control" {...register('role', { required: false })}>  <option value="lead">Lead</option>
                                            <option value="team">Team</option> /
                                        </select>
                                        </div> {errors.role && <p className="text-danger error-custom">Role is required.</p>}
                                    </div>
                                    </> ) : ( <>
                                    <div className="col-lg-6 d-none">
                                        <div className="mb-3 ">
                                        <label for="Department" className="form-label text-start">Department</label>
                                        <select className="form-control " {...register('department', { required: false })}>
                                            <option value={department} selected>{department}</option>
                                        </select>
                                        </div> {errors.department && <p className="text-danger error-custom">Department is required.</p>}
                                    </div>
                                    <div className="col-lg-6 d-none1">
                                        <div className="mb-3 ">
                                        <label for="Role" className="form-label text-start">Role</label>
                                        <select className="form-control" {...register('role', { required: false })}>  <option value="lead">Lead</option>
                                            <option value="team">Team</option>
                                        </select>
                                        </div> {errors.role && <p className="text-danger error-custom">Role is required.</p>}
                                    </div>
                                    </>
                                    )} */}

                                    
                                    <div className="col-lg-6">
                                        <div className="mb-3 ">
                                        <label for="report" className="form-label text-start">Address</label>
                                        <textarea name="Address" id="Address" cols="30" rows="20" className="form-control" {...register('address', { required: false })}></textarea>
                                        </div> {errors.address && <p className="text-danger error-custom">Address is required.</p>}
                                    </div> 
                                    {/* <div className="col-lg-6">
                                        <div className="mb-3 ">
                                        <label for="report" className="form-label text-start">Profile Picture</label>
                                        <input type="file" className="form-control" {...register('image', { required: false })} />
                                        </div> {errors.image && <p className="text-danger error-custom">Address is required.</p>}
                                    </div> */} {/* <div className="col-lg-12">
                                        <div className="mb-3 d-flex gap-3 align-items-center">
                                        <button type="submit" className="main_btn">Confirm</button>
                                        <button type="button" className="btn_outlined">Reset</button>
                                        </div>
                                    </div> */}
                                </div>
                                </div>
                                <div className="col-lg-5">
                                <div className="row g-3">
                                    <div className="col-lg-12 ">
                                    <div className="porfile_card p-4 d-flex flex-column align-items-center justify-content-center bg-white border rounded-4">
                                        <img src="../assets/Images/a12.jpg" width="110px" height="110px" className="" alt="" />
                                        <h4 className="fw-md my-3">Full Name</h4>
                                        <h6>email@adlift.com</h6>
                                    </div>
                                    </div>
                                    <div className="col-lg-12">
                                    <div className="p-4 d-flex flex-column align-items-center upload_img justify-content-center ">
                                        <img src="/assets/Icons/uplaod_line.svg" alt="" className="my-3" />
                                        <h6>Select a file or drag and drop here</h6>
                                        <span>JPG, JPEG OR PNG file size no more than 5MB</span>
                                        <button className="border fs-6 fw-normal bg-white  mt-3 rounded px-3 py-2">Select a file</button>
                                    </div>
                                    </div>
                                </div>
                                </div>
                            </div>
                            </div>
                        </div>
                        </div>
                    </form>
                    </div>
                </div>
                </div>
            </div>
            </>
    );
}
export default AddUser;