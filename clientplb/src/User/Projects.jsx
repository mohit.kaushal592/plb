import React, { useState, useEffect } from 'react';
import ReactDOM from 'react-dom';
import { Link, NavLink, useLocation } from 'react-router-dom';
import { useNavigate } from 'react-router-dom';
import '../Auth/Auth.css';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import Top from '../includes/Top';
import Header from '../includes/Header';
import axios from 'axios';
import DataTable from 'react-data-table-component';
import Loader from '../Common/Loader/Loader';
import ProjectModal from './ProjectModal';
import Avatar from "@mui/material/Avatar";
import AvatarGroup from "@mui/material/AvatarGroup";
import Button from '@mui/material/Button';
import ButtonGroup from '@mui/material/ButtonGroup';
// import Select from 'react-select';
import {InputLabel,MenuItem,FormControl,Select} from "@mui/material";


function Projects() {
    const navigate = useNavigate();
    const [department, setDepartment] = useState(localStorage.getItem("department").toLowerCase());
    const [role, setRole] = useState(localStorage.getItem("role").toLowerCase());

    var url;

    if (!process.env.NODE_ENV || process.env.NODE_ENV === 'development') {
        // Use the development URL or a default value if it's not defined
        url = process.env.REACT_APP_PRODUCTION_URL;
    } else {
        // Use the production URL from your environment variables
        url = process.env.REACT_APP_PRODUCTION_URL;
    }

    const [user, userData] = useState([]);
    const [pending, setPending] = useState(true);
    const [accessToken, setAccessToken] = useState(localStorage.getItem('accessToken'));
    const [loading, setLoading] = useState(true);
    const [modalIsOpen, setModalIsOpen] = useState(false);
    const [selectedRowData, setSelectedRowData] = useState(null);
    const [selectTeam,setSelectTeam] = useState('false')
    const [status,setStatus] = useState('working')
    const headerData = {
        headers: {
            Authorization: `Bearer ${accessToken}`
        }
    }


    const selectStatus = [
        { label: 'Working', value: 'working' },
        { label: 'Completed', value: 'completed' },
        { label: 'Rejected', value: 'rejected' },
    ];


    const getData = async (site_status=status) => {
        try {
            setLoading(true);
                //const userRes = await axios.get(`${url}/seo/project/?project=&status=${site_status}`, headerData);
                await axios.get(`${url}/seo/project_manager/?project=&status=${site_status}&team=${selectTeam}`,headerData).then((response) => {  
                if(site_status != ''){  
                    setTimeout(function(){
                        document.getElementById("staus_filter").value = site_status;
                    }, 1000);
                } 
                userData(response.data);
                setLoading(false);
                setPending(false);
            }).catch( (error)=> {
                if(error){
                    toast.error("Some tehnical issue.")
                }
            });

        } catch (error) {
            console.error('Error fetching data:', error);
        }
    };

    useEffect(() => {
        getData();
    }, [selectTeam]);

    
    const deleteData = (delete_id) => {
        if (window.confirm("Are you sure you want to delete this item?") == true) {
        axios.delete(`${url}/seo/projects/${delete_id}/`, headerData).then((response) => {
            toast.success(`Record delete successfully !!`)
            getData()
            navigate(`/projects`);
                    }).catch((error) => {
            if (error) {
                debugger;
                toast.error("Some tehnical issue.")
            }
        });
        }
    }

    const generateBackgroundColor = (char) => {
        const backgroundColors = ['#BE4600', '#0076BF','#6C00BF','#BF0056','#FF95E1'];
        const charCode = char.charCodeAt(0);
        const colorIndex = charCode % backgroundColors.length;
    
        return backgroundColors[colorIndex];
      };

      const AvatarStyle={background: "blue" ,fontSize:'14px'}

    const columns = [
        {
            name: 'S No.',
            selector: (row, index) => (index + 1).toString(),
            sortable: true,
            width:'80px'
        },
        {
            name: 'Project Name',
            selector: row => <div style={{height:'70px',display:"flex",alignItems:'center'}}>{row.project_name}</div>,
            sortable: true
        },
        {
            name: 'Domain',
            selector: row => row.domain,
            sortable: true
        },
        {
            name: 'Team',
            selector: row => {
                console.log("rowww",row)
                if (!row.user || row.user.length === 0) {
                    return "";
                }
                
                const formattedNames = row.user
                    .filter(user => user && user.first_name && user.department)
                    .map(user => `${user.department}: ${user.first_name}`);
                
                const gridColumns = `repeat(auto-fit, minmax(200px, 1fr))`;
                // row?.user?.map((selectedRow)=> <div>A</div>)


        
                return (
                //     <div className="gridCell" style={{
                //         display: 'grid',
                //         gridTemplateColumns: gridColumns,
                //         gap: '2px',
                //     }}>
                //         {formattedNames.map((nameWithDept, index) => (
                //             <div key={index} style={{
                //                 padding: '4px',
                //                 border: '1px solid #ccc'
                //             }}>
                //                 {nameWithDept}
                //             </div>
                //         ))}
                //     </div>
                
                    
                    <AvatarGroup max={4} onClick={()=>handleRowClick(row)}>
                    {row?.user?.map((selectedRow)=>
                       <Avatar  key={selectedRow?.first_name} 
                       style={{ ...AvatarStyle, backgroundColor: generateBackgroundColor(selectedRow?.first_name[0]) ,width: '30px',
                       height: '30px'}}
                       
                       >
                        {selectedRow?.first_name[0].toUpperCase()}
                    </Avatar>
                    )}
                    </AvatarGroup>
                    
                );
            },
            sortable: true,
            width: '250px' 
        },
        {
            name: 'Ownership',
            selector: row => row.ownership?.first_name ?? "",
            sortable: true
        },
        {
            name: 'Created On',
            selector: row => row.created_at,
            sortable: true
        },
        {
            name: 'Status',
            selector: row => 
            // row.status,
            (
                <div className={row?.status == "pending" ? 'pill_reject_myList' : row?.status == "working" ? 'pill_active' :row?.status == "completed" ? 'pill_reject':
                'pill_reject'}>{row?.status.charAt(0).toUpperCase() + row?.status.slice(1).toLowerCase()}
                </div>
                ),
            sortable: true
        },
        {
            name: 'Created By',
            selector: row => row.created_by.first_name,
            sortable: true
        },

        {
            name: 'Actions',
            selector: row => <>
            {(department == 'seo' || department == 'content_writing' || department == 'content_marketing') ?

           <Link to={`/addprojects?edit=${row.id}`} >
               <img src="../assets/Icons/Edit.svg" width="20px" alt="" />
           </Link>:
            
            <div className='d-flex gap-3'>
        
                <Link to={`/addproject?edit=${row.id}`} >
                    <img src="../assets/Icons/Edit.svg" width="20px" alt="" />
                </Link>
                {role=='manager'?<Link to='#' onClick={() => { deleteData(row.id) }}>
                    <img src="../assets/Icons/DeleteOutlined.svg" width="20px" data-bs-toggle="modal" data-bs-target="#delete_modal" alt="" className="ml-3" />
                </Link>:''}
                </div>
        }
            </>,
        }
    ];

    const handleChange = ({ selectedRows }) => {
        console.log('Selected Rows: ', selectedRows);
    };

    const [filterText, setFilterText] = React.useState('');
    const [resetPaginationToggle, setResetPaginationToggle] = React.useState(false);

    const filteredItems = user.filter(
        item => item.project_name && item.project_name.toLowerCase().includes(filterText.toLowerCase())
    );
 
    const subHeaderComponentMemo = React.useMemo(() => {
        const btnStyle={color:'#D7692A'}
        const handleClear = () => {
            if (filterText) {
                setResetPaginationToggle(!resetPaginationToggle);
                setFilterText('');
            }
        };
        if(department == 'seo' || department == 'content_writing' || department == 'content_marketing'){
            setSelectTeam(true)
        }
        return (
            <div className='d-flex gap-5'>
                 {(department == 'seo' || department == 'content_writing' || department == 'content_marketing') ? "":
            <ButtonGroup variant="text" aria-label="text button group">
               <Button style={{...btnStyle,backgroundColor:`${selectTeam=='false' ? '#fcdac6': ''}`}} onClick={()=>setSelectTeam("false")}>ME</Button>
               <Button style={{...btnStyle,backgroundColor:`${selectTeam=='true' ? '#fcdac6': ''}`}} onClick={()=>setSelectTeam("true")}>TEAM</Button>
            </ButtonGroup>}
           
            <div className='d-flex gap-2'>
            {/* <select className="form-control" id="staus_filter" style={{ width: "150px" }} onChange={e => getData(e.target.value)}>
                    <option value="" selected hidden>Status</option>
                    <option value='working'>Working</option>
                    <option value='completed'>Completed</option>
                    <option value='pending'>Rejected</option>
                </select> */}
                 {/* <Select
                className="custom-select"
                placeholder="Status"
                options={selectStatus}
                onChange={e => getData(e.value)}
                /> */}
                <FormControl sx={{ m: 1, minWidth: 150 }} size="small">
            <InputLabel id="demo-select-small-label">Status</InputLabel>
            <Select
              labelId="demo-select-small-label"
              id="demo-select-small"
              value={status}
              label="Status"
              onChange={(e) => {getData(e.target.value);setStatus(e.target.value)}}
            >
              {selectStatus.map((option) => (
                <MenuItem value={option?.value} key={option?.value}>
                  {option?.label}
                </MenuItem>
              ))}
            </Select>
          </FormControl>
            <input className="form-control mt-2" style={{ width: "250px",height:"40px" }} onChange={e => setFilterText(e.target.value)} onClear={handleClear} filterText={filterText} placeholder="Search..." />
            </div>
            </div>
        );
    }, [filterText, resetPaginationToggle,selectTeam,status]);

    const handleRowClick = (row) => {
        setSelectedRowData(row);
        setModalIsOpen(true);
      };

      const closeModal = () => {
        setModalIsOpen(false);
        setSelectedRowData(null);
      };
      console.log("setSelectedRowData",selectedRowData)

    const rowStyles = (rowData) => {
    
    console.log("rowData",rowData,selectedRowData)
    return({
        background:  rowData?.id.substring(4) ==selectedRowData?.id ? '#FFE4D0' : 'transparent',
        color: selectedRowData == rowData ? 'white' : 'inherit',
        cursor: 'pointer',
      })}

console.log("useruser",user)
    return (
        <div className="container-fluid ">
            <ToastContainer />
            <div className="row">
                <Top projects={true}/>
                <div className="col-lg p-0" id="right_side">
                    <Header />
                    <div class="main">
                        {/* <div class=" w-100">
                            <div class="col-xl-12 d-flex py-3 border-bottom justify-content-between align-items-center">
                                <div class="breadcrumbs   ">
                                    <nav aria-label="breadcrumb">
                                        <ol class="breadcrumb">
                                            <li class="breadcrumb-item"><a href="#">Projects</a></li>
                                            <li class="breadcrumb-item active" aria-current="page">Projects List</li>
                                        </ol>
                                    </nav>
                                </div>
                            </div>
                        </div> */}
                        <div class="d-flex py-4  justify-content-between align-items-center">
                            <h5 class="main_heading" style={{ flex: 1 }}>Projects Listing</h5>

                        </div> {loading ? (
                            <Loader />
                        ) : (
                            <div className="my-data-table">
                            <DataTable
                                columns={columns}
                                data={filteredItems}
                                pagination
                                selectableRows
                                onSelectedRowsChange={handleChange}
                                progressPending={pending}
                                subHeader
                                persistTableHead
                                subHeaderComponent={subHeaderComponentMemo}
                                onRowClicked={handleRowClick}
                                customStyles={{
                                    rows: {
                                      style: rowStyles,
                                    },
                                  }}
                            />
                            </div>
                        )}
                        {selectedRowData && (
                        <ProjectModal
                        modalIsOpen={modalIsOpen}
                        closeModal={closeModal}
                        rowData={selectedRowData}
                    />

                   
                        )}

                    </div>

                </div>
            </div>
        </div>
    );
}
export default Projects;