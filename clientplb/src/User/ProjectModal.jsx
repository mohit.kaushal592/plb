import React,{useState} from 'react';
import { Link } from 'react-router-dom';
import {ModalCustom} from '../Common/ModalCustom/Modal'; // Make sure to import ModalCustom correctly

const ProjectModal = ({modalIsOpen,closeModal,rowData}) => {
  const [teamDepartment, setDepartment] = useState(localStorage.getItem("department").toLowerCase());
  const departmentData = rowData?.user?.reduce((acc, member) => {
    if (!acc[member.department]) {
      acc[member.department] = [];
    }
    acc[member.department].push(member.first_name);
    return acc;
  }, {});

  const departments = Object.keys(departmentData);
  const badge = {
    width: '25px',
    height: '25px',
    borderRadius: '50%',
    backgroundColor: '#FF9595',
    fontWeight: 500,
    textAlign: 'center',
    fontSize: '16px',
    color:  '#0f0f0f',
    marginRight:"12px"
  };

  const generateBackgroundColor = (char) => {
    const backgroundColors = ['#FF9595', '#95FFC8','#9595FF','#FFD795','#FF95E1'];
    const charCode = char.charCodeAt(0);
    const colorIndex = charCode % backgroundColors.length;

    return backgroundColors[colorIndex];
  };

  const formatDepartmentName = (department) => {
    return department.replace(/_/g, ' ').replace(/\w\S*/g, (txt) => txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase());
  };

  return (
    <ModalCustom
      show={modalIsOpen}
      handleClose={closeModal}
      backdrop="static"
      title="Team"
    >
      <hr className='mt-0'/>
      <div class="modal-content">
        <div className="modal-body d-flex">
          <div className="d-flex flex-column">
            <div className="d-flex"><h6>Created By:</h6><h6>{rowData.created_by.first_name}</h6></div>
            <br />
            <div className="project_modal_container" style={{overflowX: 'auto'}}>
              {departments.map((department,depIndex) => (
                <>
                <div className="project_modal_column" key={department} style={{borderRight: `${depIndex==departments.length - 1 ? '':'2px solid orange'}` ,height:'100%',width:'200px'}}>
                  <h5 className="d-flex" style={{width:'100%'}}>{formatDepartmentName(department)}</h5>
                  {departmentData[department].map((member, index) => (
                    <div key={index} className='d-flex' >
                     <span style={{ ...badge, backgroundColor: generateBackgroundColor(member[0]) }}>
                          {member[0]?.toUpperCase()}
                        </span>
                      <div className='d-block'>{formatDepartmentName(member)}</div>
                    </div>
                  ))}
                </div>
                {/* {(depIndex==departments.length - 1)?'':<img src="../assets/Icons/Project_modal_vector.svg" width="2px" height="100%" alt="" />} */}
               
                </>
              ))}
            </div>
            <div className='project_modal_footer mt-5'>
            <div className='d-flex justify-content-center mt-2'>Project Name: <h5>{rowData?.project_name}</h5></div>
            <div>Link: <Link to={rowData?.domain}>{rowData?.domain}</Link></div>
            {/* <h6>Category: {rowData?.product}</h6> */}
            <div className='d-flex justify-content-center'>Category: <h5>{rowData?.product}</h5></div>
            <Link to={`${teamDepartment == 'seo' || teamDepartment == 'content_writing' || teamDepartment == 'content_marketing' ? `/addprojects?edit=${rowData?.id}` : `/addproject?edit=${rowData?.id}`}`} ><button className='btn_outlined'>Edit Team</button></Link>
            
            </div>
          </div>
        </div>
      </div>
    </ModalCustom>
  );
};

export default ProjectModal;
