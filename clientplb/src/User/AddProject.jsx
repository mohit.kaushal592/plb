import axios from "axios";
import React, { useEffect, useState } from "react";
import { ToastContainer } from "react-toastify";
import Loader from "../Common/Loader/Loader";
import { useNavigate, useSearchParams } from "react-router-dom";
import Select from 'react-select';
function AddProjects() {
    const [list, listData] = useState([]);
    const [accessToken, setAccessToken] = useState(localStorage.getItem('accessToken'));
    const [pending, setPending] = useState(true);
    const [loading, setLoading] = useState(true);
    const [projectName, setProjectName] = useState('');
    const [domain, setDomain] = useState('');
    const navigate = useNavigate();
    const [department, setDepartment] = useState(localStorage.getItem("department").toLowerCase());
    const [selectedImageFile, setSelectedImageFile] = useState(null);
    const [selectedImage, setSelectedImage] = useState(null);
    const [selectedDep, setSelectedDep] = useState('');
    const [userList, setUserList] = useState([]);
    const [selectedUsers, setSelectedUsers] = useState([]);
    const [selectedOptions, setSelectedOptions] = useState([]);
    const [userId, setUserId] = useState('')
    const [selectedOwner, setSelectedOwner] = useState([]);
    const [selectedEditOwner, setSelectedEditOwner] = useState([]);
    const [searchParams, setSearchParams] = useSearchParams()
    const [updateProjectName, setUpdateProjectName] = useState('');
    const [updateDomain, setUpdateDomain] = useState('');
    const [selectClient, setSelectClient] = useState('');
    const [selectedClient, setSelectedClient] = useState([]);
    const [selectedEditClient, setSelectedEditClient] = useState([]);
    const [scope, setScope] = useState('');
    const [updateScope, setUpdateScope] = useState('');



    const departments = [
        { label: 'Client Service', value: 'client_service' },
        // { label: 'Client', value: 'client' },
        { label: 'Seo', value: 'seo' },
        { label: "Content Writing", value: "content_writing" },
        { label: "Content Marketing", value: "content_marketing" }

    ];


    var url;

    if (!process.env.NODE_ENV || process.env.NODE_ENV === 'development') {
        // Use the development URL or a default value if it's not defined
        url = process.env.REACT_APP_PRODUCTION_URL;
    } else {
        // Use the production URL from your environment variables
        url = process.env.REACT_APP_PRODUCTION_URL;
    }

    const headerData = {
        headers: {
            Authorization: `Bearer ${accessToken}`
        }
    }
    var user_ID = searchParams.get('edit')
    useEffect(() => {
        async function getToken() {
            const userRes = await axios.get(`${url}/users/?token=${accessToken}`, headerData);
            let data = userRes?.data
            const options = data.map(item => ({
                value: item.id,
                label: `${item.first_name.toUpperCase()} ${item.last_name === null ? "" : item.last_name.toUpperCase()}`,
            }));
            setUserId(options)
            setPending(false);
        }
        getToken();
    }, []);

    const getUserId =selectedOptions.map(obj => obj.value)
    const addUsers = async () => {
        let data = {
            project_name: projectName,
            domain: domain,
            ownership: selectedOwner?.value,
            user: getUserId,
            client: selectedClient?.value,
            scope:scope


        }
        try {
            setLoading(true);
            const userRes = await axios.post(`${url}/seo/project/`, data, headerData);
            console.log("userRes", userRes)
            updateImage(userRes?.data?.id)
            listData(userRes.data);
            setLoading(false);
            setPending(false);
        } catch (error) {
            console.error('Error fetching data:', error);
        }
    };



    const updateImage = async (addLogoId='')=>{
        if (!selectedImageFile) {
            return;
          }
        const fileName = selectedImageFile.name;
        const data = new FormData(); 
        const logoId = addLogoId ? addLogoId : user_ID
        data.append('id', logoId);
        data.append('logo', selectedImageFile, fileName); 
      
        try {
          const response = await axios.post(`${url}/seo/logo/`, data, {
            ...headerData,
            'Content-Type': 'multipart/form-data', 
          });
      
          if (response.status === 201) {
            // toast.success(response.data.message);
          }
        } catch (error) {
          console.error("error", error);
          if (error) {
            // toast.error("Some technical issue.");
          }
        }
    }
    
    const handleImageChange = async (event) => {
        const selectedFile = event.target.files[0];
        setSelectedImageFile(selectedFile)
        setSelectedImage(URL.createObjectURL(selectedFile));  
      };
      
console.log("selectedImage",selectedImage)
    const handleSelectUser = (e) => {
        setSelectedOptions(e);
        setSelectedUsers(e)
    };

    const handleSelectOwner = (e) => {
        setSelectedOwner(e);
         setSelectedEditOwner(e)
    };
    const getUsers = async () => {
        try {
            setLoading(true);
            const userRes = await axios.get(`${url}/users/?department=${selectedDep?.value}`, headerData);
            let data = userRes?.data
            const formattedOptions = data.map(item => ({
                value: item.id,
                label: `${item.first_name.toUpperCase()} ${item.last_name === null ? "" : item.last_name.toUpperCase()}`,
            }));
            setUserList(formattedOptions);
            setLoading(false);
            setPending(false);
        } catch (error) {
            console.error('Error fetching data:', error);
        }
    };

    useEffect(() => {
        getUsers();
    }, [selectedDep])



    const getClient = async () => {
        try {
            setLoading(true);
            const userRes = await axios.get(`${url}/users/?department=client`, headerData);
            let data = userRes?.data
            const formatted = data.map(item => ({
                value: item.id,
                label: `${item.first_name.toUpperCase()} ${item.last_name === null ? "" : item.last_name.toUpperCase()}`,
            }));
            setSelectClient(formatted);
            setLoading(false);
            setPending(false);
        } catch (error) {
            console.error('Error fetching data:', error);
        }
    };

    useEffect(() => {
        getClient();
    }, [])

    function validateFields() {
        if (
            projectName?.length === 0 ||
            selectedDep?.length === 0 ||
            selectedOptions?.length === 0 ||
            domain?.length === 0 ||
            selectedOwner?.length === 0 ||
            selectClient?.length === 0
        ) {
            return false;
        }
        return true;
    }

    const getProjectData = async () => {
        if (user_ID != null) {
            try {
                setLoading(true);
                const userRes = await axios.get(`${url}/seo/projects/${user_ID}/`, headerData);
                let data = userRes?.data
                console.log('data', data)
                const formattedOptions = data?.user?.map(item => ({
                    value: item.id,
                    label: `${item.first_name.toUpperCase()} ${item.last_name === null ? "" : item.last_name.toUpperCase()}`,
                }));
                setSelectedUsers(formattedOptions)

                const formatted ={
                    value: data?.client?.id,
                    label: `${data?.client?.first_name.toUpperCase()} ${data?.client?.last_name === null ? "" : data?.client?.last_name.toUpperCase()}`,
                };
                setSelectedEditClient(formatted);
                setSelectedImage(`${url}${data?.logo}`)

                const formattedOwner ={
                    value: data?.ownership?.id,
                    label: `${data?.ownership?.first_name.toUpperCase()} ${data?.ownership?.last_name === null ? "" : data?.ownership?.last_name.toUpperCase()}`,
                };
                setSelectedEditOwner(formattedOwner);
                setUpdateProjectName(data?.project_name);
                setUpdateDomain(data?.domain);
                setUpdateScope(data?.scope)
                setLoading(false);
                setPending(false);
            } catch (error) {
                console.error('Error fetching data:', error);
            }
        }
    }

    useEffect(() => {
        getProjectData();
    }, [])

    const getUserIds =selectedUsers.map(obj => obj.value)
    const updateProject = async () => {
        let data = {
            project_name: updateProjectName,
            on_site: "",
            off_site: "",
            ownership: selectedEditOwner?.value ,
            user: getUserIds,
            client: selectedEditClient?.value,
            scope:updateScope
          
        }
        try {
            setLoading(true);
            const userRes = await axios.put(`${url}/seo/projects/${user_ID}/`, data, headerData);
            updateImage()
            setLoading(false);
            setPending(false);
        } catch (error) {
            console.error('Error fetching data:', error);
        }
    }

    const handleSubmit = () => {
        if (user_ID != null) {
            updateProject();
            navigate('/projects');
        } else {
            addUsers();
            navigate('/')
        }
    };

    const handleSelectClient = (e) => {
        setSelectedClient(e)
        setSelectedEditClient(e)
    }
    return (

        <div className="container-fluid d-flex align-items-center justify-content-center vh-100  login_card">
            <ToastContainer />
            <div className="row w-75">
                <div className="col-lg-12">
                    <div class="card project_card p-4">
                        <div class=" align-items-center w-100 border-bottom d-flex justify-content-between">
                            <h6>Add New Project</h6>
                            <div>

                                <button className="fs-6 border-0 btn_outlined" onClick={() => navigate('/')} > <img src="../assets/Icons/ArrowRight.svg" className="me-3" alt="" />Back to Projects List</button>
                                <button className="main_btn m-2" onClick={() => navigate("/addUserClick")}>Add User</button>
                            </div>


                        </div>
                        <div className="card-body p-0 mt-3">
                            <div className="row g-3">
                            <div className="col-md-6 mb-3 text-start d-flex flex-column gap-3">
  <label htmlFor="imageInput" className="mb-2 form-label text-start">Project logo</label>
  <input
    type="file"
    id="imageInput"
    className='form-control d-none'
    accept="image/*"
    onChange={handleImageChange}
  />
  <label htmlFor="imageInput">
    <div className="img_upload  p-3 border rounded-circle">
      <div className="rounded-circle" >
        {selectedImage ? (
        //   <img src={URL.createObjectURL(selectedImage)} alt="Uploaded Image" width='100%' height='100%' />
        <img src={(selectedImage)} alt="Uploaded Image" width='100%' height='100%' />
        ) : (
          <>
            <img src="../assets/Icons/Vector.svg" alt="" />
            <img src="../assets/Icons/camera.svg" className="camera" alt="" />
          </>)
        }
      </div>
    </div>
  </label>
</div>


                                <div className="col-md-6 mb-3 text-start">
                                    <label htmlFor="useCase" className="mb-2 form-label text-start">Assign Team</label>
                                    <Select
                                        options={departments}
                                        placeholder="Select Department"
                                        isSearchable={true}
                                        onChange={setSelectedDep}

                                    />
                                    <br />
                                    <Select
                                         isMulti
                                         options={userList}
                                         placeholder="Select User"
                                         value={selectedUsers}
                                         onChange={handleSelectUser}
                                         className="basic-multi-select"
                                         classNamePrefix="select"
                                    />
                                    <label htmlFor="useCase" className="mb-2 mt-3 form-label text-start">Choose Client</label>
                                    <Select
                                        options={selectClient}
                                        value={selectedEditClient}
                                        placeholder="Select Client"
                                        isSearchable={true}
                                        onChange={handleSelectClient}

                                    />
                                </div>
                                <div className="col-lg-6 text-start">
                                    <label htmlFor="" className="mb-2 form-label text-start">Project Name</label>
                                    <input type="text" className='form-control' name="project_Name" placeholder="" value={user_ID != null ? updateProjectName : projectName}
                                        onChange={(e) => (user_ID != null) ? setUpdateProjectName(e.target.value) : setProjectName(e.target.value)} />
                                </div>
                                <div className="col-lg-6 text-start">
                                    <label htmlFor="" className="mb-2 form-label text-start">Domain</label>
                                    <input type="text" className='form-control' placeholder="" value={user_ID != null ? updateDomain : domain}
                                        onChange={(e) => user_ID != null ? setUpdateDomain(e.target.value) : setDomain(e.target.value)} />
                                </div>     
                                <div className="col-md-6 mb-3 text-start">
                                    <label htmlFor="ownership" className="mb-2 form-label text-start">Select Ownership</label>
                                    <Select
                                        options={user_ID != null? selectedUsers : selectedOptions}
                                        placeholder="Select Ownership"
                                        isSearchable={true}
                                        value={selectedEditOwner}
                                        onChange={handleSelectOwner}
                                    />
                                </div>
                                <div className="col-lg-6 text-start">
                                    <label htmlFor="" className="mb-2 form-label text-start">Scope</label>
                                    <input type="text" className='form-control' placeholder="" value={user_ID != null ? updateScope : scope}
                                        onChange={(e) => user_ID != null ? setUpdateScope(e.target.value) : setScope(e.target.value)} />
                                </div>
                            </div>
                            <div className="row mt-4">
                                <div className="col-lg-12 justify-content-end d-flex gap-4">
                                    {/* <button className="btn_outlined">Clear</button> */}
                                    <button className="main_btn" onClick={handleSubmit} disabled={user_ID != null ? "" : validateFields() ? false : true}>{user_ID != null ? "Update Now" : "Add Now"}</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default AddProjects;
