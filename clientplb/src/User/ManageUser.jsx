import React, { useState, useEffect } from 'react';
import ReactDOM from 'react-dom';
import { Link, NavLink, useLocation } from 'react-router-dom';
import { useNavigate } from 'react-router-dom';
import '../Auth/Auth.css';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import Top from '../includes/Top';
import Header from '../includes/Header';
import axios from 'axios';
import DataTable from 'react-data-table-component';
import Loader from '../Common/Loader/Loader';


function ManageUser() {
    const navigate = useNavigate();

    var url;

    if (!process.env.NODE_ENV || process.env.NODE_ENV === 'development') {
        // Use the development URL or a default value if it's not defined
        url = process.env.REACT_APP_PRODUCTION_URL;
    } else {
        // Use the production URL from your environment variables
        url = process.env.REACT_APP_PRODUCTION_URL;
    }

    const [user, userData] = useState([]);
    const [pending, setPending] = useState(true);
    const [accessToken, setAccessToken] = useState(localStorage.getItem('accessToken'));
    const [department, setDepartment] = useState(localStorage.getItem("department").toLowerCase());
    const [loading, setLoading] = useState(true);
    const [role, setRole] = useState(localStorage.getItem("role").toLowerCase())
    const [selectedRole, setSelectedRole] = useState('');
    const [selectedDep, setSelectedDep] = useState('');
    const [isFirstDropdownDisabled, setIsFirstDropdownDisabled] = useState(true);
    const headerData = {
        headers: {
            Authorization: `Bearer ${accessToken}`
        }
    }

    const leaddropdown = [
        { label: 'Lead', value: 'lead' },
        { label: 'Team', value: 'team' },
    ];
    const managerdownOptions = [
        { label: 'Lead', value: 'lead' },
        { label: 'Team', value: 'team' },
        { label: 'Manager', value: 'manager' },

    ];

    const departments = [
        { label: 'Client Service', value: 'client_service' },
        { label: 'Client', value: 'client' },
        { label: 'Seo', value: 'seo' },
        { label: "Content Writing", value: "content_writing" },
        { label: "Content Marketing", value: "content_marketing" }

    ];

    const getData = async () => {
        try {
            setLoading(true);
            const userRes = await axios.get(`${url}/users/?max_page_size=1&department=${department !='client_service' ? department:selectedDep }&page_size=100&role=${selectedRole}`, headerData);
            userData(userRes.data);
            setLoading(false);
            setPending(false);
        } catch (error) {
            console.error('Error fetching data:', error);
        }
    };

    useEffect(() => {
        getData();
    }, [selectedRole, selectedDep]);


    const changeStatus = (user_id, is_active) => {
        axios.get(`${url}/users/?status=${!is_active}&pk_id=${user_id}`, headerData).then((response) => {
            toast.success(`Status updated successfully !!`)
            navigate(`/manageUser`);
        }).catch((error) => {
            if (error) {
                toast.error("Some tehnical issue.")
            }
        });
    }

    const deleteData = (delete_id) => {
        if (window.confirm("Are you sure you want to delete this item?") == true) {
            axios.delete(`${url}/users/${delete_id}/delete/`, headerData).then((response) => {
                toast.success(`Record delete successfully !!`)
                getData();
                navigate(`/manageUser`);
            }).catch((error) => {
                if (error) {
                    toast.error("Some tehnical issue.")
                }
            });
        }
    }

    const columns = [
        {
            name: 'Name',
            selector: row => row.first_name,
            sortable: true
        },
        {
            name: 'Email',
            selector: row => row.email,
            sortable: true
        },
        {
            name: 'Role',
            selector: row => row.role,
            sortable: true
        },
        {
            name: 'Department',
            selector: row => row.department,
            sortable: true
        },
        {
            name: 'Mobile no.',
            selector: row => row.phone_no,
            sortable: true
        },
        {
            name: 'Created on',
            selector: row => row.date_joined.split('T')[0],
            sortable: true
        },
        {
            name: 'Status',
            selector: row => <span class={`${(row.is_active) ? 'pill_active' : 'pill_inactive'}`} onClick={() => { changeStatus(row.id, row.is_active) }}>Active</span>,
        },
        {
            name: 'Actions',
            selector: row => <div className='d-flex gap-3'>
                <Link to={`/addUser?edit=${row.id}`} >
                    <img src="../assets/Icons/Edit.svg" width="20px" alt="" />
                </Link>
                <Link to='#' onClick={() => { deleteData(row.id) }}>
                    <img src="../assets/Icons/DeleteOutlined.svg" width="20px" alt="" className="ml-3" />
                </Link>
            </div>,
        }
    ];

    const handleChange = ({ selectedRows }) => {
        console.log('Selected Rows: ', selectedRows);
    };

    const [filterText, setFilterText] = React.useState('');
    const [resetPaginationToggle, setResetPaginationToggle] = React.useState(false);

    const filteredItems = user.filter(
        item => item.first_name && item.first_name.toLowerCase().includes(filterText.toLowerCase())
    );

    const subHeaderComponentMemo = React.useMemo(() => {
        const handleClear = () => {
            if (filterText) {
                setResetPaginationToggle(!resetPaginationToggle);
                setFilterText('');
            }
        };
        return (
            <input className="form-control" style={{ width: "250px" }} onChange={e => setFilterText(e.target.value)} onClear={handleClear} filterText={filterText} placeholder="Search..." />
        );
    }, [filterText, resetPaginationToggle]);


    const DropdownComponent = ({ options, onChange, name, value, disabled }) => {
        return (
            <select onChange={(e) => onChange(e.target.value)} 
            value={value} className='select_role_dropdown' disabled={disabled} >
                <option value="">{name}</option>
                {options.map((option) => (
                    <option key={option.value} value={option.value}>
                        {option.label}
                    </option>
                ))}
            </select>
        );
    };


    return (
        <div className="container-fluid ">
            <ToastContainer />
            <div className="row">
                <Top userManagement={true} manageUser={true} />
                <div className="col-lg p-0" id="right_side">
                    <Header />
                    <div class="main">
                        <div class=" w-100 mb-4">
                            {/* <div class="col-xl-12 d-flex py-3 border-bottom justify-content-between align-items-center">
                                <div class="breadcrumbs   ">
                                    <nav aria-label="breadcrumb">
                                        <ol class="breadcrumb">
                                            <li class="breadcrumb-item"><a href="#">User Management</a></li>
                                            <li class="breadcrumb-item active" aria-current="page">User List</li>
                                        </ol>
                                    </nav>
                                </div>
                            </div> */}
                        </div>
                        <div class="d-flex justify-content-between gap-4 flex-column flex-sm-row align-items-sm-center align-items-start p-2">
                            <h5 class="main_heading" style={{ flex: 1 }}>User Listing</h5>
                            
                            <div className="actions d-flex gap-3 justify-content-between flex-column flex-sm-row align-items-sm-center  align-items-center">
                            <div  className="w-100">
                                <div className="w-100">
                                    {role === "lead" && department=='client_service'?
                                        <DropdownComponent
                                            options={leaddropdown}
                                            onChange={setSelectedRole}
                                            name='Role'
                                            disabled={isFirstDropdownDisabled}
                                            value={selectedRole === "" ? "Role" : selectedRole}
                                            className='w-100'
                                        />
                                         : ""} 
                                     {role === "manager" && department=='client_service' ?  
                                    <DropdownComponent
                                        options={managerdownOptions}
                                        onChange={setSelectedRole}
                                        name='Role'
                                        value={selectedRole === "" ? "Role" : selectedRole}
                                        disabled={isFirstDropdownDisabled}
                                        className='w-100'

                                    /> 
                                    : ""} 
                                    {department !='client_service'? <DropdownComponent
                                        options={managerdownOptions}
                                        onChange={setSelectedRole}
                                        name='Role'
                                        value={selectedRole === "" ? "Role" : selectedRole}
                                        // disabled={isFirstDropdownDisabled}
                                        className='w-100'

                                    /> 
                                    : ""} 

                                </div>
                               {department=='client_service'? <div  className="w-100">
                                     <DropdownComponent
                                        options={departments}
                                        onChange={(e)=>{
                                            setSelectedDep(e);
                                            if(e=='client'){setIsFirstDropdownDisabled(true)}
                                            else {setIsFirstDropdownDisabled(false)}
                                        }}
                                        // onChange={setSelectedDep}
                                        name='Departments'
                                        value={selectedDep === "" ? "Departments" : selectedDep}

                                    /> 

                                </div>:""}
                            </div>
                                <Link to="#" class="btn_outlined"><img src="../assets/Icons/DeleteOutlined.svg" className='me-2' alt="" />Delete</Link>
                                <Link to="/addUser" class="main_btn"><img src="../assets/Icons/Plus.svg" alt="" /> Add Team Member</Link>
                            </div>
                        </div> {loading ? (
                            <Loader />
                        ) : (
                            <DataTable
                                columns={columns}
                                data={filteredItems}
                                pagination
                                selectableRows
                                onSelectedRowsChange={handleChange}
                                progressPending={pending}
                                subHeader
                                persistTableHead
                                subHeaderComponent={subHeaderComponentMemo}
                            />
                        )}

                    </div>

                </div>
            </div>
        </div>
    );
}
export default ManageUser;