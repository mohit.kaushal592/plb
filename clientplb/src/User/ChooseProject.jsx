import axios from "axios";
import React, { useEffect, useState } from "react";
import { ToastContainer, toast } from 'react-toastify';
import Loader from "../Common/Loader/Loader";
import { useNavigate,useLocation } from "react-router-dom";
import '../Auth/Auth.css';
function ChooseProjects() {
    const location = useLocation();
    const [list, listData] = useState([]);
    const [projectId, setProjectId] = useState(null);
    const [pending, setPending] = useState(true);
    const [accessToken, setAccessToken] = useState(localStorage.getItem('accessToken'));
    const [loading, setLoading] = useState(true);
    const [userId, setUserId] = useState('')
    const [filterText, setFilterText] = React.useState('');
    const [searchQuery, setSearchQuery] = useState('');
    const [paymentList, setPaymentList] = useState([]);
    const [role, setRole] = useState(localStorage.getItem("role").toLowerCase());
    const [department, setDepartment] = useState(localStorage.getItem("department").toLowerCase());

    const navigate = useNavigate();

    var url;

    if (!process.env.NODE_ENV || process.env.NODE_ENV === 'development') {
        // Use the development URL or a default value if it's not defined
        url = process.env.REACT_APP_PRODUCTION_URL;
    } else {
        // Use the production URL from your environment variables
        url = process.env.REACT_APP_PRODUCTION_URL;
    }

    const headerData = {
        headers: {
            Authorization: `Bearer ${accessToken}`
        }
    }

    const getData = () => {
        setLoading(true)
        axios.get(`${url}/seo/project/?search=${searchQuery}`, headerData)
            .then(async (response) => {
                let data = await response?.data;
                listData(data);
                setLoading(false);
            })
    }
    console.log("List",list)

    useEffect(() => {
        setTimeout(() => {
            getData();
        }, 0)

    }, [searchQuery])


    useEffect(() => {
        // Simulating an API call delay
        setTimeout(() => {
            axios.get(`${url}/users/?token=${accessToken}`, headerData)
                .then(responseData => {
                    let data = responseData?.data?.[0]?.id;
                    setUserId(data)
                    setLoading(false)
                })
                .catch(error => {
                    console.error('Error fetching data:', error);
                    setLoading(false);
                });
        }, 0); // Simulated 2-second delay
    }, []);







    const handleSearch = (event) => {
        setSearchQuery(event.target.value);
    };

    const filteredItems = list.filter(
        item => item.project_name && item.project_name.toLowerCase().includes(searchQuery.toLowerCase())
    );

    const getPayment = () => {
        if (projectId) {
            localStorage.setItem("select_project", projectId); 
            var data =
            {
                project_name: projectId
            }
            setLoading(true)
            axios.put(`${url}/user_edit/${userId}/`, data, headerData)
                .then((response) => {
                    if (response) {
                        if (department == 'seo') {
                            navigate("/dashboard");
                        }
                        else if (department == 'content_writing') {
                            navigate("/contentDashboard");
                        }
                        else if (department == 'client' ) {
                            navigate("/client");
                        }
                        else if ( department == 'content_marketing') {
                            navigate("/clientDashboard");
                        }

                        else if ((department == 'client_service') || (department == 'manager')) {
                            navigate("/managerDashboard")
                        } 
                        else if (department == 'content_writing') {
                            navigate("/contentDashboard");
                        }
                        else if (department == 'content_marketing') {
                            navigate("/clientDashboard");
                        }
                        else if (department == 'client' ) {
                            navigate("/client");
                        }
                        else if ((department == 'client_service') || (department == 'manager')) {
                            navigate("/managerDashboard")
                        }
 
                    }
                    setPaymentList(response)
                    setLoading(false);
                })
                .catch((error) => {
                    console.error(error);
                });
        };
    }

    useEffect(() => {
        getPayment();
    }, [projectId]);









    return (
        <div className="container-fluid d-flex align-items-center justify-content-center vh-100  login_card">
            <ToastContainer />
            <div className="row w-75">
                <div className="col-lg-12">
                    <div class="card project p-3">
                        <div class=" align-items-center  border-bottom py-4 d-flex justify-content-between">
                            <h4>Choose Project</h4>
                            <div className="left text-end">

                                <img src="../assets/Icons/search.svg" class="me-2" alt="" />
                                <input type="text" placeholder="Search" className="border-0" style={{ marginRight: '10px' }} value={searchQuery}
                                    onChange={handleSearch} />
                                {role === "manager" & department=="client_service" ? <button className="fs-6 btn_outlined" onClick={() => navigate("/addproject")}>Add New Project</button> : ""}
                            </div>
                        </div>

                        <div class="card-body mt-3 border-0  scrollable-container">

                            {loading ? (
                                <Loader />
                            ) : (
                                <div className="row g-4 ">
                                    {filteredItems.map((item, index) => {
                                        return (

                                            <div key={index} className="text-center col-lg-3" onClick={() => setProjectId(item?.id)
                                            }>
                                                <div className="project_card active border p-4" >
                                                {! item?.logo ? <img src="../assets/Images/avatar.png" class="me-2" alt=""
                                                    />:
                                                    <img src={`${url}${item?.logo}`} alt={item.project_name} className="me-2" style={{
                                                        width: '80px', 
                                                        height: '80px',
                                                        borderRadius: '50%', 
                                                        objectFit: 'contain', 
                                                        background:'#FFF3EC'
                                                      }}/>}
                                                </div>
                                                <h6 >{item?.project_name}</h6>
                                            </div>

                                        )
                                    })}
                                </div>
                            )}
                            <div className="row mt-4">
                                <div className="col-lg-12 justify-content-end d-flex gap-4">
                                   {location?.pathname == '/chooseproject' ? <button className="btn_outlined" onClick={()=>{navigate(-1)}}>Close</button>
                                   :
                                //    <button className="btn_outlined" onClick={() => {
                                //         if (department == 'seo') {
                                //             navigate("/dashboard");
                                //         }
                                //         else if (department == 'content_writing') {
                                //             navigate("/contentDashboard");
                                //         }
                                //         else if ( department == 'content_marketing') {
                                //             navigate("/clientDashboard");
                                //         }
                                //         else if ((department == 'client_service') || (department == 'manager')) {
                                //             navigate("/managerDashboard")
                                //         }
                                //         else if (department == 'client' ) {
                                //             navigate("/client");
                                //         }
                                //     }}>Skip</button>

                                <button className="btn_outlined" onClick={() => {
                                    localStorage.removeItem("accessToken");
                                    localStorage.removeItem("department");
                                    localStorage.removeItem("role");
                                    toast.success(`Logout successfully.`)
                                    navigate(`/login`);
                                }}>Sign Out</button>  
                                    }
                                        
                                </div>
                            </div>

                        </div>

                    </div>
                </div>
            </div>
        </div>
    )

}
export default ChooseProjects;
