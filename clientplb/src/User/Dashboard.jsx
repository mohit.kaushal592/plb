import React, { useState, useEffect } from 'react';  
import ReactDOM from 'react-dom';
import { Link, NavLink, useLocation} from 'react-router-dom';
import {useNavigate} from 'react-router-dom'; 
import '../Auth/Auth.css';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css'; 
import Top from '../includes/Top';
import Header from '../includes/Header';
import axios from 'axios';


function Dashboard() {  
  // const navigate = useNavigate();
  // const [nasaImage, setNasaImage] = useState('https://cdn.pixabay.com/photo/2015/02/22/17/56/loading-645268_960_720.jpg');
  // const [nasaImageTitle, setNasaImageTitle] = useState('Image title');
  // const [userName, setUserName] = useState(localStorage.getItem('userName'));
  
  // useEffect(() => {
  //   axios.get('https://api.nasa.gov/planetary/apod?api_key=mOpPcNjyDZeuUdgh3EDLqXATWWs8Xu34n5339gHj')
  //   .then(function (response) {  
  //       setNasaImage(response.data.url)
  //       setNasaImageTitle(response.data.title)
  //       toast.success("Image loaded succesfully!!")
  //   }).catch(function (error) { 
  //       toast(`Error`)
  //       toast.error("Some issue to load image.")
  //   }) 
  // },[]);

  // const logout = ()=> {
  //   localStorage.removeItem("accessToken"); 
  //   toast.success("Logout succesfully!!")
  //   navigate(`/`);
  // }
  return (
    <> 
    <div className="container-fluid ">
        <ToastContainer />
        <div className="row"> 
            <Top />
            <div className="col-lg p-0" id="right_side">
                <Header />
                <div className="main">
                
                    <div className="border-bottom mt-3  py-2">
                        <h5>Dashboard</h5>
                    </div>
                    <div class="row g-4 mt-4">
                <div class="col-lg-3 ">
                    <div class="content border rounded w-100  p-3 ">
                        <h5>Total Iterations</h5>
                            <h6>April 2023</h6>
                            <h3 class="fw-bold">1000</h3>
                    </div>
                </div>
                <div class="col-lg-3">
                 <div class="content rounded w-100  p-3 border">
                        <h5>Total Feedbacks</h5>
                            <h6>April 2023</h6>
                            <h3 class="fw-bold">5000</h3>
                    </div>
                </div>
                <div class="col-lg-3">
                  <div class="content rounded w-100  p-3 border">
                        <h5>Approvals</h5>
                            <h6>April 2023</h6>
                            <h3 class="fw-bold">300</h3>
                    </div>
                </div>
                <div class="col-lg-3">
                  <div class="content rounded w-100  p-3 border">
                        <h5>Rejected</h5>
                            <h6>April 2023</h6>
                            <h3 class="fw-bold">888</h3>
                    </div>
                </div>
                <div class="col-lg-12 ">
                    <canvas class="border rounded p-3" id="myChart"></canvas>
                </div>
                <div class="col-lg-12 ">
                    <div id='calendar'></div>
                </div>
              </div>
                </div>
            </div>
        </div>
    </div>
    </>
  );
}
export default Dashboard;
