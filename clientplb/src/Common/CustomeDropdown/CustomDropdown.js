import React, { useState, useEffect } from 'react';
import './CustomDropdown.css'; // Import your custom styles
import { useNavigate } from 'react-router-dom';
import { ThreeDots } from 'react-loader-spinner'

const CustomDropdown = ({ options,selectedProject,setSelectedProject }) => {
  
  const [isOpen, setIsOpen] = useState(false);
  const [loaderVisible, setLoaderVisible] = useState(true);
  const [showMore, setShowMore] = useState(false);
  const [currentProject, setCurrentProject] = useState(localStorage.getItem("select_project")??0);
  const [department, setDepartment] = useState(localStorage.getItem("department"));
  const navigate = useNavigate();
  const toggleDropdown = () => {
    setIsOpen(!isOpen);
  };

  const handleOptionSelect = (option) => { 
    if(option == 'chooseproject'){
      navigate(`/chooseproject`);
    }
    setSelectedProject(parseInt(option));
    localStorage.setItem("select_project", parseInt(option));  
    setIsOpen(false);
    window.location.reload();
  };

  // const handleClick = () => {
  //   navigate(`/chooseproject`);
  // };
 
  useEffect(() => { 

    setTimeout(function(){ 
      document.getElementById("staus_filter").value = currentProject;
      setLoaderVisible(false)
      if(department == 'client'){
        document.getElementById("staus_filter").disabled = true;
      }
    }, 3000);
  }, []);   


  // const visibleItems = showMore ? options : options.slice(0, 5);
  const visibleItems = options ;
  return (
    // <div className="custom-dropdown">
    //   <div className="selected-option" onClick={toggleDropdown}>
    //     {selectedProject ? selectedProject.project_name : 'Choose Project'}
    //   </div>
    //   {isOpen && (
    //     <ul className="options-list">
    //       {visibleItems.map((option) => (
    //         <li
    //           key={option.id}
    //           className="option"
    //           onClick={() => handleOptionSelect(option)}
    //         >
    //           {option.project_name}
    //         </li>
    //       ))}
    //       <li className='show_more' style={{display:options.length < 5 ?'none':''}}>
    //         {!showMore && (
    //           <button onClick={handleClick}>
    //             Show More
    //           </button>
    //         )}
    //       </li>
    //     </ul>
    //   )}
    // </div>
<>
     
      <select className="form-control" id="staus_filter" style={{width:'20%'}} onChange={e => handleOptionSelect(e.target.value)}>
        <option value='0'>Choose Project</option>
        {visibleItems.map((option) => (
          <option 
            value={option.id} 
          >{option.project_name}</option>
        ))} 
        {/* <option value="chooseproject" style={{display:options.length < 5 ?'none':''}}>
          {!showMore && ( <>
                 Show More 
                 </>
             )}
        </option> */}
      </select>
      <ThreeDots
        height="50"
        width="50"
        radius="9"
        color="#d17929"
        ariaLabel="three-dots-loading"
        wrapperStyle={{}}
        wrapperClassName=""
        visible={loaderVisible}
      />
      </>
  )
};

export default CustomDropdown;