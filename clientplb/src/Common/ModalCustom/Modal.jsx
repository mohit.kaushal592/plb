import Modal from "react-bootstrap/Modal";
import "./ModalStyle.css";

const ModalCustom = (props) => {
  return (
    <Modal
      show={props.show}
      backdrop={props?.backdrop}
      onHide={props.handleClose}
      centered
      size={props.size}
      className={`modal-style ${props.className}`}
    >
      <Modal.Header closeButton={props.handleClose}>
        <Modal.Title>{props.title}</Modal.Title>
      </Modal.Header>
      <Modal.Body className={props.padding}>{props.children}</Modal.Body>
    </Modal>
  );
};

export { ModalCustom };