import React, { useState, useEffect } from 'react';
import '../../Auth/Auth.css';
import 'react-toastify/dist/ReactToastify.css'; 

const ProgressBar = ({onPageProgress}) => {
    const [currentProgress, setCurrentProgress] = useState('0%');
    const progressBarStyle = {
        width: '100%',
        height: '24px',
        backgroundColor: '#f3f3f3',
        borderRadius: '10px',
        position: 'relative',
        overflow: 'hidden', 
      };
    
      const onProgressStyle = {
        width: currentProgress,
        height: '100%',
        backgroundColor: '#2ecc71',
        borderRadius: '10px',
        transition: 'width 0.5s ease-in-out', 
      };
    
      const progressTextStyle = {
        position: 'absolute',
        top: '50%',
        left: '50%',
        transform: 'translate(-50%, -50%)',
      };

     

  useEffect(() => {
    const interval = setInterval(() => {
      setCurrentProgress(prevProgress => {
        const numericOnPageProgress = parseFloat(onPageProgress);
        const numericCurrentProgress = parseFloat(prevProgress);
        
        if (numericCurrentProgress < numericOnPageProgress) {
          const newProgress = (numericCurrentProgress + 1).toFixed(2);
          return `${newProgress}%`;
        } else {
          clearInterval(interval);
          return onPageProgress;
        }
      });
    }, 100); 
    return () => clearInterval(interval);
  }, [onPageProgress]);


  return (
    <div style={progressBarStyle}>
      <div style={onProgressStyle}></div>
      <div style={progressTextStyle}>
          Progress: <em>{onPageProgress}</em>
      </div>
    </div>
  )
};


export default ProgressBar;

