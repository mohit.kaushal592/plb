import React, { useState, useEffect } from 'react';
import '../../Auth/Auth.css';


const ToDoList = ({notificationData}) => {

  return (
    <>
    {
        notificationData.map((item)=>{
          const createdAt = new Date(item.created_at);

        const formattedDate = createdAt.toLocaleString('en-US', {
          day: '2-digit',
          month: 'short',
          year: 'numeric',
          hour: 'numeric',
          minute: '2-digit',
          hour12: true, 
        });


       return ( <div className="avatar-title-container">
        <div className='d-flex'>
        <div class="col-lg-1 col-md-2 col-sm-3 mt-2"><img className="toDoAvatar" src='https://thumbs.dreamstime.com/z/default-avatar-profile-flat-icon-vector-contact-symbol-illustration-184752213.jpg?w=768' alt="User Avatar" /></div>
        <div class="col-lg-11 col-md-10 col-sm-9">
            <div className='d-flex justify-content-between m-3 ms-3'>
            <div className='d-flex flex-column'>
             <h6 className='mb-0'>{item.title}</h6>
             <div className="small">{item.message}</div>
            </div>
            <div className='formateTime'>{formattedDate}</div>  
          </div>
          </div>
          </div>

                </div>)
 } )}
      </>
  )
};


export default ToDoList;

