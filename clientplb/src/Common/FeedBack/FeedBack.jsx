import React, { useState, useEffect } from 'react';
import '../../Auth/Auth.css';


const FeedBack = ({feedbackData}) => {

  return (
    <>
    {feedbackData?.length ? <div>{

      feedbackData?.map((item)=>{
        const createdAt = new Date(item.created_at);

      const formattedDate = createdAt.toLocaleString('en-US', {
        day: '2-digit',
        month: 'short',
        year: 'numeric',
        hour: 'numeric',
        minute: '2-digit',
        hour12: true, 
      });

     return ( <div className="feedback-container">
      <div className='d-flex'>
      <div class="col-lg-1"><img className="toDoAvatar" src='https://thumbs.dreamstime.com/z/default-avatar-profile-flat-icon-vector-contact-symbol-illustration-184752213.jpg?w=768' alt="User Avatar" /></div>
      <div class="col-lg-11 d-flex justify-content-between align-items-center">
      <h5 className=''>{item.subject}</h5>
      <div className='formateTime'>{formattedDate}</div>  
      </div>
        </div>
        <div className="ms-2">{item.message}</div>
        <br/>
       </div>)
}

)}
    
 </div>:<h6>There Is No Feedback Data comming soon...</h6>}
      </>
  )
};


export default FeedBack;

