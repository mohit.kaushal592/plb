import React, { useState, useEffect } from 'react';
import Top from '../includes/Top';
import Header from '../includes/Header';
import ApexCharts from 'react-apexcharts';
import { AdapterDayjs } from '@mui/x-date-pickers/AdapterDayjs';
import { LocalizationProvider } from '@mui/x-date-pickers/LocalizationProvider';
import { DatePicker } from '@mui/x-date-pickers/DatePicker';
import TextField from '@mui/material/TextField';
import Highcharts from 'highcharts';
import HighchartsReact from 'highcharts-react-official';
import HCMore from 'highcharts/highcharts-more';
import DataTable from "react-data-table-component";
import Loader from "../Common/Loader/Loader";
import DatePickerViews from '../includes/DatePicker';
import dayjs from "dayjs";
import axios from 'axios';
import "../App.css"

HCMore(Highcharts);

const Dashboard = () => {
  const [accessToken, setAccessToken] = useState(localStorage.getItem("accessToken"));
  const [dashboard, setDashboard] = useState()
  const [durationData, setDurationData] = useState()
  const [monthCategorykey, setMonthCategoryKey] = useState()
  const [monthCategoryVal, setMonthCategoryVal] = useState()
  const [apsData,setApsData] = useState()
  const [clientOnboard, setClientOnboard] = useState()
  const [indiaPercentage, setIndiaPercentage] = useState('')
  const [outsidePercentage, setoutsidePercentage] = useState('')
  const [brandliftData,setBrandLiftData] = useState()
  const [agreement, setAgreement] = useState('')
  const [signed, setSigned] = useState('')
  const [agreementList, setAgreementList] = useState([])
  const [selectedDate, setSelectedDate] = useState(dayjs());
  const [data,setData]=useState()
  const [listData,setListData] = useState([])
  const [loading,setLoading]= useState(false)
  const [siteKey, setSiteKey] = useState()
  const [siteVal, setSiteVal] = useState()
  


  if (!process.env.NODE_ENV || process.env.NODE_ENV === "development") {
    var url = process.env.REACT_APP_PRODUCTION_URL;
  } else {
    var url = process.env.REACT_APP_PRODUCTION_URL;
  }

  const headerData = {
    headers: {
      Authorization: `Bearer ${accessToken}`,
    },
  };

  useEffect(() => {
    axios.get(`${url}/marketing/stats/`, headerData).then(function (response) {
      setDashboard(response?.data)
    }).catch(function (error) {})

  axios.get(`${url}/marketing/category_month/`, headerData).then(function (response) {
  const Data=response?.data
  const categories = Object.keys(Data);
  const seriesData = Object.values(Data);
  setMonthCategoryKey(categories)
  setMonthCategoryVal(seriesData)
    }).catch(function (error) {
     
    })

    

          

    axios.get(`${url}/marketing/category/`, headerData).then(function (response) {
        const apiData=response?.data
        const seriesData = Object.entries(apiData).map(([name, value]) => ({
            name,
            value
          }));
      
          setData(seriesData);
      }).catch(function (error) {
  
      })
      setLoading(true)
      axios.get(`${url}/marketing/listings/`,headerData).then((response) => {    
        setLoading(false)
        setListData(response.data);
      })
  }, [])

  useEffect(()=>{
    const formattedDate = selectedDate ? selectedDate.format("YYYY-MM") : "";
    axios.get(`${url}/marketing/sites/?month=${formattedDate}`, headerData).then(function (response) {
        const site=response?.data?.data[0]
        const sitecategories = Object.keys(site);
        const siteseriesData = Object.values(site);
        setSiteKey(sitecategories)
        setSiteVal(siteseriesData)
          }).catch(function (error) {          
          })
  },[selectedDate])

const columns= [

{
      name: 'Website',
      selector: row => <a href={row?.website_url} target='_blank'>{row?.website_url}</a>
  },
  {
    name: 'Annual Traffic',
    selector: row => <div>{row?.annual_traffic}</div>
},
{
    name: 'Category',
    selector: row => <div>{row?.category}</div>
},
{
    name: 'Domain Authority',
    selector: row => <div>{row?.domain_authority}</div>
},
{
    name: 'Spam Score',
    selector: row => <div>{row?.spam_score}</div>
},
  
   
  ]


  const handleDateChange = (date) => {
    setSelectedDate(date);
  };

  const Options = {
    chart: {
      type: 'packedbubble',
      height: '100%'
    },
    title: {
      text: '',
      align: 'left'
    },
    credits: {
      enabled: false
  },
    tooltip: {
      useHTML: true,
      pointFormat: '<b>{point.name}:</b> {point.value}m CO<sub>2</sub>'
    },
    plotOptions: {
      packedbubble: {
        minSize: '30%',
        maxSize: '120%',
        zMin: 0,
        zMax: 1000,
        layoutAlgorithm: {
          splitSeries: false,
          gravitationalConstant: 0.02
        },
        dataLabels: {
          enabled: true,
          format: '{point.name}',
          filter: {
            property: 'y',
            operator: '>',
            value: 250
          },
          style: {
            color: 'black',
            textOutline: 'none',
            fontWeight: 'normal'
          }
        },    
      }
    },
    series: [{
      name: 'Category',
      data: data,
    }]
  };
  



  const barOptions = {
    chart: {
      type: 'bar',
    },
 
    xaxis: {
      categories:siteKey,
      labels: {
        style: {
          colors: '',
          fontSize: '12px'
        }
      }
    },
    
    plotOptions: {
      bar: {
        horizontal: false,
        columnWidth: '30%', // Adjust as needed
        dataLabels: {
          position: 'top',
        },
      },
    },
    series: [{
      data: siteVal
    }],
  };
  

  const accountBarOptions = {
    chart: {
      type: 'bar',
    },
    xaxis: {
      categories: monthCategorykey,
      labels: {
        style: {
          colors: '',
          fontSize: '12px'
        }
      }
    },
    plotOptions: {
      bar: {
        horizontal: false,
        columnWidth: '30%',
        dataLabels: {
          position: 'top',
        },
      },
    },

    series: [{
      data: monthCategoryVal
    }],
  };

  const brandLiftBar = {
    chart: {
      type: 'bar',
    },
    xaxis: {
      categories: brandliftData?.map((item)=>item?.name),
      labels: {
        style: {
          colors: '',
          fontSize: '12px'
        }
      }
    },
    plotOptions: {
      bar: {
        horizontal: false,
        columnWidth: '30%',
        dataLabels: {
          position: 'top',
        },
      },
    },

    series: [{
      data: monthCategoryVal
    }],
  };

  const options = {
    chart: {
      type: 'area',
    },
    title: {
      text:'Client Onboard',
      align: 'left',
    },
    xaxis: {
      categories: brandliftData?.map((item)=>item?.total_ids),
    },
    yaxis: {
      opposite: true,
      title: {
        text: 'Client Count',
      },
    },
  };

  const series = [{
    name: 'Client Count',
    data: clientOnboard?.map(item => item.client_count),
  }];
  const areaOption = {

    series: [{
      name: "Onboard",
      data: [10, 9, 8]
    }],
    options: {
      chart: {
        type: 'area',
        height: 350,
        zoom: {
          enabled: false
        }
      },
      dataLabels: {
        enabled: false
      },
      stroke: {
        curve: 'straight'
      },

      title: {
        text: 'Client Onboard',
        align: 'left'
      },
      labels: ['1 january', '2 february', '3 march'],
      xaxis: {
        type: 'datetime',
      },
      yaxis: {
        opposite: true
      },
      legend: {
        horizontalAlign: 'left'
      }
    }
  }

  const chartData = [
    {
      x: 'India',
      y: indiaPercentage,
    },
    {
      x: 'Outside India',
      y: outsidePercentage,
    },
  ];
  const chartOptions = {
    chart: {
      type: 'pie',
    },
    labels: chartData.map((data) => data.x),
    colors: ['#68EABE', '#FBCB84'],
  };
  const PieChart = () => {
    const seriesData = chartData.map((data) => data.y);
    return (
      <ApexCharts
        options={chartOptions}
        series={seriesData}
        type="pie"
        width="400"
        height="300"
      />
    );
  };

  const agreementChartData = [
    {
      x: 'Agreement',
      y: agreement,
    },
    {
      x: 'Signed',
      y: signed,
    },
  ];
  const agreementChartOptions = {
    chart: {
      type: 'pie',
    },
    labels: agreementChartData.map((data) => data.x),
    colors: ['#d5e7fd', '#58a9fb'],
    states: {
      hover: {
        filter: {
          type: 'none'
        }
      }
    }

  };
  const AgreementPieChart = () => {
    const seriesData = agreementChartData.map((data) => data.y);
    return (
      <ApexCharts
        options={agreementChartOptions}
        series={seriesData}
        type="pie"
        width="400"
        height="300"
      />
    );
  };

  return (
    <>
      <div className="container-fluid">
        <div className="row">
          {/* left-side */}
          <Top salesDashboard={true} />

          {/* right-side */}
          <div className="col-lg p-0" id="right_side">
            <div className="main">
              <Header />
              <div class="row mt-2 g-4">
                <div class="col-lg-6">
                  <div class="row g-4">
                    <div class="col-6">
                      <div class="bg-white border-0 rounded text-center d-flex justify-content-between align-items-center p-4">
                        <div class="card_top">
                          <h6>Total Websites</h6>
                          <div class="data">
                            <div class="line2 d-flex align-items-center justify-content-between">
                              <span>{dashboard?.total_websites}</span>
                            </div>
                          </div>
                        </div>
                        <img src="../assets/Icons/site.svg" alt="" />
                      </div>
                    </div>
                    <div class="col-6">
                      <div class="bg-white h-100 border-0 rounded text-center d-flex justify-content-between align-items-center p-4">
                        <div class="card_top">
                          <h6>Total DA</h6>
                          <div class="data">
                          <div class="line2 d-flex align-items-center justify-content-between">
                          <span>{dashboard?.total_da}</span>
                          </div>
                          </div>
                        </div>
                        <div className='rectangle'>
                        <img src="../assets/Icons/AvgDa.svg" alt="" />
                        </div>
                      </div>
                    </div>
                    <div class="col-6">
                      <div class="bg-white border-0 rounded text-center d-flex justify-content-between align-items-center p-4">
                        <div class="card_top">
                          <h6>Total Traffic(All Sites)</h6>
                          <div class="data">
                          <div class="line2 d-flex align-items-center justify-content-between">
                              
                              <span>{dashboard?.total_traffic}</span>
                            </div>
                          </div>
                        </div>
                        <img src="../assets/Icons/Traffic.svg" alt="" />
                      </div>
                    </div>
                    <div class="col-6">
                      <div class="bg-white border-0 rounded text-center d-flex justify-content-between align-items-center p-4">
                        <div class="card_top">
                          <h6>Total Unique Sites</h6>
                          <div class="data">
                          <div class="line2 d-flex align-items-center justify-content-between">
                              
                              <span>{dashboard?.total_unique_sites}</span>
                            </div>
                          </div>
                        </div>
                        <div className='rectangle'>
                        <img src="../assets/Icons/AvgDa.svg" alt="" />
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="col-lg-6">
                  <div class="cmnBg h-100 border-0 ">
                    <h5 className='cmnChartHeading'>Total Category</h5>
                    <ApexCharts options={accountBarOptions} series={accountBarOptions.series} type="bar" height={320} />
                  </div>
                </div>
              </div>
              {/* <div class="col-lg-8">
              <div class="bg-white h-100 border-0 rounded text-center p-4">
                <h5>Client Onboard</h5>
              </div>
            </div>
            <div class="col-lg-4">
              <div class="bg-white h-100 border-0 rounded text-center p-4">
                <h5>Pending Payment List</h5>
              </div>
            </div> */}
              <div class="row mt-2 g-4">
            
              <div class="col-lg-8">
                <div class="cmnBg h-100 border-0 ">
                    {/* <h5 className='cmnChartHeading'>No of Site By Cost</h5> */}
                    <div className="chart_head  d-flex justify-content-between p-4 mb-4 border-bottom">
                                            <h5>No. of Site By Cost</h5>

                                            <div className='d-flex'>
                                            <DatePickerViews
              selectedDate={selectedDate}
              handleDateChange={handleDateChange}
            />
                                            </div>
                                        </div>
                                        <div style={{ textAlign: "end" }}>
                    <ApexCharts options={barOptions} series={barOptions.series} type="bar" height={380} />
                    </div>

                  </div>

                </div>
                <div class="col-lg-4">
                <div class="cmnBg h-100 border-0 ">
                    <h5 className='cmnChartHeading'>Top Category</h5>
                    
                    <HighchartsReact highcharts={Highcharts} options={Options} />

                  </div>

                </div>
              </div>
            
              <div class="row mt-2 g-4">
              <div class="col-lg-12">
                <div class="cmnBg h-100 border-0 ">
                {loading ? (
              <Loader />
            ) : (
              <DataTable
                columns={columns}
                data={listData}
                pagination
              />
            )}
                  </div>
                </div>
                </div> 

            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default Dashboard;