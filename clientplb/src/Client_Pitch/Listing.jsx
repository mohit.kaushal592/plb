import React, { useState, useEffect } from 'react';
import ReactDOM from 'react-dom';
import { Link, NavLink, useLocation } from 'react-router-dom';
import { useNavigate } from 'react-router-dom';
import '../Auth/Auth.css';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import Top from '../includes/Top';
import Header from '../includes/Header';
import axios from 'axios';
import DataTable from 'react-data-table-component';
import Loader from '../Common/Loader/Loader';
import Select from 'react-select';
import ApexCharts from 'react-apexcharts';


function formatUSNumber(n) {
    return n.toLocaleString('en-US');
}

function ManageSeo() {
    const navigate = useNavigate();

  if(!process.env.NODE_ENV || process.env.NODE_ENV === 'development') { 
      var url = process.env.REACT_APP_PRODUCTION_URL;
  }else{
      var url = process.env.REACT_APP_PRODUCTION_URL;
  }



    const [list, listData] = useState([]);
    const [pending, setPending] = useState(true);
    const [topicName, setTopicName] = useState('');
    const [topicData, setTopicData] = useState([]);
    const [accessToken, setAccessToken] = useState(localStorage.getItem('accessToken'));
    const [loading, setLoading] = useState(false);
    const [clientCat, setClientCat] = useState([]);
    const [selectedOptions, setSelectedOptions] = useState('');
    const [selectedItems, setSelectedItems] = useState([]);
    const [isModalOpen, setModalOpen] = useState(false);
    const [additionalState, setAdditionalState] = useState(null);
    const [isOpenSubmitted, setOpenSubmitted] = useState(false);
    const [dataFromChild1, setDataFromChild1] = useState('');
    const [isFilterPopupOpen, setIsFilterPopupOpen] = useState(false);
    const [selectedHead, setSelectedHead] = useState(null);
    // const [selectedSubHead, setSelectedSubHead] = useState(null);
    const [selectedSubHead, setSelectedSubHead] = useState({
        daItem: null, drItem: null, ssItem: null, apsItem: null, trafficItem: null, backLinksItem: null
    });
    const [selectedddSubHead, setSelecteddSubHead] = useState(null);
    const [wishListName, setWishListName] = useState([])
    const [selectedWishListName, setSelectedWishListName] = useState('');
    const [addListName, setAddListName] = useState('');
    const [isCreateNewList, setIsCreateNewList] = useState(false);
    const [iterationGraph, setIterationGraph] = useState('')
    const [workingGraph, setWorkingGraph] = useState('')
    const [selectedDaOption, setSelectedDaOption] = useState(null);
    const [selectedDrOption, setSelectedDrOption] = useState(null);
    const [selectedSpamScoreOption, setSelectedSpamScoreOption] = useState(null);
    const [selectedTrafficOption, setSelectedTrafficOption] = useState(null);
    const [selectedBacklinkOption, setSelectedBacklinkOption] = useState(null);
    const [selectedAPSOption, setSelectedAPSOption] = useState(null);
    const [isFirstCSVModal, setIsFirstCSVModal] = useState(false);
    const [isSecondCSVModal, setIsSecondCSVModal] = useState(false);
    const [isThirdCSVModal, setIsThirdCSVModal] = useState(false);
    const [csvWishList,setCsvWishList] = useState([])
    const [error, setError] = useState("");
    const [file, setFile] = useState("");
    const [wish, setWish] = useState('')
    const [disable,setDisable] = useState(false)
    const [page ,setPage] =useState(1)
    const [totalRows, setTotalRows] = useState(0);
	const [perPage, setPerPage] = useState(10);

    const headerData = {
        headers: {
            Authorization: `Bearer ${accessToken}`
        }
    }


    // const getClientName = async () => {
    //     try {
    //         // setLoading(true);
    //         const userRes = await axios.get(`${url}/client/name`, headerData);
    //         let data = userRes?.data
    //         // let addNew={ value: 'Add New',
    //         //     label: 'Create New',}
    //         const formattedOptions = data.map(item => ({
    //             value: item?.id,
    //             label: item?.name,
    //         }));
    //         setCsvWishList(formattedOptions)
    //         setWishListName([{
    //             value: 'customButton',
    //             label: (
    //                 <button className="btn_outlined_create_new" onClick={createNewList} >Create New List</button>
    //             ),
    //         }, ...formattedOptions])
    //         // setLoading(false);
    //         setPending(false);
    //     } catch (error) {
    //         console.error('Error fetching data:', error);
    //     }
    // };

    // useEffect(() => {
    //     getClientName();
    // }, [])

    const handleWishlistName = (e) => {
        if (e.value === 'customButton') {
            createNewList();
        } else {
            console.log("in the onchange", e)
            setSelectedWishListName(e);
            setAdditionalState(selectedItems);
            setModalOpen(true);
        }

    };

    const columns = [
        // {
        //     name: 'Name',
        //     selector: row => row.DR,
        //     sortable: true
        // },
        {
            name: 'Domain (URL)',
            selector: row => row.website_url,
            sortable: true,
        },
        {
            name: 'Category',
            selector: row => row.category,
            sortable: true
        },
        {
            name: (
                <div>
                    APS {' '}
                    <img src="../assets/Icons/Aps.svg" width="16px" height="16px" alt="" />
                </div>
            ),
            selector: row => row.ads,
            sortable: true
        },
        {
            name: (
                <div>
                    DA {' '}
                    <img src="../assets/Images/DA.png" width="16px" height="16px" alt="" />
                </div>
            ),
            selector: row => row.domain_authority,
            sortable: true
        },
        {
            name: (
                <div>
                    DR {' '}
                    <img src="../assets/Images/DR.png" width="16px" height="16px" alt="" />
                </div>
            ),
            selector: row => row.domain_rating,
            sortable: true
        },
        {
            name: (
                <div>
                    Annual Spam Score {' '}
                    <img src="../assets/Images/Spam.png" width="16px" height="16px" alt="" />
                </div>
            ),
            selector: row => (<div className="center-aligned-spamScore">{row.spam_score}</div>),
            sortable: true
        },
        {
            name: (
                <div>
                    Traffic {' '}
                    <img src="../assets/Images/Traffic.png" width="16px" height="16px" alt="" />
                </div>
            ),
            selector: row => row?.annual_traffic?.toLocaleString('en-US'),
            sortable: true
        },
        {
            name: (
                <div>
                    Backlinks {' '}
                    <img src="../assets/Images/DR.png" width="16px" height="16px" alt="" />
                </div>
            ),
            selector: row => row?.backlinks?.toLocaleString('en-US'),
            sortable: true
        },
        // {
        //     name: 'Actions',
        //     selector: row => <>
        //         <Link to={`/addSeo?edit=${row.id}`} >
        //             <img src="../assets/Icons/Edit.svg" width="20px" alt="" />
        //         </Link>

        //         <Link to='#' onClick={() => { deleteData(row.id) }}>
        //             <img src="../assets/Icons/DeleteOutlined.svg" width="20px" data-bs-toggle="modal" data-bs-target="#delete_modal" alt="" className="ml-3" />
        //         </Link>
        //     </>,
        // }
    ];

    const filterItems = [
        {
            id: 1,
            title: "DA",
            data: [{
                id: 1,
                item: "< 30",
                value:
                    { key: 'sort_da', value: '<30' },

            },
            {
                id: 2,
                item: "30 - 50",
                value:
                    { key: 'sort_da', value: '30 - 50' },

            },
            {
                id: 3,
                item: "50 - 70",
                value:
                    { key: 'sort_da', value: "50 - 70" },

            },
            {
                id: 4,
                item: "70 - 100",
                value:
                    { key: 'sort_da', value: "70 - 100" },
            }]
        },
     
        {
            id: 3,
            title: "Spam Score",
            data: [{
                id: 9,
                item: "Greater than 10",
                value:
                    { key: 'min_spam', value: ">10" },

            },
            {
                id: 20,
                item: "Less than 10",
                value:
                    { key: 'min_spam', value: "<10" },

            }
        
        ]
        }, {
            id: 4,
            title: "Traffic",
            data: [
                {
                    id: 10,
                    item: "0 - 100K",
                    value:
                        { key: 'traffic_range', value: "less_than_1lakh" },
                }, {
                    id: 11,
                    item: "100K - 500K",
                    value:
                        { key: 'traffic_range', value: "less_than_5lakh" },


                }, {
                    id: 12,
                    item: "Above 500K",
                    value:
                        { key: 'traffic_range', value: "5lakh" },


                }
            ]
        },
        {
            id: 5,
            title: "Backlinks",
            data: [
                {
                    id: 13,
                    item: "0 - 10K",
                    value:
                        { key: 'backlinks_range', value: "10000-50000" },

                },
                {
                    id: 14,
                    item: "10 - 50K",
                    value:
                        { key: 'backlinks_range', value: "50000-100000" },
                },
                 {
                    id: 15,
                    item: "Above 100K",
                    value:
                        { key: 'backlinks_range', value: ">100000" },
                }
            ]
        },
        {
            id: 2,
            title: "DR",
            data: [
                {
                    id: 5,
                    item: "< 30",
                    value:
                        { key: 'sort_dr', value: "<30" },


                },
                {
                    id: 6,
                    item: "30 - 50",
                    value:
                        { key: 'sort_dr', value: "30 - 50" },
                },
                {
                    id: 7,
                    item: "50 - 70",
                    value:
                        { key: 'sort_dr', value: "50 - 70" },
                },
                {
                    id: 8,
                    item: "70 - 100",
                    value:
                        { key: 'sort_dr', value: "70 - 100" },

                }
            ]
        },
        {
            id: 6,
            title: "APS",
            data: [
                {
                    id: 16,
                    item: "< 30",
                    value:
                        { key: 'ads', value: "<30" },


                },
                {
                    id: 17,
                    item: "30 - 50",
                    value:
                        { key: 'ads', value: "30 - 50" },
                },
                {
                    id: 18,
                    item: "50 - 70",
                    value:
                        { key: 'ads', value: "50 - 70" },
                },
                {
                    id: 19,
                    item: "70 - 100",
                    value:
                        { key: 'ads', value: "70 - 100" },

                }
            ]
        },
    ]
    console.log("selectedOptions", selectedOptions)
    // useEffect(() => {
    //     axios.get(`${url}/client/categories/${selectedOptions ? selectedOptions?.value : 'Total'}/`, headerData).then(function (response) {
    //         let get_response_data = response.data;
    //         console.log("bargraphdata", get_response_data)
    //         setIterationGraph(get_response_data.categories)
    //         setWorkingGraph(get_response_data.counts)

    //     }).catch(function (error) {
    //         toast(`Error`)
    //         toast.error("Some issue to load keyword.")
    //     })


    // }, [selectedOptions]);

    // async function getData() {
    //      let daKey=selectedSubHead?.daItem?.item?.value?.key
    //      let daValue=selectedSubHead?.daItem?.item?.value?.value
    //      let trafficKey=selectedSubHead?.trafficItem?.item?.value?.key
    //      let trafficVal=selectedSubHead?.trafficItem?.item?.value?.value
    //      let drKey=selectedSubHead?.drItem?.item?.value?.key
    //      let drVal=selectedSubHead?.drItem?.item?.value?.value
    //      let ssKey=selectedSubHead?.ssItem?.item?.value?.key
    //      let ssVal=selectedSubHead?.ssItem?.item?.value?.value
    //      let apsKey=selectedSubHead?.apsItem?.item?.value?.key
    //      let apsVal=selectedSubHead?.apsItem?.item?.value?.value
    //      let backlinkKey=selectedSubHead?.backLinksItem?.item?.value?.key
    //      let backlinkVal=selectedSubHead?.backLinksItem?.item?.value?.value

    //     setLoading(true);
    //     let apiEndpoint = `${url}/client/listings/`;
    //     if (selectedOptions) {
    //         apiEndpoint += `?category=${selectedOptions?.value}&${selectedSubHead?.daItem?.item?.value?.key}=${selectedSubHead?.daItem?.item?.value?.value}&${selectedSubHead?.trafficItem?.item?.value?.key}=${selectedSubHead?.trafficItem?.item?.value?.value}&${selectedSubHead?.drItem?.item?.value?.key}=${selectedSubHead?.drItem?.item?.value?.value}&${selectedSubHead?.ssItem?.item?.value?.key}=${selectedSubHead?.ssItem?.item?.value?.value}&${selectedSubHead?.apsItem?.item?.value?.key}=${selectedSubHead?.apsItem?.item?.value?.value}&${selectedSubHead?.backLinksItem?.item?.value?.key}=${selectedSubHead?.backLinksItem?.item?.value?.value}`
    //     }
    //     else if (selectedSubHead) apiEndpoint += `?${selectedSubHead?.daItem?.item?.value?.key}=${selectedSubHead?.daItem?.item?.value?.value}&${selectedSubHead?.trafficItem?.item?.value?.key}=${selectedSubHead?.trafficItem?.item?.value?.value}&${selectedSubHead?.drItem?.item?.value?.key}=${selectedSubHead?.drItem?.item?.value?.value}&${selectedSubHead?.ssItem?.item?.value?.key}=${selectedSubHead?.ssItem?.item?.value?.value}&${selectedSubHead?.apsItem?.item?.value?.key}=${selectedSubHead?.apsItem?.item?.value?.value}&${selectedSubHead?.backLinksItem?.item?.value?.key}=${selectedSubHead?.backLinksItem?.item?.value?.value}`
    //     const userRes = await axios.get(apiEndpoint, headerData);
    //     listData(userRes.data);
    //     setLoading(false);
    //     setPending(false);
    // }
    // useEffect(() => {
    //     getData();
    // }, [selectedOptions, selectedSubHead]);

    async function getData() {
        // setLoading(true);
        let apiEndpoint = `${url}/marketing/data_listings/?page=${page}&page_size=${perPage}`;
        const queryParams = [];
      
        if (selectedOptions) {
          queryParams.push(`category=${selectedOptions.value}`);
        }
      
        const parameters = [
          { key: selectedSubHead?.daItem?.item?.value?.key, value: selectedSubHead?.daItem?.item?.value?.value },
          { key: selectedSubHead?.trafficItem?.item?.value?.key, value: selectedSubHead?.trafficItem?.item?.value?.value },
          { key: selectedSubHead?.drItem?.item?.value?.key, value: selectedSubHead?.drItem?.item?.value?.value },
          { key: selectedSubHead?.ssItem?.item?.value?.key, value: selectedSubHead?.ssItem?.item?.value?.value },
          { key: selectedSubHead?.apsItem?.item?.value?.key, value: selectedSubHead?.apsItem?.item?.value?.value },
          { key: selectedSubHead?.backLinksItem?.item?.value?.key, value: selectedSubHead?.backLinksItem?.item?.value?.value },
        ];
      
        for (const { key, value } of parameters) {
          if (key !== undefined && value !== undefined) {
            queryParams.push(`${key}=${value}`);
          }
        }
      
        if (queryParams.length > 0) {
          apiEndpoint += `&filter=True&${queryParams.join('&')}`;
        }
        else {
            apiEndpoint += `&filter=False`;
        }
      
        const userRes = await axios.get(apiEndpoint, headerData);
        listData(userRes?.data?.data);
        setTotalRows(userRes?.data?.total_count?.total_count);
        // setLoading(false);
        setPending(false);
      }
      
      useEffect(() => {
        getData();  
      }, [selectedOptions, selectedSubHead,page,perPage]);

      const handlePageChange = pageno => {
        if(pageno!=1){
        console.log("pageno",pageno)
        // getData(pageno)
        setPage(pageno)
        }
		
	};

	const handlePerRowsChange = async (newPerPage, pageno) => {
        if(pageno!=1){
            setPage(pageno)
            setPerPage(newPerPage);
            }   
	};
      

    console.log("checkList", list)

    async function clientCatDrop() {
        // setLoading(true);
        const userRes = await axios.get(`${url}/client/category/`, headerData);
        let data = userRes?.data
        const formattedOptions = data.map(item => ({
            value: item.category,
            label: item.category.toUpperCase(),
        }));
        setClientCat(formattedOptions);
        // setLoading(false);
        setPending(false);
    }

    useEffect(() => {
        clientCatDrop()
    }, [])

    const createWishlist = async () => {
        console.log("in the crete wish list", addListName)
        let data = {
            name: addListName,
        }
        try {
            // setLoading(true);
            const userRes = await axios.post(`${url}/client/wishlists/`, data, headerData);
            let res_data = userRes?.data
            console.log("res_data?.id", res_data?.id)
            let newName = { value: res_data?.id, label: res_data?.name };
            const updatedData = (prevState) => prevState.concat(newName);

            setWishListName(updatedData)
            let data1 = {
                items:
                    additionalState.map((item) => {
                        console.log("additional state item", item)
                        return ({
                            wishlist: res_data?.id,
                            website: item.website_url,
                            da: item.domain_authority,
                            dr: item.domain_rating,
                            ads: item.ads,
                            spam_score: item.spam_score,
                            annual_traffic: item.annual_traffic,
                            category: item.category,
                            backlinks: item.backlinks,
                            serial_number: item.serial_number ? item.serial_number : ''
                        })
                    })

            }
            console.log("createMyList", data1)

            try {
                // setLoading(true);
                const userRes = await axios.post(`${url}/client/wishlist-items/`, data1, headerData);
                let res_data = userRes?.data
                if (userRes.status == 201) {
                    console.log("res_datares_data", res_data)
                }
                // setLoading(false);
                setPending(false);
            } catch (error) {
                toast.error(error?.response?.data?.message)
            }
            // setLoading(false);
            setPending(false);
            window.location.reload();
        } catch (error) {
            toast.error(error?.response?.data?.message)
        }
    };

    const handleChange = ({ selectedRows }) => {
        // setSelectedItems(selectedRows);
    };

    // const createMyList = async () => {
    //     console.log("selectedWishListName?.value,",additionalState,saveData.value)
    //     let data = {
    //         items:
    //             additionalState.map((item) => ({
    //                 wishlist: saveData?.value,
    //                 website: item.website_url,
    //                 da: item.domain_authority,
    //                 dr: item.domain_rating,
    //                 ads: item.ads,
    //                 spam_score: item.spam_score,
    //                 annual_traffic: item.annual_traffic,
    //                 category: item.category,
    //                 backlinks: item.backlinks,
    //             }))

    //     }
    //     console.log("createMyList",data)
    //     try {
    //         setLoading(true);
    //         const userRes = await axios.post(`${url}/client/wishlist-items/`, data, headerData);
    //         let res_data = userRes?.data
    //         if (userRes.status == 201) {
    //             console.log("res_datares_data",res_data)
    //             // setDataFromChild1({res_data:res_data,selectedName:saveData});

    //         }
    //         setLoading(false);
    //         setPending(false);
    //     } catch (error) {
    //         toast.error(error?.response?.data?.message)
    //     }
    // }


    const handleSaveListName = () => {
        setAddListName(addListName);
        createWishlist();
        setIsCreateNewList(false);

    };

    console.log("addListName", addListName)
    // const getCanvasData = (topic_name,topic_data) => {
    //     console.log('topic_data',topic_data)
    //     setTopicName(topic_name)
    //     if(topic_name == 'Keyword'){
    //         const update_topic_data = `${(topic_data[0]?.keywords != undefined)?topic_data[0]?.keywords:''} <br> ${(topic_data[1]?.keywords != undefined)?topic_data[1]?.keywords:''} <br> ${(topic_data[2]?.keywords != undefined)?topic_data[2]?.keywords:''}` 
    //         setTopicData(update_topic_data)
    //     }

    //     if(topic_name == 'Sv'){
    //         const update_topic_data = `${(topic_data[0]?.sv != undefined)?topic_data[0]?.sv:''} <br> ${(topic_data[1]?.sv != undefined)?topic_data[1]?.sv:''} <br> ${(topic_data[2]?.sv != undefined)?topic_data[2]?.sv:''}` 
    //         setTopicData(update_topic_data)
    //     }

    //     if(topic_name == 'Rank'){
    //         const update_topic_data = `${(topic_data[0]?.rank != undefined)?topic_data[0]?.rank:''} <br> ${(topic_data[1]?.rank != undefined)?topic_data[1]?.rank:''} <br> ${(topic_data[2]?.rank != undefined)?topic_data[2]?.rank:''}` 
    //         setTopicData(update_topic_data)
    //     }

    //     if(topic_name == 'Url'){list
    //         const update_topic_data = `${(topic_data[0]?.target_url != undefined)?topic_data[0]?.target_url:''} <br> ${(topic_data[1]?.target_url != undefined)?topic_data[1]?.target_url:''} <br> ${(topic_data[2]?.target_url != undefined)?topic_data[2]?.target_url:''}` 
    //         setTopicData(update_topic_data)
    //     }

    // }


    const [filterText, setFilterText] = React.useState('');
    const [resetPaginationToggle, setResetPaginationToggle] = React.useState(false);

    const filteredItems = list && list.length > 0 ? list.filter(
        item => item.website_url && item.website_url.toLowerCase().includes(filterText.toLowerCase())
    ) : list;
    const handleSelectUser = (e) => {
        setSelectedOptions(e);
    };
    const closeModal = () => {
        setModalOpen(false);
    };

    function validateFields() {
        if (
            selectedItems?.length === 0
        ) {
            return false;
        }
        return true;
    }

    const closeSubModal = () => {
        setOpenSubmitted(false);
        window.location.reload();


    };
    const handleSubmitList = (data) => {
        setDataFromChild1(data);
        setOpenSubmitted(true);
        setModalOpen(false);

    }
    const handleObjectSelect = (item) => {
        console.log("itemmm", item)
        setSelectedSubHead(item);
        // const updatedData = (prevState) => prevState.concat(selectNew);
        // setSelectMuNew(updatedData)


    };


    const removeItem = (indexToRemove) => {
        console.log("indexToRemove", indexToRemove)
        const updatedItems = additionalState.filter((_, index) => index !== indexToRemove);
        setAdditionalState(updatedItems);
    };

    const subHeaderComponentMemo = React.useMemo(() => {
        const handleClear = () => {
            if (filterText) {
                setResetPaginationToggle(!resetPaginationToggle);
                setFilterText('');
            }
        };

        return (
            <input className="form-control" style={{ width: "180px", height: '42px' }}
                onChange={e => setFilterText(e.target.value)} onClear={handleClear} filterText={filterText} placeholder="Search By Domain" />
        );
    }, [filterText, resetPaginationToggle]);



    const FilterPopup = ({ onClose }) => {
        return (
            <div className="filter-popup">
                <div className="filter-content"  >

                    <div style={{ display: "flex" }}>
                        <ul style={{ 'padding-left': '0rem' }}>
                            {filterItems.map((item, index) => (
                                <li style={{ cursor: "pointer", padding: '8px 24px 8px 12px', 'white-space': 'nowrap' }}
                                    key={index} onClick={() => setSelectedHead({ item, index })} >{item.title}</li>)
                            )}
                        </ul>
                        <ul style={{ 'padding-left': '0rem', 'border-left': '1px solid #D7692A', textAlign: "center" }}>
                            {selectedHead ? (
                                selectedHead?.item?.data?.map((item, index) => (
                                    <li style={{ cursor: "pointer", padding: '12px 8px', 'white-space': 'nowrap' }}
                                        key={index} onClick={() => {
                                            handleObjectSelect({ item, index })
                                        }}>
                                        {item?.item}</li>
                                ))
                            ) : (<p>No item selected</p>)}
                        </ul></div>

                    <button className='btn_outlined' style={{ float: "right" }}
                        onClick={onClose}>Close</button>
                    {/* <button className='main_btn'
                        style={{ float: "right", marginRight: "12px" }} onClick={() => { alert("hii") }}>Apply</button> */}



                </div>
            </div>
        );
    };
    const openFilterPopup = () => {
        setIsFilterPopupOpen(true);
    };
    const closeFilterPopup = () => {
        setIsFilterPopupOpen(false);
    };

    const createNewList = () => {
        console.log("createNewList")
        setIsCreateNewList(true);
        setAdditionalState(selectedItems);
    }
    const createNewListModalClose = () => {
        setIsCreateNewList(false);
    };
    const handleDaFilter = (selectedDaOption) => {
        setSelectedDaOption(selectedDaOption);
        const selectedItem = filterItems[0].data.find((item) =>
            item.value == selectedDaOption.value
        );
        window.stop();
        const selectedIndex = filterItems[0].data.indexOf(selectedItem);
        console.log('Selected Item000:', { item: selectedItem, index: selectedIndex });
        setSelectedSubHead({ ...selectedSubHead, daItem: { item: selectedItem, index: selectedIndex } })

    }
    const handleSpamScoreFilter = (selectedSpamOption) => {
        setSelectedSpamScoreOption(selectedSpamOption);
        const selectedSpamItem = filterItems[1].data.find((item) =>
            item.value == selectedSpamOption.value
        );
        window.stop();
        const selectedIndex = filterItems[1].data.indexOf(selectedSpamItem);
        console.log('Selected Item1111:', { item: selectedSpamItem, index: selectedIndex });
        setSelectedSubHead({ ...selectedSubHead, ssItem: { item: selectedSpamItem, index: selectedIndex } })
    }
    const handleTrafficFilter = (selectedTrafficOption) => {
        setSelectedTrafficOption(selectedTrafficOption);
        const selectedItem = filterItems[2].data.find((item) =>
            item.value == selectedTrafficOption.value
        );
        window.stop();
        const selectedIndex = filterItems[2].data.indexOf(selectedItem);
        console.log('Selected Item2222:', { item: selectedItem, index: selectedIndex });
        setSelectedSubHead({ ...selectedSubHead, trafficItem: { item: selectedItem, index: selectedIndex } })
    }
    const handleBackLinkFilter = (selectedBackLinks) => {
        setSelectedBacklinkOption(selectedBackLinks);
        const selectedItem = filterItems[3].data.find((item) =>
            item.value == selectedBackLinks.value
        );
        window.stop();
        const selectedIndex = filterItems[3].data.indexOf(selectedItem);
        console.log('Selected Item3333:', { item: selectedItem, index: selectedIndex });
        setSelectedSubHead({ ...selectedSubHead, backLinksItem: { item: selectedItem, index: selectedIndex } })
    }

    const handleDrFilter = (selectedDr) => {
        setSelectedDrOption(selectedDr);
        const selectedItem = filterItems[4].data.find((item) =>
            item.value == selectedDr.value
        );
        window.stop();
        const selectedIndex = filterItems[4].data.indexOf(selectedItem);
        console.log('Selected Item3333:', { item: selectedItem, index: selectedIndex });
        setSelectedSubHead({ ...selectedSubHead, drItem: { item: selectedItem, index: selectedIndex } })
    }

    const handleAPSFilter = (selectedAPS) => {    
        setSelectedAPSOption(selectedAPS)
        const selectedApsItem = filterItems[5].data.find((item) =>
            item.value == selectedAPS.value
        )
        window.stop();
        const selectIndex = filterItems[5].data.indexOf(selectedApsItem)
        setSelectedSubHead({ ...selectedSubHead, apsItem: { item: selectedApsItem, index: selectIndex } })
    }


    // const matchingObjects = selectedHead?.item?.data?.filter((obj) =>
    //     obj.item.some((item) =>
    //         selectedSubHead.item.item.some((selectedItem) => selectedItem.item === item.title)
    //     )
    // );



    // console.log("selectedHead",matchingObjects)

    const barOptions = {
        series: [
            {
                name: "Count",
                data: workingGraph
            }
        ],
        chart: {
            type: 'bar',
            height: 500,
            events: {
                dataPointMouseEnter: function (event, chartContext, config) {
                    // Get the category name from the x-axis categories
                    const categoryName = barOptions.xaxis.categories[config.dataPointIndex];

                    // Custom tooltip HTML
                    const tooltipHTML = `
                    <div class="custom-tooltip">
                      <div>Category: ${categoryName}</div>
                    </div>
                  `;

                    // Create a tooltip element
                    const tooltip = document.createElement("div");
                    tooltip.innerHTML = tooltipHTML;

                    // Append the tooltip to the chart container
                    chartContext.el.appendChild(tooltip);
                },
                dataPointMouseLeave: function (event, chartContext) {
                    // Remove the custom tooltip
                    const tooltip = chartContext.el.querySelector(".custom-tooltip");
                    if (tooltip) {
                        tooltip.remove();
                    }
                },
            },
        },
        plotOptions: {
            bar: {
                borderRadius: 0,
                horizontal: false,
                colors: {
                    ranges: [
                        {
                            from: 0,
                            to: 20000,
                            color: '#FFA24C' // Light orange color
                        }
                    ]
                }
            }
        },
        dataLabels: {
            enabled: false
        },
        xaxis: {
            categories: iterationGraph,
        },
        tooltip: {
            enabled: true,
            title: {
                formatter: function (val) {
                    return "Category: " + val;
                },
            },
        },
    };

    var chart = new ApexCharts(document.querySelector("#chart"), barOptions);
    chart.render();

    console.log("filteredItems", filteredItems)
    const handleClearFilter = () => {
        setSelectedSubHead({
            daItem: null, drItem: null, ssItem: null, apsItem: null, trafficItem: null, backLinksItem: null
        })
        setSelectedDaOption(null)
        setSelectedDrOption(null)
        setSelectedSpamScoreOption(null)
        setSelectedAPSOption(null)
        setSelectedBacklinkOption(null)
        setSelectedTrafficOption(null)
    }
    console.log("setSelectedSubHeadsetSelectedSubHead", selectedSubHead)
    
    const closeCsvFirstModal = () => {
        setIsFirstCSVModal(false); 
      };
      const closeSecondCsvModal = () => {
        setIsSecondCSVModal(false);
    };
    const handleCreateNewList =()=>{
        setIsFirstCSVModal(false); 
        setIsSecondCSVModal(true)
    }

    const handleSelectWishList = (wishId) =>{
        setIsFirstCSVModal(false); 
        setIsThirdCSVModal(true)
        setWish(wishId)
       
    }
    const closeCsvThirdModal = () =>{
            setWish("");
            setFile('');
            setDisable(false)
            setIsThirdCSVModal(false);
            setIsSecondCSVModal(false);
            setIsFirstCSVModal(false);
            
        
    }

    const handleSaveCSVListName = async () => {
        setIsSecondCSVModal(false);
        console.log("in the crete wish list", addListName)
        let data = {
            name: addListName,
        }
        try {
            // setLoading(true);
            const userRes = await axios.post(`${url}/client/wishlists/`, data, headerData);
            let res_data = userRes?.data
            console.log("res_data?.id", res_data?.id)
            let newName = { value: res_data?.id, label: res_data?.name };
            const updatedData = (prevState) => prevState.concat(newName);
            setCsvWishList(updatedData)
            setIsFirstCSVModal(true)
        }
           catch (error) {
            toast.error(error?.response?.data?.message)
        }
        
    };

    const allowedExtensions = ["csv"];

    const handleFileChange = (e) => {
        
        setError("");
        if (e.target.files?.length) {
            const inputFile = e.target.files[0];

            const fileExtension = inputFile?.type.split("/")[1];
            if (!allowedExtensions.includes(fileExtension)) {
                setError("Please input a csv file");
                return;
            }

            setFile(inputFile);
            if(inputFile){
                setDisable(true)
            }
        }
    };
    console.log("filefile",file)

    const handleParse = () => {
        // setLoading(true)
        const formData = new FormData();
        formData.append(
            "file",
            file,
            "file"
        );
        formData.append('wish', wish);
        axios.post(`${url}/marketing/upload/`, formData, headerData).then((response) => {
            if (response.status == 201) {
                // setLoading(false)
                setDisable(false)
                toast.success(`${response.data.message}.`)
                setTimeout(() => {
                    toast.dismiss(); 
                    //  window.location.reload();
                    
                    navigate(`/list?id=${wish}`)
                }, 1000);
            }
        }).catch((error) => {
            if (error) {
                toast.error("Some tehnical issue.")
            }
        });
    };

   

    return (
        <div class="container-fluid ">
            <div class="row">
                <Top marketingPublisher={true} />
                <div class="col-lg p-0" id="right_side">
                    <Header />
                    <ToastContainer/>
                    <div class="main" style={{textAlign:"left"}}>
                        <br></br>

                        <br></br>
                        <div className="d-flex justify-content-between align-items-center">
                          
                        </div>

                        <div className='site_List_selectes_table_header'>
                            <div className='selected_table_container'>
                                <div className='selected_table_frame2 mt-4' style={{textAlign:'left'}}>
                                    {selectedItems?.length ? (<>
                                        <div className='selecte_publisher' >
                                            <div >Publisher Selected</div>
                                            <h2 className="fw-bold mt-3">{selectedItems?.length}</h2>
                                        </div>
                                        <div><img src="../assets/Icons/verticalVector.svg" alt="" /></div>
                                        <div className='selecte_publisher'>
                                            <div ></div>
                                            <Select
                                                options={wishListName}
                                                placeholder="New List"
                                                onChange={handleWishlistName}
                                                className="custom-select"
                                                disabled={!selectedItems?.length}
                                            />

                                        </div></>) : ""}


                                    <div>{subHeaderComponentMemo}</div>

                                    <Select
                                        value={selectedDaOption}
                                        className="custom-select1"
                                        onChange={handleDaFilter}
                                        options={filterItems[0].data.map((item) => ({
                                            value: item.value,
                                            label: item.item,
                                        }))}
                                        placeholder="DA"
                                    />

                                    <Select
                                        value={selectedSpamScoreOption}
                                        onChange={handleSpamScoreFilter}
                                        className="custom-select"
                                        options={filterItems[1].data.map((item) => ({
                                            value: item.value,
                                            label: item.item,
                                        }))}
                                        placeholder="Spam Score"
                                    />

                                    <Select
                                        value={selectedTrafficOption}
                                        onChange={handleTrafficFilter}
                                        className="custom-select1"
                                        options={filterItems[2].data.map((item) => ({
                                            value: item.value,
                                            label: item.item,
                                        }))}
                                        placeholder="Traffic"
                                    />

                                    <Select
                                        value={selectedBacklinkOption}
                                        onChange={handleBackLinkFilter}
                                        className="custom-select1"
                                        options={filterItems[3].data.map((item) => ({
                                            value: item.value,
                                            label: item.item,
                                        }))}
                                        placeholder="Backlinks"
                                    />

                                    <Select
                                        value={selectedDrOption}
                                        onChange={handleDrFilter}
                                        className="custom-select1"
                                        options={filterItems[4].data.map((item) => ({
                                            value: item.value,
                                            label: item.item,
                                        }))}
                                        placeholder="DR"
                                    />
                                  
                                    <Select
                                options={clientCat}
                                placeholder="Select Category"
                                onChange={handleSelectUser}
                                className="custom-select"
                            />
                              <Select
                                        className="custom-select1"
                                        value={selectedAPSOption}
                                        onChange={handleAPSFilter}
                                        options={filterItems[5].data.map((item) => ({
                                            value: item.value,
                                            label: item.item,
                                        }))}
                                        placeholder="APS   "
                                    />
                                    {(selectedSubHead.daItem || selectedSubHead.drItem || selectedSubHead.apsItem || selectedSubHead.ssItem || selectedSubHead.trafficItem || selectedSubHead.backLinksItem) ? <button className="clear_filter_btn" onClick={() => handleClearFilter()}>Clear Filter</button> : ''}


                                    {/* <div className='selecte_publisher'>
                 
                   <div>{subHeaderComponentMemo}</div>
                   </div>           */}
                                </div>
                            </div>

                        </div>
                        {/* {loading ? (
                            <Loader />
                        ) : ( */}
                            <DataTable
                                columns={columns}
                                data={filteredItems}
                                pagination
                                selectableRows
                                onSelectedRowsChange={handleChange}
                                progressPending={pending}
                                subHeader
                                persistTableHead
                                loading={loading}
                                paginationServer
			                    paginationTotalRows={totalRows}
			                    onChangePage={handlePageChange}
                                onChangeRowsPerPage={handlePerRowsChange}
                           
                            />
                        {/* )} */}
                    </div>
                </div>

            </div>

        </div>
    );
}
export default ManageSeo;