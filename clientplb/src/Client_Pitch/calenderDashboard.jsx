import React, { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';
import { useNavigate } from 'react-router-dom';
import '../Auth/Auth.css';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import Top from '../includes/Top';
import Header from '../includes/Header';
import Calendar from 'react-awesome-calendar';
import axios from 'axios';
import ReactApexChart from 'react-apexcharts';
import Loader from '../Common/Loader/Loader';
import ApexCharts from 'react-apexcharts';
import dayjs from 'dayjs';
import DatePickerViews from '../includes/DatePicker';
import { AdapterDayjs } from '@mui/x-date-pickers/AdapterDayjs';
import { LocalizationProvider } from '@mui/x-date-pickers/LocalizationProvider';
import { DatePicker } from '@mui/x-date-pickers/DatePicker';
import ToDoList from '../Common/TodoList/TodoList';
import FeedBack from '../Common/FeedBack/FeedBack';
import { MenuItem, FormControl, Select, InputLabel } from "@mui/material";
import CustomizedTimeline from '../includes/ProjectTimeLine';


function Dashboard() {
    // const navigate = useNavigate();


    const [dashboard, setDashboard] = useState();
    const [dashboardData,setDashboardData] = useState()
    const [barDateGraph, setBarDateGraph] = useState(['Na']);
    const [barKeywordGraph, setBarKeywordGraph] = useState(['Na']);
    const [barCountryGraph, setBarCountryGraph] = useState(['Na']);
    const [barUrlGraph, setBarUrlGraph] = useState(['Na']);
    const [CalendarData, setCalendarData] = useState([]);
    const [pieApproveGraph, setPieApproveGraph] = useState(0);
    const [pieRejectedGraph, setPieRejectedGraph] = useState(0);
    const [clientGraph, setClientGraph] = useState(0);
    const [seoGraph, setSeoGraph] = useState(0);
    const [contentWGraph, setContentWGraph] = useState(0);
    const [contentMGraph, setContentMGraph] = useState(0);
    const [clientSerGraph, setClientSerGraph] = useState(0);
    const [totalSubmissions, setTotalSubmissions] = useState(0);
    const [totalworking, setTotalworking] = useState(0);
    const [totalPending, setTotalPending] = useState(0);
    const [score,setScore] = useState(null)
    const [barCategories,setBarCategories] = useState([])
    const [barCount,setBarCount] = useState([])
    const [selectedData, setSelectedData] = useState([]);
    const [selectedManagerBarData, setSelectedManagerBarData] = useState([]);
    const [selectedDate, setSelectedDate] = React.useState(dayjs());
    const [piData,setPiData] = useState({})
    const [threeMonth, setThreeMonth] = useState([]);
    const [sixMonth, setSixMonth] = useState([])
    const [notificationData, setNotificationData] = useState([]);
    const [notificationLoading, setNotificationLoading] = useState(true);
    const [selectedSite,setSelectedSite] = useState('off_site')
    const [selectedMonth,setSelectedMonth] = useState('3months')
    const [combineToDoData,setCombineToDoData] = useState([])
    const [timeLinekData, setTimeLineData] = useState(null)
    const [selectedTimelineSite, setSelectedtimelineSite] = useState('off_site')
    const [selectedProjectTimeLineDate, setSelectedProjectTimeLineDate] = React.useState(dayjs());
    const [selectedActivity,setSelectedActivity] = useState('marketing')



    var url;

    if (!process.env.NODE_ENV || process.env.NODE_ENV === 'development') {
        // Use the development URL or a default value if it's not defined
        url = process.env.REACT_APP_PRODUCTION_URL;
    } else {
        // Use the production URL from your environment variables
        url = process.env.REACT_APP_PRODUCTION_URL;
    }
    const [accessToken, setAccessToken] = useState(localStorage.getItem('accessToken'));
    const [role, setRole] = useState(localStorage.getItem("role").toLowerCase())
    const [department, setDepartment] = useState(localStorage.getItem("department").toLowerCase());

    const headerData = {
        headers: {
            Authorization: `Bearer ${accessToken}`
        }
    }
    console.log("dashboardData",dashboardData)


    const getPiData = () => {
        setLoading(true)
        axios.get(`${url}/content_writing/status-count-pie-chart`, headerData)
            .then(async (response) => {
                let data = await response?.data;
                setPiData(data);
                setLoading(false);
            })
    }

    const getCalendarData = (get_calendar_date='') => {
        axios.get(`${url}/notify/logs/`, headerData).then(function (response) {
            let calendar_data = response.data;
            var a_calendar = [];

            calendar_data.forEach((data) => {
              const id = data.id;
              const timestamp = data.timestamp;
              const message = data.message;
              const seo=data.seo;
              const tags=data.tags;
              const writing=data.writing
            
              a_calendar.push({
                id: id,
                color: '#FF3E52',
                from: timestamp,
                to: timestamp,
                // title: (<div style={{fontSize:'10px',fontWeight:'bold'}}>{message}</div>),
                title: message,
                seo:seo,
                tags:tags,
                writing:writing,
              });
            });
            setCalendarData(a_calendar)
            setLoading(false)
        }).catch(function (error) {
            toast(`Error`)
            toast.error("Some issue to load keyword.")
        })
    }

    async function getTimeLineData() {
        const formattedDate = selectedProjectTimeLineDate ? selectedProjectTimeLineDate.format('YYYY-MM') : '';
        setLoading(true);
        await axios.get(`${url}/marketing/project_timeline?site=${selectedTimelineSite}&Activity_status=${selectedTimelineSite=='on_site'?"":selectedActivity}&date=${formattedDate}`, headerData).then((response) => {
            console.log('response__', response.data)
            setTimeLineData(response?.data)
            setLoading(false);
        }).catch((error) => {
            if (error) {
                toast.error("Some tehnical issue.")
            }
        });
    }
    useEffect(() => {
        getTimeLineData()
    }, [selectedTimelineSite,selectedActivity, selectedProjectTimeLineDate])

    useEffect(() => {
        axios.get(`${url}/content_writing/dashboard`, headerData).then(function (response) {
            setDashboard(response.data)


        }).catch(function (error) {
            toast(`Error`)
            toast.error("Some issue to load image.")
        })

        axios.get(`${url}/manager/chart/`, headerData).
            then(function (response) {
                let data = response?.data
                setClientGraph(data?.client)
                setSeoGraph(data?.seo)
                setContentWGraph(data?.content_writing)
                setContentMGraph(data?.content_marketing)
                setClientSerGraph(data?.client_service)
            }).catch(function (error) {
                toast(`Error`)
                toast.error("Some issue to load image.")
            })

            axios.get(`${url}/content_writing/bar/`, headerData).then(function (response) {
                let get_response_data = response.data;
                console.log("get_response_data",get_response_data)
                setBarCategories(get_response_data.categories)
                setBarCount(get_response_data.count)
    
            }).catch(function (error) {
                toast(`Error`)
                toast.error("Some issue to load keyword.")
            })

            axios.get(`${url}/marketing/dashboard/`, headerData).then((response) => { 
                console.log('response__',response)
                setDashboardData(response.data)
            }).catch((error) => {
                if (error) {
                    toast.error("Some tehnical issue.")
                }
            });
            getPiData()
            getCalendarData()

            axios.get(`${url}/notify/notifications/`, headerData).then((response) => { 
                console.log('response__',response)
                if (response.status == 200) { 
                    setNotificationLoading(false);
                    let today_data = response.data.today
                    let previous_data = response.data.previous 
                    var combine_notification = [...today_data, ...previous_data];
                    setCombineToDoData(combine_notification)
                    
                    var visibleItems = combine_notification.slice(0, 5)
                    // setNotificationData(visibleItems);  
                    setNotificationData(combine_notification); 
                }
            }).catch((error) => {
                if (error) {
                    toast.error("Some tehnical issue.")
                }
            });

    }, []);

    useEffect(()=>{
        const formattedDate = selectedDate
        ? selectedDate.format('MM-YYYY')
        : '';
        setLoading(true)
        axios.get(`${url}/content_writing/status-count-by-user/?date_filter=${formattedDate}`, headerData)
            .then(async (response) => {
                let data = await response?.data;
                setSelectedManagerBarData(data);
                setLoading(false);
            })

     },[selectedDate])

    useEffect(()=>{
        getTeamBarData()
    },[selectedMonth,selectedSite])

    const barOptions = {
        title: {
            text: 'Delivery Status'
        },
        series: [{
            name: "count",
            data: barCount
        }],
        chart: {
            type: 'bar',
             height: 500
        },
        plotOptions: {
            bar: {
                borderRadius: 0,
                horizontal: true,
                colors: {
                    // backgroundBarColors: ['#FFA24C', '#FFC542', '#FFD94C', '#FFED4C', '#FFB74D', '#FF9E4C'],
                    ranges: [{
                        from: 0,
                        to: 50,
                        color: '#DC143C'
                    }, {
                        from: 50,
                        to: 80,
                        color: '#FFA24C' 
                    }, {
                        from: 101,
                        to: 150,
                        color: '#FFA24C' 
                    }, {
                        from: 151,
                        to: 200,
                        color: '#AFE1AF'
                    }, {
                        from: 201,
                        to: 250,
                        color: '#AFE1AF'
                    }, {
                        from: 251,
                        to: 300,
                        color: '#AFE1AF'
                    }]
                }
            }
        },
        dataLabels: {
            enabled: true
        },
        xaxis: {
            categories: barCategories,
        }
    };

    const navigate = useNavigate();

    const [list, listData] = useState([]);
    const [canvaData, setCanvaData] = useState([]);
    const [pending, setPending] = useState(true);
    const [content, setContent] = useState('');
    const [shortDescription, setShortDescription] = useState('');
    const [loading, setLoading] = useState(true); // Add loading state
    const [selectedStatus, setSelectedStatus] = useState('');

    useEffect(() => {
        async function getData() {
            setLoading(true);

            const statusQueryParam = selectedStatus ? `${selectedStatus}` : 'approved';

            const userRes = await axios.get(`${url}/content_writing/api/content-writing/?status=${statusQueryParam}`, headerData);
            setLoading(false);
            console.log('userRes', userRes)
            listData(userRes.data);
            setPending(false);
        }
        getData();

    }, [selectedStatus]);

    
    const handleDateChange = (date) => {
        setSelectedDate(date);
      };

    const getScoreData = async (Id) => {
        const responseData = await axios.get(`${url}/content_writing/api/score/${Id}/`, headerData);
        setScore(responseData?.data?.data[0])
    }

    const getCanvasData = async (dataID) => {
        const responseData = await axios.get(`${url}/content_writing/api/writing/${dataID}`, headerData);
        setCanvaData(responseData.data)
        //console.log('1__',responseData.data)
        getScoreData(responseData?.data?.id)
        if (responseData?.data.description.length > 500) {
            setShortDescription(responseData?.data.description.slice(0, 300) + " \n\n <span class='read-more' onclick='readFull()'>Read More</span></p>")
        } else {
            setShortDescription(responseData?.data.description.slice(0, 500))
        }
        setContent(responseData.data.description);
    }

    window.readFull = () => {
        let content_length = content.length;
        setShortDescription(content.slice(0, content_length) + " \n\n <span class='read-more' onclick='readLess()'>Read Less</span></p>")
    }

    window.readLess = () => {
        setShortDescription(content.slice(0, 300) + " \n\n <span class='read-more' onclick='readFull()'>Read More</span></p>")
    }

    const getTeamBarData = () => {
        setLoading(true)
        axios.get(`${url}/content_writing/team_content_chart/?date_by=${selectedMonth}&filter_by=${selectedSite}`, headerData)
            .then(async (response) => {
                let data = await response?.data;
                setThreeMonth(data.last_3_months)
                setSixMonth(data?.last_6_months)
                if (selectedMonth == '3months') {
                    setSelectedData(data.last_3_months);
                } else if (selectedMonth == '6months') {
                    setSelectedData(data?.last_6_months);
                }
                setLoading(false);
            })
    }

    const handleDropdownChange = (event) => {
        const selectedValue = event.target.value;
        setSelectedMonth(event.target.value)

    };
    const handleSiteDropdown=(event)=>{
        setSelectedSite(event?.target?.value)
    }

    const projectTimeLineDateChange = (date) => {
        setSelectedProjectTimeLineDate(date);
    };
    const projectTimelineSiteDropdown = (event) => {
        setSelectedtimelineSite(event?.target?.value)
    }

    const [filterText, setFilterText] = React.useState('');
    const [resetPaginationToggle, setResetPaginationToggle] = React.useState(false);

    const changeCalendarEvent = (date) => {
        if(date.mode == 'dailyMode'){
            let click_date = `${date.year}-${date.month+1}-${date.day}`
            
            const filteredData = CalendarData?.filter((item) => {
                const itemDate = new Date(item?.from);
                console.log("itemDate",itemDate)
                
                return (
                  itemDate.getFullYear() == date.year &&
                  itemDate.getMonth() == date.month &&
                  itemDate.getDate() == date.day
                );
              });


              if(filteredData[0]?.seo.length !==0 ){
                console.log("navigate to seo")
                navigate(`/seo-calendar-event/?from_event=${click_date}`,{
                    state: filteredData[0]?.id
                  });
              }
              else if(filteredData[0]?.tags.length !==0){
                console.log("navigate to tags")
                navigate(`/tags-calendar-event/?from_event=${click_date}`,{
                    state: filteredData[0]?.id
                  });
              }
              else if(filteredData[0]?.writing.length !==0){
                console.log("navigate to writing")
                navigate(`/content-calendar-event/?from_event=${click_date}`,{
                    state: filteredData[0]?.id
                  });
              }
        }
        if(date.mode == 'monthlyMode'){
            getCalendarData(`?target_date=${date.year}-${date.month+1}`)
        }
        if(date.mode == '"yearlyMode"'){
            getCalendarData(`?target_date=${date.year}`)
        }
    }

    const updateLink = (link_value, id) => {
        if (link_value != '') {

            axios.get(`${url}/content_writing/api/writing/${id}/?article=${link_value}`, headerData).then((response) => {
                //console.log('1',response)
                if (response.status == 200) {
                    toast.success(`Link updated successfully.`)
                    //setContentLoaderVisible(false)
                }
            }).catch((error) => {
                if (error) {
                    toast.error("Some tehnical issue.")
                    //setContentLoaderVisible(false)
                }
            });
        }
    }


    const barOptions1 = {
        chart: {
          type: 'bar', 
        },
        // title: {
        //   text: 'Keyword Allocations',
        // },
        xaxis: {
            categories: 
            selectedManagerBarData?.map(item => item?.username)
          },
        yaxis: {
          title: {
            text: 'Allocations',
          },
        },
        plotOptions: {
          bar: {
            horizontal: false,
            columnWidth: '55%', 
            dataLabels: {
              position: 'top',
            },
          },
        },
        series:  [
              {
                name: 'Live',
                data: selectedManagerBarData?.map(item => item?.live),
              },
              {
                name: 'Pending',
                data: selectedManagerBarData?.map(item => item?.pending),
              },
            ]
      };
    const barTeamOptions = {
        chart: {
          type: 'bar', // Use 'bar' for a column chart
        },
        title: {
          text: 'Keyword Allocations',
        },
        xaxis: {
            categories: selectedData?.length === 0
              ? threeMonth?.map(item => item?.month)
              : selectedData?.map(item => item?.month),
          },
        yaxis: {
          title: {
            text: 'Allocations',
          },
        },
        plotOptions: {
          bar: {
            horizontal: false,
            columnWidth: '55%', // Adjust as needed
            dataLabels: {
              position: 'top',
            },
          },
        },
        series: selectedData.length === 0
          ? [
              {
                name: 'Completed',
                data: threeMonth.map(item => item.complete),
              },
              {
                name: 'Pending',
                data: threeMonth.map(item => item.pending),
              },
            ]
          : [
              {
                name: 'Completed',
                data: selectedData.map(item => item.complete),
              },
              {
                name: 'Pending',
                data: selectedData.map(item => item.pending),
              },
            ],
      };

    const chartOptions = {
        chart: {
          type: 'donut',
          width: 350,
        },
         labels: Object?.keys(piData)?.filter(label => label != 'total')?.map(label => label?.charAt(0)?.toUpperCase() + label?.slice(1)),
        // labels: [],
        dataLabels: {
          enabled: false, // Disable data labels
        },
        plotOptions: {
          pie: {
            donut: {
              labels: {
                show: true,
                name: {
                  show: true,
                  fontSize: '22px',
                  fontFamily: 'Rubik',
                  color: '#dfsda',
                  offsetY: -10,
                },
                value: {
                  show: true,
                  fontSize: '16px',
                  fontFamily: 'Helvetica, Arial, sans-serif',
                  color: undefined,
                  offsetY: 16,
                  formatter: function (val) {
                    return val;
                  },
                },
                total: {
                  show: true,
                  label: 'Total',
                  color: '#373d3f',
                  formatter: function (w) {
                    const total = piData?.total;
                    return total.toString(); 
                  },
                },
              },
            },
          },
        },
      };
      
      const PieChart = () => {
        const seriesData = Object?.values(piData)?.filter(label => label !== piData.total);
        return (
          <ReactApexChart
            options={chartOptions}
            series={seriesData} 
            type="donut"
            width="400"
            height="400"
          />
        );
      };


    return (
        <>
            <div className="container-fluid ">
                <ToastContainer />
                <div className="row">
                    {/* <Top marketingContent={true} /> */}
                    <Top marketingDashboard={true} />
                    <div className="col-lg p-0" id="">
                        {/* <Header /> */}
                        <div className="main">
                            <div class="row g-4 ">
                                <div class="col-lg-4 col-xl-3 col-sm-6">
                                <Link className="dropdown-item">
                                     <div class="content  Iteration border rounded  w-100  p-3 ">
                                        <div className='d-flex justify-content-between'>
                                            <h5 class="fw-bold">Off Page</h5>
                                            <div className='d-flex flex-column gap-4'>

                                            <div className='d-flex gap-5'>
                                                <div style={{fontSize:'16px',fontWeight:'600'}}>Live</div>
                                                <h5 class="fw-bold">{dashboardData?.total_live}</h5>
                                            </div>
                                            {/* <div className='d-flex gap-5'>
                                            <div style={{fontSize:'16px',fontWeight:'600'}}>Pending</div>
                                                <h5 class="fw-bold">{dashboard?.total_on_page}</h5>
                                            </div> */}
                                           </div>
                                        </div>
                                    </div>
                                    </Link>
                                </div>
                                <div class="col-lg-4 col-xl-3 col-sm-6">
                                <Link className="dropdown-item">
                                    <div class="content  Iteration border rounded  p-3 ">
                                        <div className='d-flex justify-content-between'>
                                        {/* <h5 class="fw-bold">Content<br />Backlinks</h5> */}
                                        <h5 class="fw-bold">Off Page</h5>
                                            <div className='d-flex flex-column gap-4'>

                                            {/* <div className='d-flex justify-content-between'>
                                                <div style={{fontSize:'16px',fontWeight:'600'}}>Live</div>
                                                <h5 class="fw-bold">{dashboard?.total_rejected}</h5>
                                            </div> */}
                                            <div className='d-flex gap-5'>
                                            <div style={{fontSize:'16px',fontWeight:'600'}}>Pending</div>
                                                <h5 class="fw-bold">{dashboardData?.total_pending}</h5>
                                            </div>
                                           </div>
                                        </div>
                                    </div>
                                </Link>
                                </div>
                                <div class="col-lg-4 col-xl-3 col-sm-6">
                                    <div class="content rounded  Approval w-100 p-3 border">
                                        <h1 class="fw-bold">{dashboardData?.total_active_projects}</h1>
                                        <h6>Total Active</h6>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-xl-3 col-sm-6">
                                    <div class="content rounded  Rejected w-100  p-3 border">
                                        <h1 class="fw-bold">{dashboardData?.total_users}</h1>
                                        <h6>Total User</h6>
                                    </div>
                                </div>
                                {(role=='manager')? <>
                                <div class="col-lg-7">
                                    <div className=" bg-white h-100 dashboard_box">
                                    <div className="chart_head shadow-sm d-flex justify-content-between p-4 mb-4 border-bottom">
                                            <h4>Keyword Allocations</h4>
                                            <div style={{width:'200px'}}>
                                            <LocalizationProvider dateAdapter={AdapterDayjs}>
                                                 <DatePicker
                                                   label={'month and year'}
                                                   openTo="month"
                                                   views={['year', 'month']}
                                                   value={selectedDate}
                                                   onChange={handleDateChange}
                                                 />
                                              </LocalizationProvider>
                                            </div>
                                            
                                        </div>
                                        <div style={{ textAlign: "end" }}>

                                            <ApexCharts options={barOptions1} series={barOptions1.series} type="bar" height={350} />
                                        </div>

                                    </div>

                                </div></>:''}
                                    
                                {/* {( role=='manager')? 
                                <div class="col-lg-4">
                                    <div className=" bg-white d-flex flex-column h-100 dashboard_box">
                                        <div className="chart_head  p-4 mb-4 border-bottom">
                                            <h4>Content Status</h4>
                                        </div>
                                        <div className='d-flex flex-column align-items-center justify-content-center'>
                                        <PieChart />
                                        </div>
                                    </div>
                                </div>:''} */}

{( role=='manager')? 
                                <div class="col-lg-5">
                                    {/* <div className=" bg-white d-flex flex-column h-100 dashboard_box">
                                        <div className="chart_head  p-4 mb-4 border-bottom">
                                            <h4>Content Status</h4>
                                        </div>
                                        <div className='d-flex flex-column align-items-center justify-content-center'>
                                        <PieChart />
                                        </div>
                                    </div> */}
                                     <div className=" bg-white dashboard_box h-100">
                                       <div className="p-3 border-bottom">
                                           <div className='d-flex justify-content-between'>
                                               <h5 style={{ color: '#D7692A' }}>Project Timeline</h5>
                                           </div>
                                       </div>
                                       <div className="d-flex">
                                       <div class="col-6">
                                           <CustomizedTimeline timeLinekData={timeLinekData?.data} />
                                       </div>
                                       <div class="col-6 ms-5 mt-3">
                                       <div className='d-flex flex-column gap-3 justify-content-between'>
                                                   <div >
                                                   <FormControl sx={{ m: 1, minWidth: 170 }} size="small">
                                                   <DatePickerViews selectedDate={selectedProjectTimeLineDate} handleDateChange={projectTimeLineDateChange} />
                                                   </FormControl>
                                                      
                                                   <FormControl sx={{ m: 1, minWidth: 180 }} size="small">
                                                       <InputLabel id="demo-select-small-label">Select Site</InputLabel>
                                                       <Select
                                                           labelId="demo-select-small-label"
                                                           id="demo-select-small"
                                                           value={selectedTimelineSite}
                                                           label="Select Title"
                                                           onChange={projectTimelineSiteDropdown}
                                                       >
                                                           <MenuItem value='off_site' >Off Page</MenuItem>
                                                           <MenuItem value='on_site' >On Page</MenuItem>
                                                       </Select>
                                                   </FormControl>
                                                  
                                                  {selectedTimelineSite=='off_site'?<FormControl sx={{ m: 1, minWidth: 180 }} size="small">
                                                       <InputLabel id="demo-select-small-label">Select Activity</InputLabel>
                                                       <Select
                                                           labelId="demo-select-small-label"
                                                           id="demo-select-small"
                                                            value={selectedActivity}
                                                           label="Select Title"
                                                            onChange={(event)=>setSelectedActivity(event?.target?.value)}
                                                       >
                                                         <MenuItem value='marketing'>Content Marketing</MenuItem>
                                                         <MenuItem value='article'>Article Submission</MenuItem>
                                                        <MenuItem value='ppt_pdf'>PDF/PPT Submission</MenuItem>
                                                       <MenuItem value='image'>Image/Infofraphics</MenuItem>
                                                   <MenuItem value='video'>Video Submission</MenuItem>
                                                   <MenuItem value='quora'>Quora</MenuItem>
                                                       </Select>
                                                   </FormControl>:''}
                                                   </div>
                                       </div>
                                       </div>
                                       </div>
                                   </div>
                                </div>:''}


                            </div>
                            
                            <div className="row mt-4">
                                {role =='manager' ? <div class="col-lg-7">
                                    <div className="dashboard_box" style={{ overflowY: 'auto' ,height:'550px'}}>
                                        <div id='calendar' className="p-4"> 
                                            <Calendar events={CalendarData} onChange={(date)=>{changeCalendarEvent(date)}} />
                                        </div>
                                    </div>
                                </div>:''}
                                <div class="col-lg-5">
                                    <div className="dashboard_box p-3" style={{ overflowY: 'auto' ,height:'550px'}}>
                                        <div style={{ display:'flex',justifyContent:'space-between',}}> 
                                        <h3 className='pb-3' style={{ color: '#D7692A' }}>To Do List</h3>
                                        {combineToDoData.length >5 ?<Link to={`/notification`} style={{ color: '#D7692A' }}> View All </Link>:''}
                                        </div>
                                        {notificationLoading ? (
                                            <Loader />
                                        ) : (
                                            <ToDoList notificationData={notificationData}/>
                                        )}
                                    </div>
                                </div>
                            </div>
                            <div className="row mt-4">
                            {(role=='manager') ? <>
                            <div className="col-lg-7">
                                <div className=" bg-white h-100 dashboard_box">
                                <div className="chart_head shadow-sm d-flex justify-content-between p-4 mb-4 border-bottom">
                                            <h4>Keyword Allocation</h4>
                                            
                                                <div className='d-flex'>
                                                <select onChange={handleSiteDropdown}>
                                                <option value="off_site">Off Page</option>
                                                <option value="on_site">On Page</option>
                                            </select>
                                           
                                           <div style={{marginLeft:'10px'}}>
                                            <select onChange={handleDropdownChange}>
                                                <option value="3months">Last 3 Months</option>
                                                <option value="6months">Last 6 Months</option>
                                            </select>
                                            </div>
                                            </div>
                                            </div>
                                            <div style={{ textAlign: "end" }}>
                                            <ApexCharts options={barTeamOptions} series={barTeamOptions.series} type="bar" height={350}/>
                                        </div>
                                        </div>
                               
                               
                                </div>

          {/* <div class="col-lg-5">
         <div className="dashboard_box p-3" style={{ overflowY: 'auto' ,height:'550px'}}>
           <div style={{ display:'flex',justifyContent:'space-between',}}> 
             <h3 className='pb-3' style={{ color: '#D7692A' }}>Feedback</h3>
            {combineToDoData.length >5 ?<Link to={`/notification`} style={{ color: '#D7692A' }}> View All </Link>:''}
           </div>
          {notificationLoading ? (
        <Loader />
           ) : (
        <FeedBack notificationData={notificationData}/>
              )}
           </div>
         </div> */}

         <div class="col-lg-5">
         <div className="dashboard_box p-3" style={{ overflowY: 'auto' ,height:'550px'}}>
           <div style={{ display:'flex',justifyContent:'space-between',}}> 
             <h3 className='pb-3' style={{ color: '#D7692A' }}>Feedback</h3>
            {combineToDoData.length >5 ?<Link to={`/feedback`} style={{ color: '#D7692A' }}> View All </Link>:''}
           </div>
          {notificationLoading ? (
        <Loader />
           ) : (
        <FeedBack notificationData={[]}/>
              )}
           </div>
         </div> 





</>          
                                :""}</div>
                    

                        </div>
                        <div class="offcanvas offcanvas-end bg-white" tabindex="-1" id="offcanvasExample" aria-labelledby="offcanvasExampleLabel">
                            <div class="offcanvas-header">
                                <h2 class="offcanvas-title" id="offcanvasExampleLabel">
                                    Details
                                </h2>
                                <button type="button" class="btn-close" data-bs-dismiss="offcanvas" aria-label="Close"></button>
                            </div>
                            <div class="offcanvas-body">
                                <div className="row">
                                    <div className="col-lg-12">
                                        <h5>Topic</h5>
                                        <h6>{canvaData?.title}</h6>
                                    </div>
                                </div>

                                <div className="mt-3">
                                    <h6 className="fw-bold">Block Content</h6>
                                    <p>
                                        {/* {listData.description} */}
                                        {/* {shortDescription} */}
                                        <div dangerouslySetInnerHTML={{ __html: shortDescription }} />
                                    </p>
                                </div>
                                <div className="mt-3">
                                    <h6 className="fw-bold">Article Link</h6>

                                    {canvaData?.articelink ? (
                                        <a href={canvaData?.articelink}>{canvaData?.articelink}</a>
                                    ) : (
                                        <input className="form-control" placeholder="Add Link" type="text" onBlur={(e) => updateLink(e.target.value, canvaData.id)} />
                                    )}

                                </div>
                                <div className="mt-3 border p-3 rounded">
                                    <h5 className="fw-bold">Overall Score</h5>
                                    <div className="row border-top pt-3 mt-3">
                                    <div className="col-lg-6">
                                <div id="gaugeDemo" class="gauge gauge-big gauge-green">
                                    <div class="gauge-arrow" data-percentage={score?.score} style={{ transform: `rotate(-50deg) translateX(50%) rotate(${score?.score}deg)`,
        }}></div>
                                </div>
                                <h6 className="text-center me-5 mt-3">Overall score is Good</h6>
                            </div>
                                        <div className="col-lg-6">
                                            <div className="gramarly d-flex gap-3 mb-3">
                                                <img src="/assets/Icons/contentQuality.svg" className="me-2" width="40px" height="40px" alt="" />
                                                <div>
                                                    <h6>Content Quality</h6>
                                                    <span className={`${score?.grammarly=='Good' ? 'good' : 'fair'}`}>{score?.grammarly}</span>
                                                </div>
                                            </div>
                                            <div className="gramarly d-flex gap-3">
                                                <img src="/assets/Icons/Duplicacy.svg" className="me-2" width="40px" height="40px" alt="" />
                                                <div>
                                                <h6>Duplicacy Checker</h6>
                                                <span className={`${score?.copyscpae=='Good' ? 'good' : 'fair'}`}>{score?.copyscpae}</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                                <div>
                                    <table class="table mt-3">
                                        <thead className="rdt_TableHeadRow ">
                                            <tr>
                                                <th scope="col"></th>
                                                <th scope="col">Keyword</th>
                                                <th scope="col">ASV</th>
                                                <th scope="col">Rank</th>
                                                <th scope="col">DA</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            {canvaData?.manager?.seo?.keyword_sets ? canvaData.manager.seo.keyword_sets.map((keywordData, key) => {
                                                return (
                                                    <tr>
                                                        <th scope="row">{(key == 1) ? 'Secondary' : 'Primary'}</th>
                                                        <td>{keywordData.keywords}</td>
                                                        <td>{keywordData.sv}</td>
                                                        <td>{keywordData.rank}</td>
                                                        <td>{keywordData.rank}</td>
                                                    </tr>
                                                )
                                            }) : ''}
                                        </tbody>
                                    </table>
                                </div>
                                <div>

                                </div>
                            </div>

                        </div>
                        <div class="col-lg-5">
                            <div id='calendar' class="text-center">

                            </div>
                        </div>
                    </div>
                </div>
            </div>
            {/* <button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#feedback_modal">
                Launch demo modal
            </button> */}

            <div class="modal fade" id="feedback_modal" tabindex="-1" aria-labelledby="feedback_modalLabel" aria-hidden="true">
                <div class="modal-dialog modal-lg modal-dialog-centered">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="feedback_modalLabel">Modal title</h5>
                            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                        </div>
                        <div class="modal-body">
                            <table class="table">
                                <thead className="rdt_TableHeadRow ">
                                    <tr>
                                        <th scope="col"></th>
                                        <th scope="col">Keyword</th>
                                        <th scope="col">ASV</th>
                                        <th scope="col">Rank</th>
                                        <th scope="col">Target Url</th>
                                        <th scope="col">Synopsis</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <th scope="row">Primary</th>
                                        <td>Mark</td>
                                        <td>Otto</td>
                                        <td>@mdo</td>
                                        <td>@mdo</td>
                                        <td>@mdo</td>
                                    </tr>
                                    <tr>
                                        <th scope="row">Secondary</th>
                                        <td>Jacob</td>
                                        <td>Thornton</td>
                                        <td>@fat</td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td colspan="2">Larry the Bird</td>
                                        <td>@twitter</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class="modal-footer text-start justify-content-start d-flex gap-4">
                            <div>
                                <span>Owner:</span><span>Juhi Sharma</span>
                            </div>
                            <div>
                                <span>Feedback:</span><span> <img src="../assets/Icons/chat.svg" data-bs-toggle="offcanvas" data-bs-target="#offcanvasExample" aria-controls="offcanvasExample" width="20px" ></img></span>
                            </div>
                        </div>

                       
                    </div>
                </div>
            </div>
            <div class="offcanvas offcanvas-end" tabindex="-1" id="feedback" aria-labelledby="feedbackLabel">
                            <div class="offcanvas-header">
                                <h2 class="offcanvas-title" id="feedbackLabel">Feedback</h2>
                                <button type="button" class="btn-close text-reset" data-bs-dismiss="offcanvas" aria-label="Close"></button>
                            </div>
                            <div class="offcanvas-body">
                              <div className="row">
                                  <div className="col-lg-12 py-3 border-bottom">
                                      <h6>Keywords</h6>
                                  </div>
                              </div>
                              <div className="row mt-3">
                                  <div className="col-lg-12 ">
                                      <div className="user_details d-flex align-items-center gap-3">
                                          <img src="../assets/Images/avatar.png"  width="40px" alt="" />
                                          <h6>NDR (Client Name)</h6>
                                      </div>
                                      <div className="feedback mt-3"><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p></div>
                                  </div>
                              </div>
                              <div className="row mt-3">
                                  <div className="col-lg-12 ">
                                      <div className="user_details d-flex align-items-center gap-3">
                                          <img src="../assets/Images/avatar.png"  width="40px" alt="" />
                                          <h6>NDR (Client Name)</h6>
                                      </div>
                                      <div className="feedback mt-3"><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p></div>
                                  </div>
                              </div>
                            </div>
                        </div>
        </>
    );
}
export default Dashboard;