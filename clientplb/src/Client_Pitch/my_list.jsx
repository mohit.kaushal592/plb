import React, { useState, useEffect } from 'react';
import ReactDOM from 'react-dom';
import { Link, NavLink, useLocation, useSearchParams } from 'react-router-dom';
import { useNavigate } from 'react-router-dom';
import '../Auth/Auth.css';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import Top from '../includes/Top';
import Header from '../includes/Header';
import axios from 'axios';
import DataTable from 'react-data-table-component';
import Loader from '../Common/Loader/Loader';
import Select from 'react-select';
import { ModalCustom } from '../Common/ModalCustom/Modal';
import { useForm} from "react-hook-form";
import OffCanvasFeedback from '../includes/OffCanvasFeedback';
 
function ManageSeo() {
    const navigate = useNavigate();
    const location = useLocation();
    const [searchParams, setSearchParams] = useSearchParams()
 

    // Use the production URL from your environment variables
  if(!process.env.NODE_ENV || process.env.NODE_ENV === 'development') { 
      var url = process.env.REACT_APP_PRODUCTION_URL;
  }else{
      var url = process.env.REACT_APP_PRODUCTION_URL;
  }

    //var selectedWishListName = location.state;
    //var selectedWishListName = searchParams.get('id');

    const [list, listData] = useState([]);
    const [pending, setPending] = useState(true);
    const [topicName, setTopicName] = useState('');
    const [topicData, setTopicData] = useState([]);
    const [selectedReject, setSelectedReject] = useState([]);
    const [accessToken, setAccessToken] = useState(localStorage.getItem('accessToken'));
    const [loading, setLoading] = useState(true)
    const [wishListName, setWishListName] = useState([])
    const [categoryName,setCategoryName] = useState([])
    const [selectedWishListName, setSelectedWishListName] = useState(searchParams.get('id')??0);
    const [selectedRows, setSelectedRows] = useState([]);
    // const [selectedStatus, setSelectedStatus] = useState({label: "Approve",
    // value: "approve"});
    const [selectedStatus, setSelectedStatus] = useState('');
    const [isFirstDropdownDisabled, setIsFirstDropdownDisabled] = useState(true);
    const [isModalOpen, setModalOpen] = useState(false);
    const [editModalOpen,setEditModalOpen] = useState(false)
    const [editData,setEditData] = useState(null)
    const [deletedId,setDeletedId]=useState('')
    const [isDeleteWebSite, setIsDeleteWebSite] = useState(false);
    const [selectedAction, setSelectedAction] = useState('');
    const [selectedItem, setSelectedItem] = useState([]);
    const [feedbackData,setFeedbackData] = useState([])
    const [isEditWebsite,setIsWebsite] = useState(false)
    const { register, control, handleSubmit, formState: { errors }, setValue } = useForm();



    const headerData = {
        headers: {
            Authorization: `Bearer ${accessToken}`
        }
    }
    console.log("headerData",headerData)
    const getClientName = async () => {
        try {
            setLoading(true);
            const userRes = await axios.get(`${url}/client/name`, headerData);
            let data = userRes?.data
            const formattedOptions = data.map(item => ({
                value: item?.id,
                label: item?.name,
            }));
            setWishListName(formattedOptions)

            setLoading(false);
            setPending(false);
        } catch (error) {
            console.error('Error fetching data:', error);
        }
    };

    const getCategoryName = async () => {
        try {
            setLoading(true);
            const userRes = await axios.get(`${url}/client/category`, headerData);
            let data = userRes?.data
            console.log("getCategoryName",data)
            setCategoryName(data)
          
        } catch (error) {
            console.error('Error fetching data:', error);
        }
    };

    useEffect(() => {
        console.log('selectedWishListName',selectedWishListName)
        getClientName();
        getCategoryName()
    }, [])


    async function getData() {
        setLoading(true);
        let apiEndpoint = `${url}/client/my-lists/`;
        if (selectedWishListName) {
            apiEndpoint += `?name=${selectedWishListName}`;
        }
        if (selectedStatus.value != undefined) {
            apiEndpoint += `&status=${selectedStatus.value}`;
        }
        //console.log('apiEndpoint_',apiEndpoint)
        const userRes = await axios.get(apiEndpoint, headerData);
        let data = userRes?.data
        //console.log('1112___',data)
        listData(data);
        //listData(data?.[0]?.items);
        setLoading(false);
        setPending(false);


    }
    console.log("listtt",list)
    useEffect(() => {
        //alert('__2__')
        getData();
    }, [selectedWishListName, selectedStatus]);

    // useEffect(()=>{
    //     if(selectedAction.value=='Bulk Approve'){
    //         console.log("this is bulk approve")
    //     }
    //     if(selectedAction.value=='Bulk Delete'){
    //         //console.log("this is bulk delete")
    //         //let selected_value = Object.assign({}, selectedReject);
    //        // console.log('selectedReject', selected_value)
    //         if (window.confirm("Are you sure you want to delete bulk items?") == true) { 
    //             axios.post(`${url}/client/bulk-archive/`,`ids=${selectedReject}`, headerData).then((response) => {
    //                 console.log('response',response)
    //                 if(response.status == 200){
    //                     toast.success(`Record delete successfully !!`)
    //                     navigate(`/list`);
    //                     getData();
    //                 }
    //             }).catch((error) => {
    //                 if (error) {
    //                     toast.error("Some tehnical issue.")
    //                 }
    //             });
    //         }
    //     }
    // },[selectedAction])

    // const handleWishlistName = (e) => {
    //       setSelectedWishListName(e);
    // };

    const handleChange = ({ selectedRows }) => {
        // console.log("selectedRows33222",selectedRows)
        const idArray = selectedRows?.map(item => item.id);
        console.log("selectedRows33222",selectedRows,idArray)
        setSelectedReject(idArray)
      
       setSelectedItem(selectedRows)
    //    if(selectedRows[0]?.id != undefined){
    //         selectedReject.push(selectedRows[0]?.id)
    //    }
    //     console.log('Selected Rows: ', selectedReject);
        //setSelectedRows(selectedReject);
    };

    const handleClickOnDelete = (delete_id) => {
        setModalOpen(true);
        setDeletedId(delete_id)
    }
    console.log("delete_id",deletedId,typeof(deletedId))

    const handleClickOnEdit = (dataForEdit)=>{
        console.log("setEditData",dataForEdit)
        setEditModalOpen(true);
        setEditData(dataForEdit)
        setValue('category',dataForEdit?.category)
        setValue('da',dataForEdit?.da)
        setValue('dr',dataForEdit?.dr)
        setValue('spam_score',dataForEdit?.spam_score)
        setValue('annual_traffic',dataForEdit?.annual_traffic)
        setValue('backlinks',dataForEdit?.backlinks)
        setValue('backlinks',dataForEdit?.backlinks)
        setValue('website',dataForEdit?.website)
      

        
    }

    const handleDeleteWebsite=(delete_id)=>{
        console.log("list",list)
        const updatedItems = list.filter(item => item.id !== delete_id);
        console.log("updatedItems",updatedItems)
        listData(updatedItems);
        // axios.post(`${url}/client/${delete_id}/archive/`, headerData).then((response) => {
        //             console.log("response of post ",response)
        //             toast.success(`Record delete successfully !!`)
        //             getData();
        //              navigate(`/list`);
        //         }).catch((error) => {
        //             if (error) {
        //                 toast.error("Some tehnical issue.")
        //             }
        //         });
        setModalOpen(false);

    }

    // const getCanvasData = (topic_name,topic_data) => {
    //     console.log('topic_data',topic_data)
    //     setTopicName(topic_name)
    //     if(topic_name == 'Keyword'){
    //         const update_topic_data = `${(topic_data[0]?.keywords != undefined)?topic_data[0]?.keywords:''} <br> ${(topic_data[1]?.keywords != undefined)?topic_data[1]?.keywords:''} <br> ${(topic_data[2]?.keywords != undefined)?topic_data[2]?.keywords:''}` 
    //         setTopicData(update_topic_data)
    //     }

    //     if(topic_name == 'Sv'){
    //         const update_topic_data = `${(topic_data[0]?.sv != undefined)?topic_data[0]?.sv:''} <br> ${(topic_data[1]?.sv != undefined)?topic_data[1]?.sv:''} <br> ${(topic_data[2]?.sv != undefined)?topic_data[2]?.sv:''}` 
    //         setTopicData(update_topic_data)
    //     }

    //     if(topic_name == 'Rank'){
    //         const update_topic_data = `${(topic_data[0]?.rank != undefined)?topic_data[0]?.rank:''} <br> ${(topic_data[1]?.rank != undefined)?topic_data[1]?.rank:''} <br> ${(topic_data[2]?.rank != undefined)?topic_data[2]?.rank:''}` 
    //         setTopicData(update_topic_data)
    //     }

    //     if(topic_name == 'Url'){list
    //         const update_topic_data = `${(topic_data[0]?.target_url != undefined)?topic_data[0]?.target_url:''} <br> ${(topic_data[1]?.target_url != undefined)?topic_data[1]?.target_url:''} <br> ${(topic_data[2]?.target_url != undefined)?topic_data[2]?.target_url:''}` 
    //         setTopicData(update_topic_data)
    //     }

    // }

    function validateFields() {
        if (
            selectedItem?.length === 0
        ) {
            return false;
        }
        return true;
    }

    function validateApproveFields() {
        if (selectedItem.length == 0) {
          return true;
        }
      
        const hasRejectedItem = selectedItem.some(item => item?.status == 'approve');
        console.log("hasRejectedItem",hasRejectedItem)
      
        return hasRejectedItem;
      }
      
    const columns = [
        // {
        //     name: 'Name',
        //     selector: row => row.DR,
        //     sortable: true
        // },
        {
            name: 'Domain (URL)',
            selector: row => row.website,
            sortable: true,
        },
        {
            name: (
                <div>
                    Category
                    {/* <img src="../assets/Images/DR.png" width="16px" height="16px" alt="" /> */}
                </div>
            ),
            selector: row => row.category,
            sortable: true
        },
        {
            name: (
                <div>
                    APS {' '}
                    <img src="../assets/Icons/Aps.svg" width="16px" height="16px" alt="" />
                </div>
            ),
            selector: row => row.ads,
            sortable: true
        },
        {
            name: (
                <div>
                    DA {' '}
                    <img src="../assets/Images/DA.png" width="16px" height="16px" alt="" />
                </div>
            ),
            selector: row => row.da,
            sortable: true
        },
        // {
        //     name: (
        //         <div>
        //             DR {' '}
        //             <img src="../assets/Images/DA.png" width="16px" height="16px" alt="" />
        //         </div>
        //     ),
        //     selector: row => row.dr,
        //     sortable: true
        // },
        // {
        //     name: 'Campaign',
        //     selector: row => row.campaign,
        //     sortable: true
        // },
        {
            name: (
                <div>
                    DR {' '}
                    <img src="../assets/Images/DR.png" width="16px" height="16px" alt="" />
                </div>
            ),
            selector: row => row.dr,
            sortable: true
        },
        {
            name: (
                <div>
                    Annual Spam Score {' '}
                    <img src="../assets/Images/Spam.png" width="16px" height="16px" alt="" />
                </div>
            ),
            selector: row => (<div className="center-aligned-spamScore">{row.spam_score}</div>),
            sortable: true
        },
        {
            name: (
                <div>
                    Traffic {' '}
                    <img src="../assets/Images/Traffic.png" width="16px" height="16px" alt="" />
                </div>
            ),
            selector: row => row.annual_traffic.toLocaleString('en-US'),
            sortable: true
        },
        {
            name: (
                <div>
                    Backlinks {' '}
                    <img src="../assets/Images/DR.png" width="16px" height="16px" alt="" />
                </div>
            ),
            selector: row => row.backlinks.toLocaleString('en-US'),
            sortable: true,
            
        },

        {
            name: 'Feedback',
            selector: row => {
                return(<div >
                       {row?.comments?.length ? <div className='d-flex gap-2 justify-content-center align-items-center'
                        data-bs-toggle="offcanvas" href="#offcanvasFeedback" role="button" aria-controls="offcanvasFeedback" onClick={() => setFeedbackData(row?.comments)}>
                        <img  src="../assets/Icons/feedback-message.svg" />
                       <h6 className='mt-1' style={{color:'#D7692A'}}>{row?.comments?.length}</h6>
                       </div>: <img  src="../assets/Icons/feedback-message-disabled.svg" />
            }
                      </div>)},
                sortable: true
        },
        {
            name: 'Approval status',
            cell: row => {console.log("row",row)
            
            return (
                <div className="status-container">
            <div className={row.status === "pending" ? 'pill_pending' : row.status === "approved" ? 'pill_active' :row.status === "rejected" ? 'pill_reject_myList':
            'pill_reject'}>{row.status.charAt(0).toUpperCase() + row.status.slice(1).toLowerCase()}
          
</div>
        {/* {row.status === "rejected" && (
            <div className="delete-icon">
             <button onClick={() => handleClickOnDelete(row.id) } > 
            <img src="../assets/Icons/DeleteOutlined.svg" width="24px" alt="" className="ml-3"/></button>
            </div>
          )} */}
          </div>
        )},
            sortable: true
        },

        // {
        //     name: 'Actions',
        //     width:'150px',
        //     selector: row => <>
        //         <button onClick={() => handleClickOnDelete(row.id) } className='delete_button'> 
        //     <img src="../assets/Icons/Trash.svg" width="20px" alt="" className="ml-3"/><span className='delete_text'>Delete</span></button>
        //     </>,
        // }

        {
            name: 'Actions',
            selector: row => <div className='d-flex gap-3'>
                {row.status === "rejected" ?<button onClick={() => handleClickOnDelete(row.id) }>
                    <img src="../assets/Icons/DeleteOutlined.svg" width="20px" alt="" className="ml-3" />
                </button>: ""}
               
                <button onClick={() => handleClickOnEdit(row) }>
                    <img src="../assets/Icons/Edit.svg" width="20px" alt="" />
                </button>
            </div>,
        }
    ];


    const selectStatus = [
        { label: 'Approved', value: 'approved' },
        { label: 'Pending', value: 'pending' },
        { label: 'Rejected', value: 'rejected' },
        { label: 'Submitted', value: 'submitted' },

    ];

    const clientApproval = async () => {
      
       console.log("this is bulk approve")
       let id=searchParams.get('id')
       let data = {
        ids: selectedReject,
    }
    try {
        setLoading(true);
        const userRes = await axios.post(`${url}/client/article_approval/`, data, headerData);
        if (userRes.status == 200) {
            toast.success(userRes?.data?.message)
            navigate(`/list?id=${id}`);
            getData();
        }
        setLoading(false);
        setPending(false);
    } catch (error) {
        toast.error(error?.userRes?.data?.message)
    }

    };

    const onSubmit = data => {
            axios.put(`${url}/client/wishlist-items/${editData.id}/`, data, headerData).then((response) => {
                if (response.status == 200) {
                    setEditModalOpen(false);
                    toast.success(`Value updated successfully.`)
                    getData()
                }
            }).catch((error) => {
                if (error) {
                    toast.error("Some tehnical issue.")
                }
            });
      
    }
   

    const deleteRecord = async () => {
         let data = {
            ids: selectedReject,
        }
         let id=searchParams.get('id')
        if (window.confirm("Are you sure you want to delete bulk items?") == true) { 
            axios.post(`${url}/client/bulk-archive/`, data, headerData).then((response) => {
                console.log('response',response)
                if(response.status == 200){
                    toast.success(`Record delete successfully !!`)
                     navigate(`/list?id=${id}`);
                     getData();
                }
            }).catch((error) => {
                if (error) {
                    toast.error("Some tehnical issue.")
                }
            });
        }
    };
    
    const actionList = [
        { label: 'Client Approval', value: 'Bulk Approve' },
        { label: 'Delete', value: 'Bulk Delete' },
    ];
    console.log("setSelectedAction",selectedAction)

    const [filterText, setFilterText] = React.useState('');
    const [resetPaginationToggle, setResetPaginationToggle] = React.useState(false);


    const filteredItems = list && list.length > 0 ? list?.filter(
        item => item.website && item.website.toLowerCase().includes(filterText.toLowerCase())
    ) : list;

    const rowDisabledCriteria = row => row.status === 'approved' || row.status === 'submitted';

    const subHeaderComponentMemo = React.useMemo(() => {
        const handleClear = () => {
            if (filterText) {
                setResetPaginationToggle(!resetPaginationToggle);
                setFilterText('');
            }
        };
        return (<>
            {/* <input className="form-control" style={{ width: "250px",height:'40px' ,marginRight:'5px'}} onChange={e => setFilterText(e.target.value)} onClear={handleClear} filterText={filterText} placeholder="Search..." /> */}
            <div className="actions d-flex">
            
            {/* <div >
                        <Select
                                    className="custom-select"
                                    placeholder="Actions"
                                    options={actionList}
                                    onChange={setSelectedAction}
                                />

                            </div> */}

                               
        </div>
        </>
        );
    }, [filterText, resetPaginationToggle]);
    const closeModal = () => {
        setModalOpen(false);
        setDeletedId('')
    };
    const closeEditModal = () => {
        setEditData(null)
        setEditModalOpen(false);
        // setDeletedId('')
    };

    return (
        <div class="container-fluid ">
            <div class="row">
                <Top selectedMyList={selectedWishListName} />
                <div class="col-lg p-0" id="right_side">
                    <Header />
                    <ToastContainer/>
                    <div class="main">
                        <div class="row mt-3">
                            <div class="col-xl-12 d-flex justify-content-between align-items-center">
                            {/* <h5 class="main_heading">My List</h5> */}
                            </div>
                        </div>
{/* 
                        <div class="d-flex py-4  justify-content-between align-items-center">
                            <h5 class="main_heading" style={{ flex: 1 }}>My List</h5>
                            <Select
                                    options={wishListName}
                                    placeholder="Select List"
                                    onChange={handleWishlistName}
                                    className="custom-select"
                                />
                            */}
                            {/* <div style={{ marginLeft: "12px" }}>
                            <Select
                                className="custom-select"
                            />
                            </div> */}
                            <div class="border-bottom my-3 d-flex flex-column gap-3 flex-sm-row justify-content-between  py-4">
                            

                            
                                <div style={{ marginRight: 12, display: "flex" }}>
                              

                           
                                <Select
                                    className="custom-select"
                                    placeholder="Approval Status"
                                    options={selectStatus}
                                    onChange={setSelectedStatus}
                                />

                            </div>
                            <div>
                                <div className="d-flex">
                                <button className="main_btn " onClick={clientApproval} disabled={validateApproveFields() }> Client Approval</button> &nbsp;
                                <button className="main_btn " onClick={deleteRecord} disabled={validateFields() ? false:true}> Delete</button>
                                </div>
                                </div>
                                </div>
                                
                        {/* </div> */}
                        {loading ? (
                            <Loader />
                        ) : (
                            <DataTable
                                columns={columns}
                                data={filteredItems}
                                pagination
                                selectableRows
                                selectableRowDisabled={rowDisabledCriteria}
                                onSelectedRowsChange={handleChange} 
                                progressPending={pending}
                                subHeader
                                persistTableHead
                                subHeaderComponent={subHeaderComponentMemo}
                            />
                        )}
                    </div>
                </div>
            </div>

            <div class="offcanvas offcanvas-end bg-white" tabindex="-1" id="offcanvasExample" aria-labelledby="offcanvasExampleLabel">
                <div class="offcanvas-header">
                    <h2 class="offcanvas-title" id="offcanvasExampleLabel">
                        {topicName}
                    </h2>
                    <button type="button" class="btn-close" data-bs-dismiss="offcanvas" aria-label="Close"></button>
                </div>

                <div class="offcanvas-body">
                    <div className="row">
                        <div className="col-lg-12">
                            <p>
                                <div dangerouslySetInnerHTML={{ __html: topicData }} />
                            </p>
                        </div>
                    </div>
                </div>

            </div>
            {isModalOpen && (
                <ModalCustom
                    show={isModalOpen}
                    handleClose={closeModal}
                    backdrop="static"
                    title="Are you sure"
                >
                    <img src="../assets/Images/Siteimg.png" width="80px" alt="" />
                    <h5 className="my-3 ">Are you sure. you want to delete the website</h5>
                    <div className="delete_modal_button_container" >
                        <button className='delete_modal_No_Buton' onClick={()=>{ setModalOpen(false) ;setDeletedId('')}}>NO</button>
                        <button className='delete_modal_YES_Buton' onClick={()=>handleDeleteWebsite(deletedId)}>YES</button>
                    </div>
                </ModalCustom >
            )
            }

<div
        class="offcanvas offcanvas-end bg-white"
        tabindex="-1"
        id="offcanvasFeedback"
        aria-labelledby="offcanvasExampleLabel"
      >
        <div class="offcanvas-header">
          <h4 class="offcanvas-title" id="offcanvasExampleLabel">
          Reason of Rejection
          </h4>
          <button
            type="button"
            class="btn-close"
            data-bs-dismiss="offcanvas"
            aria-label="Close"
          ></button>
        </div>
        <h5 className='ms-3'>Total Feedback</h5>

        <div class="offcanvas-body">
            <OffCanvasFeedback feedbackData={feedbackData}/>

         
        </div>
      </div>

         {editModalOpen && (
                <ModalCustom
                    show={editModalOpen}
                    handleClose={closeEditModal}
                    backdrop="static"
                    title="Edit Details"
                   
                >
                    <hr />
                    <form method="POST" onSubmit={handleSubmit(onSubmit)}>
                    <div className='d-flex-column gap-3'>
                        <div className='d-flex gap-2' ><h6>Website</h6> <div className='d-flex gap-2'>

                {isEditWebsite ? <div className='d-flex gap-3'><input className="form-control mb-2" id="website" defaultValue={editData?.website} {...register('website', { required: false })}/>
               </div> :<div {...register('website', { required: false })}>{editData?.website}</div>
                 }

                 {isEditWebsite?'':<img src="../assets/Icons/Edit.svg" width="20px" alt="" onClick={()=>setIsWebsite(true)}/>}
</div>
</div>
                        <div className='d-flex gap-2'>
                            {/* <h6>Category</h6>  */}
                            <h6><label htmlFor="category" className="">Category</label></h6>
                                 {/* <Select
                                    placeholder="Approval Status"
                                    options={categoryName}
                                    onChange={setSelectedStatus}
                                /> */}
                                         <select className="form-control w-50" id="category" {...register('category', { required: false })}>
                                         {categoryName?.map((item)=><option value={item?.category}  key={item}>{item?.category}</option>)}
                                        </select>
                        </div>
                    </div>
                    <hr/>
                    <div className="d-flex w-100 gap-3">
                                            <div className="mb-3 ">
                                                <label htmlFor="DA" className="form-label text-start">DA</label>
                                                <input type="text" className="form-control " id="DA"  {...register('da', { required: false })}/>
                                            </div>
                                            <div className="mb-3 ">
                                                <label htmlFor="DR" className="form-label text-start">DR</label>
                                                <input type="text" className="form-control " id="DR" {...register('dr', { required: false })}/>
                                            </div>
                                            <div className="mb-3 ">
                                                <label htmlFor="SS" className="form-label text-start">SS</label>
                                                <input type="text" className="form-control " id="SS" {...register('spam_score', { required: false })}/>
                                            </div>
                                            
                    </div>
                    <div className="d-flex w-100 gap-3 mt-2">
                                            <div className="mb-3 ">
                                                <label htmlFor="traffic" className="form-label text-start">Traffic</label>
                                                <input type="text" className="form-control " id="traffic"  {...register('annual_traffic', { required: false })}/>
                                            </div>
                                            <div className="mb-3 ">
                                                <label htmlFor="backlinks" className="form-label text-start">Backlinks</label>
                                                <input type="text" className="form-control " id="backlinks" {...register('backlinks', { required: false })}/>
                                            </div>
                                            
                                            
                    </div>
                    <div className='d-flex justify-content-end'>
                                        {/* <button type="button" className="btn_outlined" >Reset</button> */}
                                        <button type="submit"  className='main_btn ms-3'>Update</button>
                    </div>
                    </form>
                </ModalCustom >
            )
            }
        </div>
    );
}
export default ManageSeo;