import React, { useState, useEffect } from 'react';
import ReactDOM from 'react-dom';
import { Link, NavLink, useLocation} from 'react-router-dom';
import {useNavigate} from 'react-router-dom';  
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css'; 
import axios from 'axios';

function Login(){
  const navigate = useNavigate();

  if(!process.env.NODE_ENV || process.env.NODE_ENV === 'development') { 
      var url = process.env.REACT_APP_PRODUCTION_URL;
  }else{
      var url = process.env.REACT_APP_PRODUCTION_URL;
  }

  const submitData = (e)=>{ 
      e.preventDefault(); 
  
      const sendData = {
          'email' : e.target.userEmail.value,
        //   'password' : e.target.userPassword.value, 
      } 
      
      axios.post(`${url}/forgot-password/`, sendData).then((response) => { 
        console.log("forget pass response",response.data,response.status) 
        toast.success(response?.data?.message)
        if(response.status == 200){ 
            toast.success(response?.data?.message)
            setTimeout(() => {
                toast.dismiss(); 
                navigate("/create-new-password")
               }, 1000);

            //  navigate("/create-new-password")
            }
            else {
                toast.error("An error occurred.");
              }
      }).catch( (error)=> {
          if(error){
            toast.error("Some tehnical issue.")
          }
      }
      ); 
  }

  return (
    <>
    <div class="container-fluid vh-100 d-flex align-items-center justify-content-center login_card">
        <div className="cnt">
        <div className="brand_icon " style={{display:'flex',justifyContent:'center',alignItems:'center'}}>
                        <a className="text-decoration-none text-dark " href="#"><img
                            src="../assets/Images/ContentAdliftLogo.png" alt="" width="400px" />
                            </a>
                    </div>
        <ToastContainer />
        <div class="row login_box" style={{marginTop:'40px'}}> 
            <div class="col-lg-6 p-0">
                <video autoPlay loop muted playsInline width="100%">
                    <source src="../assets/Images/adlift (1).mp4" type="video/mp4" />
                </video>
            </div>
            <div class="col-lg-6 d-flex align-items-center p-0">
                <div class="login_section m-auto">
                <form className="md-float-material form-material" method="POST" onSubmit={submitData}>
                        <h3 class="text-center">Find Your Account</h3>
                        <p class="text-center">Enter the email to generate a new password</p><br></br>
                        <div class="mb-3">
                            <label for="exampleInputEmail1" class="form-label w-auto">Email address</label>
                            <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" name="userEmail"/>
                        </div>
                        <button type="submit" class="mt-4 main_btn w-100">Send Link</button>
                    </form>
                    <Link className='nav-link mt-3' style={{ color:'#D7692A',textAlign: 'right' }} to='/login'>
                        <img src="../assets/Icons/back_arrow.svg" alt="" width="" />Back to Login
                    </Link> 
                </div>
            </div>   
        </div>
        </div>
    </div>

    <div className="container-fluid d-none"  style={{ transform: 'scale(0.8)' }} >
        <ToastContainer />
        <div className="row">  
            <div className="col-lg p-0" id="right_side-old"> 
                <div className="main"> 
                    <div className="mx-auto mt-5 align-items-center justify-content-center col-md-6 p-5">
                    <form className="md-float-material form-material" method="POST" onSubmit={submitData}>
                        <div className="text-center">
                            <h2>Adlift</h2>
                        </div>
                        <div className="auth-box card">
                            <div className="card-block p-5">
                                <div className="row m-b-20">
                                    <div className="col-md-12">
                                        <h3 className="text-center">Sign In</h3>
                                    </div>
                                </div>
                                <div className="form-group form-primary">
                                    <input type="text" name="userEmail" className="form-control" required="" />
                                    {/* <span className="form-bar"></span>
                                    <label className="float-label">Your Email Address</label> */}
                                </div>
                                <div className="form-group form-primary mt-4">
                                    <input type="password" name="userPassword" className="form-control" required="" />
                                    {/* <span className="form-bar"></span>
                                    <label className="float-label">Password</label> */}
                                </div>
                                <div className="row m-t-25 text-left mt-4">
                                    <div className="col-12">
                                        <div className="checkbox-fade fade-in-primary d-">
                                            <label>
                                                <input type="checkbox" value="" />
                                                <span className="cr"><i className="cr-icon icofont icofont-ui-check txt-primary"></i></span>
                                                <span className="text-inverse"> Remember me</span>
                                            </label>
                                        </div>
                                        {/* <div className="forgot-phone text-right f-right">
                                            <a href="#" className="text-right f-w-600"> Forgot Password?</a>
                                        </div> EFC3AA */}
                                    </div>
                                </div>
                                <div className="row m-t-30 mt-4">
                                    <div className="col-md-12">
                                        <button type="submit" className="btn main_btn btn-md btn-block1 waves-effect waves-light text-center m-b-20 active">Sign in</button>
                                    </div>
                                </div> 
                            </div>
                        </div>
                    </form> 
                    </div>
                </div>
            </div>
        </div>
    </div>
    </>
  );
};

export default Login;
