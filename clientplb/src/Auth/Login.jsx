import React, { useState, useEffect } from 'react';
import ReactDOM from 'react-dom';
import { Link, NavLink, useLocation} from 'react-router-dom';
import {useNavigate} from 'react-router-dom';  
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css'; 
import axios from 'axios';
import {IconButton,OutlinedInput,InputLabel,InputAdornment,FormControl} from '@mui/material';
import {Visibility,VisibilityOff }from '@mui/icons-material';

function Login(){
  const navigate = useNavigate();

  if(!process.env.NODE_ENV || process.env.NODE_ENV === 'development') { 
      var url = process.env.REACT_APP_PRODUCTION_URL;
  }else{
      var url = process.env.REACT_APP_PRODUCTION_URL;
  }

  const [showPassword, setShowPassword] = React.useState(false);

  const handleClickShowPassword = () => setShowPassword((show) => !show);

  const handleMouseDownPassword = (event) => {
    event.preventDefault();
  };

  const submitData = (e)=>{ 
      e.preventDefault(); 
  
      const sendData = {
          'email' : e.target.userEmail.value,
          'password' : e.target.userPassword.value, 
      } 
      
      axios.post(`${url}/login/`, sendData).then((response) => { 
        console.log(response.data) 
          if(response.status == 200){ 
            localStorage.setItem('accessToken',response.data.access_token)
            localStorage.setItem('department',response.data.data.department)
            localStorage.setItem('role',response.data.data.role)
            localStorage.setItem('name',`${response.data.data.first_name} ${response.data.data.last_name != null?response.data.data.last_name:''}`)
            toast.success(`Welcome ${response.data.data.first_name} ${response.data.data.last_name}!!`)
            if (response.data.data.department == 'content_marketing' ) { 
                navigate("/clientDashboard");
            }
            else {navigate(`/`);}
          }
          else if(response.status == 401){ 
            toast.error(`Email or Password is Incorrect`)
          }
      }).catch( (error)=> {
          if(error){
            console.log("error",error)
            toast.error(error?.response?.data?.message)
          }
      }
      ); 
  }

  // const navigate = useNavigate();
  // useEffect(() => {
  //   axios.post('ec2-16-170-226-46.eu-north-1.compute.amazonaws.com/login/')
  //   .then(function (response){
  //       toast.success("Image loaded succesfully!!")
  //   }).catch(function (error){ 
  //       toast(`Error`)
  //       toast.error("Some issue to load image.")
  //   }) 
  // },[]);

  return (
    <>
    <div class="container-fluid vh-100 d-flex align-items-center justify-content-center login_card">
        <div className="cnt">
        <div className="brand_icon " style={{display:'flex',justifyContent:'center',alignItems:'center'}}>
                        <a className="text-decoration-none text-dark " href="#"><img
                            src="../assets/Images/ContentAdliftLogo.png" alt="" width="400px" />
                            </a>
                    </div>
        <ToastContainer />
        <div class="row login_box" style={{marginTop:'40px'}}> 
            <div class="col-lg-6 p-0">
                <video autoPlay loop muted playsInline width="100%">
                    <source src="../assets/Images/adlift (1).mp4" type="video/mp4" />
                </video>
            </div>
            <div class="col-lg-6 d-flex align-items-center  p-0">
                <div class="login_section m-auto">
                    <form className="md-float-material form-material" method="POST" onSubmit={submitData}>
                        <h3 class="text-center">Login to your account</h3>
                        <p class="text-center">Enter the credentials to proceed to your account</p>
                        {/* <div class="mb-3 mt-5">
                            <label for="exampleInputEmail1" class="form-label w-auto">Email address</label>
                            <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" name="userEmail"/>
                        </div> */}
                        <div class="mb-3 mt-5">
                        <FormControl sx={{ m: 1, width: '35ch' }} variant="outlined">
                        <InputLabel htmlFor="exampleInputEmail1" >Email address</InputLabel>
                        <OutlinedInput type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" name="userEmail" label="Email address" placeholder='Email address'/>
                        </FormControl>
                        </div>
                        {/* <div class="mb-3">
                            <label for="exampleInputPassword1" class="form-label w-auto">Password</label>
                            <input type="password" class="form-control" id="exampleInputPassword1" name="userPassword"/>
                        </div> */}
                        <div class="mb-3">
                        <FormControl sx={{ m: 1, width: '35ch' }} variant="outlined">
                        <InputLabel htmlFor="exampleInputPassword1" >Password</InputLabel>
                        <OutlinedInput id="exampleInputPassword1" type={showPassword ? 'text' : 'password'} name="userPassword" label="Password" placeholder='Password'
                          endAdornment={
                          <InputAdornment position="end" >
                            <IconButton aria-label="toggle password visibility" onClick={handleClickShowPassword} onMouseDown={handleMouseDownPassword} edge="end">
                               {showPassword ?  <Visibility />:<VisibilityOff />}
                            </IconButton>
                         </InputAdornment>
                          }
                          /></FormControl>
                          </div>
                        <div class="d-flex justify-content-between">
                            <div class="mb-3 form-check d-flex align-items-center gap-2 ">
                                <input type="checkbox" class="form-check-input" id="exampleCheck1" />
                                <label class="form-check-label" for="exampleCheck1">Remember me</label>
                            </div>
                            <div class="mb-3 form-check">
                                <a href="/forgot" class="text-dark text-decoration-none">Forgot Password ?</a>
                            </div>
                        </div>
                        <button type="submit" class="mt-4 main_btn w-100">Login</button>
                    </form>
                </div>
            </div>
        </div>
        </div>
    </div>

    <div className="container-fluid d-none"  style={{ transform: 'scale(0.8)' }} >
        <ToastContainer />
        <div className="row">  
            <div className="col-lg p-0" id="right_side-old"> 
                <div className="main"> 
                    <div className="mx-auto mt-5 align-items-center justify-content-center col-md-6 p-5">
                    <form className="md-float-material form-material" method="POST" onSubmit={submitData}>
                        <div className="text-center">
                            <h2>Adlift</h2>
                        </div>
                        <div className="auth-box card">
                            <div className="card-block p-5">
                                <div className="row m-b-20">
                                    <div className="col-md-12">
                                        <h3 className="text-center">Sign In</h3>
                                    </div>
                                </div>
                                <div className="form-group form-primary">
                                    <input type="text" name="userEmail" className="form-control" required="" />
                                    {/* <span className="form-bar"></span>
                                    <label className="float-label">Your Email Address</label> */}
                                </div>
                                <div className="form-group form-primary mt-4">
                                    <input type="password" name="userPassword" className="form-control" required="" />
                                    {/* <span className="form-bar"></span>
                                    <label className="float-label">Password</label> */}
                                </div>
                                <div className="row m-t-25 text-left mt-4">
                                    <div className="col-12">
                                        <div className="checkbox-fade fade-in-primary d-">
                                            <label>
                                                <input type="checkbox" value="" />
                                                <span className="cr"><i className="cr-icon icofont icofont-ui-check txt-primary"></i></span>
                                                <span className="text-inverse"> Remember me</span>
                                            </label>
                                        </div>
                                        {/* <div className="forgot-phone text-right f-right">
                                            <a href="#" className="text-right f-w-600"> Forgot Password?</a>
                                        </div> EFC3AA */}
                                    </div>
                                </div>
                                <div className="row m-t-30 mt-4">
                                    <div className="col-md-12">
                                        <button type="submit" className="btn main_btn btn-md btn-block1 waves-effect waves-light text-center m-b-20 active">Sign in</button>
                                    </div>
                                </div> 
                            </div>
                        </div>
                    </form> 
                    </div>
                </div>
            </div>
        </div>
    </div>
    </>
  );
};

export default Login;
