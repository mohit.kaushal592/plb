import React, { useState, useEffect } from 'react';
import ReactDOM from 'react-dom';
import { Link, NavLink, useLocation} from 'react-router-dom';
import {useNavigate} from 'react-router-dom';  
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css'; 
import axios from 'axios';

function Login(){
  const navigate = useNavigate();

  if(!process.env.NODE_ENV || process.env.NODE_ENV === 'development') { 
      var url = process.env.REACT_APP_PRODUCTION_URL;
  }else{
      var url = process.env.REACT_APP_PRODUCTION_URL;
  }

  const [password,setPassword]=useState('')
  const [rePassword,setRePassword]=useState('')
  const [token,setToken] = useState('')


  const submitData = (e)=>{ 
    console.log("eeeeeeeeeee",e)
      e.preventDefault(); 
  
      const sendData = {
        'token':e.target.token.value,
        'password' : e.target.newPassword.value,
        'new_password' : e.target.reNewPassword.value,
      } 

    //   const formData = new FormData();
    //   formData.append('token', e.target.token.value);
    //   formData.append('password', e.target.newPassword.value);
    //   formData.append('new_password', e.target.reNewPassword.value);
      
      axios.post(`${url}new-password/`, sendData).then((response) => { 
        console.log("forget pass response",response.data) 
        // toast.success(response?.data?.message)
        if(response.status == 200){ 
        toast.success(response?.data?.message)
         navigate("/login")
        }
        else {
            toast.error("An error occurred.");
          }
      }).catch( (error)=> {
          if(error){
            toast.error("Some tehnical issue.")
          }
      }
      ); 
  }

  return (
    <>
    <div class="container-fluid vh-100 d-flex align-items-center justify-content-center login_card">
        <div className="cnt">
        <div className="brand_icon " style={{display:'flex',justifyContent:'center',alignItems:'center'}}>
                        <a className="text-decoration-none text-dark " href="#"><img
                            src="../assets/Images/ContentAdliftLogo.png" alt="" width="400px" />
                            </a>
                    </div>
        <ToastContainer />
        <div class="row login_box" style={{marginTop:'40px'}}> 
            <div class="col-lg-6 p-0">
                <video autoPlay loop muted playsInline width="100%">
                    <source src="../assets/Images/adlift (1).mp4" type="video/mp4" />
                </video>
            </div>
            <div class="col-lg-6 d-flex align-items-center  p-0">
                <div class="login_section m-auto">
                <form className="md-float-material form-material" method="POST" onSubmit={submitData}>
                        <h3 class="text-center">Create Password</h3>
                        <br/>
                        <div class="mb-3">
                            <label for="token" class="form-label w-auto">Verify Token</label>
                            <input type="text" class="form-control" id="token" onChange={(e)=>{setToken(e.target.value)}} name="token"/>
                        </div>
                        <div class="mb-3">
                            <label for="newPassword" class="form-label w-auto">Enter new password</label>
                            <input type="password" class="form-control" id="newPassword" aria-describedby="password" onChange={(e)=>{setPassword(e.target.value)}} name="newPassword"/>
                        </div>
                        <div class="mb-3">
                            <label for="reNewPassword" class="form-label w-auto">Re-Enter new password</label>
                            <input type="password" class="form-control" id="reNewPassword" aria-describedby="password" onChange={(e)=>{setRePassword(e.target.value)}} name="reNewPassword"/>
                        </div>
                        <button type="submit" disabled={!password || !rePassword || password !== rePassword} class="mt-4 main_btn w-100">Done</button>
                    </form>
                </div>
            </div>
        </div>
        </div>
    </div>
    </>
  );
};

export default Login;
