import React, { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';
import axios from 'axios';
import 'react-toastify/dist/ReactToastify.css';
import Top from './Top';
import Header from './Header';
import DataTable from 'react-data-table-component';
import Loader from '../Common/Loader/Loader';
import { useNavigate } from 'react-router-dom';
import BootstrapFeedback from 'react-bootstrap/esm/Feedback';

function Feedbacks() {
    console.log("Feedbacks component is rendering");
    const navigate = useNavigate();
    const url = process.env.REACT_APP_PRODUCTION_URL;

    const [list, listData] = useState([]);
    const [pending, setPending] = useState(true); 
    const [accessToken, setAccessToken] = useState(localStorage.getItem('accessToken'));
    const [loading, setLoading] = useState(true);
    const [conversationData, setConversationData] = useState({});

    const headerData = {
        headers: {
            Authorization: `Bearer ${accessToken}`
        }
    }

    useEffect(() => {
        axios.get(`${url}/feedback/seo-conversation/?seo_id=4`, headerData).then((response) => {
            console.log("Fetched conversation data:", response.data);
            setConversationData(response.data);
        }).catch((error) => {
            console.error("Error fetching conversation data:", error);
        });
    }, []);
    

    function Conversation({ data }) {
        console.log("Data:", data); // Log the received data

        if (!data.messages || !data.replies) {
            console.log("No messages or replies found.");
            return <div>Coming Soon...</div>;
        }
    
        console.log("Rendering conversation data...");
    
    
            return (
                <div className="conversation-container">
                    <h2>{data.subject}</h2>
                    <p>{data.message}</p>
        
                    <div className="messages">
                        {data.messages.map((message) => {
                            const isClientMessage = !data.team.includes(message.user);
                            return (
                                <div className={isClientMessage ? 'client-message' : 'team-message'} key={message.id}>
                                    {message.message_text}
                                </div>
                            );
                        })}
        
                        {data.replies.map((reply) => (
                            <div className="team-message" key={reply.id}>
                                {reply.reply_text}
                            </div>
                        ))}
                    </div>
                </div>
            );
        }

    


    return (
        <div className="row">
            <Top />
            <div className="col-lg p-0" id="right_side">
                <Header />
                <div className="main">
                    <div className="row mt-3">
                    <div className="col-xl-12 d-flex justify-content-between align-items-center">
                            <div className="breadcrumbs">
                                <nav aria-label="breadcrumb">
                                    <ol className="breadcrumb">
                                        <li className="breadcrumb-item"><Link to="/dashboard">Dashboard</Link></li>
                                        <li className="breadcrumb-item active" aria-current="page">feedback</li>
                                    </ol>
                                </nav>
                            </div>
                            <Link to="/addSeo" className="btn_outlined">Back</Link>
                        </div>
                    </div>
                    <div className="mt-3  p-2">
                        <h5 className="main_heading">feedback</h5>
                    </div>
                    <Conversation data={conversationData} />
                </div>
            </div>
        </div>
    );
}

const styles = `
    .conversation-container {
      padding: 20px;
      background-color: #f9f9f9;
      border-radius: 5px;
    }

    .client-message {
      align-self: flex-start;
      background-color: #f1f1f1;
      border-radius: 8px;
      padding: 10px;
      margin: 5px 5px 5px 0;
      max-width: 70%;
      word-wrap: break-word;
    }

    .team-message {
      align-self: flex-end;
      background-color: #007AFF;
      color: white;
      border-radius: 8px;
      padding: 10px;
      margin: 5px 0 5px 5px;
      max-width: 70%;
      word-wrap: break-word;
    }

    .messages {
      display: flex;
      flex-direction: column;
    }
`;

export default Feedbacks;
