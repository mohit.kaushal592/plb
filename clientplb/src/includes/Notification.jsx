import React, { useState, useEffect } from 'react';
import ReactDOM from 'react-dom';
import { Link, NavLink, useLocation } from 'react-router-dom';
import { useNavigate } from 'react-router-dom';
import '../Auth/Auth.css';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import Top from '../includes/Top';
import Header from '../includes/Header';
import axios from 'axios';
import DataTable from 'react-data-table-component';
import Loader from '../Common/Loader/Loader';
import {Button,ButtonGroup} from "@mui/material";

function Notification() {
    const navigate = useNavigate();

    var url;

    if (!process.env.NODE_ENV || process.env.NODE_ENV === 'development') {
        // Use the development URL or a default value if it's not defined
        url = process.env.REACT_APP_PRODUCTION_URL;
    } else {
        // Use the production URL from your environment variables
        url = process.env.REACT_APP_PRODUCTION_URL;
    }

    const [list, listData] = useState([]);
    const [pending, setPending] = useState(true); 
    const [accessToken, setAccessToken] = useState(localStorage.getItem('accessToken'));
    const [loading, setLoading] = useState(true);
    const [selectTeam,setSelectTeam] = useState('all')
    const [selectedDay,setSelectedDay] = useState('')


    const headerData = {
        headers: {
            Authorization: `Bearer ${accessToken}`
        }
    }
    async function getData(notification_status='') {
        setLoading(true); 
        axios.get(`${url}/notify/notifications/`, headerData).then((response) => { 
            if (response.status == 200) { 
                setLoading(false);
                if(notification_status == 'previous'){
                    listData(response.data.previous);  
                }else if(notification_status == 'today'){
                    listData(response.data.today); 
                }else{   
                    let today_data = response.data.today
                    let previous_data = response.data.previous 
                    var combine_notification = [...today_data, ...previous_data];
                     listData(combine_notification);  
                    //  listData(today_data); 
                } 
                setTimeout(function(){
                    document.getElementById("staus_filter").value = notification_status;
                }, 1000);
                setPending(false);
            }
        }).catch((error) => {
            if (error) {
                toast.error("Some tehnical issue.")
            }
        });

        // const userRes = await axios.get(`${url}/seo/?site_status=${site_staus}`, headerData);
        // console.log('userRes',userRes)
        // setLoading(false);
        // listData(userRes.data); 
        // setPending(false);
    }
    useEffect(() => {
        getData();
    }, []);
    console.log('list',list)
    const columns = [
        {
            name: 'S No.',
            selector: (row, index) => (index + 1).toString(),
            sortable: true,
             width: '150px' 
        },
        {
            name: 'Title',
            selector: row => row.title,
            sortable: true,
            // width: '450px' 
        },
        {
            name: 'Message',
            selector: row => row.message, 
            sortable: true
        },
        {
            name: 'Created On',
            selector: row => {
                const createdAt = new Date(row.created_at);
                console.log("createdAt",createdAt)
                const formattedDate = createdAt.toLocaleString('en-US', {
                    day: '2-digit',
                    month: 'short',
                    year: 'numeric',
                    hour: 'numeric',
                    minute: '2-digit',
                    hour12: true, 
                  });
                return <>{formattedDate}</>}, 
            sortable: true
        }
    ];

    const handleChange = ({ selectedRows }) => {
        console.log('Selected Rows: ', selectedRows);
    };

    const [filterText, setFilterText] = React.useState('');
    const [resetPaginationToggle, setResetPaginationToggle] = React.useState(false);

    const filteredItems = list.filter(
        item => item.title && item.title.toLowerCase().includes(filterText.toLowerCase())
    );

    const subHeaderComponentMemo = React.useMemo(() => {
        const btnStyle={color:'#D7692A'}
        const handleClear = () => {
            if (filterText) {
                setResetPaginationToggle(!resetPaginationToggle);
                setFilterText('');
            }
        }; 
        return (
            <>
                {/* <select className="form-control" style={{ width: "250px",marginRight:"20px" }} value={selectedDay} onChange={e => {setSelectedDay(e.target.value);getData(e.target.value)}} id="staus_filter">    
                    <option value=''>All</option>   
                    <option value='today'>Today</option> 
                    <option value='previous'>Previous</option>  
                </select> */}
                <ButtonGroup variant="text" aria-label="text button group">
                <Button style={{...btnStyle,backgroundColor:`${selectTeam=='all' ? '#fcdac6': ''}`}} onClick={()=>{setSelectTeam("all");getData('all')}}>All</Button>
               <Button style={{...btnStyle,backgroundColor:`${selectTeam=='today' ? '#fcdac6': ''}`}} onClick={()=>{setSelectTeam("today");getData('today')}}>Today</Button>
               <Button style={{...btnStyle,backgroundColor:`${selectTeam=='previous' ? '#fcdac6': ''}`}} onClick={()=>{setSelectTeam("previous");getData('previous')}}>Previous</Button>
               </ButtonGroup>
                {/* <input className="form-control" style={{ width: "250px" }} onChange={e => setFilterText(e.target.value)} onClear={handleClear} filterText={filterText} placeholder="Search..." /> */}
            </>
        );
    }, [filterText, resetPaginationToggle,selectedDay,selectTeam]);
 
    return (
        <div class="container-fluid ">
            <div class="row">
                <Top logs={true} logsTodo={true}/>
                <div class="col-lg p-0" id="right_side">
                    <Header />
                    <div class="main">
                        {/* <div class="row mt-3">
                            <div class="col-xl-12 d-flex justify-content-between align-items-center">
                                <div class="breadcrumbs">
                                    <nav aria-label="breadcrumb">
                                        <ol class="breadcrumb">
                                            <li class="breadcrumb-item"><Link to="/dashboard">Dashboard</Link></li>
                                            <li class="breadcrumb-item active" aria-current="page">To-Do List</li>
                                        </ol>
                                    </nav>
                                </div>
                                <Link to="/addSeo" class="btn_outlined">Back</Link>
                            </div>
                        </div> */}
                        <div class="mt-3  p-2">
                            <h5 class="main_heading">To-Do List</h5>
                        </div>

                        {loading ? (
                            <Loader />
                        ) : (
                            <DataTable
                                columns={columns}
                                data={filteredItems}
                                pagination
                                // selectableRows
                                onSelectedRowsChange={handleChange}
                                progressPending={pending}
                                subHeader
                                persistTableHead
                                subHeaderComponent={subHeaderComponentMemo}
                            />
                        )}
                    </div>
                </div>
            </div>
        </div>
    );
}
export default Notification;