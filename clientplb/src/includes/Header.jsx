import React, { useState, useEffect, useRef } from 'react';
import { Link, NavLink, useLocation } from 'react-router-dom';
import axios from 'axios';
import '../Auth/Auth.css';
import { toast } from 'react-toastify';
import { useNavigate } from 'react-router-dom';
import Loader from '../Common/Loader/Loader';
import CustomDropdown from '../Common/CustomeDropdown/CustomDropdown';
import Top from './Top';

function Header() {
    const [role, setRole] = useState(localStorage.getItem("role").toLowerCase());
    const [department, setDepartment] = useState(localStorage.getItem("department").toLowerCase());
    const [name, setName] = useState(localStorage.getItem("name").toLowerCase());
    const [currentDate, setCurrentDate] = useState(new Date().toJSON().slice(0, 10).replace(/-/g, ', '));
    const [pending, setPending] = useState(true);
    const [list, listData] = useState([]);
    const [selectedProject, setSelectedProject] = useState(null);
    const [accessToken, setAccessToken] = useState(localStorage.getItem('accessToken'));
    const [isOpen, setIsOpen] = useState(false);
    const [userId, setUserId] = useState('')
    const [loading, setLoading] = useState(true);
    const [notification, setNotification] = useState([]);
    const [isSideOpen,setIsSideOpen] = useState(false)
    const [projectScope,setProjectScope] = useState('')
    const [projecName,setProjectName] = useState('')

    const dropdownRef = useRef(null);

    var url;

    if (!process.env.NODE_ENV || process.env.NODE_ENV === 'development') {
        // Use the development URL or a default value if it's not defined
        url = process.env.REACT_APP_PRODUCTION_URL;
    } else {
        // Use the production URL from your environment variables
        url = process.env.REACT_APP_PRODUCTION_URL;
    }

    const headerData = {
        headers: {
            Authorization: `Bearer ${accessToken}`
        }
    }

    const navigate = useNavigate();

    useEffect(() => {
        async function getData() {
            const userRes = await axios.get(`${url}/seo/project/?project=`, headerData);
            //console.log('1__',userRes.data)
            listData(userRes.data);
            setPending(false);
        }
        getData();
    }, []);

    const getToken = () => {
        setLoading(true)
        axios.get(`${url}/users/?token=${accessToken}`, headerData)
            .then(async (response) => {
                setLoading(false)
                let data = await response?.data?.[0]?.id;
                console.log('token__',response.data)
                setProjectScope(response?.data?.[0]?.project_name?.[0]?.scope)
                setProjectName(response?.data?.[0]?.project_name?.[0]?.project_name)
                setUserId(data)
                setPending(false);
            })
    }
    useEffect(() => {
        getToken();


    }, [])

    const getPayment = async () => {
        if (selectedProject) {
            try {
                setLoading(true);
                let data = { project_name: selectedProject }
                const userRes = await axios.put(`${url}/user_edit/${userId}/`, data, headerData);
                console.log(userRes)
                setLoading(false);
                setPending(false);
            } catch (error) {
                console.error('Error fetching data:', error);
            }
        }
    };

    useEffect(() => {
        getPayment()
    }, [selectedProject]);
    
    useEffect(() => {

        axios.get(`${url}/notify/notifications/`, headerData).then((response) => { 
            if (response.status == 200) { 
                setNotification(response.data) 
            }
        }).catch((error) => {
            if (error) {
                toast.error("Some tehnical issue.")
            }
        });

    }, []);

    const handleDropdownToggle = () => {
        setIsOpen(!isOpen);
    };
  

    const handleOutsideClick = (event) => {
        if (dropdownRef.current && !dropdownRef.current.contains(event.target)) {
            setIsOpen(false);
        }
    };

    useEffect(() => {
        document.addEventListener('click', handleOutsideClick);
        return () => {
            document.removeEventListener('click', handleOutsideClick);
        };
    }, []);

    console.log('notification?.today',notification?.today)
    const [isSidebarOpen, setIsSidebarOpen] = useState(false);

    const toggleSidebar = () => {
      setIsSidebarOpen(!isSidebarOpen);
    };

    return (
        <header class="top_nav d-flex p-4 bg-white shadow-sm  gap-3 align-items-center">
            <img src="../assets/Icons/menu.svg" className="mobile_icon" width="40px" alt="" onClick={toggleSidebar}/>
            {isSidebarOpen? <div style={{height:'100%'}}><Top  isSidebarOpen={isSidebarOpen} toggleSidebar={toggleSidebar}/></div>:""}
            <div class="avatar pe-3 d-flex gap-2 align-items-center">
                <div class="user_img me-2  position-relative" >
                    <img src="../assets/Images/avatar.png" width="40px" alt="" onClick={handleDropdownToggle}
                    />
                    <div class="active"></div>
                   
                </div>
                <div class="user_details">
                    <h6 class="name capital">{name}</h6>
                    <h6 className="position capital">
                        {
                            role === 'team' ? 'Team' :
                                role === 'lead' ? 'Lead' :
                                    role} / {department === 'content_writing' ? 'Content Writing' :
                                        department === 'seo' ? 'SEO' :
                                            department === 'client_service' ? 'Client Service' :
                                                department === 'content_marketing' ? 'Content Marketing' :
                                                department}
                    </h6>
                </div>
            </div>
        </header>
    );
}

export default Header;