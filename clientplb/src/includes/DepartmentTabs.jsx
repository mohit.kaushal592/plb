import React, { useState, useEffect, useRef } from 'react';
import { Link, NavLink, useLocation } from 'react-router-dom';
import axios from 'axios';
import '../Auth/Auth.css';
import { toast } from 'react-toastify';
import { useNavigate } from 'react-router-dom';
import { Tabs, Tab, Box } from "@mui/material";
import ArrowDropUpIcon from '@mui/icons-material/ArrowDropUp';

function DepartMentTabs() {
    const [role, setRole] = useState(localStorage.getItem("role").toLowerCase());
    const [department, setDepartment] = useState(localStorage.getItem("department").toLowerCase());
    const [accessToken, setAccessToken] = useState(localStorage.getItem('accessToken'));
    const [selectedActivity, setSelectedActivity] = useState("client_service");
   

    var url;

    if (!process.env.NODE_ENV || process.env.NODE_ENV === 'development') {
        // Use the development URL or a default value if it's not defined
        url = process.env.REACT_APP_PRODUCTION_URL;
    } else {
        // Use the production URL from your environment variables
        url = process.env.REACT_APP_PRODUCTION_URL;
    }

    const headerData = {
        headers: {
            Authorization: `Bearer ${accessToken}`
        }
    }

    const navigate = useNavigate();

    // useEffect(() => {
    //     async function getData() {
    //         const userRes = await axios.get(`${url}/seo/project/?project=`, headerData);
    //         listData(userRes.data);
    //         setPending(false);
    //     }
    //     getData();
    // }, []);

    const TabsOptions = [
        { value: "client_service", label: "Client Service" },
        { value: "content_marketing", label: "Content Marketing" },
        { value: "content_writing", label: "Content Writing" },
        { value: "seo", label: "Seo "}  
      ];

    const handleTabChange = (event, newValue) => {
        console.log("newValuenewValue",newValue)
        setSelectedActivity(newValue);
        switch (newValue) {
            case 'client_service': {}
              break;
            case 'content_marketing': {}
              break;
            case 'content_writing': {}
              break;
            case 'seo': {}
              break;
            default:
              break;
          }
      };

    const CustomIndicator = () => (
        <Box
          display="flex"
          justifyContent="center"
          alignItems="center"
          height="100%"
          position="relative"
          top="-9px" 
          style={{ color: '#D7692A' }}
        >
          <ArrowDropUpIcon fontSize="large" />
        </Box>
      );

    return (
        <div class="d-flex p-2 bg-white gap-1 align-items-center">
             <Box sx={{ width: "100%" }}>
          <Tabs
            value={selectedActivity}
            onChange={handleTabChange}
            textColor="secondary"
            indicatorColor="transparent" 
            TabIndicatorProps={{ children: <CustomIndicator /> }}
          >
            {TabsOptions?.map((tabs) => (
              <Tab key={tabs.value} value={tabs.value} label={tabs.label}/>
            ))}
          </Tabs>
        </Box>
            
        </div>
    );
}

export default DepartMentTabs;