import React, { useState, useEffect } from 'react';
import '../Auth/Auth.css';
import { toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import Top from '../includes/Top';
import Header from '../includes/Header';
import axios from 'axios';
import DataTable from 'react-data-table-component';
import Loader from '../Common/Loader/Loader';

function FeedbackLogs() {

    var url;

    if (!process.env.NODE_ENV || process.env.NODE_ENV === 'development') {
        // Use the development URL or a default value if it's not defined
        url = process.env.REACT_APP_PRODUCTION_URL;
    } else {
        // Use the production URL from your environment variables
        url = process.env.REACT_APP_PRODUCTION_URL;
    }

    const [list, listData] = useState([]);
    const [pending, setPending] = useState(true); 
    const [accessToken, setAccessToken] = useState(localStorage.getItem('accessToken'));
    const [loading, setLoading] = useState(true);


    const headerData = {
        headers: {
            Authorization: `Bearer ${accessToken}`
        }
    }
    async function getData() {
        setLoading(true); 
        axios.get(`${url}/feedback/comment/`, headerData).then((response) => { 
            if (response.status == 200) { 
                setLoading(false);
                listData(response?.data)
                setPending(false);
            }
        }).catch((error) => {
            if (error) {
                toast.error("Some tehnical issue.")
            }
        });
    }
    useEffect(() => {
        getData();
    }, []);
 
    const columns = [
        {
            name: 'S No.',
            selector: (row, index) => (index + 1).toString(),
            sortable: true,
             width: '200px' 
        },
        {
            name: 'Message',
            selector: row => row.message, 
            sortable: true
        },
        {
            name: 'Created On',
            selector: row => {
                const createdAt = new Date(row.created_at);
                console.log("createdAt",createdAt)
                const formattedDate = createdAt.toLocaleString('en-US', {
                    day: '2-digit',
                    month: 'short',
                    year: 'numeric',
                    hour: 'numeric',
                    minute: '2-digit',
                    hour12: true, 
                  });
                return <>{formattedDate}</>}, 
            sortable: true
        }
    ];

    const handleChange = ({ selectedRows }) => {
        console.log('Selected Rows: ', selectedRows);
    };
    console.log("list",list)

    const [filterText, setFilterText] = React.useState('');
    const [resetPaginationToggle, setResetPaginationToggle] = React.useState(false);

    const filteredItems = list.filter(
        item => item?.message && item?.message.toLowerCase().includes(filterText.toLowerCase())
    );

    const subHeaderComponentMemo = React.useMemo(() => {
        const btnStyle={color:'#D7692A'}
        const handleClear = () => {
            if (filterText) {
                setResetPaginationToggle(!resetPaginationToggle);
                setFilterText('');
            }
        }; 
        return (
            <>
            <input className="form-control" style={{ width: "250px" }} onChange={e => setFilterText(e.target.value)} onClear={handleClear} filterText={filterText} placeholder="Search..." />
            </>
        );
    }, [filterText, resetPaginationToggle]);
 
    return (
        <div class="container-fluid ">
            <div class="row">
                <Top logs={true} logsFeedback={true} />
                <div class="col-lg p-0" id="right_side">
                    <Header />
                    <div class="main">
                        <div class="mt-3  p-2">
                            <h5 class="main_heading">Feedback</h5>
                        </div>

                        {loading ? (
                            <Loader />
                        ) : (
                            <DataTable
                                columns={columns}
                                data={filteredItems}
                                pagination
                                // selectableRows
                                onSelectedRowsChange={handleChange}
                                progressPending={pending}
                                subHeader
                                persistTableHead
                                subHeaderComponent={subHeaderComponentMemo}
                            />
                        )}
                    </div>
                </div>
            </div>
        </div>
    );
}
export default FeedbackLogs;