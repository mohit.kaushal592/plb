import * as React from 'react';
import { AdapterDayjs } from '@mui/x-date-pickers/AdapterDayjs';
import { LocalizationProvider } from '@mui/x-date-pickers/LocalizationProvider';
import { DatePicker } from '@mui/x-date-pickers/DatePicker';
import TextField from '@mui/material/TextField';


export default function DatePickerViews({selectedDate,handleDateChange}) {

  return (
    <LocalizationProvider dateAdapter={AdapterDayjs} >
      <div style={{ width: '180px', height: '100%' }}>
      <DatePicker
        label={'Month'}
          openTo="month"
          views={['year', 'month']}
          value={selectedDate}
          onChange={handleDateChange}
      />
      </div>
    </LocalizationProvider>
  );
}
