import * as React from 'react';
import Timeline from '@mui/lab/Timeline';
import TimelineItem from '@mui/lab/TimelineItem';
import TimelineSeparator from '@mui/lab/TimelineSeparator';
import TimelineConnector from '@mui/lab/TimelineConnector';
import TimelineContent from '@mui/lab/TimelineContent';
import TimelineDot from '@mui/lab/TimelineDot';
import Typography from '@mui/material/Typography';


export default function CustomizedTimeline({timeLinekData}) {
  console.log("timeLinekData",timeLinekData?.data)
  return (<div style={{display:'flex',alignItems:'flex-start'}}>
    <Timeline>
      <TimelineItem>
        <TimelineSeparator>
          <TimelineDot sx={{ backgroundColor:timeLinekData?.seo_approved_status ? '#EEFFEB':
          (!timeLinekData?.seo_approved_status && !timeLinekData?.sites_approved_status) ?'#F6EBD0': 'white'}}>
          {timeLinekData?.seo_approved_status ? <img src="../assets/Icons/keyword.svg"  alt="" /> :
          (!timeLinekData?.seo_approved_status && !timeLinekData?.sites_approved_status) ?
          <img src="../assets/Icons/keyword_progress.svg"  alt="" /> 
          :<img src="../assets/Icons/keyword_grey.svg"  alt="" /> 
          }
          </TimelineDot>
          <TimelineConnector sx={{ backgroundColor: timeLinekData?.seo_approved_status ?'#2CBB15':''}} />
        </TimelineSeparator>
        <TimelineContent sx={{ py: '15px' }}>
          <Typography  variant="p" component="span" color={timeLinekData?.seo_approved_status ? '#1F2734':'#ACACAC'}>
          Keywords
          </Typography>
        </TimelineContent>
      </TimelineItem>

      <TimelineItem>
        <TimelineSeparator>
          <TimelineDot sx={{ backgroundColor:timeLinekData?.sites_approved_status ? '#EEFFEB': (timeLinekData?.seo_approved_status && !timeLinekData?.title_approved_status) ?'#F6EBD0': 'white'}}>
          {timeLinekData?.sites_approved_status ? <img src="../assets/Icons/sites.svg"  alt="" />
          :
          (timeLinekData?.seo_approved_status && !timeLinekData?.title_approved_status) ?
          <img src="../assets/Icons/site_progress.svg"  alt="" /> 
          :
          <img src="../assets/Icons/site_grey.svg"  alt="" />}
            </TimelineDot>
          <TimelineConnector sx={{ backgroundColor: timeLinekData?.sites_approved_status ?'#2CBB15':''}} />
        </TimelineSeparator>
        <TimelineContent sx={{ py: '15px' }}><Typography variant="p" component="span" color={timeLinekData?.sites_approved_status ? '#1F2734':'#ACACAC'}>
        Site List
          </Typography></TimelineContent>
      </TimelineItem>

      <TimelineItem>
        <TimelineSeparator>
          <TimelineDot sx={{ backgroundColor:timeLinekData?.title_approved_status ? '#EEFFEB':(timeLinekData?.sites_approved_status && !timeLinekData?.article_approved_status) ?'#F6EBD0': 'white'}}>
          {timeLinekData?.title_approved_status ?<img src="../assets/Icons/title.svg"  alt="" />
          :
          (timeLinekData?.sites_approved_status && !timeLinekData?.article_approved_status) ?
          <img src="../assets/Icons/title_progress.svg"  alt="" /> 
          
          :
          <img src="../assets/Icons/title_grey.svg"  alt="" />}
            </TimelineDot>
          <TimelineConnector sx={{ backgroundColor: timeLinekData?.title_approved_status ?'#2CBB15':''}} />
        </TimelineSeparator>
        <TimelineContent sx={{ py: '15px' }}><Typography variant="p" component="span" color={timeLinekData?.title_approved_status ? '#1F2734':'#ACACAC'}>
          Title
          </Typography></TimelineContent>
      </TimelineItem>

      <TimelineItem>
        <TimelineSeparator>
          <TimelineDot sx={{backgroundColor:timeLinekData?.article_approved_status ? '#EEFFEB':(timeLinekData?.title_approved_status && !timeLinekData?.publish_approved_status) ?'#F6EBD0': 'white'}}>
          {timeLinekData?.article_approved_status ?<img src="../assets/Icons/article.svg"  alt="" />
          :
          (timeLinekData?.title_approved_status && !timeLinekData?.publish_approved_status) ?
          <img src="../assets/Icons/article_progress.svg"  alt="" /> 
          
          :
          <img src="../assets/Icons/article_grey.svg"  alt="" />}
            </TimelineDot>
          <TimelineConnector sx={{ backgroundColor: timeLinekData?.article_approved_status ?'#2CBB15':''}} />
        </TimelineSeparator>
        <TimelineContent sx={{ py: '15px' }}><Typography variant="p" component="span" color={timeLinekData?.article_approved_status ? '#1F2734':'#ACACAC'}>
          Article
          </Typography></TimelineContent>
      </TimelineItem>

      <TimelineItem>
        <TimelineSeparator>
          <TimelineDot sx={{ backgroundColor:timeLinekData?.publish_approved_status ? '#EEFFEB':(timeLinekData?.article_approved_status && !timeLinekData?.live_approved_status) ?'#F6EBD0': 'white'}}>
          {timeLinekData?.publish_approved_status ?<img src="../assets/Icons/Publish_green.svg"  alt="" />
          :
          (timeLinekData?.article_approved_status && !timeLinekData?.live_approved_status) ?
          <img src="../assets/Icons/publish_progress.svg"  alt="" /> 
          
          :<img src="../assets/Icons/publish.svg"  alt="" />}
            </TimelineDot>
          <TimelineConnector sx={{ backgroundColor: timeLinekData?.publish_approved_status ?'#2CBB15':''}} />
        </TimelineSeparator>
        <TimelineContent sx={{ py: '15px' }}><Typography variant="p" component="span" color={timeLinekData?.publish_approved_status ? '#1F2734':'#ACACAC'}>
          Publish
          </Typography></TimelineContent>
      </TimelineItem>
      
      <TimelineItem>
        <TimelineSeparator>
          <TimelineDot sx={{ backgroundColor:timeLinekData?.live_approved_status ? '#EEFFEB':(timeLinekData?.publish_approved_status && !timeLinekData?.live_approved_status) ?'#F6EBD0': 'white'}}>
          {timeLinekData?.live_approved_status ? <img src="../assets/Icons/live_green.svg"  alt="" />
          :
          (timeLinekData?.publish_approved_status && !timeLinekData?.live_approved_status) ?
          <img src="../assets/Icons/live_progress.svg"  alt="" /> 
          
          :
          <img src="../assets/Icons/live_grey.svg"  alt="" />}
            </TimelineDot>  
        </TimelineSeparator>
        <TimelineContent sx={{ py: '15px' }}><Typography variant="p" component="span" color={timeLinekData?.live_approved_status ? '#1F2734':'#ACACAC'}>
          Live
          </Typography></TimelineContent>
      </TimelineItem>
    </Timeline>

  </div>);
}
