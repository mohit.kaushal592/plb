import React, { useState, useEffect } from 'react';
import ReactDOM from 'react-dom';
import { Link, NavLink, useLocation, useSearchParams } from 'react-router-dom';
import { useNavigate } from 'react-router-dom';
import '../Auth/Auth.css';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import Top from './Top';
import Header from './Header';
import axios from 'axios';
import DataTable from 'react-data-table-component';
import Loader from '../Common/Loader/Loader';

function CalendarEvent() {
    const navigate = useNavigate();
    const [searchParams, setSearchParams] = useSearchParams() 

    var url;

    if (!process.env.NODE_ENV || process.env.NODE_ENV === 'development') {
        // Use the development URL or a default value if it's not defined
        url = process.env.REACT_APP_PRODUCTION_URL;
    } else {
        // Use the production URL from your environment variables
        url = process.env.REACT_APP_PRODUCTION_URL;
    }

    const [list, listData] = useState([]);
    const [pending, setPending] = useState(true); 
    const [accessToken, setAccessToken] = useState(localStorage.getItem('accessToken'));
    const [loading, setLoading] = useState(true);
    const [calendarData, setCalendarData] = useState([]);
    const [canvaData, setCanvaData] = useState([]);
    const [topicName, setTopicName] = useState('');
    const [topicData, setTopicData] = useState([]);

    const headerData = {
        headers: {
            Authorization: `Bearer ${accessToken}`
        }
    }

    const getCanvasData = (topic_name,topic_data) => {
        console.log('topic_data',topic_data)
        setCanvaData(topic_data)
        setTopicName(topic_name) 
        console.log('update_topic_data',topic_data.keyword_sets[0].keywords)
        if(topic_name == 'Keyword'){
            const update_topic_data = `${(topic_data.keyword_sets[0]?.keywords != undefined)?topic_data.keyword_sets[0]?.keywords:''} <br /> ${(topic_data.keyword_sets[1]?.keywords != undefined)?topic_data.keyword_sets[1]?.keywords:''} <br /> ${(topic_data.keyword_sets[2]?.keywords != undefined)?topic_data.keyword_sets[2]?.keywords:''}`
            console.log('update_topic_data',update_topic_data)
            setTopicData(update_topic_data)
        }

        if(topic_name == 'Sv'){
            const update_topic_data = `${(topic_data.keyword_sets[0]?.sv != undefined)?topic_data.keyword_sets[0]?.sv:''} <br> ${(topic_data.keyword_sets[1]?.sv != undefined)?topic_data.keyword_sets[1]?.sv:''} <br> ${(topic_data.keyword_sets[2]?.sv != undefined)?topic_data.keyword_sets[2]?.sv:''}` 
            setTopicData(update_topic_data)
        }

        if(topic_name == 'Rank'){
            const update_topic_data = `${(topic_data.keyword_sets[0]?.rank != undefined)?topic_data.keyword_sets[0]?.rank:''} <br> ${(topic_data.keyword_sets[1]?.rank != undefined)?topic_data.keyword_sets[1]?.rank:''} <br> ${(topic_data.keyword_sets[2]?.rank != undefined)?topic_data.keyword_sets[2]?.rank:''}` 
            setTopicData(update_topic_data)
        }

        if(topic_name == 'Url'){
            const update_topic_data = `${(topic_data.keyword_sets[0]?.target_url != undefined)?topic_data.keyword_sets[0]?.target_url:''} <br> ${(topic_data.keyword_sets[1]?.target_url != undefined)?topic_data.keyword_sets[1]?.target_url:''} <br> ${(topic_data.keyword_sets[2]?.target_url != undefined)?topic_data.keyword_sets[2]?.target_url:''}` 
            setTopicData(update_topic_data)
        }
        
    }

    var from_event_date =  searchParams.get('from_event') 

    async function getData(from_event_date,notification_status='all') { console.log('1__')
        setLoading(true); 
        axios.get(`${url}/content_writing/calendar/?target_date=${from_event_date}`, headerData).then(function (response) {  
            let calendar_data = response.data; 
            console.log('calendar_data',calendar_data)
            var a_calendar = [];
            let i = 0;
            for(let key in calendar_data){   
                i++; 
                let a_content_data = calendar_data[key].content_data
                let j = 0; 
                
                if(notification_status == 'content' || notification_status == 'all'){
                    for(let content_key in a_content_data){  
                        j++    
                        let from_date = a_content_data[content_key].created_at;
                        let to_date = a_content_data[content_key].created_at;
                        let title = a_content_data[content_key].title ??'';
                        let description = a_content_data[content_key].description ??'';
                        let keyword_sets = a_content_data[content_key].keyword_sets??[];
                        let keywords = a_content_data[content_key].keywords ??'';
                        let sv = a_content_data[content_key].sv ??'';
                        let rank = a_content_data[content_key].rank ??'';
                        let target_url = a_content_data[content_key].target_url ??'';

                        a_calendar.push(
                            {
                                id: i+''+j,
                                color: '#fd3153',
                                from: from_date,
                                to: to_date,
                                title: title,
                                description: description,
                                keyword_sets:keyword_sets,
                                keywords:keywords,
                                sv:sv,
                                rank:rank,
                                target_url:target_url


                            }
                        )
                    }
                }
  
                let a_seo_data = calendar_data[key].seo_data
                let m = 0; 
                if(notification_status == 'seo' && a_content_data.length == 0 || notification_status == 'all'){
                    for(let seo_key in a_seo_data){  
                        m++    
                        let from_date = a_seo_data[seo_key].created_at;
                        let to_date = a_seo_data[seo_key].created_at;
                        let title = a_seo_data?.[seo_key]?.title ?? '';
                        let description = a_seo_data?.[seo_key]?.description ?? '';
                        
                        let keyword_sets = a_seo_data[seo_key].keyword_sets??[];

                        a_calendar.push(
                            {
                                id: i+''+m,
                                color: '#fd3153',
                                from: from_date,
                                to: to_date,
                                title: title,
                                description: description,
                                keyword_sets:keyword_sets
                            }
                        )
                    }
                }

            } 
            console.log('a_calendar',a_calendar)
            setLoading(false)
            setPending(false)
            setTimeout(function(){
                document.getElementById("staus_filter").value = notification_status;
            }, 1000);
            setCalendarData(a_calendar)
        }).catch(function (error) {
            toast(`Error`)
            toast.error("Some issue to load keyword.")
        }) 
    }
    useEffect(() => {
        getData(from_event_date);
    }, []); 
    const columns = [
        {
            name: 'S No.',
            selector: (row, index) => (index + 1).toString(),
            sortable: true,
            width: '100px' 
        },
        {
            name: 'keywords',
            selector: row => row.keywords,
            sortable: true,
            width: '300px',
        },
        {
            name: 'search volume',
            selector: row => row.sv,
            sortable: true,
            width: '100px',
        },
        {
            name: 'rank',
            selector: row => row.rank,
            sortable: true,
            width: '150px',
        },
        {
            name: 'target_url',
            selector: row => row.target_url,
            sortable: true,
            width: '500px',
        },
        {
            name: 'Title',
            selector: row => <div data-bs-toggle="modal" data-bs-target="#exampleModal" role="button" aria-controls="offcanvasExample" onClick={() => getCanvasData('Keyword',row)}>{row.title}</div>,
            sortable: true
        },
        {
            name: 'Description',
            selector: row => row.description, 
            sortable: true,
            width: '500px',
        },
        {
            name: 'articelink',
            selector: row => row.articelink, 
            sortable: true,
            width: '500px',
        },
        {
            name: 'live',
            selector: row => row.live, 
            sortable: true,
            width: '500px',
        }
    ];

    const handleChange = ({ selectedRows }) => {
        console.log('Selected Rows: ', selectedRows);
    };

    const [filterText, setFilterText] = React.useState('');
    const [resetPaginationToggle, setResetPaginationToggle] = React.useState(false);

    const filteredItems = calendarData.filter(
        item => item.title && item.title.toLowerCase().includes(filterText.toLowerCase())
    );

    const subHeaderComponentMemo = React.useMemo(() => {
        const handleClear = () => {
            if (filterText) {
                setResetPaginationToggle(!resetPaginationToggle);
                setFilterText('');
            }
        }; 
        return (
            <>
                <select className="form-control d-none" style={{ width: "250px",marginRight:"20px" }} onChange={e => getData(from_event_date,e.target.value)} id="staus_filter">    
                    <option value='all'>All</option>   
                    <option value='seo'>Seo Data</option> 
                    <option value='content'>Content Data</option>  
                </select>
                {/* <input className="form-control" style={{ width: "250px" }} onChange={e => setFilterText(e.target.value)} onClear={handleClear} filterText={filterText} placeholder="Search..." /> */}
            </>
        );
    }, [filterText, resetPaginationToggle]);
 
    return (
        <div class="container-fluid ">
            <div class="row">
                <Top />
                <div class="col-lg p-0" id="right_side">
                    <Header />
                    <div class="main">
                        <div class="row mt-3">
                            <div class="col-xl-12 d-flex justify-content-between align-items-center">
                                <div class="breadcrumbs">
                                    <nav aria-label="breadcrumb">
                                        <ol class="breadcrumb">
                                            <li class="breadcrumb-item"><Link to="/dashboard">Dashboard</Link></li>
                                            <li class="breadcrumb-item active" aria-current="page">Calender Event Listing</li>
                                        </ol>
                                    </nav>
                                </div>
                                <Link to="/addSeo" class="btn_outlined">Back</Link>
                            </div>
                        </div>
                        <div class="mt-3  p-2">
                            <h5 class="main_heading">Calender Event Listing</h5>
                        </div>

                        {loading ? (
                            <Loader />
                        ) : (
                            <DataTable
                                columns={columns}
                                data={filteredItems}
                                pagination
                                selectableRows
                                onSelectedRowsChange={handleChange}
                                progressPending={pending}
                                subHeader
                                persistTableHead
                                subHeaderComponent={subHeaderComponentMemo}
                            />
                        )}
                    </div>
                </div>
            </div>
            
            <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog modal-xl modal-dialog-centered">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">{topicName}</h5>
                            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                        </div>
                        <div class="modal-body">
                            <div dangerouslySetInnerHTML={{ __html: topicData }} />
                        </div> 
                    </div>
                </div>
            </div>
 
        </div>
    );
}
export default CalendarEvent;