export const TabsOptions = [
    { value: "marketing", label: "Content Marketing" },
    { value: "article", label: "Article Submission" },
    { value: "ppt_pdf", label: "PDF/PPT Submission" },
    { value: "image", label: "Image/Infofraphics" },
    { value: "video", label: "Video Submission" },
    { value: "quora", label: "Quora" },
    { value: "faq", label: "Faq" },
  ];

  export const MarketingTabsOptions = [
    { value: "marketing", label: "Content Marketing" },
    { value: "article", label: "Article Submission" },
    { value: "ppt_pdf", label: "PDF/PPT Submission" },
    { value: "image", label: "Image/Infofraphics" },
    { value: "video", label: "Video Submission" },
  ];

  export const ContentTabsOptions = [
    { value: "marketing", label: "Content Marketing" },
    { value: "article", label: "Article Submission" },
    { value: "quora", label: "Quora" },
    { value: "faq", label: "Faq" },
  ];

  export const ContentTabsTitleOptions = [
    { value: "marketing", label: "Content Marketing" },
    { value: "article", label: "Article Submission" },
  ];