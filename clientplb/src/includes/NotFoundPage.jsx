import React, { useState, useEffect } from 'react';
import { useNavigate } from 'react-router-dom';


function NotFound() {
    const navigate = useNavigate();
  
    if(!process.env.NODE_ENV || process.env.NODE_ENV === 'development') {
        var url = process.env.REACT_APP_PRODUCTION_URL; 
       //console.log('1___',process.env.REACT_APP_PRODUCTION_URL)
    }else{
        var url = process.env.REACT_APP_PRODUCTION_URL; 
    }
    const [department, setDepartment] = useState(localStorage.getItem("department").toLowerCase());
    return (
        <>
            <div className="container-fluid">
             
                   
                    
                        <div className="main mt-5">
                        <div className="d-flex flex-column justify-content-center align-items-center">
                        <img src="../assets/Icons/not_found.svg" className="mt-5" alt="" />
                        <h2 className='mt-5'>Page not Found</h2>
                        <h6 className='mt-2'>We're sorry, the page you requested could not be found
                            please go back to the homepage</h6>
                        <button className="main_btn mt-5 mb-sm-0" 
                        onClick={()=>{
                            {(department == 'seo') ? navigate('/dashboard') : 
                            (department == 'content_writing') ? navigate('/contentDashboard') : 
                            (department == 'content_marketing') ? navigate('/clientDashboard'):
                            (department == 'client') ? navigate('/client') :
                            (department == 'client_service') ? navigate('/managerDashboard'):navigate('')
                        
                        }
                        }}
                        >Back to Dashboard</button>
                        </div>
                        </div>
                    
              
            </div>
        </>
    );
}
export default NotFound;