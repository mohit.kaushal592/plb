import React, { useState, useEffect } from 'react';
import { Link, NavLink, useLocation } from 'react-router-dom';
import { useNavigate } from 'react-router-dom';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import axios from 'axios';
import { ModalCustom } from '../Common/ModalCustom/Modal';
import ManageSeo from '../Client_Pitch/my_list';

function Top(props) {
    const navigate = useNavigate();
    const [role, setRole] = useState(localStorage.getItem("role").toLowerCase());
    const [department, setDepartment] = useState(localStorage.getItem("department").toLowerCase());


    var url;

    if (!process.env.NODE_ENV || process.env.NODE_ENV === 'development') {
        url = process.env.REACT_APP_PRODUCTION_URL;
    } else {
        url = process.env.REACT_APP_PRODUCTION_URL;
    }

    const [accessToken, setAccessToken] = useState(localStorage.getItem('accessToken'));
    const [pending, setPending] = useState(true);
    const [loading, setLoading] = useState(true)
    const [wishListName, setWishListName] = useState([])
    const [totalCount,setTotalCount] = useState()
    const headerData = {
        headers: {
            Authorization: `Bearer ${accessToken}`
        }
    }


    const getClientName = async () => {
        try {
            setLoading(true);
            const userRes = await axios.get(`${url}/client/name`, headerData);
            let data = userRes?.data
            const formattedOptions = data.map(item => ({
                value: item?.id,
                label: item?.name,
                count: item?.submitted_count
            }));
            const totalSubmittedCount = formattedOptions.reduce((accumulator, currentValue) => {
                return accumulator + (currentValue.count || 0);
            }, 0);
            ///console.log('1__',formattedOptions)
            setWishListName(formattedOptions)
            setTotalCount(totalSubmittedCount)
            setLoading(false);
            setPending(false);
        } catch (error) {
            console.error('Error fetching data:', error);
        }
    };

    useEffect(() => {
        getClientName();
    }, [])

    const logOut = () => {
        localStorage.removeItem("accessToken");
        localStorage.removeItem("department");
        localStorage.removeItem("role");
        toast.success(`Logout successfully.`)
        navigate(`/login`);
    }


    return (
        <>
            <div className={`col-lg p-0 ${props?.isSidebarOpen ? 'sidebar-open' : ''}`} id="left_side" style={{textAlign:"left"}}>
                <ToastContainer />
                <aside className="side_nav px-2">
                    <div className="brand_icon gap-4 d-flex" style={{display:'flex',justifyContent:'center',alignItems:'center'}}>
                        <a className="text-decoration-none text-dark " href="#" >
                            <img src="../assets/Images/AdliftLogo.png" alt="" width="150px"  onClick={()=>{
                                {(department == 'seo') ? navigate('/dashboard') : 
                                (department == 'content_writing') ? navigate('/contentDashboard') : 
                                (department == 'content_marketing') ? navigate('/clientDashboard'):
                                (department == 'client') ? navigate('/client') :
                                (department == 'client_service') ? navigate('/managerDashboard'):navigate('/not-found')
                            
                            }
                            }}/>
                        </a>
                        <img src="../assets/Icons/menu.svg" className="mobile_icon" width="40px" alt="" onClick={()=>{props?.toggleSidebar(props?.isSidebarOpen)}}/>           
                    </div>
                    <ul className="navbar-nav p-2 me-auto ">
                        { }

                        
                            <>
                                <li className={`nav-item ${props?.marketingDashboard ? 'nav_item_stickness':''}`}>
                                    <Link className="nav-link" to="/clientDashboard">
                                        <img src="../assets/Icons/Dashboard.svg" class="me-2" alt="" /> Dashboard
                                    </Link>
                                </li>
                            </>
                    

              
                            <>
                                <li className={`nav-item ${props?.marketingPublisher ? 'nav_item_stickness':''}`}>
                                    <Link className="nav-link" to="/listing">
                                        <img src="../assets/Icons/mylist.svg" class="me-2" alt="" width={20} height={20}/> Listing
                                    </Link>

                                </li>
                            </>
                    
                        <li>
                        </li>
                        <li className="nav-item">
                            <a className="nav-link" href="#" onClick={logOut}>Log Out</a>
                        </li>
                    </ul>
                </aside>
            </div>
        </>
    );
}
export default Top;