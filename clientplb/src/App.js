import React from 'react';
import { BrowserRouter as Router, Routes, Route, Navigate } from 'react-router-dom';
import Login from './Auth/Login';
import ProtectedRoute from './middleware/ProtectedRoute'; 
import RedirectIfLogin from './middleware/RedirectIfLogin'; 
import CreateNewPassword from './Auth/CreateNewPassword'
import Client from './Client_Pitch/Dashboard';
import Listing from './Client_Pitch/Listing'

 
function App() {
  return (
    <div className="App">
      <Router>
        <Routes>
            <Route element={<RedirectIfLogin />}>
            <Route path="/login" element={<Login />} />
            <Route path="/create-new-password" element={<CreateNewPassword />} />
          </Route>
          <Route element={<ProtectedRoute />}>
            <Route path="/" element={<Client />} />
            <Route path="/clientDashboard" element={<Client />} />
            <Route path="/listing" element={<Listing />} />
          </Route>
        </Routes>
      </Router>
    </div>
  );
}

export default App;