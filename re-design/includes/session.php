<?php
/**
 * @package	PLB tool
 * @module	Session Helper
 * @author	Akhtar Khan
 * @email	akhtar.khan@adlift.com
 * */
// A class to help work with Sessions
// In our case, primarily to manage logging users in and out

// Keep in mind when working with sessions that it is generally 
// inadvisable to store DB-related objects in sessions

class Session {
	private $logged_in=false;
	
	function __construct() {
		session_start();
		$this->check_message();
		$this->check_login();
	}
	
	public function is_logged_in() {
		return $this->check('User.id');
	}

	public function login($user) {
		// database should find user based on username/password
		if($user){
			$this->write('User', $user['User']);
			if($this->check('User.password')){
				$this->delete('User.password');
			}
			$this->logged_in = true;
		}
	}
  
	public function logout() {
		$this->delete('User');
		$this->logged_in = false;
	}

	public function message($msg="") {
		if(!empty($msg)) {
			// then this is "set message"
			$this->write('message', $msg);
		} else {
			// then this is "get message"
			$out = $this->read('message');
			$this->delete('message');
			return $out;
		}
	}

	private function check_login() {
		if($this->check('User.id')) {
			$this->logged_in = true;
		} else {
			$this->logged_in = false;
		}
	}
  
	private function check_message() {
		// Is there a message stored in the session?
		if($this->check('message')) {
			// Add it as an attribute and erase the stored version
			$this->message = $this->read('message');
			$this->delete('message');
		} else {
			$this->message = "";
		}
	}
	
	public function write($key, $value=null){
		
		if (empty($key)) {
			return false;
		}
		$write = $key;
		if (!is_array($key)) {
			$write = array($key => $value);
		}
		foreach ($write as $ky => $val) {
			self::rewrite($_SESSION, self::_insert($_SESSION, $ky, $val));
			if ($this->read($ky) !== $val) {
				return false;
			}
		}
		return true;
	}
	
	public function read($key=''){
		return self::_get($_SESSION, $key);
	}
	
	public function check($key=''){
		return self::_get($_SESSION, $key) !== null;
	}
	public function delete($key=''){
		if ($this->check($key)) {
			self::rewrite($_SESSION, self::_remove($_SESSION, $key));
			return !$this->check($key);
		}
		return false;
	}
	
	private static function add_remove_op($op, $data, $path, $values = null) {
		$_list =& $data;

		$count = count($path);
		$last = $count - 1;
		foreach ($path as $i => $key) {
			if (is_numeric($key) && intval($key) > 0 || $key === '0') {
				$key = intval($key);
			}
			if ($op === 'insert') {
				if ($i === $last) {
					$_list[$key] = $values;
					return $data;
				}
				if (!isset($_list[$key])) {
					$_list[$key] = array();
				}
				$_list =& $_list[$key];
				if (!is_array($_list)) {
					$_list = array();
				}
			} elseif ($op === 'remove') {
				if ($i === $last) {
					unset($_list[$key]);
					return $data;
				}
				if (!isset($_list[$key])) {
					return $data;
				}
				$_list =& $_list[$key];
			}
		}
	}
	
	// re-write values to overwrite
	private static function rewrite(&$old, $new) {
		if (!empty($old)) {
			foreach ($old as $key => $var) {
				if (!isset($new[$key])) {
					unset($old[$key]);
				}
			}
		}
		foreach ($new as $key => $var) {
			$old[$key] = $var;
		}
	}
	
	private static function _remove(array $data, $key) {
		$keyTokens = explode('.', $key);
		return self::add_remove_op('remove', $data, $keyTokens);
	}
	
	protected static function _insert(array $data, $key, $values = null) {
		$keyTokens = explode('.', $key);
		return self::add_remove_op('insert', $data, $keyTokens, $values);
	}
	
	protected static function _get(array $data, $key) {
		if (empty($data)) {
			return null;
		}
		if (is_string($key) || is_numeric($key)) {
			$keys = explode('.', $key);
		} else {
			$keys = $key;
		}
		foreach ($keys as $ky) {
			if (is_array($data) && isset($data[$ky])) {
				$data =& $data[$ky];
			} else {
				return null;
			}
		}
		return $data;
	}
	
}

$session = new Session();

?>