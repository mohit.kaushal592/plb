<?php
/**
 * @package	PLB tool
 * @module	Campaign
 * @author	Akhtar Khan
 * @email	akhtar.khan@adlift.com
 * */
// If it's going to need the database, then it's 
// probably smart to require it before we start.
require_once(LIB_PATH.DS.'database.php');

class Campaign extends DatabaseObject {
	
	public $table_name="campaigns";
	public $db_fields = array('id','name', 'website','keywords','competitors', 'type_report', 'frequency','status_act','status_plb', 'plb_da','plb_pr','moz_external', 'moz_root', 'update_moz','user_update_plb','secondary_approval', 'secondary_approval_user', 'email','mail_report', 'google_report','individual_mail', 'graph_date','dateFormat', 'secondary_keywords','cronstatus', 'datetimeupdated');
	
	public $relationTables = array(
		'belongsTo' =>array(
		),
		'hasMany' => array(
			'ApprovalLink'=>array(
				'foreignKey'=> 'campaign_id'
			),
			'BlacklistDomain'=>array(
				'foreignKey'=> 'campaign_id'
			)
		)
	);
	
	public function getList(){
		$campaignsList = $this->find_list(array('where'=>'Campaign.status_plb=1', 'order'=>'Campaign.name ASC', 'fields'=>array('id','name')));
		return $campaignsList;
	}
	
}

?>