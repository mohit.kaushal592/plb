<?php
/**
 * @package	PLB tool
 * @module	RejectReason
 * @author	Akhtar Khan
 * @email	akhtar.khan@adlift.com
 * */
// If it's going to need the database, then it's 
// probably smart to require it before we start.
require_once(LIB_PATH.DS.'database.php');

class RejectReason extends DatabaseObject {
	
	public $table_name="tbl_plb_reject_reasons";
	public $db_fields = array('id', 'data','status', 'added_on');
	
	public $relationTables = array(
		'belongsTo' =>array(
		),
		'hasMany' => array(
		)
	);

}

?>