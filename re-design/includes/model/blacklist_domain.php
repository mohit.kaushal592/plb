<?php
/**
 * @package	PLB tool
 * @module	Domain
 * @author	Akhtar Khan
 * @email	akhtar.khan@adlift.com
 * */
// If it's going to need the database, then it's 
// probably smart to require it before we start.
require_once(LIB_PATH.DS.'database.php');

class BlacklistDomain extends DatabaseObject {
	
	public $table_name="blacklisted_domains";
	public $db_fields = array('id','domain_id','campaign_id','created');
	
	public $relationTables = array(
		'belongsTo' =>array(
			'Domain'=>array(
				'foreignKey'=> 'domain_id'
			),
			'Campaign'=>array(
				'foreignKey'=> 'campaign_id'
			)
		),
		'hasMany' => array(
		)
	);
	
}

?>