<?php
/**
 * @package	PLB tool
 * @module	Currency
 * @author	Akhtar Khan
 * @email	akhtar.khan@adlift.com
 * */
// If it's going to need the database, then it's 
// probably smart to require it before we start.
require_once(LIB_PATH.DS.'database.php');

class Currency extends DatabaseObject {
	
	public $table_name="currency";
	public $db_fields = array('id','currency', 'usd_rate', 'added_on', 'updated_on');
	
	public $relationTables = array(
		'belongsTo' =>array(
		),
		'hasMany' => array(
		)
	);
	
	public function getRate() {
		$url = "http://openexchangerates.org/api/latest.json?app_id=4c9045d8439e433697b1b47dc887f04c";
		$ch = curl_init();
		$timeout = 0;
		curl_setopt ($ch, CURLOPT_URL, $url);
		curl_setopt ($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch,  CURLOPT_USERAGENT , "Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.1)");
		curl_setopt ($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
		$json = curl_exec($ch);
		curl_close($ch);
		$jsonOb = json_decode($json);
		if($jsonOb->error == 1){
			return array();
		}
		$currencyRatesArray = object_to_array($jsonOb->rates);
		return $currencyRatesArray;
	}
	
	public function calcPrice($amount, $currency){
		if(!empty($amount)){
		    switch(strtolower($currency)){
			case 'dollars':
			case 'usd':
			    $currency = 'USD';
			    break;
			case 'pounds':
			case 'gbp':
			    $currency = 'GBP';
			    break;
			case 'euro':
			case 'eur':
			    $currency = 'EUR';
			    break;
		    }
		    
		    $latestRates = $this->getCurrency($currency);
		    if(key_exists($currency, $latestRates)){
			$amount = $latestRates[$currency]*$amount;
		    }
		}
		return $amount;
	}
	
	public function getCurrency($currency){
		$isTodayUpdated = $this->count(array('where'=>"DATE_FORMAT(Currency.updated_on, '%Y-%m-%d') = '".date('Y-m-d')."' AND Currency.currency = '$currency'"));
		if($isTodayUpdated==0){
			$list = $this->find_list(array('fields'=>array('id', 'currency')));
			$_ratesAsBaseUsd = $this->getRate();
			if(!empty($_ratesAsBaseUsd)){
				foreach($list as $_id=>$_currency){
					$data  = array('Currency'=>array('id'=>$_id, 'usd_rate'=>$this->getUsdRate($_currency,$_ratesAsBaseUsd), 'updated_on'=>date('Y-m-d H:i:s')));
					$this->save($data);	
				}
			}
		}
		$rateList = $this->find_list(array('fields'=>array('currency', 'usd_rate')));
		return $rateList;
	}
	
	private function getUsdRate($cuurency, $currencyRates=array()){
		if(!empty($currencyRates)){
			$rate = round(1/$currencyRates[strtoupper($cuurency)], 3);
			return $rate;
		}
		return '0';
	}
	
}

?>