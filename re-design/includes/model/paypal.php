<?php
/**
 * @package	PLB tool
 * @module	Paypal
 * @author	Akhtar Khan
 * @email	akhtar.khan@adlift.com
 * */
// If it's going to need the database, then it's 
// probably smart to require it before we start.
require_once(LIB_PATH.DS.'database.php');

class Paypal extends DatabaseObject {
	
	public $table_name="tbl_plb_paypals";
	public $db_fields = array('id','name','domain_id_string','status', 'added_on','updated_on');
	
	public $relationTables = array(
		'belongsTo' =>array(
		),
		'hasMany' => array(
		)
	);
	
	public function fetchAll($options=array(), $paging=false){
		$options = is_array($options) ? $options : (array)$options;
		$results = $this->find_all($options, $paging);
		$Domain = new Domain;
		foreach($results as &$result){
			if(key_exists('domain_id_string', $result['Paypal'])){
				$domainStr = join(',', array_filter(explode(',', $result['Paypal']['domain_id_string'])));
				if(!empty($domainStr)){
					$domainResults = $Domain->find_all(array('where'=>'Domain.id IN ('.$domainStr.')'));
					foreach($domainResults as $domain){
						$result['Domain'][] = $domain['Domain'];
					}
				}else{
					$result['Domain']=array();
				}
			}
		}
		return $results;
	}
	
	public function getList($options=array()){
		$filter = array('fields'=>array('id','name'));
		if(!empty($options)){
			$options = is_array($options)?$options : (array)$options;
			if($options['where']){
				$filter['where'] =  $options['where'];
			}
			if($options['limit']){
				$filter['limit'] =  $options['limit'];
			}
		}
		$list = $this->find_list($filter);
		$result = array();
		if(!empty($list)){
			foreach($list as $id=>$name){
				$result[$name] = $name;
			}
		}
		
		return $result;
	}
	
}

?>