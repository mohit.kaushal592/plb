<?php
class PHPMail{
    protected $mailFrom = '';
    protected $mailTo = array();
    protected $mailCc = array();
    protected $mailBcc = array();
    protected $mailReplyTo = '';
    protected $mailSubject = '';
    protected $mailBody = '';
    
    public function setFrom($email){
        $this->mailFrom = $email;
    }
    
    public function setTo($email){
        array_push($this->mailTo, $email);
    }
    
    public function setCc($email){
        array_push($this->mailCc, $email);
    }
    
    public function setBcc($email){
        array_push($this->mailBcc, $email);
    }
    
    public function setReplyTo($email){
        $this->mailReplyTo = $email;
    }
    
    public function setSubject($text=''){
        $this->mailSubject = $text;
    }
    
    public function setBody($text=''){
        $this->mailBody = $text;
    }
    
    protected function getTo(){
        return implode(', ', array_filter($this->mailTo));
    }
    
    protected function getHeaders(){
        // To send HTML mail, the Content-type header must be set
        
        $headers = array(
            'mimeVersion' => 'MIME-Version: 1.0',
            'contentType' => 'Content-type: text/html; charset=iso-8859-1',
            'mailFrom' => 'From: <'.$this->mailFrom.'>',
            );
        $mailCc = array_filter($this->mailCc);
        $mailBcc = array_filter($this->mailBcc);
        $headers['mailCc'] = !empty($mailCc) ? 'Cc: '. implode(', ', $mailCc) : null;
        $headers['mailBcc'] = !empty($mailBcc) ? 'Bcc: '. implode(', ', $mailBcc) : null;
        $headers = array_filter($headers);
        return implode("\r\n", $headers);
    }
    
    public function sendMail(){
        return @mail($this->getTo(), $this->mailSubject, $this->mailBody, $this->getHeaders());
    }
}
?>