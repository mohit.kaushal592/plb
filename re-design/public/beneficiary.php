<?php
require_once("../includes/initialize.php");
 if (!$session->is_logged_in()) { redirect_to("login.php"); }

   function beneficiary_index(){
      global $viewData;
      $viewData->setTitle('Submitted Links Listing');
      $filters = __paymentFilterVars();
      
   }
   
   function beneficiary_data(){
      global $viewData;
      $Payment = new Beneficiary;
      $filters = __paymentFilterVars();
      $isExport = base64_decode($_REQUEST['_export'])==1? true : false;
	  
      $paymentFilter = array('contain'=>array('User'), 'order'=>array('Beneficiary.id DESC'));
      if(!empty($filters['Beneficiary'])){
         $paymentFilter['where'] = !empty($paymentFilter['where']) ? $paymentFilter['where'].' AND '. $filters['Beneficiary'] : $filters['Beneficiary'];
      }
      
		   $submittedLinks = $Payment->find_all($paymentFilter, $isExport==false ? true : false);  
		   
	  

      $viewData->set('submittedLinks', $submittedLinks);
      if($isExport===true){
		  include_once "views/beneficiary_export.php";
        exit;
      }
	   else{
		   ob_start();
         include_once "views/beneficiary_data.php";
         $_html = ob_get_contents();
         ob_end_clean();
         echo json_encode(array('html'=>$_html, 'pagination'=>$viewData->get('pageLinks'), 'totalRecords'=> $viewData->get('totalRecords')));
         
      }
      exit;      
   }
   
   function beneficiary_filter_form(){
      global $viewData;
      if($_POST['filterVars']){
         $viewData->set('filterVars', $_POST['filterVars']);
      }
      
      include_once "views/beneficiary_filter_form.php";
      exit;
   }
      
   function submitted_links_do_action(){
      switch($_REQUEST['whatDo']){
       case 'link_delete':
        __action_link_delete($_REQUEST['data']);
        break;
       case 'link_payment':
        __action_link_payment($_REQUEST['data']);
        break;
      }
      exit;
   }
   
   function submitted_links_webmaster_list_json(){
      global $viewData;
      $Payment = new Payment;
      $options=array('group'=>'client_mail', 'fields'=>array('id','client_mail'));
      if(!empty($_GET['q'])){
         $options['where'] = "Payment.client_mail LIKE '%".$_GET['q']."%'";
         $options['limit'] = !empty($_GET['page_limit']) and (int)$_GET['page_limit']>0 ? '0, '.$_GET['page_limit'] : '0, 10';
         
         $clientsEmailFromPayment = $Payment->find_list($options);
         
         $clientEmailList = array();
         if(!empty($clientsEmailFromPayment)){
            foreach($clientsEmailFromPayment as $id=>$email){
               $clientEmailList[$email]=$email;
            }
         }
         echo json_encode(select2DataFormat($clientEmailList));
      }
      exit;
   }


   
   
      
   
   function __paymentFilterVars(){
      global $viewData;
      $filter = array();
      $filterVars = array();
    
      
      if(!empty($_POST['_bc'])){
       $bencode = trim(base64_decode($_POST['_bc']));
       $filterVars['bencode'] = $bencode;
      
       $filter['Beneficiary'][] = " (Beneficiary.bencode LIKE '%".$bencode."%' ) ";
      }
      if(!empty($_POST['_bn'])){
       $benname = trim(base64_decode($_POST['_bn']));
       $filterVars['benname'] = $benname;
      
       $filter['Beneficiary'][] = " (Beneficiary.benname LIKE '%".$benname."%' ) ";
      }
	  if(!empty($_POST['_ba'])){
       $benaccount = trim(base64_decode($_POST['_ba']));
       $filterVars['benaccount'] = $benaccount;
      
       $filter['Beneficiary'][] = " (Beneficiary.benaccount LIKE '%".$benaccount."%' ) ";
      }
	  if(!empty($_POST['_bi'])){
       $ifsccode = trim(base64_decode($_POST['_bi']));
       $filterVars['ifsccode'] = $ifsccode;
      
       $filter['Beneficiary'][] = " (Beneficiary.ifsccode LIKE '%".$ifsccode."%' ) ";
      }
      if(!empty($_POST['_dt'])){
         list($dateFrom, $dateTo) = explode('_', base64_decode($_POST['_dt']));
         if(!empty($dateFrom) and !empty($dateTo)){
          $filterVars['dateFrom'] = $dateFrom;
          $filterVars['dateTo'] = $dateTo;
          $filter['Beneficiary'][] = " DATE_FORMAT(Beneficiary.postdate, '%Y-%m-%d') BETWEEN '".date('Y-m-d', strtotime($dateFrom))."' AND '".date('Y-m-d', strtotime($dateTo))."' "; 
         } else if(!empty($dateFrom) and empty($dateTo)){
          $filterVars['dateFrom'] = $dateFrom;
          $filter['Beneficiary'][] = " DATE_FORMAT(Beneficiary.postdate, '%Y-%m-%d') = '".date('Y-m-d', strtotime($dateFrom))."' "; 
         }
      }
     
      $filters['Beneficiary'] = $filter['Beneficiary'] ? implode(' AND ', $filter['Beneficiary']) : '';

      $viewData->set('filterVars', $filterVars);
      return $filters;
   }
   
  
 // auto call function related to page if exists
$action = isset($_GET['act']) ? $_GET['act'] : 'index';
if(function_exists('beneficiary_'.$action)){ 
 call_user_func('beneficiary_'.$action);
}
// include default template
$useLayout = isset($_GET['act']) ? 'beneficiary_'.$_GET['act'].'.php' : 'beneficiary_index.php';
include "views/default.php";
 ?>