<?php
require_once("../includes/initialize.php");
 if (!$session->is_logged_in()) { redirect_to("login.php"); }

$useLayout = isset($_GET['act']) ? 'new_dashboard_'.$_GET['act'].'.php' : 'new_dashboard.php';
include "views/default.php";
 ?>