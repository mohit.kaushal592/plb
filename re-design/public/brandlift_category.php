<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- css-files -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <!-- <link rel="stylesheet" href="css/bootstrap.min.css" /> -->
    <link rel="stylesheet" href="css/bootstrap-responsive.min.css" />
    <link rel="stylesheet" href="css/uniform.css" />
    <link rel="stylesheet" href="css/datepicker.css" />
    <link rel="stylesheet" href="css/fullcalendar.css" />
    <!-- <link rel="stylesheet" href="css/style.css" /> -->
    <link rel="stylesheet" href="css/media.css" />
    <link rel="stylesheet" href="css/select2.css" />
    <link rel="stylesheet" href="css/jquery-ui-1.10.3.custom.min.css" />
    <link href="css/font-awesome.css" rel="stylesheet" />
    <link href="css/jquery.multiselect.css" rel="stylesheet" />
    <link href="css/jquery.multiselect.filter.css" rel="stylesheet" />
    <link rel="stylesheet" href="css/jquery.gritter.css" />
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,700,800' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="Assets/Css/Style.css">
    <link rel="stylesheet" href="Assets/Css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.13.7/css/dataTables.bootstrap5.min.css">
    <script src="https://code.jquery.com/jquery-3.7.0.js"></script>
    <script src="https://code.highcharts.com/highcharts.js"></script>
    <script src="https://code.highcharts.com/modules/exporting.js"></script>
    <script src="https://code.highcharts.com/modules/accessibility.js"></script>
</head>

<body>
    <div class="container">
        <div class="row mt-4">
            <h4 class="darkBreadcrumb">
                <span class="lightBeradcrumb">Reports</span> / Brandlift
            </h4>
        </div>
        <div class="header_container">
            <div class="chartContainer">
                <div class="chartCard my-2">
                    <div class="chartHeading">
                        <h4 class="chart_title">
                            BrandLift <span  class="card_view">View Detail ></span>
                        </h4>
                    </div>
                    <div class="pieChart_Card">
                        <h4>Chart Img</h4>
                    </div>
                </div>
            </div>
        </div>
        <div class="table-responsive mt-3">
            <div class="tableHeadSection">
                <h3 class="tableMainTitle">Reports</h3>
                <div class="d-flex pb-3 w-50">
                    <select class="form-select form-control mx-2">
                        <option disabled selected>Campaign</option>
                        <option value="1">All</option>
                    </select>
                    <select class="form-select form-control mx-2">
                        <option disabled selected>Amount</option>
                        <option value="1">All</option>
                    </select>
                    <select class="form-select form-control mx-2">
                        <option disabled selected>DA</option>
                        <option value="1">All</option>
                    </select>
                    <select class="form-select form-control actionDrop">
                        <option disabled selected>Actions</option>
                        <option value="1">All</option>
                    </select>
                </div>
            </div>
            <table class="table table-bordered bg-white">
                <tr style="text-align:left; background-color:#FFEFE4">
                    <th>Domain</th>
                    <th>Category</th>
                    <th>PR</th>
                    <th>DA</th>
                    <th>SC</th>
                    <th>Country</th>
                    <th>User</th>
                    <th>Date</th>
                    <th>Count</th>
                    <th>Campaign
                        <span style="float:left">
                            <input type="checkbox" id="checkAllApproval" />
                            <input type="checkbox" id="checkAllApproval" />
                        </span>
                    </th>
                    <th>Text</th>
                    <th>Admin Text</th>
                    <th>Content Writer</th>
                    <th>Action</th>
                </tr>

                <tr>
                    <td><span style="color:#179AFF">http://thewritelife.com</span><br>
                        141.193.213.11
                    </td>
                    <td>Technology</td>
                    <td>00</td>
                    <td>00</td>
                    <td>00</td>
                    <td>USA</td>
                    <td>Name</td>
                    <td>July 12, 2023</td>
                    <td>1</td>
                    <td>
                        <div class="campaign_data">Loremipsum ($15)
                            <span><img src="Assets/Icons/CampCheck.svg" alt=""></span>
                        </div>
                    </td>
                    <td>Already Paid</td>
                    <td></td>
                    <td>Adlift</td>
                    <td>
                        <a href="#">
                            <img src="Assets/Icons/NotePencil.svg" alt="">
                        </a>
                        <br>
                        <a href="#">
                            <img src="Assets/Icons/DotsThreeCircle.svg" alt="">
                        </a>
                    </td>
                </tr>
            </table>
        </div>
    </div>
</body>

</html>