<?php
require_once("../includes/initialize.php");
 if (!$session->is_logged_in()) { redirect_to("login.php"); }

 function influencers_index() {
   global $viewData;
   $viewData->setTitle('Influencers Listing');
   $Influencer  = new Influencer;
   $influencerFilter =  array();
   $isExport = base64_decode($_REQUEST['_export'])==1? true : false;
   $influencers = $Influencer->find_all($influencerFilter, !$isExport);
   //  var_dump($influencers); die;
   $viewData->set('influencers', $influencers);
   if($isExport){
      include_once "views/influencers_export.php";
      exit;
   }
 }
 
 function influencers_add(){
   global $session, $viewData;
   $viewData->setTitle('Add New Influencer');
   
   $categories = array('Technology','Finance','Business','Fashion','Travel','Health','Mommy');
   
   $viewData->set('categories',$categories);
   
   if(!empty($_POST['data'])){
      $data = $_POST['data'];
      
      $influencer_name = $data['Influencer']['name'];
      $influencer_blogurl = $data['Influencer']['blog_url'];
      $influencer_category = $data['Influencer']['category'];
      $influencer_twitter_handle = $data['Influencer']['twitter_handle'];
      $influencer_facebook_id = $data['Influencer']['facebook_id'];
      $influencer_gplus_id = $data['Influencer']['gplus_id'];
      $influencer_instagram_id = $data['Influencer']['instagram_id'];
      $influencer_price = $data['Influencer']['price'];
      $influencer_location = $data['Influencer']['location'];
      $influencer_comment = $data['Influencer']['comment'];
      $user_id = $_SESSION['User']['id']; //$session['user_id'];
      $influencer_domain_id = $data['Influencer']['domain_ids'];
      $influencer_twitter_handle_price = $data['Influencer']['twitter_handle_price'];
	  $influencer_facebook_id_price = $data['Influencer']['facebook_id_price'];
	  $influencer_instagram_id_price = $data['Influencer']['instagram_id_price'];
      $Influencer = new Influencer;
      
      $_data = array('Influencer'=>array('name'=>$influencer_name, 'blog_url'=> $influencer_blogurl, 'domain_ids'=> $influencer_domain_id, 'category'=> $influencer_category, 'twitter_handle'=> $influencer_twitter_handle, 'facebook_id'=> $influencer_facebook_id, 'gplus_id'=> $influencer_gplus_id, 'instagram_id'=> $influencer_instagram_id, 'price'=> $influencer_price, 'twitter_handle_price'=> $influencer_twitter_handle_price, 'facebook_id_price'=> $influencer_facebook_id_price, 'instagram_id_price'=> $influencer_instagram_id_price, 'location'=> $influencer_location, 'user_id'=> $user_id, 'comment'=> $influencer_comment, 'added_on'=>date('Y-m-d H:i:s')));
            
            $isSubmit = true;
      
//Check if already there - TODO
            
            if($isSubmit===true){
               $Influencer->save($_data);   
            }
      $session->message('Influencer has been added.');
      redirect_to('influencers.php');
      }
   }

function influencers_import() {
    //var_dump($_FILES);
    global $database, $session;
    if(!empty($_FILES['csv'])){
        
        $csv = array();

        // check there are no errors
        if($_FILES['csv']['error'] == 0){
            $name = $_FILES['csv']['name'];
            $ext = strtolower(end(explode('.', $_FILES['csv']['name'])));
            $type = $_FILES['csv']['type'];
            $tmpName = $_FILES['csv']['tmp_name'];

            // check the file is a csv
            if($ext === 'csv'){
                if(($handle = fopen($tmpName, 'r')) !== FALSE) {
                    // necessary if a large csv file
                    set_time_limit(0);

                    $row = 0;
                    fgetcsv($handle);
                    $Influencer = new Influencer;
                    $skipped_count = 0;
                    $imported_count = 0;
                    
                    $categories = array('Technology','Finance','Business','Fashion','Travel','Health','Mommy');
                    
                    while(($data = fgetcsv($handle, 1000, ',')) !== FALSE) {
                        
                        // number of fields in the csv
                        $col_count = count($data);

                        // get the values from the csv
                        $csv[$row]['name'] = $data[0];
                        $csv[$row]['blog_url'] = $data[1];
                        $csv[$row]['domain_ids'] = $data[2];
                        $csv[$row]['category'] = array_keys($categories, $data[3]);
                        $csv[$row]['twitter_handle'] = $data[4];
                        $csv[$row]['facebook_id'] = $data[5];
                        $csv[$row]['gplus_id'] = $data[6];
                        $csv[$row]['instagram_id'] = $data[7];
                        $csv[$row]['price'] = $data[8];
                        $csv[$row]['user_id'] = isset($_SESSION['User']['id'])?$_SESSION['User']['id']:58;
                        $csv[$row]['location'] = $data[9];
                        $csv[$row]['comment'] = $data[10];
                        // inc the row
                        
                        //Insert Into DB
                        if($csv[$row]['facebook_id'] != '') {
                        $sql1 = "SELECT id FROM tbl_influencers WHERE name='".$csv[$row]['name']."' AND facebook_id = '".$csv[$row]['facebook_id']."'";
                        $result_set = $database->query($sql1);
                        $data_existing = $database->fetch_array($result_set);
                        }
                        else{
                        $sql2 = "SELECT id FROM tbl_influencers WHERE name='".$csv[$row]['name']."' AND twitter_handle = '".$csv[$row]['twitter_handle']."'";
                        $result_set = $database->query($sql2);
                        $data_existing = $database->fetch_array($result_set);
                        }
                        //var_dump(count($data_existing));
                        if(empty($data_existing)) {
                        $_data = array('Influencer'=>array('name'=>$csv[$row]['name'], 'blog_url'=> $csv[$row]['blog_url'], 'domain_ids'=> $csv[$row]['domain_ids'], 'category'=> $csv[$row]['category'], 'twitter_handle'=> $csv[$row]['twitter_handle'], 'facebook_id'=> $csv[$row]['facebook_id'], 'gplus_id'=> $csv[$row]['gplus_id'], 'instagram_id'=> $csv[$row]['instagram_id'], 'price'=>  $csv[$row]['price'], 'location'=> $csv[$row]['location'], 'user_id'=> $csv[$row]['user_id'], 'comment'=> $csv[$row]['comment'], 'added_on'=>date('Y-m-d H:i:s')));
                        $Influencer->save($_data);
                        $imported_count++;
                        }
                        else $skipped_count++;
                        
                        $row++;
                    }
                    fclose($handle);
                }
            }
        }
        //var_dump($csv);
        //die;
    }
$session->message('Total Records Imported : '.$imported_count.' | Records Skipped : '.$skipped_count);
redirect_to('influencers.php');    
}
   
 function influencers_list_json(){
   $Influencer = new Influencer;
   $q = isset($_GET['query']) ? " Influencer.name LIKE '%{$_GET['query']}%'" : '';
   $filterArgs = array('fields'=>array('id','name'));
   if(!empty($q)){
      $filterArgs['where'] = $q;
   }
   $list = $Influencer->find_list($filterArgs);
   $influencers = array();
   if(!empty($list)){
      foreach($list as $_k=>$_v){
         $influencers[] = array('id'=>$_k, 'name'=>$_v);
      }
   }
   echo json_encode($influencers);
   exit;
 }
 
 function influencers_delete_influencer(){
        global $database;
        //var_dump($_POST['data']['Influencer']['delete']); die;
        //Delete selected records from db
        foreach($_POST['data']['Influencer']['delete'] as $rec) {
            
             $database->query('DELETE FROM tbl_influencers WHERE id = '.(int)$rec.' LIMIT 1');
            $msg = array('msg' => 'Influencer has been deleted.', 'status' => 'success'); 
        }
 echo json_encode($msg);
     exit;
 }
 
 function __influencerFilterVars(){
   global $viewData;
  $filter = array();
  $filterVars = array();
  if(!empty($_GET['_dom'])){
   $domain = trim(base64_decode($_GET['_dom']));
   $filterVars['domain'] = $domain;
   $domain = rtrim($domain,"/");	
   $domain = str_replace(array("https://","http://","www."),"",$domain);	
   $domain = preg_replace('/^\./', '', get_domain_name($domain));
   $filter['Domain'][] = " (Domain.name LIKE 'http://".$domain."%' OR Domain.name LIKE 'http://www.".$domain."%') ";
  }
  if(!empty($_GET['_st'])){
      $st = base64_decode($_GET['_st']);
      $filterVars['status'] = explode(',', $st);
      $filter['Domain'][] = " Domain.status IN ('".join("','", $filterVars['status'])."') ";
  }
  $filters['Domain'] = $filter['Domain'] ? implode(' AND ', $filter['Domain']) : '';
   $viewData->set('filterVars', $filterVars);
   return $filters;
 }
 
 function __action_update_influencer($data=array()){
   global $database;
   $msg = array('msg'=>'Sorry, domain status could not changed. Please try again later.', 'status'=>'error');
   if(!empty($data)){
      $Domain = new Domain;
    // domain approval accept for campaigns
    if(!empty($data['Domain']['accept'])){
     $_ids = $data['Domain']['accept'];
     if(!empty($_ids)){
      $database->query('UPDATE '. $Domain->table_name.' SET status=1 WHERE id IN('. join(',', $_ids). ') AND status!=1');
      $msg = array('msg'=>'Domain status has been changed.', 'status'=>'success');
     }
    }
    // domain approval reject for campaigns
    if(!empty($data['Domain']['reject'])){
     $_ids = $data['Domain']['reject'];
     if(!empty($_ids)){
      $database->query('UPDATE '. $Domain->table_name.' SET status=0 WHERE id IN('. join(',', $_ids). ') AND status!=0');
      $msg = array('msg'=>'Domain status has been changed.', 'status'=>'success');
     }
    }
   }
   echo json_encode($msg);
 }
 
 
 // auto call function related to page if exists
$action = isset($_GET['act']) ? $_GET['act'] : 'index';
if(function_exists('influencers_'.$action)){ 
 call_user_func('influencers_'.$action);
}
// include default template
$useLayout = isset($_GET['act']) ? 'influencers_'.$_GET['act'].'.php' : 'influencers_index.php';
include "views/default.php";
 ?>