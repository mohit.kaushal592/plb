<?php echo output_message($session->message()) ?>
<div class="row mt-4">
    <h4 class="darkBreadcrumb">
        <span class="lightBeradcrumb">Users</span> / User Listings
    </h4>
</div>
<div class="row mt-1">
    <h3 class="tableMainTitle">Advance Filter</h3>
</div>
<?php include(WWW_ROOT.DS."layouts".DS."filter_users.php") ?>
 
<div class="row mt-4">
    <div class="col-lg-12">
        <h4 class="tableMainTitle my-2"> Users Listing </h4>
        <table id="example" class="table table" style="width:100%">
            <thead>
                <tr>
                    <th>Sr No</th>
                    <th>Name</th>
                    <th>UserName</th>
                    <th>Email</th>
                    <th>Created on</th>
                    <th>Type</th>
                    <th>Status</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                <?php $users = $viewData->get('users') ?>
                <?php if(!empty($users)): ?>
                <?php $i = $viewData->get('recordStartFrom') ?>
                <?php foreach($users as $user): ?>
                <?php 
                          //Added By Arvind
                          if($user['User']['user_type']=='user' && $user['User']['supper_user']== 1)
                          {
                              $user['User']['user_type'] = 'supper user';
                          }
                          //Ended By Arvind
                          ?>
                <tr class="odd gradeX">
                    <td><?php echo ++$i ?></td>
                    <td>
                        <a style="color:#1F2734; text-decoration:none;"
                            href="users.php?act=profile&uid=<?php echo $user['User']['id'] ?>" target="_blank">
                            <?php echo ucwords($user['User']['first_name'] . " " . $user['User']['last_name']) ?>
                        </a>
                    </td>
                    <td><?php echo $user['User']['username'] ?></td>
                    <td class="center"><?php echo $user['User']['email_id'] ?></td>
                    <td class="center"><?php echo date_to_text($user['User']['created_on']) ?></td>
                    <td class="center"><?php echo $user['User']['user_type'] ?></td>
                    <td class="center">
                        <?php if($user['User']['status'] == 1): ?>
                        <div class="activeList">
                            <a class="activeListText"
                                href="users.php?act=ustatus&id=<?php echo $user['User']['id'] ?>&st=no">
                                Active</a>
                        </div>
                        <?php else: ?>
                        <div class="inActive">
                            <a class="inActiveListText"
                                href="users.php?act=ustatus&id=<?php echo $user['User']['id'] ?>&st=yes">
                                Deactive</a>
                        </div>
                        <?php endif ?>
                    </td>
                    <td>
                        <a href="users.php?act=edit&uid=<?php echo $user['User']['id'] ?>" class="mx-1">
                            <img src="Assets/Icons/editUser.svg" alt="">
                        </a>
                        <a href="users.php?act=change_password&uid=<?php echo $user['User']['id'] ?>">
                            <img src="Assets/Icons/Vector.svg" alt="">
                        </a>
                    </td>
                </tr>
                <?php endforeach ?>
                <?php else: ?>
                <tr>
                    <td colspan="8">No records found.</td>
                </tr>
                <?php endif ?>
            </tbody>
        </table>

    </div>
    <?php echo $viewData->get('pageLinks') ?>
</div>
<?php $viewData->scripts(array('js/users_list.js'), array('inline'=>false)) ?>



<!-- jquery-files -->

<script>
$(document).ready(function() {
    $('#example').DataTable({
        bFilter: false,
        bInfo: false
    });
})
</script>