<?php
// echo 'yesss noooo'; die;
$objPHPExcel = new PHPExcel();
$objPHPExcel->getProperties()->setCreator("Nitin Pant")
							 ->setLastModifiedBy("Nitin Pant")
							 ->setTitle("Influencers Data")
							 ->setSubject("Influencers Data")
							/* ->setDescription("Test document for PHPExcel, generated using PHP classes.")
							 ->setKeywords("office PHPExcel php")*/
							 ->setCategory("Adlift PLB Tool");
$objPHPExcel->setActiveSheetIndex(0)
	    ->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$objPHPExcel->getActiveSheet()
	    ->setCellValue('A1', 'Influencers Report');
	    
$objPHPExcel->getActiveSheet()->mergeCells('A1:R1');
$objPHPExcel->getActiveSheet()->getStyle("A1:R1")->getFont()->setSize(20);

$objPHPExcel->setActiveSheetIndex(0)
        ->setCellValue('A3', 'Influencer Name')
        ->setCellValue('B3', 'Blog URL')
		 ->setCellValue('C3', 'Location')
        ->setCellValue('D3', 'Category')
        ->setCellValue('E3', 'Twitter Handle')
		 ->setCellValue('F3', 'Twitter Handle Influencer Price')
        ->setCellValue('G3', 'Facebook ID')
		 ->setCellValue('H3', 'Facebook ID Influencer Price')        
        ->setCellValue('I3', 'Instagram ID')
		 ->setCellValue('J3', 'Instagram ID Influencer Price')
		 ->setCellValue('K3', 'G+ ID')
        ->setCellValue('L3', 'User Name')
       
        ->setCellValue('M3', 'Special Comment')
        ->setCellValue('N3', 'Facebook Likes')
        ->setCellValue('O3', 'Twitter Followers')
        ->setCellValue('P3', 'Instagram Followers')
        ->setCellValue('Q3', 'Google Plus Followers')
        ->setCellValue('R3', 'Klout Score')
        ->setCellValue('S3', 'Domain Authority');
$objPHPExcel->getActiveSheet()->getStyle('A3:C3')->getFont()->setBold(true);
$objPHPExcel->getActiveSheet()->getStyle('D3:E3')->getFont()->setBold(true);
$objPHPExcel->getActiveSheet()->getStyle('F3:G3')->getFont()->setBold(true);
$objPHPExcel->getActiveSheet()->getStyle('H3:I3')->getFont()->setBold(true);
$objPHPExcel->getActiveSheet()->getStyle('J3:K3')->getFont()->setBold(true);
$objPHPExcel->getActiveSheet()->getStyle('L3:M3')->getFont()->setBold(true);
$objPHPExcel->getActiveSheet()->getStyle('N3:O3')->getFont()->setBold(true);
$objPHPExcel->getActiveSheet()->getStyle('P3:Q3')->getFont()->setBold(true);
$objPHPExcel->getActiveSheet()->getStyle('R3')->getFont()->setBold(true);

$influencers = $viewData->get('influencers');
$categories = array('Technology','Finance','Business','Fashion','Travel','Health','Mommy');
if(!empty($influencers)){
    $$influencersStatusArray = array('Active'=>1, 'Not Active'=>0);
    $i=4;
    foreach($influencers as $influencer){
	$objPHPExcel->getActiveSheet()
		->setCellValue('A'. $i, $influencer['Influencer']['name'])
                ->setCellValue('B'. $i, $influencer['Influencer']['blog_url'])
				->setCellValue('C'. $i, $influencer['Influencer']['location'])
                ->setCellValue('D'. $i, $categories[$influencer['Influencer']['category']])
                ->setCellValue('E'. $i, $influencer['Influencer']['twitter_handle'])
				 ->setCellValue('F'. $i, $influencer['Influencer']['twitter_handle_price'])
                ->setCellValue('G'. $i, $influencer['Influencer']['facebook_id'])
				->setCellValue('H'. $i, $influencer['Influencer']['facebook_id_price'])
				->setCellValue('I'. $i, $influencer['Influencer']['instagram_id'])
				->setCellValue('J'. $i, $influencer['Influencer']['instagram_id_price'])
                ->setCellValue('K'. $i, $influencer['Influencer']['gplus_id']) 
                ->setCellValue('L'. $i, get_user_name_by_id($influencer['Influencer']['user_id']))
                
                ->setCellValue('M'. $i, $influencer['Influencer']['comment'])
                ->setCellValue('N'. $i, $influencer['Influencer']['fb_likes'])
                ->setCellValue('O'. $i, $influencer['Influencer']['twitter_followers'])
                ->setCellValue('P'. $i, $influencer['Influencer']['instagram_followers'])
                ->setCellValue('Q'. $i, $influencer['Influencer']['gplus_followers'])
                ->setCellValue('R'. $i, $influencer['Influencer']['klout_score'])
                ->setCellValue('S'. $i, $influencer['Influencer']['domain_authority']);
	$i++;
    }
}
            
$objPHPExcel->getActiveSheet()->setTitle('Influencers');
$objPHPExcel->setActiveSheetIndex(0);
//$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
//$objWriter->save('domains_da_'.time().'.xlsx');

// Redirect output to a client�s web browser (Excel2007)
header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename="influencers_report_'.time().'.xls"');
header('Cache-Control: max-age=0');
// If you're serving to IE 9, then the following may be needed
header('Cache-Control: max-age=1');

// If you're serving to IE over SSL, then the following may be needed
header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
header ('Pragma: public'); // HTTP/1.0

$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
$objWriter->save('php://output');
exit;
?>