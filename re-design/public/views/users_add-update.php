<?php echo output_message($session->message()) ?>
<div class="row mt-4">
     <h4 class="darkBreadcrumb">
         <span class="lightBeradcrumb">Users</span> / Add Users
     </h4>
 </div>
 <script>
    function changeInputType(){
        var input = document.getElementById('UserPassword');
        if(input.type === 'text'){
            input.type = 'password';
        }else{
            input.type = 'text';
        }
    }
    function changeInputReType(){
        var input = document.getElementById('UserRetypePassword');
        if(input.type === 'text'){
            input.type = 'password';
        }else{
            input.type = 'text';
        }
    }
</script>    
 <form action="users.php?act=add" method="post" id="AddUserForm" enctype="multipart/form-data" >
 <div class="row mt-1">
     <h3 class="tableMainTitle">Add user</h3>
     <div class="col-lg-3">
         <div class="mb-3">
             <label for="ListOfPersons" class="form-label">First Name</label>
             <input type="text" class="form-control" id="ListOfPersons" placeholder="First Name" name="data[User][first_name]" id="UserFirstName" minlength="2" value="<?php echo htmlentities($_POST[data][User][first_name]) ?>" required>
         </div>
     </div>
     <div class="col-lg-3">
         <div class="mb-3">
             <label for="ListOfPersons" class="form-label">Last Name</label>
             <input type="text" class="form-control" id="ListOfPersons" placeholder="Last Name" name="data[User][last_name]"  value="<?php echo htmlentities($_POST[data][User][last_name]) ?>" id="UserLastName">
         </div>
     </div>
     <div class="col-lg-3">
         <div class="mb-3">
             <label for="ListOfPersons" class="form-label">Email</label>
             <input type="email" class="form-control" id="ListOfPersons" placeholder="Email Address" name="data[User][email_id]" id="UserEmailId" value="<?php echo htmlentities($_POST[data][User][email_id]) ?>" required>
         </div>
     </div>
     <div class="col-lg-3">
         <div class="mb-3">
             <label for="ListOfPersons" class="form-label">Password</label>
             <div class="passOuter">
             <input type="password" class="form-control" placeholder="Password" name="data[User][password]" id="UserPassword" required>
             <span>
                <img src="Assets/Icons/pass.svg" onclick="changeInputType()">
            </span>
            </div>
         </div>
     </div>
     <div class="col-lg-3">
         <div class="mb-3">
             <label for="ListOfPersons" class="form-label">Retype Password</label>
             <div class="passOuter">
             <input type="password" class="form-control" placeholder="Retype Password" name="data[User][re_password]" id="UserRetypePassword" equalTo="#UserPassword" required>
             <span>
                <img src="Assets/Icons/pass.svg" onclick="changeInputReType()">
            </span>
            </div>
         </div>
     </div>
     <div class="col-lg-3">
         <label for="ListOfPersons" class="form-label">User Type</label>
          <select name="data[User][user_type]" id="UserUserType1" class="form-select1 form-control1">
            <option value="superadmin">Super Admin</option>
            <option value="admin">Admin</option>
            <option value="user-supper" selected="selected">Supper User</option>
            <option value="user" selected="selected">User</option>
          </select>
     </div>
     <div class="col-lg-3">
         <div class="mb-3">
             <label for="ListOfPersons" class="form-label">Employ Id</label>
             <input type="text" class="form-control" id="ListOfPersons" placeholder="Employ Id" name="data[User][employ_id]" id="UserEmployId" value="<?php echo htmlentities($_POST[data][User][employ_id]) ?>" >
         </div>
     </div>
     <div class="col-lg-3">
         <div class="mb-3">
             <label for="ListOfPersons" class="form-label">Designation</label>
             <input type="text" class="form-control" id="ListOfPersons" placeholder="Designation" placeholder="Designation" name="data[User][designation]" id="UserDesignation" value="<?php echo htmlentities($_POST[data][User][designation]) ?>">
         </div>
     </div>
     <div class="col-lg-3">
         <div class="mb-3">
             <label for="ListOfPersons" class="form-label">Mobile Number</label>
             <input type="phone" class="form-control" id="ListOfPersons" placeholder="Mobile No" name="data[User][mobile_no]" id="UserMobileNo" value="<?php echo htmlentities($_POST[data][User][mobile_no]) ?>">
         </div>
     </div>
     
     <div class="col-lg-3">
         <div class="mb-3">
             <label for="ListOfPersons" class="form-label">Alternate Email</label>
             <input type="text" class="form-control" id="ListOfPersons" placeholder="Alternate Email Address" name="data[User][alternate_email]" value="<?php echo htmlentities($_POST[data][User][alternate_email]) ?>">
         </div>
     </div>

     <div class="col-lg-12">
         <div class="mb-3">
             <label for="ListOfPersons" class="form-label">Address</label>
             <textarea class="form-control" name="data[User][address]" id="UserAddress"></textarea>
         </div>
     </div>

 </div>
 <div class="row">
     <div class="col-lg-2">
         <label for="ListOfPersons" class="form-label">Upload Image</label>
         <button type="button" class="form-control btn btn-outline-danger" name="data[User][user_pic]" id="UserUserPic">Choose File</button>
     </div>
     <div class="col-lg-2 mt-4 py-3">
         <p>Upload Image <sup class="text-danger"> *</sup>
         </p>
     </div>
     <div class="col-lg-12 mt-4">
         <button type="reset" class="clearbtn">Reset</button>
         <button type="submit" class="filterbtn mx-3">Submit</button>
     </div>
 </div>
</form>
</div>
</div>
</div>
<?php $viewData->scriptStart() ?>
    jQuery(document).ready(function(){
      jQuery('#AddUserForm').validate({errorElement : 'span'});
    })
<?php $viewData->scriptEnd() ?>
 