<form action="submitted_links.php?act=do_action" method="post" id="PaymentListForm">
<input type="hidden" name="whatDo" id="PaymentWhatDo" />
<table class="table table-bordered table-striped tbl-resize sortable_tbl">
 <thead>
   <tr>
       <?php if(canUserDoThis(array('payment','link_delete'))): ?>
        <th style="width: 15px;"><span style="float:left"><input type="checkbox" id="checkAllPayment" /></span></th>
       <?php endif ?>
       <th>Campaign</th>
       <th>Domain</th>
       <th>Url</th>
       <th>Webmaster</th>
       <th>Paypal</th>
       <th>Anchor</th>
       <th>Amount</th>
       <th>User</th>
       <th>Comment</th>
       <th style="width:90px;">Submitted On</th>
       <th>Paid On</th>
       <th style="width:90px;">Pay for Mon / Yr</th>
       <th style="width:100px;"> <span style="float:left"><input type="checkbox" id="checkAllPaymentAccepted"  /></span>
                        <span style="float:left"><input type="checkbox" id="checkAllPaymentReceted" style="opacity: 1;"/></span>Status</th>
       <th>Action</th>
   </tr>
 </thead>
 <tbody> 
   <?php $submittedLinks = $viewData->get('submittedLinks') ?>
  <?php if(!empty($submittedLinks)): ?>
   <?php foreach($submittedLinks as $submittedLink): ?>
   <tr class="odd gradeX">
       <?php if(canUserDoThis(array('payment','link_delete'))): ?>
       <td>
       <?php if(canDeleteLink($submittedLink['Payment']['status'], $submittedLink['Payment']['user_id'])): ?> 
        <input type="checkbox" id="PaymentId<?php echo $submittedLink['Payment']['id'] ?>" class="_payment_id" name="data[Payment][id][]" value="<?php echo $submittedLink['Payment']['id'] ?>" />
       <?php endif ?>
       </td>
       <?php endif ?>
       <td><?php echo $submittedLink['Payment']['campaign_name'] ?></td>
       <td style="color:#DA542E;">
        <a href="<?php echo $submittedLink['Payment']['domain'] ?>" target="_blank"><?php echo $submittedLink['Payment']['domain'] ?></a> <br />
        <?php echo $submittedLink['Payment']['ip'] ?> <br />
        DA:  <?php echo round($submittedLink['Payment']['da_value'], 2) ?> <br />
        PR: <?php echo round($submittedLink['Payment']['pr_value'],2) ?>
       </td>
       <td><a href="<?php echo $submittedLink['Payment']['url'] ?>" target="_blank"><?php echo $submittedLink['Payment']['url'] ?></a></td>
       <td><?php echo $submittedLink['Payment']['client_mail'] ?></td>
       <td><?php echo $submittedLink['Payment']['paypal_email'] ?></td>
       <td><?php $anchors = explode('[', $submittedLink['Payment']['anchor_text']);
        if(!empty($anchors)):
         $anchAr = array();
         foreach($anchors as $anchr):
          $anchAr[] = "<li>$anchr</li>";
         endforeach;
         
         echo !empty($anchAr) ? "<ol>". join('', $anchAr)."</ol>" : null;
        endif;
       ?></td>
       <td><?php echo currency_format($submittedLink['Payment']['amount'], $submittedLink['Payment']['currency']) ?></td>
       <td><?php echo ucwords($submittedLink['User']['first_name'].' '.$submittedLink['User']['last_name']) ?></td>
       <td><?php echo $submittedLink['Payment']['narration_text'] ?></td>
       <td><?php echo date_to_text($submittedLink['Payment']['added_on']) ?></td>
       <td><?php echo ($submittedLink['Payment']['payment_date']!= '0000-00-00') ? date_to_text($submittedLink['Payment']['payment_date']) : '' ?></td>
       <td><?php echo $submittedLink['Payment']['payment_month'].' / '.$submittedLink['Payment']['payment_year']; ?></td>
       <td><?php
       $iconStatus = $submittedLink['Payment']['status'] == 'Paid' ? 1 : ($submittedLink['Payment']['status'] == 'Underprocess'? -1 : 0) ;
       
       $_cmpHtml = "";
       if($iconStatus=='-1'){
            if(canUserDoThis('payment')):
             $_cmpHtml .= "<span>";
             $_cmpHtml .= '<input type="checkbox" id="PaymentAccept'. $submittedLink['Payment']['id'].'_0" class="_payment_accept" name="data[Payment][accept][]" value="'.$submittedLink['Payment']['id'].'" title="Paid"/> ';
             $_cmpHtml .= '<input type="checkbox" id="PaymentReject'. $submittedLink['Payment']['id'].'_1" class="_payment_reject" name="data[Payment][reject][]" value="'.$submittedLink['Payment']['id'].'"  title="Rejected"/> ';
             $_cmpHtml .= '</span> ';
            endif;			 
       }
       if(!empty($_cmpHtml)){
        echo "<ul><li>".$_cmpHtml."</li></ul>";
       }else{
        echo getIconHtml($iconStatus, array('1'=>'Paid', '-1'=>'Underprocess'));
       }
       
       ?></td>
       <td>
        <?php if($iconStatus == -1 and canUserDoThis('payment_edit', array('user_id'=>$submittedLink['User']['id']))): ?>
        <a href="submitted_links.php?act=edit&lid=<?php echo $submittedLink['Payment']['id'] ?>" title="Click to edit link." class="tip-left"><i class="icon-edit icon-large"></i> &nbsp;</a>
        <?php endif ?>
       </td>
   </tr>
   <?php endforeach ?>
   <?php else: ?>
   <tr><td colspan="9" class="no-record-found">No records found.</td></tr>
   <?php endif ?>
 </tbody>
</table>
</form>