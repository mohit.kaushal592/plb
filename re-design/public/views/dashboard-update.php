<?php $approvalCount = $viewData->get('approvalCount') ?>
<style>
.chart,
.pie,
.bars,
#donut,
#interactive,
#placeholder,
#placeholder2 {
    height: 300px;
    max-width: 100%;
}

.col a {
    color: black;
    text-decoration: none;
}
</style>
<div class="row mt-3">
    <div class="col-lg-12">
        <div class="row">
            <div class="col my-2">
                <a href="index.php" class="card border-0 text-center d-flex align-items-center p-3">
                    <img src="Assets/Icons/Gauge.svg" width="32px" height="32px" alt="">
                    <p class="p-0 m-0 dash_section">Dashboard</p>
                </a>
            </div>
            <div class="col my-2">
                <a href="charts.php?act=approval" class="card border-0 text-center d-flex align-items-center p-3">
                    <img src="Assets/Icons/ChartBar.svg" width="32px" height="32px" alt="">
                    <p class="p-0 m-0 dash_section">Charts</p>
                </a>
            </div>
            <div class="col my-2 aprroval_section">
                <a href="approvals.php<?php echo $session->read('User.user_type')=='user' ? '?_uid='. base64_encode($session->read('User.id')) : '' ?>"
                    class="card border-0 text-center d-flex align-items-center p-3">
                    <img src="Assets/Icons/CheckSquareOffset.svg" width="32px" height="32px" alt="">
                    <p class="p-0 m-0 dash_section">Approval Listing</p>
                </a>
                <span class="label label-success" id="_goToApprovalList" style="cursor: pointer;"
                    data-href="approvals.php?_st=<?php echo base64_encode(-1) ?><?php echo $session->read('User.user_type')=='user' ? '&_uid='. base64_encode($session->read('User.id')) : '' ?>"><?php echo $approvalCount['pending'] ?></span>
            </div>
            <?php if(in_array($session->read('User.user_type'), array('superadmin','admin'))): ?>
            <div class="col my-2">
                <a href="users.php" class="card border-0 text-center d-flex align-items-center p-3">
                    <img src="Assets/Icons/Users.svg" width="32px" height="32px" alt="">
                    <p class="p-0 m-0 dash_section">User Listing</p>
                </a>
            </div>
            <div class="col my-2">
                <a href="javascript:void(0)" class="card border-0 text-center d-flex align-items-center p-3">
                    <img src="Assets/Icons/Drop.svg" width="32px" height="32px" alt="">
                    <p class="p-0 m-0 dash_section">Updated Domain PR</p>
                </a>
            </div>
            <?php endif ?>
        </div>
    </div>
</div>
<div class="row mt-4">
    <div class="col-lg-12">
        <h4 class="tableMainTitle"> Charts & Graphs </h1>
            <div class="row mt-3">
                <div class="col-lg-6">
                    <div class="card">
                        <div class="appl_bottom">
                            <img class="appl_img" src="Assets/Icons/Wallet.svg" alt="">
                            <h5 class="appl_listing">Payments</h5>
                        </div>
                        <div class="pie" id="ApprovalPieGraph"></div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="card">
                        <div class="appl_bottom">
                            <img class="appl_img" src="Assets/Icons/listing.svg" alt="">
                            <h5 class="appl_listing">Approval Listing</h5>
                            <img class="appl_img" src="Assets/Icons/appl.svg" alt="">
                        </div>
                        <div <?php if(in_array($session->read('User.user_type'), array('superadmin','admin'))): ?>
                            class="barrr2" <?php endif ?>
                            <?php if($session->read('User.user_type')=='user'): ?>class="barrr" <?php endif ?>
                            id="ApprovalBarGraph"></div>
                    </div>
                    <?php if($session->read('User.user_type')=='user'): ?>
                    <div class="card mt-2">
                        <div class="appl_bottom">
                            <img class="appl_img" src="Assets/Icons/LinkSimple.svg" alt="">
                            <h5 class="appl_listing">Approval Links</h5>
                            <img class="appl_img" src="Assets/Icons/appl.svg" alt="">

                        </div>

                        <div class="widget-content">
                            <div class="widget-section">
                                <div style="flex:1.8;"
                                    onclick="window.location='approvals.php?<?php echo $session->read('User.user_type')=='user' ? '_uid='. base64_encode($session->read('User.id')) : '' ?>';">
                                    <img src="Assets/Icons/TotalList.svg" alt="">
                                    <small class="approvalTotalTitle">Total
                                        Links</small>
                                    <strong class="approvalTotalLinks"><?php echo $approvalCount['total'] ?></strong>
                                </div>
                                <div style="flex:1;"
                                    onclick="window.location='approvals.php?_st=<?php echo base64_encode(-1) ?><?php echo $session->read('User.user_type')=='user' ? '&_uid='. base64_encode($session->read('User.id')) : '' ?>';">
                                    <img src="Assets/Icons/Pending.svg" alt="">
                                    <small class="approvalTotalTitle" style=color:#777777;>Pending</small>
                                    <strong class="approvalTotalLinks"
                                        style=color:#777777;><?php echo $approvalCount['pending'] ?></strong>

                                </div>
                            </div>
                            <div class="widget-section">
                                <div style="flex:1.8;"
                                    onclick="window.location='approvals.php?_st=<?php echo base64_encode(1) ?><?php echo $session->read('User.user_type')=='user' ? '&_uid='. base64_encode($session->read('User.id')) : '' ?>';">
                                    <img src="Assets/Icons/Approved.svg" alt="">
                                    <small class="approvalTotalTitle" style=color:#0EB229;>Approved</small>
                                    <strong class="approvalTotalLinks"
                                        style=color:#0EB229;><?php echo $approvalCount['approved'] ?></strong>
                                </div>
                                <div style="flex:1;"
                                    onclick="window.location='approvals.php?_st=<?php echo base64_encode(0) ?><?php echo $session->read('User.user_type')=='user' ? '&_uid='. base64_encode($session->read('User.id')) : '' ?>';">
                                    <img src="Assets/Icons/Rejected.svg" alt="">
                                    <small class="approvalTotalTitle" style=color:#940909;>Rejected</small>
                                    <strong class="approvalTotalLinks"
                                        style=color:#940909;><?php echo $approvalCount['rejected'] ?></strong>
                                </div>
                            </div>
                            <!-- <ul>
                                
                                <li class="bg_ly"
                                    onclick="window.location='approvals.php?<?php echo $session->read('User.user_type')=='user' ? '_uid='. base64_encode($session->read('User.id')) : '' ?>';">
                                    <i class="icon-list"></i>
                                    <strong><?php echo $approvalCount['total'] ?></strong>
                                    <small>Total
                                        Links</small>
                                </li>
                                <li class="bg_lg"
                                    onclick="window.location='approvals.php?_st=<?php echo base64_encode(1) ?><?php echo $session->read('User.user_type')=='user' ? '&_uid='. base64_encode($session->read('User.id')) : '' ?>';">
                                    <i class="icon-ok"></i>
                                    <strong><?php echo $approvalCount['approved'] ?></strong>
                                    <small>Approved</small>
                                </li>
                                <li class="bg_lo"
                                    onclick="window.location='approvals.php?_st=<?php echo base64_encode(0) ?><?php echo $session->read('User.user_type')=='user' ? '&_uid='. base64_encode($session->read('User.id')) : '' ?>';">
                                    <i class="icon-ban-circle"></i>
                                    <strong><?php echo $approvalCount['rejected'] ?></strong>
                                    <small>Rejected</small>
                                </li>
                                <li class="bg_lh"
                                    onclick="window.location='approvals.php?_st=<?php echo base64_encode(-1) ?><?php echo $session->read('User.user_type')=='user' ? '&_uid='. base64_encode($session->read('User.id')) : '' ?>';">
                                    <i class="icon-ok"></i>
                                    <strong><?php echo $approvalCount['pending'] ?></strong>
                                    <small>Pending</small>
                                </li>
                            </ul> -->
                        </div>
                    </div>








                    <!-- <div class="col-lg-2">
                        <div class="widget-box">
                            <div class="widget-title"> <span class="icon"> <i class="icon-check"></i> </span>
                                <h5>Approval Links</h5>
                            </div>
                            <div class="widget-content">
                                <ul class="site-stats">
                                    <li class="bg_ly"
                                        onclick="window.location='approvals.php?<?php echo $session->read('User.user_type')=='user' ? '_uid='. base64_encode($session->read('User.id')) : '' ?>';">
                                        <i class="icon-list"></i>
                                        <strong><?php echo $approvalCount['total'] ?></strong>
                                        <small>Total
                                            Links</small>
                                    </li>
                                    <li class="bg_lg"
                                        onclick="window.location='approvals.php?_st=<?php echo base64_encode(1) ?><?php echo $session->read('User.user_type')=='user' ? '&_uid='. base64_encode($session->read('User.id')) : '' ?>';">
                                        <i class="icon-ok"></i>
                                        <strong><?php echo $approvalCount['approved'] ?></strong>
                                        <small>Approved</small>
                                    </li>
                                    <li class="bg_lo"
                                        onclick="window.location='approvals.php?_st=<?php echo base64_encode(0) ?><?php echo $session->read('User.user_type')=='user' ? '&_uid='. base64_encode($session->read('User.id')) : '' ?>';">
                                        <i class="icon-ban-circle"></i>
                                        <strong><?php echo $approvalCount['rejected'] ?></strong>
                                        <small>Rejected</small>
                                    </li>
                                    <li class="bg_lh"
                                        onclick="window.location='approvals.php?_st=<?php echo base64_encode(-1) ?><?php echo $session->read('User.user_type')=='user' ? '&_uid='. base64_encode($session->read('User.id')) : '' ?>';">
                                        <i class="icon-ok"></i>
                                        <strong><?php echo $approvalCount['pending'] ?></strong>
                                        <small>Pending</small>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div> -->

                </div>

                <?php endif ?>
            </div>

    </div>
</div>
</div>
<hr>
<div <?php if($session->read('User.user_type')=='user'): ?> class="row main" <?php endif ?>>
    <div <?php if(in_array($session->read('User.user_type'), array('superadmin','admin'))): ?> class="col-lg-12"
        <?php endif ?> <?php if($session->read('User.user_type')=='user'): ?> class="col-lg-8" <?php endif ?>>
        <div class="widget-box1">
            <h4 class="tableMainTitle">Approval Listing</h4>
            <div class="widget-content nopadding1 table-responsive" id="LatestApprovalList">
                <p> &nbsp; No record found</p>
            </div>
        </div>

    </div>
    <?php if($session->read('User.user_type')=='user'): ?>
    <div class="col-lg-4">
        <div class="widget-box">
            <!-- <div class="widget-title" style="background: #DA542E;"> <span class="icon"> <i class="icon-th"
                        style="color: #fff;"></i> </span> -->
            <h4 class="tableMainTitle">Links Submission</h4>
            <!-- </div> -->
            <div class="widget-content nopadding1 table-responsive" id="LinkSubmissionList">
                <p> &nbsp; No record found</p>
            </div>
        </div>
    </div>
    <?php endif ?>

    <div class="row mt-4 d-none">
        <div class="col-lg-12">
            <h4> Approval Listing </h1>
                <div class="table-responsive mt-3">
                    <table class="table bg-white">
                        <thead>
                            <tr>
                                <th scope="col">Domain</th>
                                <th scope="col">PR</th>
                                <th scope="col">DA</th>
                                <th scope="col">Campaign</th>
                                <th scope="col">Date</th>
                                <th scope="col">User</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>5</td>
                                <td>5</td>
                                <td>56</td>
                                <td>Fastrack, Titan Watch</td>
                                <td>Jul 5, 2023</td>
                                <td>Sapna Gagneja</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
        </div>
    </div>
    <?php $viewData->scripts(array('js/jquery.flot.min.js','js/jquery.flot.pie.min.js','js/charts.js'), array('inline'=>false)) ?>
    <?php $viewData->scriptStart() ?>
    $(document).ready(function(){
    var LatestApprovalList = function(){
    var $where = $('#LatestApprovalList');
    var loader = '<div style="display:block;width:100%;text-align: center;"><img src="img/ajax-loader.gif" /></div>';
    $where.html(loader);
    $.get('index.php?act=approval_list', function(data){
    $where.html(data);
    }, 'html').fail(function(){
    $where.html("<div>Error occurred in request processing</div>");
    });
    }
    LatestApprovalList();
    <?php if($session->read('User.user_type')=='user'): ?>
    var LinkSubmissionList = function(){
    var $where = $('#LinkSubmissionList');
    var loader = '<div style="display:block;width:100%;text-align: center;"><img src="img/ajax-loader.gif" /></div>';
    $where.html(loader);
    $.get('index.php?act=link_submission', function(data){
    $where.html(data);
    }, 'html').fail(function(){
    $where.html("<div>Error occurred in request processing</div>");
    });
    }
    LinkSubmissionList()
    <?php endif ?>


    var ApprovalPieGraph = function(){
    var $where = $('#ApprovalPieGraph');
    var loader = '<div style="display:block;width:100%;text-align: center;"><img src="img/ajax-loader.gif" /></div>';
    $where.html(loader);
    $.get('index.php?act=pie_graph_approval', function(jsonData){
    var data = []
    $.each(jsonData, function(k, v){
    data.push({ label: k, data: v });
    });
    if(data.length>0){
    renderPieGraph(data, $where);
    }else{
    $where.html("<div>Record not found oto render graph.</div>");
    }
    }, 'json').fail(function(){
    $where.html("<div>Error occurred in request processing</div>");
    });
    }
    ApprovalPieGraph();

    var ApprovalBarGraph = function(){
    var $where = $('#ApprovalBarGraph');
    var loader = '<div style="display:block;width:100%;text-align: center;"><img src="img/ajax-loader.gif" /></div>';
    $where.html(loader);
    $.get('index.php?act=bar_graph_payment', function(jsonData){
    var dataDone = [];
    var dataPending = [];
    var xLabel = [];
    var data = [];
    var cnt = 0;
    $.each(jsonData, function(k, v){
    if(v.done){
    dataDone.push([cnt, v.done]);
    xLabel.push([cnt, k]);
    cnt = cnt+1;
    }
    if(v.pending){
    dataPending.push([cnt, v.pending]);
    xLabel.push([cnt, k]);
    cnt = cnt+1;
    }
    });
    data.push({ label: 'Done', data: dataDone});
    data.push({ label: 'Pending', data: dataPending});
    if(data.length>0){
    renderBarGraph({'data': data, 'where': $where, 'axisLabel' : {'x': 'Campaigns', 'y': 'Links'}, 'x_ticks':
    {'label':xLabel, hide:true}, 'tooltip': true});
    }else{
    $where.html("<div>Record not found oto render graph.</div>");
    }
    }, 'json').fail(function(){
    $where.html("<div>Error occurred in request processing</div>");
    });
    }
    ApprovalBarGraph();

    $('#_goToApprovalList').on('click', function(){
    var url = $(this).attr('data-href');
    if(url.length>0){
    window.location = url;
    }
    });
    });
    <?php $viewData->scriptEnd() ?>