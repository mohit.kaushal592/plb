 <h3 class="tableMainTitle mt-3"><?php echo $viewData->getTitle() ?></h3>
 <?php echo output_message($session->message()) ?>
 <?php include(WWW_ROOT.DS."layouts".DS."filter_beneficiary.php") ?>

 <div class="row mt-4">
     <div class="col-lg-12 d-flex justify-content-between">
         <h4 class="tableMainTitle">Beneficiary Listing</h4>
         <a href="javascript:void(0)" class="exportList clearbtn">Export &nbsp;<img src="Assets/Icons/exportBtn.svg"></a>
     </div>
     <div class="widget-content nopadding" id="SubmittedLinksData"></div>
     <div class="span11" id="SubmittedLinksPaging"></div>
     <?php $viewData->scriptStart() ?>
     $(function(){

     toggleAcceptRejectCheckbox();
     var SubmittedLinksData = function(data, $where, $qStr){
     var loader = '<div style="display:block;width:100%;text-align: center;"><img src="img/ajax-loader.gif" /></div>';
     $where.html(loader);

     $.post('beneficiary.php'+ ($qStr.length>0?$qStr : '?act=data'), data, function(responseData){
     $where.html(responseData.html);
     $('#SubmittedLinksPaging').html(responseData.pagination);
     $('.ttl-record h5 span').html(responseData.totalRecords);
     tblResizeInIt();
     uniformInIt($where);
     checkAllInIt();
     toggleAcceptRejectCheckbox();
     $('table.sortable_tbl').tablesorter();
     $('#SubmittedLinksPaging').find('.pagination a').bind('click', function(e){
     e.preventDefault();
     SubmittedLinksData(<?php echo json_encode($_POST) ?>, $('#SubmittedLinksData'), $(this).attr('href'));
     });
     }, 'json')
     .fail(function(){
     $where.html('<div>Error in request processing.</div>');
     });
     }
     SubmittedLinksData(<?php echo json_encode($_POST) ?>, $('#SubmittedLinksData'), '');

     $('#RowPerPage').on('multiselectcreate', function(event, ui){
     $('#RowPerPage').multiselect('destroy');
     })
     $('#RowPerPage').on('change', function(){
     var _rp = $('#RowPerPage').val();
     SubmittedLinksData(<?php echo json_encode($_POST) ?>, $('#SubmittedLinksData'), '?act=data&_rp='+_rp);
     });
     });
     jQuery(document).ready(function($){
     checkAllInIt();
     });
     <?php $viewData->scriptEnd() ?>
     <?php $viewData->scripts(array('js/beneficiary_index.js'), array('inline'=>false)) ?>
 </div>
 </div>