<script src="https://cdn.datatables.net/1.11.5/js/jquery.dataTables.min.js"></script>
<link rel="stylesheet" href="https://cdn.datatables.net/1.11.5/css/jquery.dataTables.min.css">

<style>
    .dataTables_wrapper .dataTables_paginate .paginate_button {
        padding: 0.5rem 0.75rem;
        margin-left: 5px;
        font-size: 14px;
        color: #007bff;
        border: 1px solid #007bff;
        border-radius: 4px;
        cursor: pointer;
    }

    .dataTables_wrapper .dataTables_paginate .paginate_button.current {
        background-color: #007bff;
        color: #fff;
        border-color: #007bff;
    }

    .dataTables_wrapper .dataTables_paginate .paginate_button.disabled {
        color: #6c757d;
        border-color: #6c757d;
        cursor: not-allowed;
    }

    .filter-container {
        display: flex;
        justify-content: space-between;
    }

    .filter-container select,
    .filter-container input {
        margin-left: auto;
    }

    .filter-container input {
        margin-right: 0;
    }

    .dataTables_wrapper .dataTables_paginate .paginate_button {
        padding: 0.5rem 0.75rem;
        margin-left: 5px;
        font-size: 14px;
        color: #007bff;
        border: 1px solid #007bff;
        border-radius: 4px;
        cursor: pointer;
    }
    .filter-dropdown {
        width: 60px; /* Adjust the width as needed */
    }

    .dataTables_wrapper .dataTables_paginate .paginate_button.current {
        background-color: #007bff;
        color: #fff;
        border-color: #007bff;
    }

    .dataTables_wrapper .dataTables_paginate .paginate_button.disabled {
        color: #6c757d;
        border-color: #6c757d;
        cursor: not-allowed;
    }

    .status-active {
        background-color: lightgreen;
        color: white;
        padding: 5px 10px;
        border-radius: 5px;
    }

    .status-inactive {
        background-color: lightcoral;
        color: white;
        padding: 5px 10px;
        border-radius: 5px;
    }
</style>

<div class="row mt-4">
    <h4 class="darkBreadcrumb">
        <span class="lightBeradcrumb">Campaign</span> / Campaign Lists
    </h4>
</div>

<div class="row mt-2">
    <div class="col-lg-12">
    <!-- <div class="filter-container" style="width: 40%; justify-content: flex-end;">
    <select id="countryFilter" class="filter-dropdown" style="width: 70px; margin-right: 10px;">
        <option value="">Select Country</option>
        <option value="India">India</option>
        <option value="USA">Usa</option>
    </select>

    <select id="statusFilter" class="filter-dropdown" style="width: 70px; margin-right: 10px;">
        <option value="">Select Status</option>
        <option value="activated">Active</option>
        <option value="deactivated">Inactive</option>
    </select>
    </div> -->
        <div class="table-responsive mt-3">
            <div id="loader" style="display: none;">Loading...</div>
            <table id="campaignTable" class="table bg-white table-bordered" style="width:100%">
                <thead>
                    <tr>
                        <th>Sr No</th>
                        <th>Campaign Name</th>
                        <th>Keywords</th>
                        <th>Frequency</th>
                        <th>Status</th>
                        <th>Country</th>
                        <th>Total No. of Links</th>
                        <th>Added Date</th>
                    </tr>
                </thead>
                <tbody id="campaignData">
                    <!-- Data will be loaded here by AJAX -->
                </tbody>
            </table>
        </div>
    </div>
</div>
<!-- Load jQuery -->
<script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>

<!-- Load DataTables -->
<script src="https://cdn.datatables.net/1.11.5/js/jquery.dataTables.min.js"></script>
<!-- Load jQuery -->


<script>
    var $j = jQuery.noConflict(); // Avoid conflicts between jQuery and other libraries

    $j(document).ready(function () {
        // Function to load data based on filter selection
        function loadData(statusFilter) {
            $j("#loader").show();
            $j.ajax({
                url: "https://api.adlift.ai/marketing/campaign-listings/",
                type: "GET",
                dataType: "json",
                success: function (data) {
                    var html = "";
                    var rowNumber = 1; // Initialize row number
                    $j.each(data, function (index, campaign) {
                        // Check if the status matches the filter
                        if (!statusFilter || campaign.status_act === statusFilter) {
                            html += "<tr>";
                            html += "<td>" + rowNumber++ + "</td>"; // Increment row number
                            html += "<td>" + campaign.name + "</td>";
                            html += "<td>" + renderKeywords(campaign.keywords) + "</td>";
                            html += "<td>" + campaign.frequency + "</td>";
                            html += "<td>" + renderStatus(campaign.status_act) + "</td>";
                            html += "<td>" + campaign.country + "</td>";
                            html += "<td>"  + campaign.competitors + "</td>"; // Replace with dynamic data if necessary
                            html += "<td>" + renderDate(campaign.graph_date) + "</td>";
                            html += "</tr>";
                        }
                    });
                    $j("#campaignData").html(html);
                    $j("#loader").hide();
                    // Initialize DataTables with pagination and search functionality
                    $j('#campaignTable').DataTable({
                        "paging": true,
                        "pageLength": 10,
                        "searching": true // Enable search functionality
                    });
                },
                error: function (xhr, status, error) {
                    console.log("Error:", error);
                    $j("#loader").hide();
                }
            });
        }

        // Load data initially with 'active' filter applied
        loadData('activated');

        // Event listener for filter change
        $j("#statusFilter").change(function () {
            var statusFilter = $j(this).val();
            loadData(statusFilter);
        });

        // Function to render status buttons
        function renderStatus(status) {
            if (status === 'activated') {
                return '<div class="status-active">Active</div>';
            } else if (status === 'deactivated') {
                return '<div class="status-inactive">Inactive</div>';
            }
        }

        // Function to render date
        function renderDate(date) {
            if (date) {
                return date.split('T')[0];
            } else {
                return 'null';
            }
        }

        // Function to render keywords with "Read More" functionality
        function renderKeywords(keywords) {
            if (keywords.length > 40) {
                var shortenedKeywords = keywords.substr(0, 40) + '...';
                return shortenedKeywords + '<a href="#" class="read-more">Read More</a>';
            } else {
                return keywords;
            }
        }


        // Event listener for "Read More" click
        $j('#campaignData').on('click', '.read-more', function (e) {
            e.preventDefault();
            var keywords = $j(this).prev().text(); // Get full keywords text
            $j(this).parent().html(keywords); // Replace with full keywords
        });
    });
</script>
