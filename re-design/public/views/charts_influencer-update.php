<div class="row mt-4">
     <h4 class="darkBreadcrumb">
         <span class="lightBeradcrumb">Charts</span> / Influencers Chart
     </h4>
 </div>
 <div class="row mt-4">
     <h3 class="tableMainTitle">Advance Filter</h3>
 </div>
 <?php $filterVars = $viewData->get('filterVars') ?>
 <form action="#" method="post" class="form-horizontal filter-form" id="FilterForm">
 <div class="row mt-4">
    <div class="col-lg-2">
        <div class="mb-3">
            <label for="FilterDateFrom" class="form-label">Pick Date From</label>
            <input type="date" class="form-control" name="from" value="<?php echo !empty($filterVars['dateFrom']) ? $filterVars['dateFrom'] : '' ?>" id="FilterDateFrom"  data-date-format="mm-dd-yyyy">
        </div>
    </div>
    <div class="col-lg-2">
        <div class="mb-3">
            <label for="FilterDateTo" class="form-label">To</label>
            <input type="date" class="form-control" value="<?php echo !empty($filterVars['dateTo']) ? $filterVars['dateTo'] : '' ?>" name="to" id="FilterDateTo"  data-date-format="mm-dd-yyyy">
        </div>
    </div>
    <div class="row">
    <div class="col-lg-6">
        <button type="reset" class="clearbtn">Clear</button>
        <button type="submit" class="filterbtn mx-3">Filter</button>
    </div>
    </div>
 </div>
 </form>
 <div class="row mt-4">
     <div class="col-lg-12">
         <h4> Approval Links </h4>
         <div class="widget-content nopadding">
            <div id="influencers-chart"><p style="padding-left:10px;">Record not found to render chart.</p></div>
          </div>
     </div>
 </div>

<?php $viewData->scripts(array('js/highcharts/highcharts.js', 'js/highcharts/themes/grid.js'), array('inline'=>false)) ?>
<?php $viewData->scriptStart() ?>
$(document).ready(function(){
	$('#FilterForm').bind('submit', function(e){
		e.preventDefault();
		var dateFrom = $('#FilterDateFrom').val();
		var dateTo = $('#FilterDateTo').val();
		var dateRange = (dateFrom+'_'+dateTo);
		dateRange = dateRange.replace(/^\s*\_\s*$/g, '');
		var curUrl = $.parseUrl();
		var queryString = curUrl.query;
		queryString._dt = $.base64.encode(dateRange);
		var urlParams = [];
		$.each(queryString, function(k,v){
			if(v.length>0){
				urlParams.push(k+'='+v);
			}
		});
		if(urlParams.length>=1){
			window.location = '?'+urlParams.join('&');
		}
	});
});
<?php echo $viewData->get('chart') ? $viewData->get('chart')->render("chart1") : ''; ?>
<?php $viewData->scriptEnd() ?>