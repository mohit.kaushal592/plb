<h4 style="color:#DA542E;"><?php echo $viewData->getTitle() ?></h4>
  <hr>
    <?php
    if($_GET['not_added_domains'] != '') echo ($_GET['not_added_domains'].' domains were not added because already present'); 
    if($_GET['not_added_ips'] != '') echo ($_GET['not_added_ips'].' ips were not added because already present');
    ?>
    <div class="row-fluid">
     <div class="span12">
         
         
        <div class="btn-group action-right">
          <a href="javascript:void(0)" class="btn btn-primary">Actions</a>
          <a href="javascript:void(0)" data-toggle="dropdown" class="btn btn-primary dropdown-toggle"><span class="caret"></span></a>
          <ul class="dropdown-menu">
	  
            <?php if(canUserDoThis('paypal')): ?>
	     <li><a href="javascript:void(0)" id="_DeleteExceptions" rel="delete_exceptions" class="doAct"><i class="icon icon-tint icon-large"></i> Delete Selected</a></li>
	    <?php endif ?>
	  
          </ul>
        </div>
	  
	  <div class="btn-group action-right">
	   <label>Records: 
	   <select id="RowPerPage" class="row-per-page">
	    <?php echo getFormOptions(array(10=>10, 20=>20, 30=>30, 40=>40, 50=>50, 60=>60, 70=>70, 80=>80, 90=>90, 100=>100), $_GET['_rp']); ?>
	   </select>
	   </label>
	  </div>
	  <!-- End Actions -->
	
	<div class="widget-box">
          <div class="widget-title" style="background: #DA542E;"> <span class="icon"> <i class="icon-th" style="color: #fff;"></i> </span>
            <h5 style="color: #fff;">Exceptions Listing</h5>
	    <div class="span2 ttl-record"><h5>Total Records - <?php echo $viewData->get('totalRecords') ?></h5></div>
          </div>
          <div class="widget-content nopadding">
            <form action="approvals.php?act=delete_exceptions" method="post" id="ExceptionsListForm">
	     <input type="hidden" name="whatDo" id="InfluencersWhatDo" />
	    <table class="table table-bordered table-striped tbl-resize sortable_tbl">
              <thead>
                <tr>
        <th>Select</th>
        <th>S. No.</th>
        <th>Campaign</th>
        <th>Type</th>
                </tr>
              </thead>
              <tbody> 
                <?php 
                $DomainExceptionsList = $viewData->get('ExceptionsList');
                $DomainExceptionsList = $DomainExceptionsList[0];  
                $IPExceptionsList = $viewData->get('ExceptionsList');
                $IPExceptionsList = $IPExceptionsList[1]; 
                $DomainIdsList = $viewData->get('IdsList');
                       $DomainIdsList =  $DomainIdsList[0]; 
                $IpIdsList = $viewData->get('IdsList');
                        $IpIdsList = $IpIdsList[1];
                ?>
      <?php if(!(empty($DomainExceptionsList) && empty($IPExceptionsList))): ?>
      <?php $i=1;foreach($DomainExceptionsList as $domexp): ?>
      <tr>
        <td style="color:#DA542E;">
            <input type="checkbox" name="delete_selected[]" class="" value="<?php echo $DomainIdsList[$i-1] ?>"/>
	</td>
        <td style="color:#DA542E;"><?php echo $i; ?></td>
        <td style="color:#DA542E;">
          <?php echo $domexp; ?>
        </td>
        <td style="color:#DA542E;"><?php echo 'Domain'; ?></td>
      </tr>
      <?php $j=0; $i++; endforeach; ?>
      <?php foreach($IPExceptionsList as $ipexp): ?>
      <tr>
        <td style="color:#DA542E;">
            <input type="checkbox" name="delete_selected[]" class="" value="<?php echo $IpIdsList[$j] ?>"/>
	</td>
        <td style="color:#DA542E;"><?php echo $i; ?></td>
        <td style="color:#DA542E;">
          <?php echo $ipexp; ?>
        </td>
        <td style="color:#DA542E;"><?php echo 'IP'; ?></td>
      </tr>
      <?php $i++; $j++; endforeach; ?>
      <?php else: ?>
      <tr><td colspan="7">Records not found.</td></tr>
      <?php endif ?>
              </tbody>
            </table>
	    </form>
          </div>
        </div>
         <?php echo $viewData->get('pageLinks');//var_dump($viewData); ?>
	 </div>
   </div>
    
<?php $viewData->scripts(array('js/domains_index.js'), array('inline'=>false)) ?>