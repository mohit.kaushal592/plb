<div class="row mt-4">
     <h4 class="darkBreadcrumb">
         <span class="lightBeradcrumb">Approval</span> / Exceptions Listing
     </h4>
 </div>
<div class="row mt-1">
     <h3 class="tableMainTitle">Exceptions Listing</h3>
 </div>
 <?php
    if($_GET['not_added_domains'] != '') echo ($_GET['not_added_domains'].' domains were not added because already present'); 
    if($_GET['not_added_ips'] != '') echo ($_GET['not_added_ips'].' ips were not added because already present');
?>
 <div class="row mb-4">
     <div class="col-lg-12"> 
             <table id="example" class="table table-bordered" cellspacing="0" width="100%">
                 <div class="row justify-content-end">
                     <div class="col-lg-3 mb-2">
                         <select class="form-select form-control "> 
                             <?php if(canUserDoThis('paypal')): ?>
                              <option><a href="javascript:void(0)" id="_DeleteExceptions" rel="delete_exceptions" class="doAct"><i class="icon icon-tint icon-large"></i> Delete Selected</a></option>
                              <?php endif ?>
                         </select>
                     </div>
                 </div>
                 <thead> 
                    <tr>
                      <th>Select</th>
                      <th>S. No.</th>
                      <th>Campaign</th>
                      <th>Type</th> 
                    <tr>
                 </thead>
                 <tbody> 
                <?php 
                $DomainExceptionsList = $viewData->get('ExceptionsList');
                $DomainExceptionsList = $DomainExceptionsList[0];  
                $IPExceptionsList = $viewData->get('ExceptionsList');
                $IPExceptionsList = $IPExceptionsList[1]; 
                $DomainIdsList = $viewData->get('IdsList');
                       $DomainIdsList =  $DomainIdsList[0]; 
                $IpIdsList = $viewData->get('IdsList');
                        $IpIdsList = $IpIdsList[1];
                ?>
                  <?php if(!(empty($DomainExceptionsList) && empty($IPExceptionsList))): ?>
                  <?php $i=1;foreach($DomainExceptionsList as $domexp): ?>
                  <tr>
                    <td style="color:#DA542E;">
                        <input type="checkbox" name="delete_selected[]" class="" value="<?php echo $DomainIdsList[$i-1] ?>"/>
                </td>
                    <td style="color:#DA542E;"><?php echo $i; ?></td>
                    <td style="color:#DA542E;">
                      <?php echo $domexp; ?>
                    </td>
                    <td style="color:#DA542E;"><?php echo 'Domain'; ?></td>
                  </tr>
                  <?php $j=0; $i++; endforeach; ?>
                  <?php foreach($IPExceptionsList as $ipexp): ?>
                  <tr>
                    <td style="color:#DA542E;">
                        <input type="checkbox" name="delete_selected[]" class="" value="<?php echo $IpIdsList[$j] ?>"/>
              </td>
                    <td style="color:#DA542E;"><?php echo $i; ?></td>
                    <td style="color:#DA542E;">
                      <?php echo $ipexp; ?>
                    </td>
                    <td style="color:#DA542E;"><?php echo 'IP'; ?></td>
                  </tr>
                  <?php $i++; $j++; endforeach; ?>
                  <?php else: ?>
                  <tr><td colspan="7">Records not found.</td></tr>
                  <?php endif ?>
              </tbody>
             </table>
     </div>
     <?php echo $viewData->get('pageLinks');//var_dump($viewData); ?>
 </div>
 <?php $viewData->scripts(array('js/domains_index.js'), array('inline'=>false)) ?>