<div class="row mt-4">
	<h3 class="tableMainTitle">Submitted Links Listing</h3>

<!-- <div class="row mt-4 shadow p-3 mb-5 bg-white rounded p-4">
<div class="widget-box1"> -->
         
      <div id="FilterFormBlock">
       
   <!-- </div>
  </div> -->

<?php $viewData->scriptStart() ?>
function PaypalListRender(){
	select2InIt('#FilterPaypalEmail', "paypal.php?act=email_list_json");
}
function WebmasterListRender(){
	select2InIt('#FilterClientMail', "submitted_links.php?act=webmaster_list_json");
}
function inItFilterForm(){
	multiSelectRender();
	uniformInIt();
	checkAllInIt();
	// autocomplete
	$('#FilterDomain').typeahead({
		ajax: 'domains.php?act=list_json',
		display: 'name',
		val: 'name'
	});
	$('.datepicker').datepicker();
	$('#FilterForm').on('submit', function(e){
		e.preventDefault();
		var users = $("#FilterUser").multiselect("getChecked").map(function(){
			return this.value;    
		     }).get();
		var campaigns = $("#FilterCampaign").multiselect("getChecked").map(function(){
			return this.value;    
		     }).get();
		var status = $("#FilterPaymentStatus").multiselect("getChecked").map(function(){
			return this.value;    
		     }).get();
		var payment_type = $("#Filterpayment_type").val();	
        var bencode = $("#Filterbencode").val();			
		var month = $("#FilterMonth").multiselect("getChecked").map(function(){
			return this.value;    
		     }).get();
		var year = $("#FilterYear").multiselect("getChecked").map(function(){
			return this.value;    
		     }).get();
                //Start: Added by Jitendra: 29-Nov-2014
                var payment_month = $("#FilterPaymentMonth").multiselect("getChecked").map(function(){
			return this.value;    
		     }).get();
		var payment_year = $("#FilterPaymentYear").multiselect("getChecked").map(function(){
			return this.value;    
		     }).get();
                //End: Added by Jitendra: 29-Nov-2014
		var client_mail = $("#FilterClientMail").select2("val"); 
		var paypal_email = $("#FilterPaypalEmail").select2("val"); 
		var pr_op = $("#FilterPrOperator").multiselect("getChecked").map(function(){
			return this.value;    
		     }).get()[0];
		var pr_vl = $("#FilterPrValue").multiselect("getChecked").map(function(){
			return this.value;    
		     }).get()[0];
		var da_op = $("#FilterDaOperator").multiselect("getChecked").map(function(){
			return this.value;    
		     }).get()[0];
		var da_vl = $("#FilterDaValue").multiselect("getChecked").map(function(){
			return this.value;    
		     }).get()[0];
		
		var exportDta = $('#exportList').val();
		var exportDtaBank = $('#exportListBank').val();
		var dateFrom = $('#FilterDateFrom').val();
		var dateTo = $('#FilterDateTo').val();
		var dateRange = (dateFrom+'_'+dateTo);
		dateRange = dateRange.replace(/^\s*\_\s*$/g, '');
           //Start: Added by Ajit : 23 june 2017 
        var dateFromSubmted = $('#FilterDateFromSubmted').val();
		var dateToSubmted = $('#FilterDateToSubmted').val();
		var dateRangeSubmted = (dateFromSubmted+'_'+dateToSubmted);
		dateRangeSubmted = dateRangeSubmted.replace(/^\s*\_\s*$/g, '');
       //End: Added by Ajit : 23 june 2017 	
	   console.log('1__')
		var _prOp='', _prVl='', _daOp='', _daVl='';
		if(pr_vl>0){
			_prVl=pr_vl;
			_prOp=pr_op;
		}
		if(da_vl>0){
			_daVl=da_vl;
			_daOp=da_op;
		}
		var domain = $('#FilterDomain').val();

		var curUrl = $.parseUrl();
		var queryString = curUrl.query;
		queryString._uid = $.base64.encode(users.toString());
		queryString._camp = $.base64.encode(campaigns.toString());
		queryString._st = $.base64.encode(status.toString());
		queryString._pst = $.base64.encode(payment_type);
		queryString._bc = $.base64.encode(bencode);
		queryString._m = $.base64.encode(month.toString());
		queryString._y = $.base64.encode(year.toString());
                //Start: Added by Jitendra: 29-Nov-2014
		queryString._pm = $.base64.encode(payment_month.toString());
		queryString._py = $.base64.encode(payment_year.toString());
                //End: Added by Jitendra: 29-Nov-2014
		queryString._dom = $.base64.encode(domain);
		queryString._ce = $.base64.encode(client_mail.toString());
		queryString._pe = $.base64.encode(paypal_email.toString());
		queryString._prOp = $.base64.encode(_prOp);
		queryString._prVl = $.base64.encode(_prVl);
		queryString._daOp = $.base64.encode(_daOp);
		queryString._daVl = $.base64.encode(_daVl);
		queryString._export = $.base64.encode(exportDta);
		queryString._exportBank = $.base64.encode(exportDtaBank);
		queryString._dt = $.base64.encode(dateRange);
		queryString._dtstb = $.base64.encode(dateRangeSubmted);
		var urlParams = [];
		
		var postFormHtml = [];
		$.each(queryString, function(k,v){
			if(v.length>0){
				urlParams.push(k+'='+v);
				postFormHtml.push("<input type='hidden' name='"+k+"' value='"+v+"' />");
			}
		});
		
		/*if(urlParams.length>=1){
			window.location = '?'+urlParams.join('&');
		}*/
		
		if(postFormHtml.length>0){
			
			if(parseInt(exportDtaBank)==1)
			{
			var urlToSent = parseInt(exportDtaBank)==1? 'submitted_links.php?act=data' : $(this).attr('action'); 
			}
			else{
				var urlToSent = parseInt(exportDta)==1? 'submitted_links.php?act=data' : $(this).attr('action'); 
			}	
	 			
			var formFilter = $('<form acttion="'+ urlToSent +'" method="post"></form>');
			formFilter.attr('action', urlToSent);
			$('body').append(formFilter);
			formFilter.html(postFormHtml.join(''));
			formFilter.submit();
		}else{
			alert('Choose any option for filter.');
		}
	});
}
$(document).ready(function(){
	var postData = <?php echo json_encode($viewData->get('filterVars')) ?>;
	var loader = '<div style="display:block;width:100%;text-align: center;"><img src="img/ajax-loader.gif" /></div>';
	$('#FilterFormBlock').html(loader);
	$.post('submitted_links.php?act=filter_form',
		{'filterVars':postData},
		function(data){
			$('#FilterFormBlock').html(data);
			WebmasterListRender();
			PaypalListRender();
			inItFilterForm();
		}, 'html')
		.fail(function(){
			$('#FilterFormBlock').html('<div>Error occurred in filter form processing.</div>');
		});
});
<?php $viewData->scriptEnd() ?>

</div>
<div class="row mb-4">
    <div class="col-lg-12">
		  	<div id="SubmittedLinksData"></div>
			<div id="SubmittedLinksPaging"></div>
		<?php $viewData->scriptStart() ?>
		$(function(){
			
			
			toggleAcceptRejectCheckbox();
		var SubmittedLinksData = function(data, $where, $qStr){
		var loader = '<div style="display:block;width:100%;text-align: center;"><img src="img/ajax-loader.gif" /></div>';
		$where.html(loader);
		
		$.post('submitted_links.php'+ ($qStr.length>0?$qStr : '?act=data'), data, function(responseData){
			$where.html(responseData.html);
			$('#SubmittedLinksPaging').html(responseData.pagination);
			$('.ttl-record h5 span').html(responseData.totalRecords);
			tblResizeInIt();
			uniformInIt($where);
			checkAllInIt();
			toggleAcceptRejectCheckbox();
			$('table.sortable_tbl').tablesorter();
			$('#SubmittedLinksPaging').find('.pagination a').bind('click', function(e){
			e.preventDefault();
			SubmittedLinksData(<?php echo json_encode($_POST) ?>, $('#SubmittedLinksData'), $(this).attr('href'));
			});
		}, 'json')
		.fail(function(){
		$where.html('<div>Error in request processing.</div>');
		});
		}
		SubmittedLinksData(<?php echo json_encode($_POST) ?>, $('#SubmittedLinksData'), '');
		
		$('#RowPerPage').on('multiselectcreate', function(event, ui){
		$('#RowPerPage').multiselect('destroy');
		})
		$('#RowPerPage').on('change', function(){
			var _rp = $('#RowPerPage').val();
			SubmittedLinksData(<?php echo json_encode($_POST) ?>, $('#SubmittedLinksData'), '?act=data&_rp='+_rp);
		});
		});
		jQuery(document).ready(function($){
			checkAllInIt();
			});
		<?php $viewData->scriptEnd() ?>
		<?php $viewData->scripts(array('js/submitted_links_index.js'), array('inline'=>false)) ?>
	</div>
	</div> 