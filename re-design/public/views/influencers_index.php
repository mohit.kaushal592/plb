<h4 style="color:#DA542E;"><?php echo $viewData->getTitle() ?></h4>
  <hr>
    <?php echo $session->message; ?>
    <div class="row-fluid">
     <div class="span12">
        <div class="btn-group action-right">
          <a href="javascript:void(0)" class="btn btn-primary">Actions</a>
          <a href="javascript:void(0)" data-toggle="dropdown" class="btn btn-primary dropdown-toggle"><span class="caret"></span></a>
          <ul class="dropdown-menu">
	  
            <?php if(canUserDoThis('paypal')): ?>
	     <li><a href="javascript:void(0)" id="_DeleteInfluencer" rel="delete_influencer" class="doAct"><i class="icon icon-tint icon-large"></i> Delete Selected</a></li>
	    <?php endif ?>
	  
	    <li><a id="_ExportToExcel" href="javascript:void(0)"><i class="icon-download-alt icon-large"></i> Export</a></li>
          </ul>
        </div>
	  
	  <div class="btn-group action-right">
	   <label>Records: 
	   <select id="RowPerPage" class="row-per-page">
	    <?php echo getFormOptions(array(10=>10, 20=>20, 30=>30, 40=>40, 50=>50, 60=>60, 70=>70, 80=>80, 90=>90, 100=>100), $_GET['_rp']); ?>
	   </select>
	   </label>
	  </div>
	  <!-- End Actions -->
	
	<div class="widget-box">
          <div class="widget-title" style="background: #DA542E;"> <span class="icon"> <i class="icon-th" style="color: #fff;"></i> </span>
            <h5 style="color: #fff;">Influencers Listing</h5>
	    <div class="span2 ttl-record"><h5>Total Records - <?php echo $viewData->get('totalRecords') ?></h5></div>
          </div>
          <div class="widget-content nopadding">
            <form action="influencers.php?act=delete_influencer" method="post" id="InfluencersListForm">
	     <input type="hidden" name="whatDo" id="InfluencersWhatDo" />
	    <table class="table table-bordered table-striped tbl-resize sortable_tbl">
              <thead>
                <tr>
		 <?php if(canUserDoThis('paypal')): ?>
		 <th style="width: 50px;">Select</th>
		 <?php  endif ?>
                  <th>Influencer Name</th>
                  <th>Blog URL</th>
				   <th>Location</th>
		  <th>Category</th>
                  <th>Twitter Handle</th>
				  <th>Twitter Handle Influencer Price</th>
                  <th>Facebook ID</th>  
                  <th>Facebook ID Influencer Price</th>  				  
                  <th>Instagram ID</th>  
                  <th>Instagram ID Influencer Price</th>				  
                  <th>User Name</th>
                 
                  <th>Comment</th>
                  <th>Added On</th>
                </tr>
              </thead>
              <tbody> 
                <?php $influencers = $viewData->get('influencers') ?>
               <?php if(!empty($influencers)): ?>
	        <?php foreach($influencers as $influencer): ?>
                <tr class="odd gradeX">
		 <?php if(canUserDoThis('paypal')): ?>
		 <td>
		  <input type="checkbox" name="data[Influencer][delete][]" class="_delete_influencer" value="<?php echo $influencer['Influencer']['id'] ?>"/>
		 </td>
		 <?php  endif ?>
                  <td>   <?php echo $influencer['Influencer']['name'] ?> </td>
                  
                  <?php
                  $categories = array('Technology','Finance','Business','Fashion','Travel','Health','Mommy');
            
                  $cat_name = $categories[$influencer['Influencer']['category']];
                  ?>
                  
                  <td><?php echo $influencer['Influencer']['blog_url'] ?></td>
				  <td><?php echo $influencer['Influencer']['location'] ?></td>
                  <td><?php echo $cat_name ?></td>
                  <td><?php echo $influencer['Influencer']['twitter_handle'] ?></td>
				  <td><?php echo $influencer['Influencer']['twitter_handle_price'] ?></td>
                  <td><?php echo $influencer['Influencer']['facebook_id'] ?></td>  
<td><?php echo $influencer['Influencer']['facebook_id_price'] ?></td>				  
                  <td><?php echo $influencer['Influencer']['instagram_id'] ?></td>  
<td><?php echo $influencer['Influencer']['instagram_id_price'] ?></td>				  
                  <td><?php echo get_user_name_by_id($influencer['Influencer']['user_id']) ?></td>
                  
                  <td><?php echo $influencer['Influencer']['comment'] ?></td>
                  <td><?php echo $influencer['Influencer']['added_on'] ?></td>
                  
                </tr>
                <?php endforeach ?>
		<?php else: ?>
		<tr><td colspan="9" class="no-record-found">No records found.</td></tr>
		<?php endif ?>
              </tbody>
            </table>
	    </form>
          </div>
        </div>
        <?php echo $viewData->get('pageLinks') ?>
	 </div>
   </div>
    
    
    <?php $viewData->scripts(array('js/domains_index.js'), array('inline'=>false)) ?>
	
	<?php $viewData->scriptStart() ?>
	function __submitApprovalActForm(){
    var $form = $('#InfluencersListForm');
    $('.loader_box_overlay').show();
        var $postForm = $.post( $form.attr('action'), $form.serialize(), function(data) {
        if(data.status=='success'){
         window.location = window.location;
        }else{
            alert(data.msg);
        }
    }, "json");
    $postForm.done(function() {
        $('.loader_box_overlay').hide();
    }).fail(function(){
        $('.loader_box_overlay').hide();
        alert('error in form processing.');
    });
 }
 
	$(function(){ 
	$('a.doAct').bind('click', function(e){
    e.preventDefault();
    var actType = $.trim($(this).attr('rel'));
	
		if(actType=='delete_influencer'){
        var linkCount = $('._delete_influencer:checked').length;
        if(linkCount>0){
            if (confirm('Do you want to delete selected links?')) {
                $('#ApprovalWhatDo').val(actType);
                __submitApprovalActForm();    
            }
        }else{
            alert('Please select atleast one link to process.');
        }
	}
});
});
	<?php $viewData->scriptEnd() ?>