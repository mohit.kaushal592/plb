 <div class="row mt-4">
     <h4 class="darkBreadcrumb"><span class="lightBeradcrumb">Influencers</span> / Influencer List</h4>
 </div>
 <div class="row">
     <div class="col-lg-10">
         <h3 class="tableMainTitle">
             Influencer Listing
         </h3>
     </div>
     <div class="table-responsive mt-3">
         <form action="influencers.php?act=delete_influencer" method="post" id="InfluencersListForm">
             <input type="hidden" name="whatDo" id="InfluencersWhatDo" />
             <div class="table-responsive mt-3">
                 <table class="table table-bordered bg-white">
                     <thead>
                         <tr>
                             <?php if(canUserDoThis('paypal')): ?>
                             <th style="width: 50px;">Select</th>
                             <?php  endif ?>
                             <th>Influencer Name</th>
                             <th>Blog URL</th>
                             <th>Location</th>
                             <th>Category</th>
                             <th>Twitter Handle</th>
                             <th>Twitter Handle Influencer Price</th>
                             <th>Facebook ID</th>
                             <th>Facebook ID Influencer Price</th>
                             <th>Instagram ID</th>
                             <th>Instagram ID Influencer Price</th>
                             <th>User Name</th>

                             <th>Comment</th>
                             <th>Added On</th>
                         </tr>
                     </thead>
                     <tbody>
                         <?php $influencers = $viewData->get('influencers') ?>
                         <?php if(!empty($influencers)): ?>
                         <?php foreach($influencers as $influencer): ?>
                         <tr class="odd gradeX">
                             <?php if(canUserDoThis('paypal')): ?>
                             <td>
                                 <input type="checkbox" name="data[Influencer][delete][]" class="_delete_influencer"
                                     value="<?php echo $influencer['Influencer']['id'] ?>" />
                             </td>
                             <?php  endif ?>
                             <td> <?php echo $influencer['Influencer']['name'] ?> </td>

                             <?php
                  $categories = array('Technology','Finance','Business','Fashion','Travel','Health','Mommy');
            
                  $cat_name = $categories[$influencer['Influencer']['category']];
                  ?>

                             <td><?php echo $influencer['Influencer']['blog_url'] ?></td>
                             <td><?php echo $influencer['Influencer']['location'] ?></td>
                             <td><?php echo $cat_name ?></td>
                             <td><?php echo $influencer['Influencer']['twitter_handle'] ?></td>
                             <td><?php echo $influencer['Influencer']['twitter_handle_price'] ?></td>
                             <td><?php echo $influencer['Influencer']['facebook_id'] ?></td>
                             <td><?php echo $influencer['Influencer']['facebook_id_price'] ?></td>
                             <td><?php echo $influencer['Influencer']['instagram_id'] ?></td>
                             <td><?php echo $influencer['Influencer']['instagram_id_price'] ?></td>
                             <td><?php echo get_user_name_by_id($influencer['Influencer']['user_id']) ?></td>

                             <td><?php echo $influencer['Influencer']['comment'] ?></td>
                             <td><?php echo $influencer['Influencer']['added_on'] ?></td>

                         </tr>
                         <?php endforeach ?>
                         <?php else: ?>
                         <tr>
                             <td colspan="9" class="no-record-found">No records found.</td>
                         </tr>
                         <?php endif ?>
                     </tbody>
                 </table>
             </div>
         </form>
     </div>
     <?php echo $viewData->get('pageLinks') ?>
 </div>


 <?php $viewData->scripts(array('js/domains_index.js'), array('inline'=>false)) ?>

 <?php $viewData->scriptStart() ?>
 function __submitApprovalActForm(){
 var $form = $('#InfluencersListForm');
 $('.loader_box_overlay').show();
 var $postForm = $.post( $form.attr('action'), $form.serialize(), function(data) {
 if(data.status=='success'){
 window.location = window.location;
 }else{
 alert(data.msg);
 }
 }, "json");
 $postForm.done(function() {
 $('.loader_box_overlay').hide();
 }).fail(function(){
 $('.loader_box_overlay').hide();
 alert('error in form processing.');
 });
 }

 $(function(){
 $('a.doAct').bind('click', function(e){
 e.preventDefault();
 var actType = $.trim($(this).attr('rel'));

 if(actType=='delete_influencer'){
 var linkCount = $('._delete_influencer:checked').length;
 if(linkCount>0){
 if (confirm('Do you want to delete selected links?')) {
 $('#ApprovalWhatDo').val(actType);
 __submitApprovalActForm();
 }
 }else{
 alert('Please select atleast one link to process.');
 }
 }
 });
 });
 <?php $viewData->scriptEnd() ?>