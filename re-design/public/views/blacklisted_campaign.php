<table class="table table-bordered table-striped">
    <tr>
        <td colspan="6" class="tbl-caption">Campaigns</td>
    </tr>
    <tr>
        <th style="text-align: left;">Name</th>
        <th>Black Listed On</th>
    </tr>
    <?php $campaigns = $viewData->get('campaigns'); ?>
    <?php if(!empty($campaigns)): ?>
        <?php foreach($campaigns as $_campaign): ?>
            <tr>
                <td><?php echo $_campaign['Campaign']['name'] ?></td>
                <td style="text-align: center"><?php echo date_to_text($_campaign['BlacklistDomain']['created']) ?></td>
            </tr>
        <?php endforeach ?>
    <?php else: ?>
    <tr><td colspan="6">Campaigns not found.</td></tr>
    <?php endif ?>
</table>