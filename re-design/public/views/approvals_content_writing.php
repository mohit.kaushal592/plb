<h4 style="color:#DA542E;"><?php echo $viewData->getTitle() ?></h4>
  <hr>
    <?php echo output_message($session->message()) ?>
    <div class="row-fluid">
     <div class="span12">
      <!-- Filter Box -->
      <?php //include(WWW_ROOT.DS."layouts".DS."filter_approvals.php") ?>
      <!-- Filter Box End -->
      <!-- Actions -->
     
	  
	  <!-- End Actions -->
        <div class="widget-box">
	  <div class="widget-title" style="background: #DA542E;"> <span class="icon"> <i class="icon-th" style="color: #fff;"></i> </span>
            <h5 style="color: #fff;">Approval Listing</h5>
	    <div class="span2 ttl-record"><h5>Total Records - <?php echo $viewData->get('totalRecords') ?></h5></div>
          </div>
          <div class="widget-content nopadding">
            <form action="approvals.php?act=do_action" method="post" id="ApprovalListForm">
	     <input type="hidden" name="whatDo" id="ApprovalWhatDo" />
	     <table class="table table-bordered table-striped tbl-resize approvals_list_tbl sortable_tbl">
	       <thead>

 <tr>
		   <th width="10%">Keyword</th>
		   <th width="10%">SV</th>
		   <th width="10%">Rank</th>
		   <th width="40%">Title</th>
		   <th width="20%">Domain</th>
		   <th width="10%">Action</th>
		 </tr>

	       	<!--
		 <tr>
		   <th style="width: 206px;">
		    <?php if(canUserDoThis(array('forward','approval_delete'))): ?>
		     <span style="float:left"><input type="checkbox" id="checkAllApproval" /></span>
		    <?php endif ?>
		    Domain
		   </th>
		   <th style="width: 25px;">PR</th>
		   <th style="width: 25px;">DA</th>
		   <th style="width: 25px;">SC</th>
		   <th style="width: 30px;">Country</th>
		   <th>User</th>
		   <th>Date</th>
		   <th style="width: 25px;">Count</th>
		   <th style="width: 225px;"> <span style="float:left"><input type="checkbox" id="checkAllApprovalAccepted" /></span>
                        <span style="float:left"><input type="checkbox" id="checkAllApprovalReceted" /></span>
                       Campaign</th>
		   <th>Text</th>
		   <th>Admin Text</th>
		   <th>Content Writer</th>
		   <th>Action</th>
		 </tr>
-->

	       </thead>
	       <tbody> 
			<tr><td>
				 <?php $contents = $viewData->get('contents');
				 //print_r($contents);
				  ?>
			</td></tr>

<?php if(!empty($contents)){ 

foreach($contents as $ckey=>$cval){

	$keyword = $cval['manager']['seo']['keyword_sets']['0']['keywords'];
	$sv =  $cval['manager']['seo']['keyword_sets']['0']['sv'];
	$rank = $cval['manager']['seo']['keyword_sets']['0']['rank'];
	$title = $cval['title'];
	$target_url = $cval['manager']['seo']['keyword_sets']['0']['target_url'];
?>
			<tr>
		   <th width="10%"><?php echo $keyword; ?></th>
		   <th width="10%"><?php echo $sv; ?></th>
		   <th width="40%"><?php echo $rank;?></th>
		   <th width="10%"><?php echo $title; ?></th>
		   <th width="20%"><?php echo $target_url;?></th>
		   <th>
			<?php $querystr = "act=add_link&keyword=".$keyword."&sv=".$sv."&rank=".$rank."&title=".$title."&target_url=".$target_url; ?>

		   	<a href="/plb/app.php?<?php echo $querystr; ?>"><i class="icon-plus icon-large"></i></a></th>
		 </tr>
	<?php
}

 }else{ ?>
		 <tr><td colspan="10" class="no-record-found">No records found.</td></tr>
		<?php } ?>
		</pre>

	       </tbody>
	     </table>
	    </form>
          </div>
        </div>
        <?php //echo $viewData->get('pageLinks') ?>
	 </div>
     
     <div id="AdminCommentAdd" class="modal fade bs-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
     <div class="modal-header">
	<button type="button" class="close" data-dismiss="modal">&times;</button>
	<h3>Admin Comment <span></span></h3>
      </div>
      <div class="modal-body">
      </div>
      <div class="modal-footer">
       <a href="#" class="btn" data-dismiss="modal">Cancel</a>
       <a href="javascript:void(0)" id="admin-add-comment" class="btn btn-primary">Submit</a>
      </div>
    </div>
  </div>
     
   </div>
    
<?php $viewData->scripts(array('js/approvals_list.js'), array('inline'=>false)) ?>