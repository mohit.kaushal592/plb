<div class="row mt-4">
     <h4 class="darkBreadcrumb">
         <span class="lightBeradcrumb">Influencers</span> / Add New
     </h4>
 </div>
<div class="row mt-1">
    <h3 class="tableMainTitle"><?php echo $viewData->getTitle() ?></h3>
</div>

<form action="" class="form-horizontal"  method="post" id="AddDomainForm">
    <div class="row mt-4 shadow p-3 mb-5 bg-white rounded ">
        <div class="col-lg-4">
            <label class="form-label">Influencer Name :</label>
            <div class="mb-3">
                <input name="data[Influencer][name]" id="InfluencerName"   style="resize: none;" placeholder="Enter influencer name" class="form-control" required="required" />
            </div>
        </div>
        <?php //include(WWW_ROOT . DS . "layouts" . DS . "add_influencers_domain.php") ?> 

        <div class="col-lg-4">

            <label class="form-label">Domain : </label>
            <div class="mb-3">
                <input name="data[Influencer][influencer_domain]" placeholder="Type to search domain" autocomplete="off" id="FilterDomain" type="text" class="form-control" value="<?php echo!empty($filterVars['domain']) ? $filterVars['domain'] : '' ?>" class="typeahead" />
                <input type="hidden" name="data[Influencer][domain_ids]" id="influencer_domain_id" value="" />
            </div>
        </div>
        <div class="col-lg-4">
            <label class="form-label">Influencer Location :</label>
            <div class="mb-3">
                <input name="data[Influencer][location]" id="InfluencerLoc"   style="resize: none;" placeholder="Enter influencer Location" class="form-control" required="required" />
            </div>
        </div>

        <div class="col-lg-4">
            <label class="form-label" for="InfluencerCategory">Influencer Category :</label>
            <div class="mb-3">
                <select name="data[Influencer][category]" id="InfluencerCat" class="" required="required" >
                    <option value="0" disabled selected="">Please select</option>
                    <?php
                    $categories = $viewData->get('categories');
                    $i = 1;
                    foreach ($categories as $cat) {

                        echo '<option value="' . $i . '">' . $cat . '</option>';
                        $i++;
                    }
                    ?>

                </select>
            </div>
        </div>
        <div class="col-lg-4">
            <label class="form-label">Blog URL :</label>
            <div class="mb-3">
                <input name="data[Influencer][blog_url]" id="InfluencerUrl"   style="resize: none;" placeholder="Enter influencer Blog URL" class="form-control" required="required" />
            </div>
        </div>

        <div class="col-lg-4">
            <label class="form-label">Twitter Handle :</label>
            <div class="mb-3">
                <input name="data[Influencer][twitter_handle]" id="InfluencerHandle"   style="resize: none;" placeholder="Enter influencer twitter handle" class="form-control" required="required" />
            </div>
        </div>
        <div class="col-lg-4">
            <label class="form-label">Twitter Handle Influencer Price :</label>
            <div class="mb-3">
                <input name="data[Influencer][twitter_handle_price]" type="number" id="InfluencerHandlePrice"   style="resize: none;" placeholder="Enter influencer twitter handle Influencer Price" class="form-control" required="required" />
            </div>
        </div>   
        <div class="col-lg-4">
            <label class="form-label">Facebook ID :</label>
            <div class="mb-3">
                <input name="data[Influencer][facebook_id]" id="InfluencerFid"   style="resize: none;" placeholder="Enter influencer Facebook ID" class="form-control" required="required" />
            </div>
        </div>
        <div class="col-lg-4">
            <label class="form-label">Facebook ID Influencer Price:</label>
            <div class="mb-3">
                <input name="data[Influencer][facebook_id_price]" type="number" id="InfluencerFidPrice"   style="resize: none;" placeholder="Enter influencer Facebook ID Influencer Price" class="form-control" required="required" />
            </div>
        </div>
        

        <div class="col-lg-4">
            <label class="form-label">Instagram ID :</label>
            <div class="mb-3">
                <input name="data[Influencer][instagram_id]" id="InfluencerIid"   style="resize: none;" placeholder="Enter influencer instagram ID" class="form-control" required="required" />
            </div>
        </div>

        <div class="col-lg-4">
            <label class="form-label">Instagram ID Influencer Price :</label>
            <div class="mb-3">
                <input name="data[Influencer][instagram_id_price]" type="number" id="InfluencerIidPrice"   style="resize: none;" placeholder="Enter influencer instagram ID influencer Price" class="form-control" required="required" />
            </div>
        </div>

        <div class="col-lg-4">
            <label class="form-label">Google Plus ID :</label>
            <div class="mb-3">
                <input name="data[Influencer][gplus_id]" id="InfluencerGid"   style="resize: none;" placeholder="Enter influencer G+ ID" class="form-control" required="required" />
            </div>
        </div>


        <div class="col-lg-4">
            <label class="form-label">Special Comments :</label>
            <div class="mb-3">
                <textarea name="data[Influencer][comment]" id="InfluencerComment" rows="6"  style="resize: none;" placeholder="Enter special comments" class="form-control" ></textarea>
            </div>
        </div>     
  

        <div class="col-lg-12">
            <button type="reset" class="clearbtn">Reset</button>
            <button type="submit" class="filterbtn">Add Influencer</button>
        </div>
    </div>
</form>

<?php //$viewData->scripts(array('js/domains_add.js'), array('inline' => false)) ?>

