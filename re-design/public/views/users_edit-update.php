<?php 
$user =$viewData->get('user');
//Added By Arvind
if($user['User']['user_type'] == 'user' && $user['User']['supper_user']== 1)
{
    $user['User']['user_type'] = 'user-supper';
}
//Ended By Arvind
?>
<div class="row mt-4">
    <h4 class="darkBreadcrumb">
        <span class="lightBeradcrumb">Users</span> / Edit User
    </h4>
</div>
<?php echo output_message($session->message()) ?>
<h3 class="tableMainTitle"><?php echo $viewData->getTitle() ?> -
    <?php echo ucwords($user[User][first_name].' '.$user[User][last_name]) ?>
</h3>

<form action="users.php?act=edit" class="form-horizontal" method="post" id="AddUserForm" enctype="multipart/form-data">
    <input type="hidden" name="data[User][id]" value="<?php echo $user[User][id] ?>" />
    <div class="row mt-1">
        <div class="col-lg-3">
            <div class="mb-3">
                <label for="UserFirstName" class="form-label">First Name :</label>
                <input type="text" class="form-control" placeholder="First Name" name="data[User][first_name]"
                    id="UserFirstName" minlength="2" value="<?php echo htmlentities($user[User][first_name]) ?>"
                    required />
            </div>
        </div>

        <div class="col-lg-3">
            <div class="mb-3">
                <label class="form-label" for="UserLastName">Last Name :</label>
                <input type="text" class="form-control" placeholder="Last Name" name="data[User][last_name]"
                    value="<?php echo htmlentities($user[User][last_name]) ?>" id="UserLastName" />

            </div>
        </div>
        <div class="col-lg-3">
            <div class="mb-3">
                <label class="form-label" for="UserEmailId">Email :</label>
                <input type="email" class="form-control" placeholder="Email Address"
                    <?php echo $session->read('User.user_type') == 'superadmin' ? 'name="data[User][email_id]"' : 'readonly="readonly"' ?>
                    id="UserEmailId" value="<?php echo htmlentities($user[User][email_id]) ?>" />
            </div>
        </div>
        <div class="col-lg-3">
            <div class="mb-3">
                <label class="form-label" for="UserUserType">User Type :</label>
                <?php if($session->read('User.user_type') == 'superadmin'): ?>
                <select name="data[User][user_type]" id="UserUserType">
                    <?php //echo getFormOptions(array('superadmin'=>'Super Admin', 'admin'=>'Admin','user'=>'User'), $user[User][user_type]) ?>
                    <?php echo getFormOptions(array('superadmin'=>'Super Admin', 'admin'=>'Admin','user'=>'User','user-supper'=>'Supper User'), $user[User][user_type]) ?>
                </select>
                <?php else: ?>
                <input type="text" value="<?php echo $user[User][user_type] ?>" readonly="readonly" />
                <?php endif ?>
            </div>
        </div>
        <div class="col-lg-3">
            <div class="mb-3">
                <label class="form-label" for="UserEmployId">Employ Id :</label>
                <input type="text" class="form-control" placeholder="Employ Id" name="data[User][employ_id]"
                    id="UserEmployId" value="<?php echo htmlentities($user[User][employ_id]) ?>" />
            </div>
        </div>
        <div class="col-lg-3">
            <div class="mb-3">
                <label class="form-label" for="UserDesignation">Designation :</label>
                <input type="text" class="form-control" placeholder="Designation" name="data[User][designation]"
                    id="UserDesignation" value="<?php echo htmlentities($user[User][designation]) ?>" required />
            </div>
        </div>
        <div class="col-lg-3">
            <div class="mb-3">
                <label class="form-label" for="UserMobileNo">Mobile No :</label>
                <input type="phone" class="form-control" placeholder="Mobile No" name="data[User][mobile_no]"
                    id="UserMobileNo" value="<?php echo htmlentities($user[User][mobile_no]) ?>" required />
            </div>
        </div>

        <div class="col-lg-3">
            <div class="mb-3">
                <label class="form-label" for="UserAlternateEmail">Alternate Email :</label>
                <input type="email" class="form-control" placeholder="Alternate Email Address"
                    name="data[User][alternate_email]" id="UserAlternateEmail"
                    value="<?php echo htmlentities($user[User][alternate_email]) ?>" />
            </div>
        </div>
        <div class="col-lg-3">
            <div class="mb-3">
                <label class="form-label" for="UserUserPic">Profile Picture :</label>
                <input type="file" class="form-control btn btn-outline-danger" name="data[User][user_pic]"
                    id="UserUserPic" />
                <span class="help-block">Please select picture size 128 x 128</span>
            </div>
        </div>
        <div class="col-lg-3">
            <div class="mb-3">
                <label class="form-label" for="UserAddress">Address :</label>
                <textarea class="form-control" name="data[User][address]"
                    id="UserAddress"><?php echo $user[User][address] ?></textarea>
            </div>
        </div>
        <div class="col-lg-12 mt-4">
            <button type="submit" class="filterbtn">Submit</button>
        </div>
    </div>
</form>


<?php $viewData->scriptStart() ?>
jQuery(document).ready(function(){
jQuery('#AddUserForm').validate({errorElement : 'span'});
})
<?php $viewData->scriptEnd() ?>