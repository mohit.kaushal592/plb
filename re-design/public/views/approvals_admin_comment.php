<?php $approvalData = $viewData->get('approvalData') ?>
<div class="row-fluid">
    <div class="span12">
        <form action="approvals.php?act=save_admin_comment" data-target="#AdminCommentAdd"  class="form-horizontal"  method="post" id="AddAdminCommentForm">
          <input type="hidden" name="data[Approval][id]" value="<?php echo $approvalData['Approval']['id']?>" id="_ApprovalId" />
          <div class="control-group">
            <label class="control-label">Comment :</label>
            <div class="controls">
              <textarea name="data[Approval][narration_admin]" id="narration_admin" rows="5" style="resize: none;" class="span12" placeholder="Enter your comment"><?php echo $approvalData['Approval']['narration_admin']?></textarea>
            </div>
          </div>
        </form>
    </div>
  </div>