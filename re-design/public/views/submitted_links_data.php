<form action="submitted_links.php?act=do_action" method="post" id="PaymentListForm">
    <input type="hidden" name="whatDo" id="PaymentWhatDo" />
    <div class="row justify-content-end">
        <div class="col-lg-2">
            <select class="form-select form-control ">
                <option disabled selected>Actions</option>
                <option value="1">Payment</option>
                <option value="2">Delete Link</option>
                <option value="3">Export</option>
                <option value="4">Export Bankformat</option>
            </select>
        </div>
    </div>
    <div class="row mb-4">
        <div class="col-lg-12">
            <div class="table-responsive mt-3" style="width: 104% !important">
                <table class="table bg-white table-bordered" cellspacing="0">
                    <thead>
                        <tr>
                            <?php if(canUserDoThis(array('payment','link_delete'))): ?>
                            <th style="width: 15px;"><span style="float:left"><input type="checkbox"
                                        id="checkAllPayment" /></span></th>
                            <?php endif ?>
                            <th>Campaign</th>
                            <th>Domain</th>
                            <th>Url</th>
                            <th>Response Status</th>
                            <th>Index Status</th>
                            <th>Webmaster</th>
                            <th>Payment Type</th>
                            <th>Anchor</th>
                            <th>Amount</th>
                            <th>User</th>
                            <th>Comment</th>
                            <th style="width:90px;">Submitted On</th>
                            <th>Paid On</th>
                            <th style="width:90px;">Pay for Mon / Yr</th>
                            <th style="width:100px;">Status
                                <span style="float:left"><input type="checkbox" id="checkAllPaymentAccepted" /></span>
                                <span style="float:left"><input type="checkbox" id="checkAllPaymentReceted"
                                        style="opacity: 1;" /></span>
                            </th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $submittedLinks = $viewData->get('submittedLinks') ?>
                        <?php if(!empty($submittedLinks)): ?>
                        <?php foreach($submittedLinks as $submittedLink): ?>
                        <tr class="odd1 gradeX1">
                            <?php if(canUserDoThis(array('payment','link_delete'))): ?>
                            <td>
                                <?php if(canDeleteLink($submittedLink['Payment']['status'], $submittedLink['Payment']['user_id'])): ?>
                                <input type="checkbox" id="PaymentId<?php echo $submittedLink['Payment']['id'] ?>"
                                    class="_payment_id" name="data[Payment][id][]"
                                    value="<?php echo $submittedLink['Payment']['id'] ?>" />
                                <?php endif ?>
                            </td>
                            <?php endif ?>
                            <td><?php echo $submittedLink['Payment']['campaign_name'] ?></td>
                            <td>
                                <a class="linkList_td" href="<?php echo $submittedLink['Payment']['domain'] ?>"
                                    target="_blank"><?php echo $submittedLink['Payment']['domain'] ?></a> <br />
                                <?php echo $submittedLink['Payment']['ip'] ?> <br />
                                DA: <?php echo round($submittedLink['Payment']['da_value'], 2) ?> <br />
                                PR: <?php echo round($submittedLink['Payment']['pr_value'],2) ?>
                            </td>
                            <td><br />
                                <a class="linkButton" href="<?php echo $submittedLink['Payment']['url'] ?>"
                                    target="_blank">Link</a>
                            </td>
                            <td>
                                <?php echo $submittedLink['Payment']['response_status'] ?> <br />
                                <?php echo $submittedLink['Payment']['response_time'] ?>
                            </td>

                            <td></br>
                                <button style="background-color: #90EE90; color: black; border: none; padding: 5px 10px; cursor: pointer; border-radius: 15px; margin-left: 5px;">Indexed</button> 
                            </td>
                            <td class="lineBreak"><?php echo $submittedLink['Payment']['client_mail'] ?></td>
                            <td class="lineBreak">
                                <?php if($submittedLink['Payment']['payment_type']!='NEFT')echo $submittedLink['Payment']['paypal_email'];else echo 'NEFT'; ?>
                            </td>
                            <td><?php $anchors = explode('[', $submittedLink['Payment']['anchor_text']);
        if(!empty($anchors)):
         $anchAr = array();
         foreach($anchors as $anchr):
          $anchAr[] = "<li>$anchr</li>";
         endforeach;
         
         echo !empty($anchAr) ? "<ol>". join('', $anchAr)."</ol>" : null;
        endif;
       ?></td>
                            <td><?php if($submittedLink['Payment']['payment_type']!='NEFT')
	   {		   echo currency_format($submittedLink['Payment']['amount'], $submittedLink['Payment']['currency']) ;
	   }
	   else
	   {
		    echo 'Gross Amount: '.currency_format($submittedLink['Payment']['amount'], $submittedLink['Payment']['currency']) ;
			echo "<br/>";
			 echo 'TDS: '.currency_format($submittedLink['Payment']['tdsamount'], $submittedLink['Payment']['currency']) ;
			 echo "<br/>";
			  echo 'Net Amount: '.currency_format($submittedLink['Payment']['grossamount'], $submittedLink['Payment']['currency']) ;
		   
	   }?></td>
                            <td><?php echo ucwords($submittedLink['User']['first_name'].' '.$submittedLink['User']['last_name']) ?>
                            </td>
                            <td><?php echo $submittedLink['Payment']['narration_text'] ?></td>
                            <td><?php echo date_to_text($submittedLink['Payment']['added_on']) ?></td>
                            <td><?php echo ($submittedLink['Payment']['payment_date']!= '0000-00-00') ? date_to_text($submittedLink['Payment']['payment_date']) : '' ?>
                            </td>
                            <td><?php echo $submittedLink['Payment']['payment_month'].' / '.$submittedLink['Payment']['payment_year']; ?>
                            </td>
                            <td><?php
       $iconStatus = $submittedLink['Payment']['status'] == 'Paid' ? 1 : ($submittedLink['Payment']['status'] == 'Underprocess'? -1 : 0) ;
       
       $_cmpHtml = "";
       if($iconStatus=='-1'){
            if(canUserDoThis('payment')):
             $_cmpHtml .= "<span>";
             $_cmpHtml .= '<input type="checkbox" id="PaymentAccept'. $submittedLink['Payment']['id'].'_0" class="_payment_accept" name="data[Payment][accept][]" value="'.$submittedLink['Payment']['id'].'" title="Paid"/> ';
             $_cmpHtml .= '<input type="checkbox" id="PaymentReject'. $submittedLink['Payment']['id'].'_1" class="_payment_reject" name="data[Payment][reject][]" value="'.$submittedLink['Payment']['id'].'"  title="Rejected"/> ';
             $_cmpHtml .= '</span> ';
            endif;			 
       }
       if(!empty($_cmpHtml)){
        echo "<ul><li>".$_cmpHtml."</li></ul>";
       }else{
        echo getIconHtml($iconStatus, array('1'=>'Paid', '-1'=>'Underprocess'));
       }
       
       ?></td>
                            <td>
                                <?php if($iconStatus == -1 and canUserDoThis('payment_edit', array('user_id'=>$submittedLink['User']['id']))): ?>
                                <a href="submitted_links.php?act=edit&lid=<?php echo $submittedLink['Payment']['id'] ?>"
                                    title="Click to edit link." class="tip-left"><img src="Assets/Icons/NotePencil.svg"
                                        alt="">
                                </a>
                                <?php endif ?>
                                <a target="_blank"
                                    href="submitted_links.php?act=view&lid=<?php echo $submittedLink['Payment']['id'] ?>"
                                    class="tip-left"><img src="Assets/Icons/DotsThreeCircle.svg" alt="">
                                </a>
                                <?php if($submittedLink['Payment']['invoice_file']<>""){?>
                                <a href="http://beta.adlift.com/plb/uploads/invoice/<?php echo $submittedLink['Payment']['invoice_file']?>"
                                    target="_blank" title="Click to View  Invoice"> <i class="icon-cog icon-large"></i>
                                </a>
                                <?php
		}?>
                            </td>
                        </tr>
                        <?php endforeach ?>
                        <?php else: ?>
                        <tr>
                            <td colspan="9" class="no-record-found">No records found.</td>
                        </tr>
                        <?php endif ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</form>