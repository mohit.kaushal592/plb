<?php $user = $viewData->get('user') ?>
<div class="row-fluid">
      <div class="span12">
        <div class="widget-box">
          <div class="widget-title"> <span class="icon"> <i class="icon-briefcase"></i> </span>
            <h5 >My Profile</h5>
			<span class="label label-info">
			<a href="users.php?act=edit<?php echo !empty($_GET['uid']) ? '&uid='. $_GET['uid'] : '' ?>" style="color:#fff;"><i class="icon-edit"></i> Edit</a>&nbsp;&nbsp;
			</span>
          </div>
          <div class="widget-content">
            <div class="row-fluid">
              <div class="span6">
                <table class="">
                  <tbody>
				    <tr>
                      <td><img style="width:128px;height:128px" src="uploads/profile/<?php echo !empty($user['User']['user_pic']) ? $user['User']['user_pic'] : 'no-img.png' ?>"/></td>
                    </tr>
                    <tr>
                      <td><h4><?php echo ucwords($user['User']['first_name'].' '.$user['User']['last_name']) ?></h4></td>
                    </tr>
                    <tr>
                      <td><?php echo $user['User']['designation'] ?></td>
                    </tr>
                    <tr>
                      <td>www.adlift.com</td>
                    </tr>
                   <tr>
                      <td><?php echo $user['User']['mobile_no'] ?> </td>
                    </tr>
                    <tr>
                      <td ><?php echo $user['User']['email_id'] ?></td>
                    </tr>
                  </tbody>
                </table>
              </div>
              <div class="span6">
                <table class="table table-bordered table-invoice">
                  <tbody>
                    <tr>
                   <tr>
                      <td class="width30">Employ ID:</td>
                      <td class="width70"><strong><?php echo $user['User']['employ_id'] ?></strong></td>
                    </tr>
                    <tr>
                      <td>Created Date:</td>
                      <td><strong><?php echo date_to_text($user['User']['created_on']) ?></strong></td>
                    </tr>
                 
                  <td class="width30">Address:</td>
                    <td class="width70">
                      <?php echo nl2br($user['User']['address']) ?> <br />
                      Contact No: <?php echo $user['User']['mobile_no'] ?> <br>
                      <?php echo !empty($user['User']['alternate_email']) ? 'Email: '. $user['User']['alternate_email'] : '' ?> </td>
                  </tr>
                    </tbody>
                  
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>