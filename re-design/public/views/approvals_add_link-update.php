<div class="row mt-4">
    <h4 class="darkBreadcrumb">
        <span class="lightBeradcrumb">Approval</span> / Add Link
    </h4>
</div>
<div class="row mt-1">
    <h3 class="tableMainTitle">Add Link</h3>
    <p>Please Check <span style="color:#1A9009">Domain History</span> before submission to prevent domain duplicacy for
        same campaign</p>
</div>
<form action="" class="form-horizontal" method="post" id="AddLinkForm">
    <div class="mt-2 shadow p-3 mb-5 bg-white rounded ">
        <div class="domain_header">
            <h4 class="domain_Headering_title">Domain Information</h4>
            <img src="Assets/Icons/menuIcon.svg" alt="">
        </div>
        <div class="row">
            <div class="col-lg-3">
                <label for="ApprovalDomain" class="form-label">Domains</label>
                <input type="text" class="form-control" placeholder="Domain Name" name="data[Approval][domain]"
                    id="ApprovalDomain" minlength="4" required>
                <a href="#" class="link ipHistory history DomainIpHistory" id="DomainHistory">Check Domain
                    History</a>
            </div>
            <div class="col-lg-3">
                <label for="Category" class="form-label">Select Campaign</label>
                <select name="data[Approval][campaign][]" multiple="multiple" id="ApprovalCampaign"
                    title="Please select campaign." required class="form-select1 form-control1 form-control" >
                    <?php $campsList = $viewData->get('campaignsList');
                 echo getFormOptions($campsList);
              ?>
                </select>
            </div>
            <div class="col-lg-3">
                <label for="Category" class="form-label">Category</label>
                <select class="form-select" name="data[Approval][category]" required>
                <option value="">Select Category</option>
                <option value="Automobile">Automobile</option>
                <option value="Business">Business</option>
                <option value="Education">Education</option>
                <option value="Fashion">Fashion</option>
                <option value="Finance">Finance</option>
                <option value="Health">Health</option>
                <option value="News">News</option>
                <option value="Parenting">Parenting</option>
                <option value="Home Decor">Home Decor</option>
                <option value="Technology">Technology</option>
                <option value="Travel">Travel</option>
                <option value="Brandlift">Brandlift</option>
                <option value="Real estate">Real estate</option>
                <option value="Digital marketing">Digital marketing</option>
                <option value="General">General</option>
                <option value="Tech">Tech</option>
                <option value="Tech and games">Tech and games</option>
                <option value="Logistic">Logistic</option>
                <option value="Logistics">Logistics</option>
                <option value="Realstate">Realstate</option>
                <option value="Game">Game</option>
                <option value="Gaming">Gaming</option>
                <option value="Technology,">Technology,</option>
                <option value="Lifestyle,tech">Lifestyle,tech</option>
                <option value="Medical">Medical</option>
                <option value="Medicial">Medicial</option>
                <option value="Architect">Architect</option>
                <option value="Multi">Multi</option>
                <option value="Tecnology">Tecnology</option>
                <option value="Games">Games</option>
                <option value="Hotel">Hotel</option>
                <option value="Real-estate">Real-estate</option>
                <option value="Generic">Generic</option>

                </select>
            </div>
            <!-- <div class="col-lg-3">
                <label for="Category" class="form-label">Total No of links</label>
                <input type="number" class="form-control" placeholder="Total No of links"
                    name="data[Approval][duration]" />
            </div> -->
            <div class="col-lg-3">
                <label for="Category" class="form-label">Payment Type</label>
                <select name="data[Approval][payment_type]" id="ApprovalPayment_type"
                    title="Please select Payment Type." required class="form-select1 form-control1 form-control" >
                    <option value="">Select Payment Type</option>
                    <option value="Paypal">Paypal</option>
                    <option value="NEFT">NEFT</option>
                </select>
            </div>
            <div class="col-lg-3">
                <label for="Category" class="form-label">Currency</label>
                <select onchange="return checkCurrency(this.value);" class="form-control" name="data[Approval][Currency]"
                    id="PaymentCurrency" title="Please select Currency." required >
                    <option value=''>Select Currency</option>
                    <?php echo getFormOptions(array('INR'=>'INR', 'Euro'=>'Euro', 'Dollars'=>'Dollars', 'Pounds'=>'Pounds'), $payment['Approval']['Currency']) ?>
                </select>
            </div>
            <div class="col-lg-3">
                <label for="PaymentAmount" class="form-label">Amount</label>
                <input type="number" class="form-control" pattern="^\d+(\.)\d{2}$" placeholder=""
                    name="data[Approval][Amount]" id="PaymentAmount" required>
            </div>
            <div class="col-lg-3">
                <label for="MonthYear" class="form-label">Select Month and Year</label>
                <input type="date" class="form-control" placeholder="Select Month" name="data[Approval][updated_on]">
            </div>
            <div class="col-lg-3">
                <div class="mb-3">
                    <label for="ApprovalNarrationText" class="form-label">Narration Text</label>
                    <input type="text" class="form-control" placeholder="" name="data[Approval][narration_text]"
                        id="ApprovalNarrationText">
                </div>
            </div>
        </div>
        <div class="col-lg-12">
            <div class="mt-1">
                <button type="reset" class="clearbtn">Reset</button>
                <button type="button" class="filterbtn mx-3" id="ApprovalCalcDaPr">Calculate PR,DA and
                    IP</button>
            </div>
        </div>
    </div>
    <div class="mt-2 shadow p-3 mb-5 bg-white rounded ">
        <div class="domain_header">
            <h4 class="domain_Headering_title">IP Information</h4>
            <img src="Assets/Icons/menuIcon.svg" alt="">
        </div>
        <div class="row">
            <div class="col-lg-3">
                <div class="mb-3">
                    <label for="ApprovalIp" class="form-label">IP Address</label>
                    <input type="text" class="form-control" placeholder="IP Address" name="data[Approval][ip]"
                        id="ApprovalIp" required>
                </div>
            </div>
            <div class="col-lg-9 ipHistoryColumn">
                <div class="mt-4 d-flex align-items-center">
                    <a href="#" class="link ipHistory history DomainIpHistory" style="white-space:now-wrap;"
                        id="IpHistory">Ip History</a>
                </div>
            </div>
            <div class="col-lg-3">
                <div class="mb-3">
                    <label for="ApprovalPr" class="form-label">PR Value</label>
                    <input type="text" class="form-control" placeholder="PR value" name="data[Approval][pr]"
                        id="ApprovalPr" required>
                </div>
            </div>
            <div class="col-lg-3">
                <div class="mb-3">
                    <label for="ApprovalDa" class="form-label">DA Value</label>
                    <input type="text" class="form-control" placeholder="DA value" name="data[Approval][da]"
                        id="ApprovalDa" required>
                </div>
            </div>
            <div class="col-lg-3">
                <div class="mb-3">
                    <label for="ApprovalSc" class="form-label">SS Value (%)</label>
                    <input type="text" class="form-control" placeholder="SC value" name="data[Approval][sc]"
                        id="ApprovalSc" required>
                </div>
            </div>
            <div class="col-lg-3">
                <div class="mb-3">
                    <label for="ApprovalTraffic" class="form-label">Annual Traffic</label>
                    <input type="text" class="form-control" placeholder="Traffic value"
                        name="data[Approval][annual_traffic]" id="ApprovalTraffic" value="0">
                </div>
            </div>
            <div class="col-lg-3">
                <div class="mb-3">
                    <label for="ApprovalAc" class="form-label">Admin Country</label>
                    <input type="text" class="form-control" placeholder="Administrative Country"
                        name="data[Approval][ac]" id="ApprovalAc" required>
                </div>
            </div>
        </div>
        <!-- <input type="radio"  name="data[Approval][content_writer]"  id="ApprovalContentWriter0" value="0" selected/> -->
        <div class="col-lg-12">
            <div class="mt-1">
                <button type="submit" class="filterbtn">Submit</button>
 
                <?php 
            $keyword = isset($_REQUEST['keyword'])?$_REQUEST['keyword']:"";
            $sv = isset($_REQUEST['sv'])?$_REQUEST['sv']:"";
            $rank = isset($_REQUEST['rank'])?$_REQUEST['rank']:"";
            $title = isset($_REQUEST['title'])?$_REQUEST['title']:"";
            $title_id = isset($_REQUEST['title_id'])?$_REQUEST['title_id']:"";
            $target_url = isset($_REQUEST['target_url'])?$_REQUEST['target_url']:"";
            ?>

                <input type="hidden" name="data[Approval][keyword]" value="<?php echo $keyword; ?>">
                <input type="hidden" name="data[Approval][sv]" value="<?php echo $sv; ?>">
                <input type="hidden" name="data[Approval][rankval]" value="<?php echo $rank; ?>">
                <input type="hidden" name="data[Approval][title]" value="<?php echo $title; ?>">
                <input type="hidden" name="data[Approval][title_id]" value="<?php echo $title_id; ?>">
                <input type="hidden" name="data[Approval][target_url]" value="<?php echo $target_url; ?>">
                <input type="hidden" name="data[Approval][content_writer]" value="0">
                <input type="hidden" name="data[Approval][backlinks]" value="0">
                <input type="hidden" name="data[Approval][dr]" value="0">

            </div>
        </div>
</form>
<?php $viewData->scripts(array('js/approvals_add_link.js?1=2'), array('inline'=>false)) ?> 