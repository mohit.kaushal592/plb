<?php $user =$viewData->get('user') ?>
<h4 style="color:#DA542E;"><?php echo $viewData->getTitle() ?></h4>
  <hr>
 <?php echo output_message($session->message()) ?>
  <div class="row-fluid">
    <div class="span6">
      <div class="widget-box">
        <div class="widget-title"> <span class="icon"> <i class="icon-align-justify"></i> </span>
          <h5>Change Password for User - <?php echo ucwords($user['User']['first_name'].' '.$user['User']['last_name']) ?></h5>
        </div>
        <div class="widget-content nopadding">
          <form action="users.php?act=change_password" class="form-horizontal"  method="post" id="AddUserForm" >
	  <input type="hidden" name="data[User][id]" value="<?php echo $user[User][id] ?>" />
	    <?php if($session->read('User.user_type')!='superadmin'): ?>
	    <div class="control-group">
              <label class="control-label" for="UserOldPassword">Old Password :</label>
              <div class="controls">
                <input type="password" class="span11" placeholder="Old Password" name="data[User][old_password]" id="UserOldPassword" required/>
              </div>
            </div>
	    <?php endif ?>
	    <div class="control-group">
              <label class="control-label" for="UserPassword">New Password :</label>
              <div class="controls">
                <input type="password" class="span11" placeholder="Password" name="data[User][password]" id="UserPassword" required/>
              </div>
            </div>
	    <div class="control-group">
              <label class="control-label" for="UserRetypePassword">Retype Password :</label>
              <div class="controls">
                <input type="password" class="span11" placeholder="Retype Password" name="data[User][re_password]" id="UserRetypePassword" equalTo="#UserPassword" required/>
              </div>
            </div>
	    <div class="form-actions">
              <button type="submit" class="btn btn-success">Submit</button>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
  
 <?php $viewData->scriptStart() ?>
    jQuery(document).ready(function(){
      jQuery('#AddUserForm').validate({errorElement : 'span'});
    })
  <?php $viewData->scriptEnd() ?>