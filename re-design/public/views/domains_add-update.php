<div class="row mt-4">
  <h4 class="darkBreadcrumb"><span class="lightBeradcrumb">Domains</span> / Add new Domains</h4>
</div>
<div class="row mt-1">
  <h3 class="tableMainTitle">Add New Domain</h3>
  <p class="lightText">Please enter each domain in new line to add multiple domains at one time</p>
</div>
<form action="" class="form-horizontal"  method="post" id="AddDomainForm">
<div class="row">
  <div class="col-lg-1">
    <h4>Domains</h4>
  </div>
  <div class="col-lg-6">
    <textarea class="form-control" rows="3" placeholder="Enter each domain in new line" name="data[Domain][name]" id="DomianName" required="required"></textarea>
  </div>
</div>
<div class="row mt-4 mb-4">
  <div class="col-lg-2">
    <h4>Status</h4>
  </div>
  <div class="col-lg-6">
    <div class="controls ">
      <div class="col-lg-6">
        <label class="lightText">
          <input type="radio" value="1"  name="data[Domain][status]" id="DomainStatus1" checked="checked"/>
          Active
        </label>
      </div>
      <div class="col-lg-6">
        <label  class="lightText">
          <input type="radio" name="data[Domain][status]" id="DomainStatus0" value="0"/>
          Blacklist
        </label>
      </div>
    </div>
  </div>
</div>
<div class="col-lg-12 d-flex justify-content-center">
  <button type="submit" class="filterbtn mx-3">Submit</button>
</div>
</form>
<?php $viewData->scripts(array('js/domains_add.js'), array('inline'=>false)) ?>