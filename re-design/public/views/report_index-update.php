<div class="row mt-4">
    <h4 class="darkBreadcrumb">
        <span class="lightBeradcrumb">Report</span> / Report Listing of <?= ucWords($_GET['act']); ?>
    </h4>
</div>
<div class="row mt-1">
    <h3 class="tableMainTitle">Report Listing of <?= ucWords($_GET['act']); ?></h3>
</div> 
<?php $viewData->scriptStart() ?>
jQuery.fn.reLoadLinkStatus = function(){
var anotherSelectId = $(this).attr('id')=='FilterAppStatusPhase1' ? 'FilterAppStatusPhase2' : 'FilterAppStatusPhase1';
var selectedOpt = $(this).multiselect("getChecked").map(function(){
return this.value;
}).get();

var anotherSelect = $('select#'+ anotherSelectId).multiselect("getChecked").map(function(){
return this.value;
}).get();

var valDefault= $('select#FilterLinkStatus').attr('data-dflt');
valDefault = valDefault.length>0 ? valDefault : '-1';

if((selectedOpt.length==1 && selectedOpt[0]=='1') || (anotherSelect.length==1 && anotherSelect[0]=='1')){
$('select#FilterLinkStatus').multiselect('enable');

}else{
$('select#FilterLinkStatus').val(valDefault).multiselect('disable');
}
}
$(document).ready(function(){
// autocomplete
$('#FilterDomain').typeahead({
ajax: 'domains.php?act=list_json',
display: 'name',
val: 'name'
});

$('select[id^=FilterAppStatusPhase]').on('multiselectclick', function(event, ui){
$(this).reLoadLinkStatus();
});

$('#FilterForm').bind('submit', function(e){
    
e.preventDefault();
var users = $("#FilterUser").multiselect("getChecked").map(function(){
return this.value;
}).get();
var campaigns = $("#FilterCampaign").multiselect("getChecked").map(function(){
return this.value;
}).get();
var approvalStPhase1 = $("#FilterAppStatusPhase1").multiselect("getChecked").map(function(){
return this.value;
}).get();
var approvalStPhase2 = $("#FilterAppStatusPhase2").multiselect("getChecked").map(function(){
return this.value;
}).get();
var forword = $("#FilterForword").multiselect("getChecked").map(function(){
return this.value;
}).get()[0];
var contentWriter = $("#FilterContentWriter").multiselect("getChecked").map(function(){
return this.value;
}).get()[0];
var _daVal = $("#FilterDA").multiselect("getChecked").map(function(){
return this.value;
}).get()[0];
var _scVal = $("#FilterSC").multiselect("getChecked").map(function(){
return this.value;
}).get()[0];

var _acVal = $("#FilterAC").multiselect("getChecked").map(function(){
return this.value;
}).get()[0];

var linkStatus = !($("#FilterLinkStatus").prop('disabled')) ?
($("#FilterLinkStatus").multiselect("getChecked").map(function(){
return this.value;
}).get()[0]) : '';
contentWriter = contentWriter > -1 ? contentWriter : '';
linkStatus = linkStatus >= -1 ? linkStatus : '';
var dateFrom = $('#FilterDateFrom').val();
var dateTo = $('#FilterDateTo').val();
var domain = $('#FilterDomain').val();
var dateRange = (dateFrom+'_'+dateTo); 
dateRange = dateRange.replace(/^\s*\_\s*$/g, '');
var canSubmit = true;
var _rp = $('#RowPerPage').val();

if(approvalStPhase1.length>0 && approvalStPhase2.length>0){
alert('Please choose either Status (Approval Phase I) or Status (Approval Phase II).');
canSubmit=false;
}

if(canSubmit==true){
var curUrl = $.parseUrl();

var queryString = curUrl.query;
queryString._uid = $.base64.encode(users.toString());
queryString._camp = $.base64.encode(campaigns.toString());
queryString._st = $.base64.encode(approvalStPhase1.toString());
queryString._st2 = $.base64.encode(approvalStPhase2.toString());
queryString._dt = $.base64.encode(dateRange);
queryString._dom = $.base64.encode(domain);

//queryString._fw = $.base64.encode(forword);
queryString._cw = $.base64.encode(contentWriter);
queryString._ls = $.base64.encode(linkStatus);

queryString._rp = _rp;
queryString._da = $.base64.encode(_daVal);
queryString._sc = $.base64.encode(_scVal);
queryString._ac = $.base64.encode(_acVal);
queryString.p = 1;
var urlParams = [];

$.each(queryString, function(k,v){
//console.log('queryString__',v)
if(v && v.length>0){
urlParams.push(k+'='+v);
}
});
if(urlParams.length>=1){
window.location = '?'+urlParams.join('&');
}
}
});

$('#RowPerPage').on('change', function(){
var curUrl = $.parseUrl();
var queryString = curUrl.query;
var _rp = $('#RowPerPage').val();

var urlParams = [];
queryString._rp=_rp;
queryString.p=1;
$.each(queryString, function(k,v){
if(v.length>0){
urlParams.push(k+'='+v);
}
});

if(urlParams.length>=1){
    
window.location = '?'+urlParams.join('&');
}
});
});
<?php $viewData->scriptEnd() ?>
<script>
    Highcharts.setOptions({
        credits: {
            enabled: false // Disable credits to remove watermark
        }
    });
</script>    
<?php if(isset($_GET['act']) && $_GET['act'] == 'category'){ ?>
    <script>
        // category bar chart start
            function categoryChart(filter=''){ 
                fetch(`https://api.adlift.ai/marketing/category_month${filter}`).then(res=>{
                    return res.json()
                }).then(response=>{ 
                    const categoryNameArr = [] 
                    const categoryValueArr = [] 
                    for(let categoryName in response){
                        categoryNameArr.push(categoryName)
                        categoryValueArr.push(response[categoryName])
                    } 
                    Highcharts.chart('categoryChartContainer', {
                        chart: {
                            type: 'column'
                        },
                        title: {
                            text: '',
                            align: 'center'
                        }, 
                        xAxis: {
                            categories: categoryNameArr,
                            crosshair: true,
                            accessibility: {
                                description: 'Countries'
                            }
                        },  
                        series: [
                            {
                                name: 'Data',
                                data: categoryValueArr
                            }
                        ]
                    })
                }).catch(()=>{
                    alert('some error to fetch category data')
                })
            }
        // category bar chart end
    </script>
    <div class="row mb-4">
        <div class="col-lg-12">
            <div class="chartContainer">
                <div class="chartCard my-2">
                    <div class="chartHeading">
                        <h4 class="chart_title">
                            Total Category
                        </h4> 
                    <div class="d-flex align-items-center"> 
                        <div class="form-group mb-0">
                            <label for="datepicker" class="sr-only">Select Date</label>
                            <input type="month" class="form-control total-category-filter" id="datepicker12" placeholder="Select Month" value="<?= date('Y-m'); ?>">
                        </div>
                    </div>
                    </div>
                    <div class="pieChart_Card">
                        <div id="categoryChartContainer"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        categoryChart()
        document.getElementById('datepicker12').addEventListener('change',(e)=>{
            categoryChart(`?month=${e.target.value}`)
        })
    </script>
<?php }elseif(isset($_GET['act']) && $_GET['act'] == 'payment'){ ?>
    <script>
        // Payment chart start
            function paymentChart(filter=''){ 
                fetch(`https://api.adlift.ai/marketing/payment${filter}`).then(res=>{
                    return res.json()
                }).then(response=>{  
                    let updateResponse = response.data 
                    const nameArr = [] 
                    const paidArr = [] 
                    const pendingArr = [] 
                    for(let id in updateResponse){ 
                        nameArr.push(updateResponse[id].campaign_names)
                        paidArr.push(parseInt(updateResponse[id].Paid))
                        pendingArr.push(parseInt(updateResponse[id].Pending))
                    }  
                    Highcharts.chart('paymentChartContainer', {
                        chart: {
                            type: 'column'
                        },
                        title: {
                            text: '',
                            align: 'center'
                        }, 
                        xAxis: {
                            categories: nameArr,
                            crosshair: true,
                            accessibility: {
                                description: 'Payment'
                            }
                        },  
                        series: [
                            {
                                name: 'Paid',
                                data: paidArr
                            },
                            {
                                name: 'Pending',
                                data: pendingArr
                            }
                        ],
                        exporting: {
                            enabled: false // Disable exporting module
                        }
                        
                    });
                    
                }).catch(()=>{
                    alert('some error to fetch payment data')
                })
            }
        // Payment chart end 
    </script>
    <div class="row mb-4">
        <div class="col-lg-12">
            <div class="chartContainer">
                <div class="chartCard my-2">
                    <div class="chartHeading">
                        <h4 class="tableMainTitle">
                            <span class="icon"><img src="Assets/Icons/Wallet.svg">  </span> Payments
                        </h4>
                        <div class="d-flex align-items-center">
                        <!-- Currency Dropdown -->
                        <div class="form-group mb-0 mr-2">
                            <label for="datepicker" class="sr-only">Start Date</label>
                                <input type="date" class="form-control" id="payment-start-date" placeholder="Start Date" onChange="document.getElementById('payment-end-date').value='' ">
                            </div>
                            <!-- Datepicker Input Field -->
                            <div class="form-group mb-0">
                                <label for="datepicker" class="sr-only">End Date</label>
                                <input type="date" class="form-control" id="payment-end-date" placeholder="End Date">
                            </div>
                        </div>
                    </div>
                    <div class="pieChart_Card"> 
                        <div id="paymentChartContainer"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        paymentChart()
        document.getElementById('payment-end-date').addEventListener('change',(e)=>{
            const paymentStartDate = $('#payment-start-date').val() 
            if(paymentStartDate != undefined){
                paymentChart(`?end_date=${e.target.value}&start_date=${paymentStartDate}`)
            }
        }) 
    </script>
<?php }elseif(isset($_GET['act']) && $_GET['act'] == 'brandlift'){ ?>
    <script>
        // brandlift bar chart start
            function brandLiftChart(filter=''){
                fetch(`https://api.adlift.ai/marketing/brandlift${filter}`).then(res=>{
                    return res.json()
                }).then(response=>{  
                    const nameArr = [] 
                    const idValueArr = [] 
                    for(let id in response){ 
                        nameArr.push(response[id].name)
                        idValueArr.push(parseInt(response[id].total_ids))
                    }  
                    Highcharts.chart('brandLiftChartContainer', {
                        chart: {
                            type: 'column'
                        },
                        title: {
                            text: '',
                            align: 'center'
                        }, 
                        xAxis: {
                            categories: nameArr,
                            crosshair: true,
                            accessibility: {
                                description: 'Countries'
                            }
                        },  
                        series: [
                            {
                                name: 'Data',
                                data: idValueArr
                            }
                        ],
                        exporting: {
                            enabled: false // Disable exporting module
                        }
                    })
                }).catch(()=>{
                    alert('some error to fetch brand lift data')
                })
            }
        // brandlift bar chart end 
    </script>
    <div class="row mb-4">
        <div class="col-lg-12">
            <div class="chartContainer">
                <div class="chartCard my-2">
                    <div class="chartHeading">
                        <h4 class="tableMainTitle">
                            BrandLift
                        </h4> 
                        <div class="d-flex align-items-center">
                                <!-- Datepicker Input Field -->
                                <div class="form-group mb-0">
                                    <label for="datepicker" class="sr-only">Select Date</label>
                                    <input type="month" class="form-control" id="brandLift-select-month" placeholder="Select Month" value="<?= date('Y-m'); ?>">
                                </div>
                            </div>
                    </div>
                    <div class="pieChart_Card"> 
                        <div id="brandLiftChartContainer"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        brandLiftChart()
        document.getElementById('brandLift-select-month').addEventListener('change',(e)=>{   
            brandLiftChart(`?month=${e.target.value}`)
        }) 
    </script>
<?php } ?>

<div class="row mb-4">
    <div class="col-lg-12">
        <h4 class="mx-3"> Listing </h1>
            <div class="table-responsive">
                <div class="row justify-content-end mb-4">
                    <div class="col-lg-2">
                        <select class="form-select form-control" style="width:0px">
                            <option disabled selected>Select Option</option>
                            <?php if(canUserDoThis(array('forward','approve_phase_1','approve_phase_2','approval_delete'))): ?>
                            <?php if(canUserDoThis('forward')): ?>
                            <option><a href="javascript:void(0)" rel="approval_forward" class="doAct"><i
                                        class="icon-step-forward  icon-large"></i> Forward</a></option>
                            <?php endif ?>
                            <?php if(canUserDoThis('approve_phase_1')): ?>
                            <option><a href="javascript:void(0)" rel="approval_phase_1" class="doAct"><i
                                        class="icon-ok icon-large"></i><sup class="icontxt">1</sup> Approval Phase
                                    I</a>
                            </option>
                            <?php endif ?>
                            <?php if(canUserDoThis('approve_phase_2')): ?>
                            <option><a href="javascript:void(0)" rel="approval_phase_2" class="doAct"><i
                                        class="icon-ok icon-large"></i><sup class="icontxt">2</sup> Approval Phase
                                    II</a></option>
                            <?php endif ?>
                            <?php if(canUserDoThis('approval_delete')): ?>
                            <option><a href="javascript:void(0)" rel="approval_delete" class="doAct"><i
                                        class="icon-trash icon-large"></i> Delete Link</a></option>
                            <?php endif ?>
                            <?php endif ?>
                            <option><a id="_ExportToExcel" href="javascript:void(0)"><i
                                        class="icon-download-alt icon-large"></i> Export</a></option>
                        </select>
                    </div>
                </div>
                <table id="example" class="table table-bordered mx-3" cellspacing="0" width="100%">
                    <thead>
                        <th style="width: 206px;">
                            <?php if(canUserDoThis(array('forward','approval_delete'))): ?>
                            <span style="float:left"><input type="checkbox" id="checkAllApproval" /></span>
                            <?php endif ?>
                            Domain
                        </th>
                        <th style="width: 50px;">Category</th>

                        <th style="width: 25px;">PR</th>
                        <th style="width: 25px;">DA</th>
                        <th style="width: 25px;">SC</th>
                        <th style="width: 30px;">Country</th>
                        <th>User</th>
                        <th>Date</th>
                        <th style="width: 25px;">Count</th>
                        <th style="width: 225px;"> <span style="float:left"><input type="checkbox"
                                    id="checkAllApprovalAccepted" /></span>
                            <span style="float:left"><input type="checkbox" id="checkAllApprovalReceted" /></span>
                            Campaign
                        </th>
                        <th>Text</th>
                        <th>Admin Text</th>
                        <th>Content Writer</th>
                        <th>Action</th>
                    </thead>
                    <tbody>
                        <?php $approvals = $viewData->get('approvals') ?>
                        <?php if(!empty($approvals)): ?>
                        <?php foreach($approvals as $approval): ?>
                        <tr class="odd gradeX" domain="<?php echo $approval['Approval']['domain'] ?>">
                            <td>
                                <?php if(canUserDoThis(array('forward','approval_delete'))): ?>
                                <input type="checkbox" id="ApprovalId<?php echo $approval['Approval']['id'] ?>"
                                    class="_approval_id" name="data[Approval][id][]"
                                    value="<?php echo $approval['Approval']['id'] ?>" />
                                <?php endif ?>
                                <a  class="linkList_td" href="<?php echo $approval['Approval']['domain'] ?>"
                                    target="_blank"><?php echo $approval['Approval']['domain'] ?></a>
                                <span
                                    onclick="return getDomainHistory('<?php echo $approval['Approval']['domain'] ?>');"
                                    class="domain-ip tip tip-top"
                                    title="Click to view Domain History"><?php echo $approval['Approval']['ip'] ?></span>
                            </td>
                            <td><?php echo ucwords($approval['Approval']['category']) ?></td>
                            <td><?php echo round($approval['Approval']['pr'],2) ?></td>
                            <td><?php echo round($approval['Approval']['da'], 2) ?></td>
                            <td><?php echo round($approval['Approval']['sc'], 2) ?></td>
                            <td><?php echo $approval['Approval']['ac'] ?></td>
                            <td><?php echo ucwords($approval['User']['first_name'].' '.$approval['User']['last_name']) ?>
                            </td>
                            <td><?php echo date_to_text($approval['Approval']['added_on']) ?></td>
                            <td><?php echo $approval['Approval']['campaigns_count'] ?></td>
                            <td>
                                <?php
					$_camps = array();
					if(!empty($approval['ApprovalLink'])):
					foreach($approval['ApprovalLink'] as $_appLink):
					$_cmpHtml = "<li>";
					if(canUserDoThis(array('approve_phase_1','approve_phase_2'), $_appLink, true)):
					$_cmpHtml .= "<span>";
					$_cmpHtml .= '<input type="checkbox" id="ApprovalLinkAccept'. $approval['Approval']['id'].'_'.$_appLink['id'].'" class="_approval_accept" name="data[ApprovalLink][accept][]" value="'.$_appLink['id'].'" /> ';
					$_cmpHtml .= '<input type="checkbox" id="ApprovalLinkReject'. $approval['Approval']['id'].'_'.$_appLink['id'].'" class="_approval_reject" name="data[ApprovalLink][reject][]" value="'.$_appLink['id'].'" /> ';
					$_cmpHtml .= '</span> ';
					endif;
					$_cmpHtml .= '<span>'.ucwords($_appLink['campaign_name']);
								if($_appLink['final_link_submit_status'] == 0 && $_appLink['Amount'] != 0 && $_appLink['Currency'] != "")
								{
									$_cmpHtml .= ' ('.currency_format($_appLink['Amount'], $_appLink['Currency']).')';
								}
								elseif ($_appLink['final_link_submit_status'] == 1)
								{
									//$_cmpHtml .= ' ($ 231)';
									//echo "select Amount, Currency from tracking_data where child_table_id=".$_appLink['id'];exit;
									//$resultJitu = $dbObj->find_by_sql("select Amount, Currency from tracking_data where child_table_id=".$_appLink['id']);
									//echo "<pre>";print_r($resultJitu);exit;
								}
							
					$_cmpHtml .= '</span>';
					$_cmpHtml .= getCampaignIcons($_appLink, $viewData);
					
					$_cmpHtml .= '</li>';
					$_camps[] = $_cmpHtml;
					endforeach;
					endif;
					?>
                                <ul style="width:220px"><?php echo implode(' ', $_camps) ?></ul>
                            </td>
                            <td><?php echo $approval['Approval']['narration_text'] ?></td>
                            <td id="_row_<?php echo $approval['Approval']['id'] ?>"><span
                                    style="display: block;width: 100%;"><?php echo nl2br($approval['Approval']['narration_admin']) ?></span>
                                <?php if(canUserDoThis(array('admin_comment'))): ?>
                                <a data-toggle="modal" data-backdrop='static' data-target="#AdminCommentAdd"
                                    onmouseover="approvalId='<?php echo $approval['Approval']['id'] ?>'; domainname='<?php echo $approval['Approval']['domain'] ?>'"
                                    href="#" title="Click to add comment." class="tip-right"><img src="Assets/Icons/Plus.svg" alt=""></a>
                                <?php endif ?>
                            </td>
                            <td><?php echo $approval['Approval']['content_writer']==1?'External' : ($approval['Approval']['content_writer']==0?'Adlift' : '') ?>
                            </td>
                            <td>
                                <?php   //if($approval['User']['id'] == $session->read('User.id') || $session->read('User.user_type') == 'superadmin'):  ?>
                                <!--		    <a href="approvals.php?act=edit_link&pid=<?php echo $approval['Approval']['id'] ?>" class="tip-top" title="Click to edit link." target="_blank"> <i class="icon icon-edit icon-large"></i></a>-->
                                <?php //endif ?>

                                <?php   if($session->read('User.user_type') == 'superadmin'):  ?>
                                <a href="approvals.php?act=edit_link&pid=<?php echo $approval['Approval']['id'] ?>"
                                    class="tip-top" title="Click to edit link." target="_blank"> <img src="Assets/Icons/NotePencil.svg"
                                        alt=""></a>
                                <?php endif ?>
                            </td>
                        </tr>
                        <?php endforeach ?>
                        <?php else: ?>
                        <tr>
                            <td colspan="10" class="no-record-found">No records found.</td>
                        </tr>
                        <?php endif ?>
                    </tbody>
                </table>
            </div>
    </div>
    <?php echo $viewData->get('pageLinks') ?>
</div>
<div id="AdminCommentAdd" class="modal fade bs-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel"
    aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h3>Admin Comment <span></span>
            </h3>
        </div>
        <div class="modal-body"></div>
        <div class="modal-footer">
            <a href="#" class="btn" data-dismiss="modal">Cancel</a>
            <a href="javascript:void(0)" id="admin-add-comment" class="btn btn-primary">Submit</a>
        </div>
    </div>
</div>
<?php $viewData->scripts(array('js/approvals_list.js'), array('inline'=>false)) ?>