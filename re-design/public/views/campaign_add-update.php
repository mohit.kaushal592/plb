<div class="row mt-4">
    <h4 class="darkBreadcrumb"><span class="lightBeradcrumb">Campaign</span> / Add Campaign</h4>
</div>
<div class="row mt-1">
    <h3 class="tableMainTitle">Add Campaign</h3>
</div>  

<form action="" class="form-horizontal" method="POST" id="myForm">
    <div class="mt-2 shadow p-3 mb-5 bg-white rounded ">
        <div class="domain_header">
            <h4 class="domain_Headering_title">Campaign Information</h4>
            <img src="Assets/Icons/menuIcon.svg" alt="">
        </div>
        <div class="row mb-3">
            <div class="col-lg-2">
                <label for="" class="form-label">Name</label>
                <input type="text" class="form-control" placeholder="Campaign name" name="data[Campaigns][name]" minlength="" id="name" required>
            </div>
            <div class="col-lg-2">
                <label for="" class="form-label">Domain</label>
                <input type="text" class="form-control" placeholder="Campaign domain" name="data[Campaigns][website]" minlength="" id="website" required>
            </div>
            <div class="col-lg-2">
                <label for="" class="form-label">Client</label>
                <input type="text" class="form-control" placeholder="Client email" name="data[Campaigns][client_email]" minlength="" id="client_email" required>
            </div>
            <div class="col-lg-2">
                <label for="" class="form-label">Frequency</label>
                <select class="form-select" name="data[Campaigns][frequency]" id="frequency" required>
                    <option value="daily">daily</option>
                    <option value="monthly">monthly</option>
                </select>
            </div>
            <div class="col-lg-2">
                <label for="" class="form-label">Link</label>
                <input type="text" class="form-control" placeholder="No. of Links" name="data[Campaigns][no_of_links]" minlength="" id="no_of_links" required>
            </div>
        </div>
        <div class="row mb-3">
            <div class="col-lg-2">
                <label for="" class="form-label">Country</label>
                <select class="form-select" name="data[Campaigns][country]" id="country" required>
                    <option value="India">India</option>
                    <option value="Usa">USA</option>
                </select>
            </div>
            <div class="col-lg-2">
                <label for="" class="form-label">Google Report</label>
                <select class="form-select" name="data[Campaigns][google_report]" id="google_report" required>
                    <option value="com">com</option>
                    <option value="in">in</option>
                    <!-- <option value="uk">uk</option> -->
                </select>
            </div>
            <div class="col-lg-2 d-none">
                <label for="" class="form-label">Mail Report</label>
                <select class="form-select" name="data[Campaigns][mail_report]" id="mail_report" required>
                    <option value="on">On</option>
                    <option value="off">Off</option>
                </select>
            </div>
        </div>
        <div class="col-lg-6 mb-3">
            <label for="" class="form-label">Client Keywords</label>
            <textarea class="form-control" rows="3" placeholder="keywords" name="data[Campaigns][keywords]" id="keywords" required="required"></textarea>
        </div>
        <div class="col-lg-12">
            <div class="mt-1">
                <button type="submit" class="filterbtn">Submit</button>
            </div>
        </div>
</form>
<script>
document.getElementById('myForm').addEventListener('submit', function(e) {
    $('.loader_box_overlay').show();
    e.preventDefault() 
    const name = document.getElementById('name').value??''
    const website = document.getElementById('website').value??''
    const client_email = document.getElementById('client_email').value??''
    const frequency = document.getElementById('frequency').value??''
    const no_of_links = document.getElementById('no_of_links').value??''
    const country = document.getElementById('country').value??''
    const google_report = document.getElementById('google_report').value??''
    const mail_report = document.getElementById('mail_report').value??''
    const keywords = document.getElementById('keywords').value??''
    
    const url = 'https://api.adlift.ai/marketing/add-campaign/';
    const data = {
        name,
        website,
        client_email,
        frequency,
        no_of_links,
        country,
        google_report, 
        keywords
    }
    fetch(url, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(data)
    }).then(response => {
        if(!response.ok) {
            console.log('Network response was not ok');
        }
        return response.json()
    }).then(data => {
        $('.loader_box_overlay').hide();
        alert('Campaign added successfully','Success');
    }).catch(error => {
        console.log(error);
    });

})
</script>    