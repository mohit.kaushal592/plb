<?php $filterVars = $viewData->get('filterVars') ?>
<form action="submitted_links.php" method="post" class="form-horizontal filter-form" id="FilterForm">
	<input type="hidden" name="exportList" id="exportList" value="" />
	<div class="control-group">
	  <div class="span3">
	   <label>User</label>
	     <select name="users[]" id="FilterUser" multiple="multiple">
	       <?php echo getFormOptions($viewData->get('usersList'), $filterVars['users']); ?>
	     </select>
	  </div>
	  <div class="span3">
	   <label>Campaign</label>
	    <select name="campaigns[]" id="FilterCampaign"  multiple="multiple">
	       <?php echo getFormOptions($viewData->get('campaignsList'), $filterVars['campaigns']); ?>
	    </select>
	  </div>
	  <div class="span3">
	   <label>Domain</label>
	    <input name="domain" autocomplete="off" id="FilterDomain" type="text" class="span10" value="<?php echo !empty($filterVars['domain']) ? $filterVars['domain'] : '' ?>" class="typeahead" />
	  </div>
	  
	  <div class="span3">
	   <label>Status</label>
	    <select name="payment_status[]" id="FilterPaymentStatus" multiple="multiple">
		<?php echo getFormOptions(array('1'=>'Paid', '0'=>'Rejected', '-1'=>'Underprocess'), $filterVars['status']); ?>
	    </select>
	  </div>
	</div>
	
       <div class="control-group">
	<div class="span3">
	   <label>Month</label>
	    <select name="month[]" id="FilterMonth" multiple="multiple">
		<?php echo getFormOptions(monthsList(), $filterVars['month']); ?>
	    </select>
	  </div>
	  <div class="span3">
	   <label>Year</label>
	    <select name="year[]" id="FilterYear" multiple="multiple">
		<?php echo getFormOptions(yearsList(array('min'=>'2012','max'=>date('Y'))), $filterVars['year']); ?>
	    </select>
	  </div>
       	<div class="span3" id="WebmasterBlock">
	   <label>Webmaster</label>
	   <input type="hidden" id="FilterClientMail" value="<?php echo !empty($filterVars['client_mail']) ? join(",", $filterVars['client_mail']) : '' ?>" />
	  </div>
	<div class="span3" id="PaypalBlock">
	   <label>Paypal</label>
	    <input type="hidden" id="FilterPaypalEmail" value="<?php echo !empty($filterVars['paypal_email']) ? join(",", $filterVars['paypal_email']) : '' ?>" />
	  </div>
       </div>
       
       <div class="control-group">
	<div class="span3">
	   <label>PR (operator)</label>
	    <select name="pr_op" id="FilterPrOperator">
		<?php echo getFormOptions(array('eq'=>'= (Equal to)', 'lt'=>'< (Less than)', 'lte'=>'<= (Less than equal to)', 'gt'=>'> (Greater than)', 'gte'=>'>= (Greater than equal to)'), $filterVars['pr_op']); ?>
	    </select>
	  </div>
	<div class="span3">
	   <label>PR (value)</label>
	    <select name="pr_value" id="FilterPrValue">
		<?php $prValues[0] = 'Select Value'; for($i=1;$i<=10;$i++){$prValues[$i] = $i;} ?>
		<?php echo getFormOptions($prValues, $filterVars['pr_value']); ?>
	    </select>
	  </div>
	<div class="span3">
	   <label>DA (operator)</label>
	    <select name="da_op" id="FilterDaOperator">
		<?php echo getFormOptions(array('eq'=>'= (Equal to)', 'lt'=>'< (Less than)', 'lte'=>'<= (Less than equal to)', 'gt'=>'> (Greater than)', 'gte'=>'>= (Greater than equal to)'), $filterVars['da_op']); ?>
	    </select>
	  </div>
	<div class="span3">
	   <label>DA (value)</label>
	    <select name="da_value" id="FilterDaValue">
		<?php $daValues[0] = 'Select Value'; for($i=1;$i<=100;$i = ($i==1?$i+4 : $i+5)){$daValues[$i] = $i;} ?>
		<?php echo getFormOptions($daValues, $filterVars['da_value']); ?>
	    </select>
	  </div>
       </div>
       <div class="control-group">
	<div class="span3">
	   <label>Payment Date</label>
	     <div  data-date="" class="input-append date datepicker">
	       <input type="text" name="from" value="<?php echo !empty($filterVars['dateFrom']) ? $filterVars['dateFrom'] : '' ?>" id="FilterDateFrom"  data-date-format="mm-dd-yyyy" class="span10" >
	       <span class="add-on"><i class="icon-th"></i></span> </div>
	     <div  data-date="" class="input-append date datepicker">
	       <input type="text" value="<?php echo !empty($filterVars['dateTo']) ? $filterVars['dateTo'] : '' ?>" name="to" id="FilterDateTo"  data-date-format="mm-dd-yyyy" class="span10" >
	       <span class="add-on"><i class="icon-th"></i></span> </div>
	 </div>
         <!-- Start: Added by Jitendra: 28-Nov-2014 --->  
         <div class="span3">
	   <label>Pay for month of</label>
	    <select name="payment_month[]" id="FilterPaymentMonth" multiple="multiple">
		<?php echo getFormOptions(monthsList(), $filterVars['payment_month']); ?>
	    </select>
	  </div>
	  <div class="span3">
	   <label>Pay for year of</label>
	    <select name="payment_year[]" id="FilterPaymentYear" multiple="multiple">
		<?php echo getFormOptions(yearsList(array('min'=>'2012','max'=>date('Y'))), $filterVars['payment_year']); ?>
	    </select>
	  </div>  
         
          <!-- End: Added by Jitendra: 28-Nov-2014 --->  
       </div>
       
       <div class="form-actions">
	 <button type="reset" class="btn btn-primary">Reset</button>
	 <button type="button" class="btn btn-success" onclick="filterSubmit();">Filter</button>
       </div>
    </form>