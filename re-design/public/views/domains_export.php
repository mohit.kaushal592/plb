<?php
$objPHPExcel = new PHPExcel();
$objPHPExcel->getProperties()->setCreator("Akhtar Khan")
							 ->setLastModifiedBy("Akhtar Khan")
							 ->setTitle("Domains Data")
							 ->setSubject("Domains Data")
							/* ->setDescription("Test document for PHPExcel, generated using PHP classes.")
							 ->setKeywords("office PHPExcel php")*/
							 ->setCategory("Adlift PLB Tool");
$objPHPExcel->setActiveSheetIndex(0)
	    ->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$objPHPExcel->getActiveSheet()
	    ->setCellValue('A1', 'Domains Report');
	    
$objPHPExcel->getActiveSheet()->mergeCells('A1:D1');
$objPHPExcel->getActiveSheet()->getStyle("A1:D1")->getFont()->setSize(20);

$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('A3', 'Domain')
	    ->setCellValue('B3', 'Status');
$objPHPExcel->getActiveSheet()->getStyle('A3:B3')->getFont()->setBold(true);
$domains = $viewData->get('domains');
if(!empty($domains)){
    $domainStatusArray = array('Active'=>1, 'Black Listed'=>0);
    $i=4;
    foreach($domains as $domain){
	$objPHPExcel->getActiveSheet()
		    ->setCellValue('A'. $i, $domain['Domain']['name'])
		    ->setCellValue('B'. $i, array_search($domain['Domain']['status'], $domainStatusArray));
	$i++;
    }
}
            
$objPHPExcel->getActiveSheet()->setTitle('Domains');
$objPHPExcel->setActiveSheetIndex(0);
//$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
//$objWriter->save('domains_da_'.time().'.xlsx');

// Redirect output to a client�s web browser (Excel2007)
header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename="domains_report_'.time().'.xls"');
header('Cache-Control: max-age=0');
// If you're serving to IE 9, then the following may be needed
header('Cache-Control: max-age=1');

// If you're serving to IE over SSL, then the following may be needed
header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
header ('Pragma: public'); // HTTP/1.0

$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
$objWriter->save('php://output');
exit;
?>