<h4 style="color:#DA542E;"><?php echo $viewData->getTitle() ?></h4>
  <hr>
    <?php echo output_message($session->message()) ?>
    <div class="row-fluid">
     <div class="span12">
        <!-- Filter Box -->
      <?php include(WWW_ROOT.DS."layouts".DS."filter_clients.php") ?>
      <!-- Filter Box End -->
	<!-- Actions -->
	  <?php if(canUserDoThis(array('client'))): ?>
	  <div class="btn-group action-right">
          <a href="javascript:void(0)" class="btn btn-primary">Actions</a>
          <a href="javascript:void(0)" data-toggle="dropdown" class="btn btn-primary dropdown-toggle"><span class="caret"></span></a>
          <ul class="dropdown-menu">
	   <?php if(canUserDoThis('client')): ?>
	     <li><a href="javascript:void(0)" rel="update_client" class="doAct"><i class="icon icon-user  icon-large"></i> Client Action</a></li>
	     <li><a href="javascript:void(0)" rel="update_client_approval" class="doAct"><i class="icon-check  icon-large"></i> Approval Phase II</a></li>
	    <?php endif ?>
          </ul>
        </div>
	  <?php endif ?>
	  <div class="btn-group action-right">
	   <label>Records: 
	   <select id="RowPerPage" class="row-per-page">
	    <?php echo getFormOptions(array(10=>10, 20=>20, 30=>30, 40=>40, 50=>50, 60=>60, 70=>70, 80=>80, 90=>90, 100=>100), $_GET['_rp']); ?>
	   </select>
	   </label>
	  </div>
	  <!-- End Actions -->
	
	<div class="widget-box">
          <div class="widget-title" style="background: #DA542E;"> <span class="icon"> <i class="icon-th" style="color: #fff;"></i> </span>
            <h5 style="color: #fff;">Clients Listing</h5>
	    <div class="span2 ttl-record"><h5>Total Records - <?php echo $viewData->get('totalRecords') ?></h5></div>
          </div>
          <div class="widget-content nopadding">
            <form action="clients.php?act=do_action" method="post" id="ClientsListForm">
	     <input type="hidden" name="whatDo" id="ClientsWhatDo" />
	    <table class="table table-bordered table-striped tbl-resize sortable_tbl">
              <thead>
                <tr>
                  <th>Campaign</th>
		  <th>Status</th>
		  <th>Approval Phase II</th>
		  <th>Minimum DA / PR</th>
		  <th>Action</th>
                </tr>
              </thead>
              <tbody> 
                <?php $clients = $viewData->get('clients') ?>
               <?php if(!empty($clients)): ?>
	        <?php foreach($clients as $client): ?>
                <tr class="odd gradeX">
                  <td><?php echo $client['Campaign']['name'] ?>  </td>
                  <td>
		  <ul>
		  <li>
		  <?php if(canUserDoThis('client')): ?>
		  <span>
		   <input type="checkbox" name="data[Campaign][accept][]" class="_accept_client" value="<?php echo $client['Campaign']['id'] ?>"/>
		  <input type="checkbox" name="data[Campaign][reject][]" class="_reject_client" value="<?php echo $client['Campaign']['id'] ?>"/>
		  </span>
		  <?php endif ?>
		  <?php echo getIconHtml($client['Campaign']['status_plb'], array(0=>'Paused', 1=>'Active')) ?></li>
		  </ul>
		  </td>
                  <td><ul>
		  <li>
		  <?php if(canUserDoThis('client')): ?>
		  <span>
		   <input type="checkbox" name="data[Campaign][accept_sec][]" class="_accept_client_sec" value="<?php echo $client['Campaign']['id'] ?>"/>
		  <input type="checkbox" name="data[Campaign][reject_sec][]" class="_reject_client_sec" value="<?php echo $client['Campaign']['id'] ?>"/>
		  </span>
		  <?php endif ?>
		  <?php echo getIconHtml($client['Campaign']['secondary_approval'], array(0=>'Not Required',1=>'Required'))?></li>
		  </ul>
		  </td>
		  <td>
		  <ul>
		  <?php $_da = explode(',', $client['Campaign']['plb_da']);
		  $_pr = explode(',', $client['Campaign']['plb_pr']);
		  if(!empty($_da)){
		   foreach($_da as $k=>$v){
		    $v = round($v, 2);
		    echo "<li>{$v} / {$_pr[$k]}</li>";
		   }
		  }
		  ?>
		  </ul>
		  </td>
		  <td style="text-align: center;"><a title="Click to change DA/PR" class="tip-top" data-toggle="modal" onmouseover="_cid_=<?php echo $client['Campaign']['id'] ?>;_cname_='<?php echo ucwords($client['Campaign']['name']) ?>';" href="#" data-target="#CampaignEdit"><i class="icon-edit icon-large"></i> </a>
</td>
                </tr>
                <?php endforeach ?>
		<?php else: ?>
		<tr><td colspan="5" class="no-record-found">No records found.</td></tr>
		<?php endif ?>
              </tbody>
            </table>
	    </form>
          </div>
        </div>
        <?php echo $viewData->get('pageLinks') ?>
	 </div>
     
     <div id="CampaignEdit" class="modal fade bs-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
      <div class="modal-dialog modal-lg">
       <div class="modal-header">
	  <button type="button" class="close" data-dismiss="modal">&times;</button>
	  <h3>Edit DA/PR for Campaign - <span></span></h3>
	</div>
	<div class="modal-body">
	 Akhtar KHan
	</div>
	<div class="modal-footer">
	 <a href="#" class="btn" data-dismiss="modal">Cancel</a>
	 <a href="javascript:void(0)" id="edit-campaign-submit" class="btn btn-primary">Submit</a>
	</div>
      </div>
    </div>
     
   </div>
    <?php $viewData->scripts(array('js/clients_index.js'), array('inline'=>false)) ?>