<h4 style="color:#DA542E;"><?php echo $viewData->getTitle() ?></h4>
  <hr>
    <div class="row-fluid">
     <div class="span12">
        <div class="widget-box">
	  <div class="widget-title" style="background: #DA542E;"> <span class="icon"> <i class="icon-th" style="color: #fff;"></i> </span>
            <h5 style="color: #fff;">PR Update Status</h5>
          </div>
          <div class="widget-content">
            <div style="text-align:center;">
                Please wait! do not refresh your browser or click on back button. Process will take a while.
            </div>
            <div class="progress progress-striped active">
                <div class="bar" id="updateProgress"  role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%">
                  <span class="sr-only">0%</span>
                </div>
              </div>
          </div>
        </div>
     </div>
    </div>
    <?php $viewData->scripts(array('js/update_pr_all.js'), array('inline'=>false)) ?>