<h4 style="color:#DA542E;"><?php echo $viewData->getTitle() ?></h4>
  <hr>
    <?php echo output_message($session->message()) ?>
    <div class="row-fluid">
     <div class="span12">
      <!-- Filter Box -->
      <?php include(WWW_ROOT.DS."layouts".DS."filter_users.php") ?>
      <!-- Filter Box End -->
      
        <div class="btn-group action-right">
	   <label>Records: 
	   <select id="RowPerPage" class="row-per-page">
	    <?php echo getFormOptions(array(10=>10, 20=>20, 30=>30, 40=>40, 50=>50, 60=>60, 70=>70, 80=>80, 90=>90, 100=>100), $_GET['_rp']); ?>
	   </select>
	   </label>
	  </div>
	<div class="widget-box">
          <div class="widget-title" style="background: #DA542E;"> <span class="icon"> <i class="icon-th" style="color: #fff;"></i> </span>
            <h5 style="color: #fff;">Users Listing</h5>
	    <div class="span2 ttl-record"><h5>Total Records - <?php echo $viewData->get('totalRecords') ?></h5></div>
          </div>
          <div class="widget-content nopadding">
            <table class="table table-bordered table-striped sortable_tbl">
              <thead>
                <tr>
                  <th>Sr. No.</th>
                  <th>Name</th>
                  <th>Username</th>
                  <th>Email</th>
                  <th>Created On</th>
                  <th>Type</th>
                  <th>Status</th>
                  <th>Action</th>
                </tr>
              </thead>
              <tbody> 
                <?php $users = $viewData->get('users') ?>
		<?php if(!empty($users)): ?>
                <?php $i = $viewData->get('recordStartFrom') ?>
                <?php foreach($users as $user): ?>
                <?php 
                //Added By Arvind
                if($user['User']['user_type']=='user' && $user['User']['supper_user']== 1)
                {
                    $user['User']['user_type'] = 'supper user';
                }
                //Ended By Arvind
                ?>
                <tr class="odd gradeX">
                  <td style="color:#DA542E;"><?php echo ++$i ?></td>
                  <td>
                  <a href="users.php?act=profile&uid=<?php echo $user['User']['id'] ?>" target="_blank">
                  <?php echo ucwords($user['User']['first_name'] . " " . $user['User']['last_name']) ?>
                  </a>
                  </td>
                  <td><?php echo $user['User']['username'] ?></td>
                  <td class="center"><?php echo $user['User']['email_id'] ?></td>
                  <td class="center"><?php echo date_to_text($user['User']['created_on']) ?></td>
		    <td class="center"><?php echo $user['User']['user_type'] ?></td>
                  <td class="center">
                    <?php if($user['User']['status'] == 1): ?>
                        <a href="users.php?act=ustatus&id=<?php echo $user['User']['id'] ?>&st=no"><i class="icon-ok icon-large"></i> Active</a>
                    <?php else: ?>
                        <a href="users.php?act=ustatus&id=<?php echo $user['User']['id'] ?>&st=yes"><i class="icon-off icon-large"></i> Deactive</a>
                    <?php endif ?>
                  </td>
                  <td>
                    <a href="users.php?act=edit&uid=<?php echo $user['User']['id'] ?>" ><i class="icon-edit icon-large"></i></a> &nbsp;&nbsp;
                    <a href="users.php?act=change_password&uid=<?php echo $user['User']['id'] ?>"><i class="icon-lock icon-large" style="font-size:15px;"></i></a>
                  </td>
                </tr>
                <?php endforeach ?>
		<?php else: ?>
		<tr><td colspan="8">No records found.</td></tr>
		<?php endif ?>
              </tbody>
            </table>
          </div>
        </div>
        <?php echo $viewData->get('pageLinks') ?>

	 </div>
   </div>
    <?php $viewData->scripts(array('js/users_list.js'), array('inline'=>false)) ?>