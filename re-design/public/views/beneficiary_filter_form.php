<?php $filterVars = $viewData->get('filterVars'); ?>
<form action="beneficiary.php" method="post" class="form-horizontal filter-form" id="FilterForm">
<div class="row mt-4">
	<input type="hidden" name="exportList" id="exportList" value="" />
	<input type="hidden" name="exportListBank" id="exportListBank" value="" />
	   
	  <div class="col-lg-3">
	   <label>Ben Code</label>
	    <input name="domain" autocomplete="off" id="Filterbencode" type="text" class="form-control" value="<?php echo !empty($filterVars['bencode']) ? $filterVars['bencode'] : '' ?>" class="typeahead" />
	  </div>
	   <div class="col-lg-3">
	   <label>Ben Name</label>
	    <input name="domain" autocomplete="off" id="Filterbenname" type="text" class="form-control" value="<?php echo !empty($filterVars['benname']) ? $filterVars['benname'] : '' ?>" class="typeahead" />
	  </div>
	   <div class="col-lg-3">
	   <label>Ben Account</label>
	    <input name="domain" autocomplete="off" id="Filterbenaccount" type="text" class="form-control" value="<?php echo !empty($filterVars['benaccount']) ? $filterVars['benaccount'] : '' ?>" class="typeahead form-control" />
	  </div>
	   <div class="col-lg-3">
	   <label>IFSC Code</label>
	    <input name="domain" autocomplete="off" id="Filterifsccode" type="text" class="form-control" value="<?php echo !empty($filterVars['ifsccode']) ? $filterVars['ifsccode'] : '' ?>" class="typeahead form-control" />
	  </div>
	  
 
		<div class="col-lg-3">
	   	<label>Post Date</label>
	     <div data-date="" class="input-append date datepicker">
	       <input type="date" name="from" value="<?php echo !empty($filterVars['dateFrom']) ? $filterVars['dateFrom'] : '' ?>" id="FilterDateFrom"  data-date-format="mm-dd-yyyy" class="form-control" >
	    </div>
	    </div>
		<div class="col-lg-3">
		<label>&nbsp;</label>
	     <div  data-date="" class="input-append date datepicker">
	       <input type="date" value="<?php echo !empty($filterVars['dateTo']) ? $filterVars['dateTo'] : '' ?>" name="to" id="FilterDateTo"  data-date-format="mm-dd-yyyy" class="form-control" >
	    </div>
	 </div>
	  
         
          <!-- End: Added by Jitendra: 28-Nov-2014 --->  
       
	<div class="col-lg-12 my-4">
		<button type="reset" class="clearbtn">Reset</button>
		<button type="button" class="filterbtn mx-3" onclick="filterSubmit();">Filter</button>
	</div>

</div>
</form>