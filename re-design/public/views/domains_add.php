<h4 style="color:#DA542E;"><?php echo $viewData->getTitle() ?></h4>
  <hr> 
  <p>Please enter each domain in new line to add multiple domains at one time</p>
  <div class="row-fluid">
    <div class="span6">
      <div class="widget-box">
        <div class="widget-title"> <span class="icon"> <i class="icon-align-justify"></i> </span>
          <h5>Add Domains</h5>
        </div>
        <div class="widget-content nopadding">
          <form action="" class="form-horizontal"  method="post" id="AddDomainForm">
            <div class="control-group">
              <label class="control-label">Domains :</label>
              <div class="controls">
                <textarea name="data[Domain][name]" id="DomianName" rows="6" style="resize: none;" placeholder="Enter each domain in new line" class="span11" required="required"></textarea>
              </div>
            </div>
	 
	    <div class="control-group">
              <label class="control-label">Status :</label>
              <div class="controls">
                <label>
		<input type="radio"  name="data[Domain][status]"  id="DomainStatus1" value="1" checked="checked" /> Active </label>
		<label>
                <input type="radio" name="data[Domain][status]" id="DomainStatus0" value="0" /> Blacklist </label>
		<span class="error content-writer" style="display: none;">Please choose domain status.</span>
		</div>
            </div>
    
            <div class="form-actions">
              <button type="submit" class="btn btn-success">Submit</button>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
  
    <?php $viewData->scripts(array('js/domains_add.js'), array('inline'=>false)) ?>