<form action="submitted_links.php?act=do_action" method="post" id="PaymentListForm">
    <input type="hidden" name="whatDo" id="PaymentWhatDo" />
    <div class="table-responsive mt-3">
        <table class="table table-bordered  bg-white">
            <thead>
                <tr>

                    <th>Ben Code</th>
                    <th>Ben Name</th>
                    <th>Ben Account</th>
                    <th>IFSC Code</th>
                    <th>Address</th>
                    <th>City</th>
                    <th>State</th>
                    <th>Zip Code</th>
                    <th>Post date</th>


                </tr>
            </thead>
            <tbody>
                <?php $submittedLinks = $viewData->get('submittedLinks') ?>
                <?php if(!empty($submittedLinks)): ?>
                <?php foreach($submittedLinks as $submittedLink): ?>
                <tr class="odd gradeX">

                    <td><?php echo $submittedLink['Beneficiary']['bencode'] ?></td>
                    <td><?php echo $submittedLink['Beneficiary']['benname'] ?></td>
                    <td><?php echo $submittedLink['Beneficiary']['benaccount'] ?></td>
                    <td><?php echo $submittedLink['Beneficiary']['ifsccode'] ?></td>
                    <td><?php echo $submittedLink['Beneficiary']['address'] ?></td>
                    <td><?php echo $submittedLink['Beneficiary']['city'] ?></td>
                    <td><?php echo $submittedLink['Beneficiary']['state'] ?></td>
                    <td><?php echo $submittedLink['Beneficiary']['zip_code'] ?></td>
                    <td><?php echo $submittedLink['Beneficiary']['postdate'] ?></td>

                </tr>
                <?php endforeach ?>
                <?php else: ?>
                <tr>
                    <td colspan="9" class="no-record-found">No records found.</td>
                </tr>
                <?php endif ?>
            </tbody>
        </table>
    </div>
</form>