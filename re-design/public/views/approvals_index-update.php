<div class="row mt-4">
    <h4 class="darkBreadcrumb">
        <span class="lightBeradcrumb">Approval</span> / Approval Listing
    </h4>
</div>
<div class="row mt-1">
    <h3 class="tableMainTitle">Approval Listing</h3>
</div>
<?php echo output_message($session->message()) ?>
<?php $filterVars = $viewData->get('filterVars') ?>
<form action="#" method="post" class="form-horizontal filter-form" id="FilterForm">
    <?php $filterVars = $viewData->get('filterVars') ?>

    <!-- Filters Start -->
    <!-- <h3 class="form-label">Filters</h3>
    <div class="row">
        <div class="col-lg-2">
            <select name="users[]" class="form-select form-control" id="FilterUser" multiple="multiple">
                <?php echo getFormOptions($viewData->get('usersList'), $filterVars['users']); ?>
            </select>
        </div>

        <div class="col-lg-2">
            <select name="campaigns[]" id="FilterCampaign" class="form-select form-control" multiple="multiple">
                <?php echo getFormOptions($viewData->get('campaignsList'), $filterVars['campaigns']); ?>
            </select>
        </div>
        <div class="col-lg-2">
            <div class="search_filter">
                <input type="text" value="<?php echo !empty($filterVars['domain']) ? $filterVars['domain'] : '' ?>"
                    class="searchInput" placeholder="Search by Domain" name="domain" autocomplete="off"
                    id="FilterDomain">
                <img src="Assets/Icons/searchIcon.svg" alt="" class="searchImg">
            </div>
        </div>
     

    </div> -->
    <!-- Filters End -->



    <div class="mt-4 shadow p-3  bg-white rounded p-4">
        <div class="row">
            <div class="col-12 col-md-3 mt-3">
                <h4 for="ListOfPersons" class="form-label">User</h4>
                <select name="users[]" id="FilterUser" multiple="multiple" style="width:250px">
                    <?php echo getFormOptions($viewData->get('usersList'), $filterVars['users']); ?>
                </select>
            </div>
            <div class="col-12 col-md-3 mt-3">
                <h4 for="ListOfPersons" class="form-label">Campaign</h4>
                <select name="campaigns[]" id="FilterCampaign" multiple="multiple" style="width:250px">
                    <?php echo getFormOptions($viewData->get('campaignsList'), $filterVars['campaigns']); ?>
                </select>
            </div>
            <div class="col-lg-3 mt-3">
                <label class="form-label">Domains</label>
                <input name="domain" autocomplete="off" id="FilterDomain" type="text" class="form-control"
                    value="<?php echo !empty($filterVars['domain']) ? $filterVars['domain'] : '' ?>"
                    class="typeahead" />
            </div>
            <div class="col-12 col-md-3 mt-3">
                <h4 for="ListOfPersons" class="form-label">Status (Approval Phase I)</h4>
                <select name="approval_st_p1[]" id="FilterAppStatusPhase1" multiple="multiple" style="width:250px">
                    <?php echo getFormOptions(array('1'=>'Approved', '0'=>'Disapproved', '-1'=>'For approval'), $filterVars['status']); ?>
                </select>
            </div>
        </div>
        <div class="row">
            <div class="col-12 col-md-3 mt-3">
                <h4 for="ListOfPersons" class="form-label">Status (Approval Phase II)</h4>
                <select name="approval_st_p2[]" id="FilterAppStatusPhase2" multiple="multiple" style="width:250px">
                    <?php echo getFormOptions(array('1'=>'Approved', '0'=>'Disapproved', '-1'=>'For approval'), $filterVars['status_phase2']); ?>
                </select>
            </div>
            <!-- <div class="col-12 col-md-3 mt-3">
                <h4 for="ListOfPersons" class="form-label">Forward Status</h4>
                <select name="forword" id="FilterForword" style="width:250px">
                    <?php echo getFormOptions(array('2'=>'All', '1'=>'Forwarded', '0'=>'Non Forwarded'), $filterVars['forward']); ?>
                </select>
            </div> -->

            <div class="col-lg-3  mt-3">
                <label class="form-label">Date</label>
                <div data-date="" class="input-append date datepicker">
                    <input type="date" name="from"
                        value="<?php echo !empty($filterVars['dateFrom']) ? $filterVars['dateFrom'] : '' ?>"
                        id="FilterDateFrom" data-date-format="mm-dd-yyyy" class="form-control">
                </div>
            </div>
            <div class="col-lg-3 mt-3">
                <label class="form-label">Date</label>
                <div data-date="" class="input-append date datepicker">
                    <input type="date" value="<?php echo !empty($filterVars['dateTo']) ? $filterVars['dateTo'] : '' ?>"
                        name="to" id="FilterDateTo" data-date-format="mm-dd-yyyy" class="form-control">
                </div>
            </div>
            <div class="row">
                <div class="col-12 col-md-3 mt-3">
                    <h4 for="ListOfPersons" class="form-label">Link Status</h4>
                    <?php
				$isLinkStatusEnabled = (
				(in_array($filterVars['link_status'], array(0,1, -1)) and $filterVars['link_status']!='')
				or
					(
						(count($filterVars['status_phase2'])==1 and $filterVars['status_phase2'][0]==1)
						or
						(count($filterVars['status'])==1 and $filterVars['status'][0]==1)
					)
				);
			?>
                    <select name="link_status" id="FilterLinkStatus"
                        data-dflt="<?php echo $filterVars['link_status'] ?>"
                        <?php echo $isLinkStatusEnabled==true ? '' : 'disabled="disabled"' ?>>
                        <?php echo getFormOptions(array('-1'=>'All', '1'=>'Submitted', '0'=>'Not Submitted'), $filterVars['link_status']); ?>
                    </select>
                </div>
                <!-- <div class="col-12 col-md-3 mt-3">
                    <h4 for="ListOfPersons" class="form-label">Content Writer</h4>
                    <select name="content_writer" id="FilterContentWriter" style="width:250px">
                        <?php echo getFormOptions(array('-1'=>'All', '0'=>'Adlift', '1'=>'External'), $filterVars['content_writer']); ?>
                    </select>
                </div> -->
                <div class="col-12 col-md-3 mt-3">
                    <h4 for="ListOfPersons" class="form-label">DA</h4>
                    <select name="da" id="FilterDA" style="width:250px">
                        <option value="">Select DA</option>
                        <?php echo getFormOptions($viewData->get('daOptions'), $filterVars['da']); ?>
                    </select>
                </div>
                <div class="col-12 col-md-3 mt-3">
                    <h4 for="ListOfPersons" class="form-label">SC</h4>
                    <select name="sc" id="FilterSC" style="width:250px">
                        <option value="">Select SC</option>
                        <?php echo getFormOptions($viewData->get('scOptions'), $filterVars['sc']); ?>
                    </select>
                </div>
                <div class="col-12 col-md-3 mt-3">
                    <h4 for="ListOfPersons" class="form-label">Country</h4>
                    <select name="ac" id="FilterAC">
                        <option value="">Country</option>
                        <?php echo getFormOptions($viewData->get('acOptions'), $filterVars['ac']); ?>
                    </select>
                </div>
            </div>
        </div>
        <div class="row  my-4">
            <div class="col-lg-12">
                <button type="reset" class="clearbtn">Reset</button>
                <button type="submit" class="filterbtn mx-3">Submit</button>
            </div>
        </div>
</form>
<?php $viewData->scriptStart() ?>
jQuery.fn.reLoadLinkStatus = function(){
var anotherSelectId = $(this).attr('id')=='FilterAppStatusPhase1' ? 'FilterAppStatusPhase2' : 'FilterAppStatusPhase1';
var selectedOpt = $(this).multiselect("getChecked").map(function(){
return this.value;
}).get();

var anotherSelect = $('select#'+ anotherSelectId).multiselect("getChecked").map(function(){
return this.value;
}).get();

var valDefault= $('select#FilterLinkStatus').attr('data-dflt');
valDefault = valDefault.length>0 ? valDefault : '-1';

if((selectedOpt.length==1 && selectedOpt[0]=='1') || (anotherSelect.length==1 && anotherSelect[0]=='1')){
$('select#FilterLinkStatus').multiselect('enable');

}else{
$('select#FilterLinkStatus').val(valDefault).multiselect('disable');
}
}
$(document).ready(function(){
// autocomplete
$('#FilterDomain').typeahead({
ajax: 'domains.php?act=list_json',
display: 'name',
val: 'name'
});

$('select[id^=FilterAppStatusPhase]').on('multiselectclick', function(event, ui){
$(this).reLoadLinkStatus();
});

$('#FilterForm').bind('submit', function(e){

e.preventDefault();
var users = $("#FilterUser").multiselect("getChecked").map(function(){
return this.value;
}).get();
var campaigns = $("#FilterCampaign").multiselect("getChecked").map(function(){
return this.value;
}).get();
var approvalStPhase1 = $("#FilterAppStatusPhase1").multiselect("getChecked").map(function(){
return this.value;
}).get();
var approvalStPhase2 = $("#FilterAppStatusPhase2").multiselect("getChecked").map(function(){
return this.value;
}).get();
var forword = $("#FilterForword").multiselect("getChecked").map(function(){
return this.value;
}).get()[0];
var contentWriter = $("#FilterContentWriter").multiselect("getChecked").map(function(){
return this.value;
}).get()[0];
var _daVal = $("#FilterDA").multiselect("getChecked").map(function(){
return this.value;
}).get()[0];
var _scVal = $("#FilterSC").multiselect("getChecked").map(function(){
return this.value;
}).get()[0];

var _acVal = $("#FilterAC").multiselect("getChecked").map(function(){
return this.value;
}).get()[0];

var linkStatus = !($("#FilterLinkStatus").prop('disabled')) ?
($("#FilterLinkStatus").multiselect("getChecked").map(function(){
return this.value;
}).get()[0]) : '';
contentWriter = contentWriter > -1 ? contentWriter : '';
linkStatus = linkStatus >= -1 ? linkStatus : '';
var dateFrom = $('#FilterDateFrom').val();
var dateTo = $('#FilterDateTo').val();
var domain = $('#FilterDomain').val();
var dateRange = (dateFrom+'_'+dateTo);
dateRange = dateRange.replace(/^\s*\_\s*$/g, '');
var canSubmit = true;
var _rp = $('#RowPerPage').val();

if(approvalStPhase1.length>0 && approvalStPhase2.length>0){
alert('Please choose either Status (Approval Phase I) or Status (Approval Phase II).');
canSubmit=false;
}

if(canSubmit==true){
var curUrl = $.parseUrl();

var queryString = curUrl.query;
queryString._uid = $.base64.encode(users.toString());
queryString._camp = $.base64.encode(campaigns.toString());
queryString._st = $.base64.encode(approvalStPhase1.toString());
queryString._st2 = $.base64.encode(approvalStPhase2.toString());
queryString._dt = $.base64.encode(dateRange);
queryString._dom = $.base64.encode(domain);

//queryString._fw = $.base64.encode(forword);
queryString._cw = $.base64.encode(contentWriter);
queryString._ls = $.base64.encode(linkStatus);

queryString._rp = _rp;
queryString._da = $.base64.encode(_daVal);
queryString._sc = $.base64.encode(_scVal);
queryString._ac = $.base64.encode(_acVal);
queryString.p = 1;
var urlParams = [];

$.each(queryString, function(k,v){
//console.log('queryString__',v)
if(v && v.length>0){
urlParams.push(k+'='+v);
}
});
if(urlParams.length>=1){
window.location = '?'+urlParams.join('&');
}
}
});

$('#RowPerPage').on('change', function(){
var curUrl = $.parseUrl();
var queryString = curUrl.query;
var _rp = $('#RowPerPage').val();

var urlParams = [];
queryString._rp=_rp;
queryString.p=1;
$.each(queryString, function(k,v){
if(v.length>0){
urlParams.push(k+'='+v);
}
});

if(urlParams.length>=1){

window.location = '?'+urlParams.join('&');
}
});
});
<?php $viewData->scriptEnd() ?>

<div class="row mb-4">
    <div class="col-lg-12">
        <h4 class="mx-3"> Listing </h1>
            <div class="table-responsive">
                <div class="row justify-content-end mb-4">
                    <div class="col-lg-2">
                        <select class="form-select form-control" style="width:0px">
                            <option disabled selected>Select Option</option>
                            <?php if(canUserDoThis(array('forward','approve_phase_1','approve_phase_2','approval_delete'))): ?>
                            <?php if(canUserDoThis('forward')): ?>
                            <option><a href="javascript:void(0)" rel="approval_forward" class="doAct"><i
                                        class="icon-step-forward  icon-large"></i> Forward</a></option>
                            <?php endif ?>
                            <?php if(canUserDoThis('approve_phase_1')): ?>
                            <option><a href="javascript:void(0)" rel="approval_phase_1" class="doAct"><i
                                        class="icon-ok icon-large"></i><sup class="icontxt">1</sup> Approval Phase
                                    I</a>
                            </option>
                            <?php endif ?>
                            <?php if(canUserDoThis('approve_phase_2')): ?>
                            <option><a href="javascript:void(0)" rel="approval_phase_2" class="doAct"><i
                                        class="icon-ok icon-large"></i><sup class="icontxt">2</sup> Approval Phase
                                    II</a></option>
                            <?php endif ?>
                            <?php if(canUserDoThis('approval_delete')): ?>
                            <option><a href="javascript:void(0)" rel="approval_delete" class="doAct"><i
                                        class="icon-trash icon-large"></i> Delete Link</a></option>
                            <?php endif ?>
                            <?php endif ?>
                            <option><a id="_ExportToExcel" href="javascript:void(0)"><i
                                        class="icon-download-alt icon-large"></i> Export</a></option>
                        </select>
                    </div>
                </div>
                <table id="example" class="table table-bordered mx-3" cellspacing="0" width="100%">
                    <thead>
                        <th style="width: 206px;">
                            <?php if(canUserDoThis(array('forward','approval_delete'))): ?>
                            <span style="float:left"><input type="checkbox" id="checkAllApproval" /></span>
                            <?php endif ?>
                            Domain
                        </th>
                        <th style="width: 50px;">Category</th>

                        <th style="width: 25px;">PR</th>
                        <th style="width: 25px;">DA</th>
                        <th style="width: 25px;">SC</th>
                        <th style="width: 30px;">Country</th>
                        <th style="width: 30px;">Index Count</th>
                        <th style="width: 30px;">Adult Content</th>
                        <th style="width: 30px;">Gambling</th>
                        <th>User</th>
                        <th>Date</th>
                        <th style="width: 25px;">Count</th>
                        <th style="width: 225px;"> <span style="float:left"><input type="checkbox"
                                    id="checkAllApprovalAccepted" /></span>
                            <span style="float:left"><input type="checkbox" id="checkAllApprovalReceted" /></span>
                            Campaign
                        </th>
                        <th>Text</th>
                        <th>Admin Text</th>
                        <th>Content Writer</th>
                        <th>Action</th>
                    </thead>
                    <tbody>
                        <?php $approvals = $viewData->get('approvals') ?>
                        <?php if(!empty($approvals)): ?>
                        <?php foreach($approvals as $approval): ?>
                        <tr class="odd gradeX" domain="<?php echo $approval['Approval']['domain'] ?>">
                            <td>
                                <?php if(canUserDoThis(array('forward','approval_delete'))): ?>
                                <input type="checkbox" id="ApprovalId<?php echo $approval['Approval']['id'] ?>"
                                    class="_approval_id" name="data[Approval][id][]"
                                    value="<?php echo $approval['Approval']['id'] ?>" />
                                <?php endif ?>
                                <a class="linkList_td" href="<?php echo $approval['Approval']['domain'] ?>"
                                    target="_blank"><?php echo $approval['Approval']['domain'] ?></a>
                                <span
                                    onclick="return getDomainHistory('<?php echo $approval['Approval']['domain'] ?>');"
                                    class="domain-ip tip tip-top"
                                    title="Click to view Domain History"><?php echo $approval['Approval']['ip'] ?></span>
                            </td>
                            <td><?php echo ucwords($approval['Approval']['category']) ?></td>
                            <td><?php echo round($approval['Approval']['pr'],2) ?></td>
                            <td><?php echo round($approval['Approval']['da'], 2) ?></td>
                            <td><?php echo round($approval['Approval']['sc'], 2) ?></td>
                            <td><?php echo $approval['Approval']['ac'] ?></td>
                            <td><?php echo $approval['Approval']['index_count'] ?></td>
                            <td><?php echo $approval['Approval']['adult'] ?></td>
                            <td><?php echo $approval['Approval']['gambling'] ?></td>
                            <td><?php echo ucwords($approval['User']['first_name'].' '.$approval['User']['last_name']) ?>
                            </td>
                            <td><?php echo date_to_text($approval['Approval']['added_on']) ?></td>
                            <td><?php echo $approval['Approval']['campaigns_count'] ?></td>
                            <td>
                                <?php
					$_camps = array();
					if(!empty($approval['ApprovalLink'])):
					foreach($approval['ApprovalLink'] as $_appLink):
					$_cmpHtml = "<li>";
					if(canUserDoThis(array('approve_phase_1','approve_phase_2'), $_appLink, true)):
					$_cmpHtml .= "<span>";
					$_cmpHtml .= '<input type="checkbox" id="ApprovalLinkAccept'. $approval['Approval']['id'].'_'.$_appLink['id'].'" class="_approval_accept" name="data[ApprovalLink][accept][]" value="'.$_appLink['id'].'" /> ';
					$_cmpHtml .= '<input type="checkbox" id="ApprovalLinkReject'. $approval['Approval']['id'].'_'.$_appLink['id'].'" class="_approval_reject" name="data[ApprovalLink][reject][]" value="'.$_appLink['id'].'" /> ';
					$_cmpHtml .= '</span> ';
					endif;
					$_cmpHtml .= '<span>'.ucwords($_appLink['campaign_name']);
								if($_appLink['final_link_submit_status'] == 0 && $_appLink['Amount'] != 0 && $_appLink['Currency'] != "")
								{
									$_cmpHtml .= ' ('.currency_format($_appLink['Amount'], $_appLink['Currency']).')';
								}
								elseif ($_appLink['final_link_submit_status'] == 1)
								{
									//$_cmpHtml .= ' ($ 231)';
									//echo "select Amount, Currency from tracking_data where child_table_id=".$_appLink['id'];exit;
									//$resultJitu = $dbObj->find_by_sql("select Amount, Currency from tracking_data where child_table_id=".$_appLink['id']);
									//echo "<pre>";print_r($resultJitu);exit;
								}
							
					$_cmpHtml .= '</span>';
					$_cmpHtml .= getCampaignIcons($_appLink, $viewData);
					
					$_cmpHtml .= '</li>';
					$_camps[] = $_cmpHtml;
					endforeach;
					endif;
					?>
                                <ul style="width:220px"><?php echo implode(' ', $_camps) ?></ul>
                            </td>
                            <td><?php echo $approval['Approval']['narration_text'] ?></td>
                            <td id="_row_<?php echo $approval['Approval']['id'] ?>"><span
                                    style="display: block;width: 100%;"><?php echo nl2br($approval['Approval']['narration_admin']) ?></span>
                                <?php if(canUserDoThis(array('admin_comment'))): ?> 
                                <a data-toggle="modal" data-backdrop='static' data-target="#AdminCommentAdd"
                                    onmouseover="approvalId='<?php echo $approval['Approval']['id'] ?>'; domainname='<?php echo $approval['Approval']['domain'] ?>'"
                                    href="#" title="Click to add comment." class="tip-right"><img
                                        src="Assets/Icons/Plus.svg" alt="" id="AdminCommentAddS"></a>
                                <?php endif ?>
                            </td>
                            <td><?php echo $approval['Approval']['content_writer']==1?'External' : ($approval['Approval']['content_writer']==0?'Adlift' : '') ?>
                            </td>
                            <td>
                                <?php   //if($approval['User']['id'] == $session->read('User.id') || $session->read('User.user_type') == 'superadmin'):  ?>
                                <!--		    <a href="approvals.php?act=edit_link&pid=<?php echo $approval['Approval']['id'] ?>" class="tip-top" title="Click to edit link." target="_blank"> <i class="icon icon-edit icon-large"></i></a>-->
                                <?php //endif ?>

                                <?php   if($session->read('User.user_type') == 'superadmin'):  ?>
                                <a href="approvals.php?act=edit_link&pid=<?php echo $approval['Approval']['id'] ?>"
                                    class="tip-top" title="Click to edit link." target="_blank"> <img
                                        src="Assets/Icons/NotePencil.svg" alt=""></a>
                                <?php endif ?>
                            </td>
                        </tr>
                        <?php endforeach ?>
                        <?php else: ?>
                        <tr>
                            <td colspan="10" class="no-record-found">No records found.</td>
                        </tr>
                        <?php endif ?>
                    </tbody>
                </table>
            </div>
    </div>
    <?php echo $viewData->get('pageLinks') ?>
</div>
<div id="AdminCommentAdd" class="modal fade bs-modal-lg1" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg bg-white ">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h3>Admin Comment <span></span>
            </h3>
        </div>
        <div class="modal-body"></div>
        <div class="modal-footer">
            <!-- <a href="#" class="btn close" data-dismiss="modal" data-dismiss="modal" aria-label="Close">Cancel</a> -->
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            <a href="javascript:void(0)" id="admin-add-comment" class="btn btn-primary">Submit</a>
        </div>
    </div> 
</div>
 
<!-- <script src="https://cdn.jsdelivr.net/npm/popper.js@1.12.9/dist/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script> -->
<!-- Button trigger modal -->
 

<?php $viewData->scripts(array('js/approvals_list.js'), array('inline'=>false)) ?>