<div class="row mt-4">
    <h4 class="darkBreadcrumb"><span class="lightBeradcrumb">Domains</span> / List</h4>
</div>
<div class="row mt-3">
    <h3 class="tableMainTitle"><?php echo $viewData->getTitle() ?></h3>
</div>

<?php include(WWW_ROOT.DS."layouts".DS."filter_paypal.php") ?>

<div class="row mt-4">
    <div class="col-lg-12">
        <form action="paypal.php?act=do_action" method="post" id="PaypalListForm">
            <input type="hidden" name="whatDo" id="PaypalWhatDo" />
            <div class="table-responsive mt-3">
                <table class="table bg-white table-bordered">
                    <thead>
                        <tr>
                            <?php if(canUserDoThis('paypal')): ?>
                            <!--<th style="width:50px;"></th>-->
                            <?php endif ?>
                            <th style="font-weight: 600;">Paypal ID </th>
                            <th style="font-weight: 600;">Domains</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $paypalLists = $viewData->get('paypalLists') ?>
                        <?php if(!empty($paypalLists)): ?>
                        <?php foreach($paypalLists as $paypalList): ?>
                        <tr class="odd gradeX">
                            <?php if(canUserDoThis('paypal')): ?>
                            <!--<td></td>-->
                            <?php endif ?>
                            <td>
                                <ul style="padding-left:0px">
                                    <li style="position:relative">
                                        <?php if(canUserDoThis('paypal')): ?>
                                        <span><input type="checkbox" name="data[Paypal][accept][]"
                                                class="_accept_paypal"
                                                value="<?php echo $paypalList['Paypal']['id'] ?>" />
                                            <input type="checkbox" name="data[Paypal][reject][]" class="_reject_paypal"
                                                value="<?php echo $paypalList['Paypal']['id'] ?>" /></span>
                                        <?php endif ?>
                                        <span><?php echo $paypalList['Paypal']['name'] ?></span>
                                        <span
                                            class="tickIcon"><?php echo getIconHtml($paypalList['Paypal']['status'], array(0=>'Black Listed', 1=>'Active')) ?></span>
                                    </li>
                                </ul>
                            </td>
                            <td>
                                <?php
		    $_domains = array();
		    if(!empty($paypalList['Domain'])):
		      foreach($paypalList['Domain'] as $_domain):
		       $_htm = "<span>";
		       $_htm .= '<input type="checkbox" name="data[Domain][accept][]" class="_accept_domain" value="'. $_domain['id']. '"/>';
		       $_htm .= '<input type="checkbox" name="data[Domain][reject][]" class="_reject_domain" value="'.$_domain['id'].'"/>';
		       $_htm .= "</span>";
		       $_htm .= '<span>'. $_domain['name'] . '</span>';
		       $_htm .= '<span class="tickIcon">'.getIconHtml($_domain['status'], array(0=>'Black Listed', 1=>'Active')).'</span>';
			$_domains[] = '<li style="position:relative">'. $_htm. "</li>";
		      endforeach;
		    endif;
		    ?>
                                <ul style="padding-left:0px"><?php echo implode(' ', $_domains) ?></ul>
                            </td>
                        </tr>
                        <?php endforeach ?>
                        <?php else: ?>
                        <tr>
                            <td colspan="9" class="no-record-found">No records found.</td>
                        </tr>
                        <?php endif ?>
                    </tbody>
                </table>
            </div>
    </div>
</div>
<?php echo $viewData->get('pageLinks') ?>
</div>
<?php $viewData->scripts(array('js/paypal_index.js'), array('inline'=>false)) ?>