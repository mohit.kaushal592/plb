<h4 style="color:#DA542E;"><?php echo $viewData->getTitle() ?></h4>
  <hr> 
  <p>Please Check <span style="color:#28B779">Domain History</span> before submission to prevent domain duplicacy for same campaign</p>
  <div class="row-fluid"> 
    <div class="span6">
      <div class="widget-box">
        <div class="widget-title"> <span class="icon"> <i class="icon-align-justify"></i> </span>
          <h5>Add Link</h5>
        </div>
        <div class="widget-content nopadding">
          <form action="" class="form-horizontal"  method="post" id="AddLinkForm">
            <div class="control-group">
			<a href="#" class="history DomainIpHistory" id="DomainHistory"><img src="img/history.png"/></a>
              <label class="control-label">Domain Name :</label>
              <div class="controls">
                <input type="text" class="span11" placeholder="Domain Name" name="data[Approval][domain]" id="ApprovalDomain" minlength="4" required/>
              </div>
            </div>
	    <div class="control-group">
              <label class="control-label">Select Campaign :</label>
              <div class="controls">
                <select name="data[Approval][campaign][]" multiple="multiple" id="ApprovalCampaign" title="Please select campaign." required>
                 <?php $campsList = $viewData->get('campaignsList');
                 echo getFormOptions($campsList);
                 ?>
                </select>
              </div>
            </div>
			<div class="control-group">
              <label class="control-label">Narration text :</label>
              <div class="controls">
                <input type="text" class="span11" placeholder="" name="data[Approval][narration_text]" id="ApprovalNarrationText" />
              </div>
            </div>
              <div class="control-group">
              <label class="control-label">Payment Type :</label>
              <div class="controls">
                <select name="data[Approval][payment_type]"  id="ApprovalPayment_type" title="Please select Payment Type." required>
                <option value="">Select Payment Type</option>
				<option value="Paypal">Paypal</option>
				<option value="NEFT">NEFT</option>
                </select>
              </div>
            </div>
              
              
              <div class="control-group">
              <label class="control-label">Amount :</label>
              <div class="controls">
                <input  type="number" class="span11" pattern="^\d+(\.)\d{2}$" placeholder="" name="data[Approval][Amount]" id="PaymentAmount" required/>
              </div>
            </div>
	    <div class="control-group">
              <label class="control-label">Currency :</label>
              <div class="controls">
                  <select onchange="return checkCurrency(this.value);" name="data[Approval][Currency]" id="PaymentCurrency" title="Please select Currency." required>
		  <option value=''>Select Currency</option>
		  <?php echo getFormOptions(array('INR'=>'INR', 'Euro'=>'Euro', 'Dollars'=>'Dollars', 'Pounds'=>'Pounds'), $payment['Approval']['Currency']) ?>
                </select>
              </div>
            </div>

            <div class="control-group">
              <label class="control-label">Category :</label>
              <div class="controls">
                <input type="text" class="span11" placeholder="Category Name" name="data[Approval][category]" required/>
              </div>
            </div>

            // <div class="control-group">
            //   <label class="control-label">DR Value :</label>
            //   <div class="controls">
            //     <input type="number" class="span11" placeholder="DR Value" name="data[Approval][dr]" value='0' min="0">
            //   </div>
            // </div>

            <div class="control-group">
              <label class="control-label">Total No of links :</label>
              <div class="controls">
                <input type="text" class="span11" placeholder="Total No of links" name="data[Approval][duration]"/>
              </div>
            </div>

<!--            Ajit:   Add No Payment option in currency on 15 DESC 2015-->
              
			<div class="form-actions">
              <button type="button" class="btn btn-danger" id="ApprovalCalcDaPr">Calculate  PR,DA and IP</button>
            </div>
			
            <div class="control-group">
			<a href="#" class="history DomainIpHistory" id="IpHistory"><img src="img/iphistory.png"/></a>
              <label class="control-label">IP Address :</label>
              <div class="controls">
                <input type="text"  class="span11" placeholder="IP Address" name="data[Approval][ip]" id="ApprovalIp"  required/>
              </div>
            </div>
            <div class="control-group">
              <label class="control-label">PR value :</label>
              <div class="controls">
                <input type="text" class="span11"  placeholder="PR value" name="data[Approval][pr]" id="ApprovalPr" required/>
              </div>
            </div>
            <div class="control-group">
              <label class="control-label">DA value :</label>
              <div class="controls">
                <input type="text" class="span11"  placeholder="DA value" name="data[Approval][da]" id="ApprovalDa" required/>
				</div>
            </div>
			<div class="control-group">
              <label class="control-label">SS value (%) :</label>
              <div class="controls">
                <input type="text" class="span11"  placeholder="SC value" name="data[Approval][sc]" id="ApprovalSc" required/>
				</div>
            </div>
            <div class="control-group">
              <label class="control-label">Annual Traffic :</label>
              <div class="controls">
                <input type="text" class="span11" placeholder="Traffic value" name="data[Approval][annual_traffic]" id="ApprovalTraffic" value="0"/>
                </select>
              </div>
            </div>
            <div class="control-group">
              <label class="control-label">Administrative Country :</label>
              <div class="controls">
                <input type="text" class="span11"  placeholder="Administrative Country" name="data[Approval][ac]" id="ApprovalAc" required/>
        </div>
            </div>
	  //   <div class="control-group">
    //           <label class="control-label">Content Writer :</label>
    //           <div class="controls">
    //             <label>
		// <input type="radio"  name="data[Approval][content_writer]"  id="ApprovalContentWriter0" value="0" /> Adlift </label>
		// <label>
    //             <input type="radio" name="data[Approval][content_writer]" id="ApprovalContentWriter1" value="1" /> External</label>
		// <span class="error content-writer" style="display: none;">Please choose content writer.</span>
		// </div>
    //         </div>
<?php 
$keyword = isset($_REQUEST['keyword'])?$_REQUEST['keyword']:"";
$sv = isset($_REQUEST['sv'])?$_REQUEST['sv']:"";
$rank = isset($_REQUEST['rank'])?$_REQUEST['rank']:"";
$title = isset($_REQUEST['title'])?$_REQUEST['title']:"";
$title_id = isset($_REQUEST['title_id'])?$_REQUEST['title_id']:"";
$target_url = isset($_REQUEST['target_url'])?$_REQUEST['target_url']:"";
?>

<input type="hidden" name="data[Approval][keyword]" value="<?php echo $keyword; ?>">
<input type="hidden" name="data[Approval][sv]" value="<?php echo $sv; ?>">
<input type="hidden" name="data[Approval][rankval]" value="<?php echo $rank; ?>">
<input type="hidden" name="data[Approval][title]" value="<?php echo $title; ?>">
<input type="hidden" name="data[Approval][title_id]" value="<?php echo $title_id; ?>">
<input type="hidden" name="data[Approval][target_url]" value="<?php echo $target_url; ?>">
<input type="hidden" name="data[Approval][content_writer]" value="0">
<input type="hidden" name="data[Approval][backlinks]" value="0">



            <div class="form-actions">
              <button type="submit" class="btn btn-success">Submit</button>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
  
    <?php $viewData->scripts(array('js/approvals_add_link.js?1=2'), array('inline'=>false)) ?>
