<?php $viewData->set('bodyClass', preg_replace("/\.php$/i", '', $useLayout))?>

<?php  
    $theme_name = 'old';
    if(isset($_GET['change-theme'])){ 
        $theme_name = $_GET['change-theme'];
        setcookie('theme-name', $theme_name, time() + (86400 * 30 * 30), "/");
    }else{
        if(isset($_COOKIE['theme-name'])){
            $theme_name = $_COOKIE['theme-name'];
        }
    }
?> 

<?php //if( $theme_name == 'new' ){ ?>
    <?php if( true ){ ?>
    <?php include('layouts/header-update.php') ?>
    <?php //include('layouts/left_sidebar.php') ?>
    <?php //include('layouts/content_start.php') ?>
    <?php 
    $useLayout = str_replace('.','-update.',$useLayout);
   
    if(file_exists(dirname(__FILE__).DS.$useLayout)){ 
        //echo dirname(__FILE__).DS.$useLayout; die;
        include(dirname(__FILE__).DS.$useLayout);
    } else {
        header("HTTP/1.0 404 Not Found");
        include(dirname(__FILE__).DS.'error404.php');
    }
    ?>
    <?php include('layouts/footer-update.php') ?>

<?php
        }elseif( $theme_name == 'old' ){
?>
    <?php include('layouts/header.php') ?>
    <?php include('layouts/left_sidebar.php') ?>
    <?php include('layouts/content_start.php') ?>
    <?php
    if(file_exists(dirname(__FILE__).DS.$useLayout)){ 
        include(dirname(__FILE__).DS.$useLayout);
    } else {
        header("HTTP/1.0 404 Not Found");
        include(dirname(__FILE__).DS.'error404.php');
    }
    ?>
    <?php include('layouts/footer.php') ?>
<?php } ?>
