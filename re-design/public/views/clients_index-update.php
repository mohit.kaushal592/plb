<div class="row mt-4">
     <h4 class="darkBreadcrumb"><span class="lightBeradcrumb">Clients</span> / Client Listing</h4>
 </div>
 <div class="row mt-1">
     <h3 class="tableMainTitle"><?php echo $viewData->getTitle() ?></h3>
 </div>

 <?php echo output_message($session->message()) ?>

 <?php include(WWW_ROOT.DS."layouts".DS."filter_clients.php") ?>

 <div class="row mt-4">
     <div class="col-lg-12">
         <form action="clients.php?act=do_action" method="post" id="ClientsListForm">
             <input type="hidden" name="whatDo" id="ClientsWhatDo" />
             <div class="table-responsive mt-3">
                 <table class="table bg-white table-bordered">
                     <thead>
                         <tr>
                             <th>Campaign</th>
                             <th>Status</th>
                             <th>Approval Phase II</th>
                             <th>Minimum DA / PR</th>
                             <th>Action</th>
                         </tr>
                     </thead>
                     <tbody>
                         <?php $clients = $viewData->get('clients') ?>
                         <?php if(!empty($clients)): ?>
                         <?php foreach($clients as $client): ?>
                         <tr class="odd gradeX">
                             <td><?php echo $client['Campaign']['name'] ?> </td>
                             <td>
                                 <ul>
                                     <li>
                                         <?php if(canUserDoThis('client')): ?>
                                         <span>
                                             <input type="checkbox" name="data[Campaign][accept][]"
                                                 class="_accept_client"
                                                 value="<?php echo $client['Campaign']['id'] ?>" />
                                             <input type="checkbox" name="data[Campaign][reject][]"
                                                 class="_reject_client"
                                                 value="<?php echo $client['Campaign']['id'] ?>" />
                                         </span>
                                         <?php endif ?>
                                         <?php echo getIconHtml($client['Campaign']['status_plb'], array(0=>'Paused', 1=>'Active')) ?>
                                     </li>
                                 </ul>
                             </td>
                             <td>
                                 <ul>
                                     <li>
                                         <?php if(canUserDoThis('client')): ?>
                                         <span>
                                             <input type="checkbox" name="data[Campaign][accept_sec][]"
                                                 class="_accept_client_sec"
                                                 value="<?php echo $client['Campaign']['id'] ?>" />
                                             <input type="checkbox" name="data[Campaign][reject_sec][]"
                                                 class="_reject_client_sec"
                                                 value="<?php echo $client['Campaign']['id'] ?>" />
                                         </span>
                                         <?php endif ?>
                                         <?php echo getIconHtml($client['Campaign']['secondary_approval'], array(0=>'Not Required',1=>'Required'))?>
                                     </li>
                                 </ul>
                             </td>
                             <td>
                                 <ul>
                                     <?php $_da = explode(',', $client['Campaign']['plb_da']);
		  $_pr = explode(',', $client['Campaign']['plb_pr']);
		  if(!empty($_da)){
		   foreach($_da as $k=>$v){
		    $v = round($v, 2);
		    echo "<li>{$v} / {$_pr[$k]}</li>";
		   }
		  }
		  ?>
                                 </ul>
                             </td>
                             <td style="text-align: center;">
                             <a title="Click to change DA/PR" class="tip-top" data-toggle="modal"
                                     onmouseover="_cid_=<?php echo $client['Campaign']['id'] ?>;_cname_='<?php echo ucwords($client['Campaign']['name']) ?>';"
                                     href="#" data-target="#CampaignEdit"><i class="icon-edit icon-large"></i> </a>
                             </td>
                         </tr>
                         <?php endforeach ?>
                         <?php else: ?>
                         <tr>
                             <td colspan="5" class="no-record-found">No records found.</td>
                         </tr>
                         <?php endif ?>
                     </tbody>
                 </table>
             </div>
         </form>
     </div> 
     <?php echo $viewData->get('pageLinks') ?>

 </div>

 
 <div id="CampaignEdit" class="modal fade bs-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel"
     aria-hidden="true">
     <div class="modal-dialog modal-lg bg-white">
         <div class="modal-header">
             <button type="button" class="close" data-dismiss="modal">&times;</button>
             <h3>Edit DA/PR for Campaign - <span></span></h3>
         </div>
         <div class="modal-body">
             Akhtar KHan
         </div>
         <div class="modal-footer">
             <a href="#" class="btn" data-dismiss="modal">Cancel</a>
             <a href="javascript:void(0)" id="edit-campaign-submit" class="btn btn-primary">Submit</a>
         </div>
     </div>
 </div>

 <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/js/bootstrap.min.js"></script>

 </div>
 <?php $viewData->scripts(array('js/clients_index.js'), array('inline'=>false)) ?>