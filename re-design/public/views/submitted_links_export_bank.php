<?php
$objPHPExcel = new PHPExcel();
$objPHPExcel->getProperties()->setCreator("Akhtar Khan")
							 ->setLastModifiedBy("Akhtar Khan")
							 ->setTitle("Domains DA")
							 ->setSubject("Domains DA")
							/* ->setDescription("Test document for PHPExcel, generated using PHP classes.")
							 ->setKeywords("office PHPExcel php")
							 ->setCategory("Test result file")*/;
$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('A1', 'Transaction Type')
            ->setCellValue('B1', 'Beneficiary  Code')
            ->setCellValue('C1', 'Beneficiary  A/c No.')
            ->setCellValue('D1', 'Gross Amount')
			 ->setCellValue('E1', 'TDS Amount')
			  ->setCellValue('F1', 'Transaction Amount')
            ->setCellValue('G1', 'Benefirciary Name')
            ->setCellValue('H1', 'Drawee Location')
            ->setCellValue('I1', 'Print Location')
            ->setCellValue('J1', 'Beneficiary add line1')
            ->setCellValue('K1', 'Beneficiary add line2')
            ->setCellValue('L1', 'Beneficiary add line3')
            ->setCellValue('M1', 'Beneficiary add line4')
            ->setCellValue('N1', 'Zipcode')
            ->setCellValue('O1', 'Instrument Ref No.')
			->setCellValue('P1', 'Customer Ref No.')
            ->setCellValue('Q1', 'Advising Detail1')
			->setCellValue('R1', 'Advising Detail2')
			->setCellValue('S1', 'Advising Detail3')
			->setCellValue('T1', 'Advising Detail4')
			->setCellValue('U1', 'Advising Detail5')
			->setCellValue('V1', 'Advising Detail6')
			->setCellValue('W1', 'Advising Detail7')
			->setCellValue('X1', 'Cheque No.')
			->setCellValue('Y1', 'Instrument Date')
			->setCellValue('Z1', 'MICR No')
			->setCellValue('AA1', 'IFSC Code')
			->setCellValue('AB1', 'Bene Bank Name')
			->setCellValue('AC1', 'Bene Bank Branch')
			->setCellValue('AD1', 'Bene Email ID')
			->setCellValue('AE1', 'Debit A/C Number')
			->setCellValue('AF1', 'Value Date');
$objPHPExcel->getActiveSheet()->getStyle('A1:AF1')->getFont()->setBold(true);
$submittedlinks = $viewData->get('submittedLinks');

if(!empty($submittedlinks)){
	
    $i=2;
    foreach($submittedlinks as $_submittedLink){
        $objPHPExcel->getActiveSheet()
                    ->setCellValue('A'. $i, 'N')
                    ->setCellValue('B'. $i, $_submittedLink['Payment']['bencode'])
                    ->setCellValue('C'. $i, 'AC-'.$_submittedLink['Payment']['benaccount'])
                    ->setCellValue('D'. $i, $_submittedLink['Payment']['amount'])
					->setCellValue('E'. $i, $_submittedLink['Payment']['tdsamount'])
					->setCellValue('F'. $i, $_submittedLink['Payment']['grossamount'])
                    ->setCellValue('G'. $i, $_submittedLink['Payment']['benname'])
                    ->setCellValue('H'. $i, '')
                    ->setCellValue('I'. $i, '')
                    ->setCellValue('J'. $i, '')
                    ->setCellValue('K'. $i, '')
                    ->setCellValue('L'. $i, '')
                    ->setCellValue('M'. $i, '')
                    ->setCellValue('N'. $i, '')
                    ->setCellValue('O'. $i,'')
                    ->setCellValue('P'.$i, DATE('M',strtotime($_submittedLink['Payment']['payment_date'])).$_submittedLink['Payment']['id'])
					->setCellValue('Q'.$i, '')
					->setCellValue('R'.$i, '')
					->setCellValue('S'.$i, '')
					->setCellValue('T'.$i, '')
					->setCellValue('U'.$i, '')
					->setCellValue('V'.$i, '')
					->setCellValue('W'.$i, '')
					->setCellValue('X'.$i, '')
					->setCellValue('Y'. $i,'')
					->setCellValue('Z'.$i, '')
					->setCellValue('AA'.$i, $_submittedLink['Payment']['ifsccode'])
					->setCellValue('AB'.$i, '')
					->setCellValue('AC'.$i, '')
					->setCellValue('AD'.$i, $_submittedLink['Payment']['approached_by'])
					->setCellValue('AE'.$i, '200999073256')
					->setCellValue('AF'.$i, $_submittedLink['Payment']['payment_date']);
        $i++;
    }
}
            
$objPHPExcel->getActiveSheet()->setTitle('Submited Links');
$objPHPExcel->setActiveSheetIndex(0);
//$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
//$objWriter->save('domains_da_'.time().'.xlsx');

// Redirect output to a client�s web browser (Excel2007)
header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename="submitted_links_report_bank'.time().'.xls"');
header('Cache-Control: max-age=0');
// If you're serving to IE 9, then the following may be needed
header('Cache-Control: max-age=1');

// If you're serving to IE over SSL, then the following may be needed
header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
header ('Pragma: public'); // HTTP/1.0

$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
$objWriter->save('php://output');
exit;
?>