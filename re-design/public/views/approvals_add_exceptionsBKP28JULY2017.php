<h4 style="color:#DA542E;"><?php echo $viewData->getTitle() ?></h4>
  <hr> 
  <p><span style="color:#28B779">Add Duplicate Exclusions</span> before submission to allow Domain / IP duplicacy for the same campaign</p>
  <div class="row-fluid">
    <div class="span6">
      <div class="widget-box">
        <div class="widget-title"> <span class="icon"> <i class="icon-align-justify"></i> </span>
          <h5>Add Duplicate Exclusions</h5>
        </div>
        <div class="widget-content nopadding">
          <form action="" class="form-horizontal"  method="post" id="AddDomainsForm">
	    <div class="control-group">
              <label class="control-label">Select Campaign :</label>
              <div class="controls">
                <select name="domain_exceptions[]" multiple="multiple" id="ApprovalCampaign" title="Please select campaign." required>
                 <?php $campsList = $viewData->get('campaignsList');
                 echo getFormOptions($campsList);
                 ?>
                </select>
              </div>
            </div>
		
<!--            Ajit:   Add No Payment option in currency on 15 DESC 2015-->
              
			<div class="form-actions">
              <button type="submit" class="btn btn-danger" id="ApprovalCalcDaPr">Add Campaigns to Duplicate Domain Exceptions</button>
          
            </div>
	</form>		
            
        <form action="" class="form-horizontal"  method="post" id="AddIPsForm">
	    <div class="control-group">
              <label class="control-label">Select Campaign :</label>
              <div class="controls">
                <select name="ip_exceptions[]" multiple="multiple" id="ApprovalCampaign" title="Please select campaign." required>
                 <?php $campsList = $viewData->get('campaignsList');
                 echo getFormOptions($campsList);
                 ?>
                </select>
              </div>
            </div>
		
<!--            Ajit:   Add No Payment option in currency on 15 DESC 2015-->
              
			<div class="form-actions">
              <button type="submit" class="btn btn-danger" id="ApprovalCalcDaPr">Add Campaigns to Duplicate IP Exceptions</button>
          
            </div>
	</form>    
            
        </div>
      </div>
    </div>
  </div>
  
    <?php $viewData->scripts(array('js/approvals_add_link.js'), array('inline'=>false)) ?>