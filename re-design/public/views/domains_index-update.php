<div class="row mt-4">
    <h4 class="darkBreadcrumb"><span class="lightBeradcrumb">Domains</span> / List</h4>
</div>
<?php echo output_message($session->message()) ?>
<div class="row mt-2">
    <h3 class="tableMainTitle"><?php echo $viewData->getTitle() ?></h3>
</div>
<!-- <div class="row mt-4">
    <div class="col-lg-4">
      <div class="mb-3">
        <label for="ListOfPersons" class="form-label">Domain</label>
        <input type="text" class="form-control" id="ListOfPersons" placeholder="Input">
      </div>
    </div>
    <div class="col-lg-3">
      <label for="ListOfPersons" class="form-label">Status</label>
      <select class="form-select form-control">
        <option disabled selected>Select Option</option>
        <option value="1">All</option>
      </select>
    </div>
  </div>
  <div class="col-lg-6">
    <button type="button" class="clearbtn">Reset</button>
    <button type="button" class="filterbtn mx-3">Filter</button>
  </div> -->
<?php include(WWW_ROOT.DS."layouts".DS."filter_domains.php") ?>
</div>
<div class="row mx-3">
    <div class="col-lg-12">
        <form action="domains.php?act=do_action" method="post" id="DominsListForm">
            <input type="hidden" name="whatDo" id="DomainsWhatDo" />
            <div class="table-responsive mt-3">
                <table class="table table-bordered bg-white">
                    <thead>
                        <tr style="text-align:center">
                            <?php if(canUserDoThis('paypal')): ?>
                            <!-- <th style="width: 50px;"></th> -->
                            <?php  endif ?>
                            <th style="width: 50px;">Domains</th>
                            <th>Campaigns (Black Listed)</th>
                            <th>Status</th>
                        </tr>

                    </thead>
                    <tbody>
                        <?php $domains = $viewData->get('domains') ?>
                        <?php if(!empty($domains)): ?>
                        <?php foreach($domains as $domain): ?>
                        <tr class="odd gradeX">
                            <?php if(canUserDoThis('paypal')): ?>
                            <td>
                                <input type="checkbox" name="data[Domain][accept][]" class="_accept_domain"
                                    value="<?php echo $domain['Domain']['id'] ?>" />
                                <input type="checkbox" name="data[Domain][reject][]" class="_reject_domain"
                                    value="<?php echo $domain['Domain']['id'] ?>" />

                                <?php  endif ?>
                                <a style="color: #1F2734;
    text-decoration: none;" href="<?php echo $domain['Domain']['name'] ?>"
                                    target="_blank"><?php echo $domain['Domain']['name'] ?> </a>
                            </td>
                            <td style="text-align: center;">
                                <?php if($domain['Domain']['status']==1): ?>
                                <?php if(isset($domain['BlacklistDomain']) and count($domain['BlacklistDomain'])>0): ?>
                                <a href="blacklisted.php?act=campaign&d=<?php echo base64_encode($domain['Domain']['name']) ?>"
                                    class="tip-right show-campaigns"
                                    title="This domain is balck listed for some campaigns. Click to see!"><i
                                        class="icon-user icon-large"></i> &nbsp;</a>
                                <?php endif ?>
                                <a data-toggle="modal" data-backdrop='static' data-target="#BlackListCampaign"
                                    onmouseover="domainname='<?php echo base64_encode($domain['Domain']['name']) ?>'"
                                    href="#" title="Click to blacklist domain for one or more campaigns."
                                    class="tip-right"><img src="Assets/Icons/Plus.svg" alt=""></a>
                                <?php else: ?>
                                Checked <i class="icon-minus tip-right" title="Domain black listed for all campaigns.">
                                    &nbsp;</i>
                                <?php endif ?>
                            </td>
                            <td style="text-align: center;">
                                <?php echo getIconHtml($domain['Domain']['status'], array(0=>'Black Listed', 1=>'Active')) ?>
                            </td>
                        </tr>
                        <?php endforeach ?>
                        <?php else: ?>
                        <tr>
                            <td colspan="9" class="no-record-found">No records found.</td>
                        </tr>
                        <?php endif ?>
                    </tbody>
                </table>
            </div>
        </form>

        </table>
    </div>
    <?php echo $viewData->get('pageLinks') ?>
</div>

<!-- Modal -->
<div class="modal fade" id="BlackListCampaign" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel">Blacklist Domain for one or more campaigns</h4>
            </div>
            <div class="modal-body">
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" id="modalSubmitBtn" class="btn btn-primary">Save changes</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div id="CampaignEdit" class="modal fade bs-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel"
    aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h3>Edit DA/PR for Campaign - <span></span></h3>
        </div>
        <div class="modal-body">
        </div>
        <div class="modal-footer">
            <a href="#" class="btn" data-dismiss="modal">Cancel</a>
            <a href="javascript:void(0)" id="edit-campaign-submit" class="btn btn-primary">Submit</a>
        </div>
    </div>
</div>
<?php $viewData->scripts(array('js/domains_index.js'), array('inline'=>false)) ?>