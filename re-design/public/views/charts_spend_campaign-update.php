<div class="row mt-4">
     <h4 class="darkBreadcrumb">
         <span class="lightBeradcrumb">Charts</span> / Spend By Campaign Chart
     </h4>
 </div>
 <div class="row mt-4">
     <h3 class="tableMainTitle">Advance Filter</h3>
 </div>
 <?php $filterVars = $viewData->get('filterVars') ?>
 <form action="#" method="post" class="form-horizontal filter-form" id="FilterForm">
 <div class="row mt-4"> 
    <div class="col-lg-3">
        <label for="FilterCampaign" class="form-label">Campaign</label>
          <?php 
            $campaignsList= $viewData->get('campaignsList');
            $comdateFrom= $viewData->get('comdateFrom');
            $comdateTo= $viewData->get('comdateTo'); 
          ?>  
          <select name="campaigns[]" id="FilterCampaign" multiple="multiple" style="width:200px" class="form-select form-control">
	          <?php echo getFormOptions($viewData->get('campaignsList'), $filterVars['campaigns']); ?>
	        </select>
    </div>
    <div class="col-lg-3">
        <label for="FilterCampaign" class="form-label">User</label> 
          <select name="users[]" id="FilterUser" multiple="multiple">
	          <?php echo getFormOptions($viewData->get('usersList'), $filterVars['users']); ?>
	        </select>
    </div>
    <div class="col-lg-3">
        <label for="FilterCampaign" class="form-label">Date(P1)</label>  
        <input type="date" name="from" value="<?php echo !empty($filterVars['dateFrom']) ? $filterVars['dateFrom'] : '' ?>" id="FilterDateFrom"  data-date-format="mm-dd-yyyy" class="form-control" ><br>
        <input type="date" value="<?php echo !empty($filterVars['dateTo']) ? $filterVars['dateTo'] : '' ?>" name="to" id="FilterDateTo"  data-date-format="mm-dd-yyyy" class="form-control" >
    </div>
    <div class="col-lg-3">
        <label for="FilterCampaign" class="form-label">Compaire Date(P2)</label>  
        <input type="date" name="comfrom" value="<?php echo $comdateFrom  ?>" id="comFilterDateFrom"  data-date-format="mm-dd-yyyy" class="form-control" > 
        <br>
        <input type="date" value="<?php echo $comdateTo ?>" name="comfrom" id="comFilterDateTo"  data-date-format="mm-dd-yyyy" class="form-control" >
    </div>
    
    <div class="row">
      <div class="col-lg-6">
          <button type="reset" class="clearbtn">Clear</button>
          <button type="submit" class="filterbtn mx-3">Filter</button>
      </div>
    </div>
 </div>
 </form>
 <div class="row mt-4">
     <div class="col-lg-12">
         <h4  class="tableMainTitle"> Spend Amount </h4>
         <a id="_ExportToExcel" href="javascript:void(0)"><button type="button" class="btn btn-outline-danger pull-right">Export</button></a>  &nbsp;
         <a id="_ExportToExcelChartWithoutDetail" href="javascript:void(0)" class="mx-2"><button type="button" class="btn btn-outline-danger pull-right" style=margin-left:12px;margin-right:12px; >Export By chart Without Detail</button></a> 
         <a id="_ExportToExcelChart" href="javascript:void(0)" class="mx-2"> &nbsp;&nbsp;<button type="button" class="btn btn-outline-danger pull-right">Export By chart</button></a> 
         <div class="widget-content nopadding">
            <div id="spend-chart"><p style="padding-left:10px;">Record not found to render chart.</p></div>
          </div>
     </div>
 </div>
 
<?php $viewData->scripts(array('js/highcharts/highcharts.js', 'js/highcharts/themes/grid.js'), array('inline'=>false)) ?>
<?php $viewData->scriptStart() ?>
$(document).ready(function(){
	$('#FilterForm').bind('submit', function(e){
		e.preventDefault();
		var dateFrom = $('#FilterDateFrom').val();
		var dateTo = $('#FilterDateTo').val();
                var comdateFrom = $('#comFilterDateFrom').val();
		var comdateTo = $('#comFilterDateTo').val();
                
                if((comdateFrom!='' && comdateTo=='') || (comdateFrom=='' && comdateTo!=''))
                {
                  alert("Please select valid compaire date");
                     return false;
                
                }
                
             
		var dateRange = (dateFrom+'_'+dateTo);
                var comdateRange = (comdateFrom+'_'+comdateTo);
                var users = $("#FilterUser").multiselect("getChecked").map(function(){
			return this.value;    
		     }).get();
                      var campaigns = $("#FilterCampaign").multiselect("getChecked").map(function(){
			return this.value;    
		     }).get();
                     
                     if(campaigns=="")
                     {
                     alert("Please select atleast one campaigns");
                     return false;
                     }
		dateRange = dateRange.replace(/^\s*\_\s*$/g, '');
               
		var curUrl = $.parseUrl();
		var queryString = curUrl.query;
		queryString._dt = $.base64.encode(dateRange);
                queryString._comdt = $.base64.encode(comdateRange);
                queryString._camp = $.base64.encode(campaigns.toString());
                queryString._uid = $.base64.encode(users.toString());
		var urlParams = [];
		$.each(queryString, function(k,v){
			if(v.length>0){
				urlParams.push(k+'='+v);
			}
		});
		if(urlParams.length>=1){
			window.location = '?'+urlParams.join('&');
		}
	});
});
<?php echo $viewData->get('chart') ? $viewData->get('chart')->render("chart1") : ''; ?>
<?php $viewData->scriptEnd() ?>