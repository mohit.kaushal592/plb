<!DOCTYPE html>
<html lang="en">
    
<head>
        <title>Login</title><meta charset="UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<link href="img/favicon.ico" type="image/vnd.microsoft.icon" rel="shortcut icon">
	<link rel="stylesheet" href="css/bootstrap.min.css" />
	<link rel="stylesheet" href="css/bootstrap-responsive.min.css" />
        <link rel="stylesheet" href="css/login.css" />
        <link href="css/font-awesome.css" rel="stylesheet" />
	<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,700,800' rel='stylesheet' type='text/css'>

    </head>
    <body onload="document.getElementById('username').focus()">
        <div id="loginbox">            
		<div class="control-group normal_text"> <h3><img src="img/logo.png" alt="Logo" /></h3></div>
		
            <form id="loginform" class="form-vertical" action="" method="post">
		<?php echo output_message($session->message()); ?>
                <div class="control-group">
                    <div class="controls">
                        <div class="main_input_box">
                            <span class="add-on bg_lg"><i class="icon-user"></i></span>
			    <input type="text" id="username" placeholder="Username" name="data[username]" value="<?php echo htmlentities($username); ?>" />
                        </div>
                    </div>
                </div>
                <div class="control-group">
                    <div class="controls">
                        <div class="main_input_box">
                            <span class="add-on bg_ly"><i class="icon-lock"></i></span>
			    <input type="password" placeholder="Password" name="data[password]"  value="<?php echo htmlentities($password); ?>"/>
                        </div>
                    </div>
                </div>
                <div class="form-actions">
			<input type="hidden" name="loginNow" value="Login"/>
                    <span class="pull-left"><a href="#" class="flip-link btn btn-info" id="to-recover">Lost password?</a></span>
                    <span class="pull-right">
			<button class="btn btn-success" type="submit"> Login </button>
		    </span>
                </div>
            </form>
            <form id="recoverform" action="?act=forgot" method="post" class="form-vertical">
			
				<p class="normal_text">Enter your e-mail address below and we will send you instructions how to recover a password.</p>
				
                    <div class="controls">
                        <div class="main_input_box">
                            <span class="add-on bg_lo"><i class="icon-envelope"></i></span><input type="text" name="email" placeholder="E-mail address" />
                        </div>
                    </div>
               
                <div class="form-actions">
                    <span class="pull-left"><a href="#" class="flip-link btn btn-success" id="to-login">&laquo; Back to login</a></span>
                    <span class="pull-right">
			<button class="btn btn-info"  id="recover-pwd">Reecover</button>
		    </span>
                </div>
            </form>
        </div>
        
        <script src="js/jquery.min.js"></script>  
        <script src="js/login.js"></script> 
    </body>

</html>
