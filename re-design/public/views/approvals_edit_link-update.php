<h4 class="tableMainTitle"><?php echo $viewData->getTitle() ?></h4>
<?php $approval = $viewData->get('approval') ?>
<div class="row-fluid">
    <div class="span6">
        <div class="widget-box mt-2 shadow p-3 mb-2 bg-white rounded">
            <h5 class="fs-4">Information of Link - <span
                    class="tableMainTitle"><?php echo $approval['Approval']['domain'] ?></span></h5>
            <div class="d-flex  my-2">
                <div class="col-lg-4">Domain</div>
                <div class="col-lg-6 form-label"><?php echo $approval['Approval']['domain'] ?></div>
            </div>
            <div class="d-flex  my-2">
                <div class="col-lg-4">IP Address</div>
                <div class="col-lg-6 form-label"><?php echo $approval['Approval']['ip'] ?></div>
            </div>
            <div class="d-flex  my-2">
                <div class="col-lg-4">PR</div>
                <div class="col-lg-6 form-label"><?php echo $approval['Approval']['pr'] ?></div>
            </div>
            <div class="d-flex  my-2">
                <div class="col-lg-4">DA</div>
                <div class="col-lg-6 form-label"><?php echo round($approval['Approval']['da'],2) ?></div>
            </div>
            <div class="d-flex  my-2">
                <div class="col-lg-4">Content Writer</div>
                <div class="col-lg-6 form-label">
                    <?php echo $approval['Approval']['content_writer']==1?'External' : ($approval['Approval']['content_writer']==0?'Adlift' : '') ?>
                </div>
            </div>

            <div class="d-flex  my-2">
                <div class="col-lg-4">Narration Text</div>
                <div class="col-lg-6 form-label">
                    <textarea id="narrationText" class="form-control" rows="3"
                        style="resize:none;height: 125px;"><?php echo $approval['Approval']['narration_text'] ?></textarea>
                </div>
            </div>
        </div>
    </div>
    <div class="span6">
        <div class="widget-box mt-2 shadow p-3 mb-2 bg-white rounded">
            <h5 class="fs-4 mb-3">Campaigns under Link - <span
                    class="tableMainTitle"><?php echo $approval['Approval']['domain'] ?></span></h5>
            <a id="add-event" data-toggle="modal" href="#modal-add-event" class="filterbtn"><i
                    class="icon-plus icon-white"></i> &nbsp; Add Campaign</a>
            <div class="modal hide" id="modal-add-event">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">x</button>
                    <h3>Add Campaigns to link - <?php echo $approval['Approval']['domain'] ?></h3>
                </div>
                <div class="modal-body">
                    <form action="" method="post" id="ApprovalEditLinkSave">
                        <input type="hidden" name="data[ApprovalLink][parent_id]"
                            value="<?php echo $approval['Approval']['id'] ?>" />
                        <p>Select Campaigns:</p>
                        <p>
                            <?php $campsAdded = array() ?>
                            <?php foreach($approval['ApprovalLink'] as $_approvalLink): ?>
                            <?php $campsAdded[] = $_approvalLink['campaign_id'] ?>
                            <?php endforeach ?>
                            <select name="data[ApprovalLink][campaign][]" id="ApprovalLinkCampaign" multiple="multiple"
                                required>
                                <?php foreach($viewData->get('campaignsList') as $campId=>$campName): ?>
                                <?php if(!in_array($campId, $campsAdded)): ?>
                                <option value="<?php echo $campId ?>"><?php echo $campName ?></option>
                                <?php endif ?>
                                <?php endforeach ?>
                            </select>
                            <span class="error" style="display:none;">Please select campaign</span>
                        </p>
                    </form>
                </div>
                <div class="modal-footer"> <a href="#" class="btn" data-dismiss="modal">Cancel</a> <a
                        href="javascript:void(0)" id="add-campaign-submit" class="btn btn-primary">Add
                        Campaigns</a> </div>
            </div>
            <div class="table-responsive mt-4">
                <table class="table bg-white ">
                    <thead>
                        <th style="text-align: left;">Campaign Name</th>
                        <th>Action</th>
                    </thead>
                    <tbody>
                        <?php if(!empty($approval['ApprovalLink'])):
                foreach($approval['ApprovalLink'] as $_approvalLink):
                ?>
                        <tr>
                            <td><?php echo ucwords($_approvalLink['campaign_name']) ?></td>
                            <td style="text-align: left;">
                                <?php
                    if($session->read('User.id')==$_approvalLink['user_id'] || $session->read('User.user_type')=='superadmin'):
                    ?>
                                <?php if($_approvalLink['final_link_submit_status']!=1): ?>
                                <a href="#" class="remove-campaign tip-top" title="Remove Campaign"
                                    rel="<?php echo $_approvalLink['id'] ?>"><i class="icon icon-remove icon-large"
                                        style="color: black;
                                        text-decoration: none;"></i></a>
                                <?php endif; ?>
                                <?php endif; ?>
                            </td>
                        </tr>
                        <?php
                endforeach;
                endif;
                ?>

                    </tbody>
                </table>
            </div>
        </div>

    </div>
</div>
</div>
</div>

<?php $viewData->scripts(array('js/approvals_edit_link.js'), array('inline'=>false)) ?>