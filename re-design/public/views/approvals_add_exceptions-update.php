<div class="row mt-4">
     <h4 class="darkBreadcrumb">
         <span class="lightBeradcrumb">Approval</span> / Add Duplicate Exclusions
     </h4>
 </div>
<div class="row mt-1">
    <h3 class="tableMainTitle">Add Duplicate Exclusions</h3>
</div>
<form action="" class="form-horizontal"  method="post" id="AddDomainsForm">
    <div class="row mt-4 shadow p-3 mb-5 bg-white rounded p-4">
      <div class="col-lg-6">
          <label for="ListOfPersons" class="form-label">Select Campaign</label>
          <select name="domain_exceptions[]" multiple="multiple" id="ApprovalCampaign" title="Please select campaign." required style="width:400px">
                <?php $campsList = $viewData->get('campaignsList');
                echo getFormOptions($campsList);
                ?>
          </select>
      </div>
      <div class="col-lg-6 mt-3">  
        <button type="submit" class="filterbtn mx-3 mt-3" id="ApprovalCalcDaPr">Add Campaigns to Duplicate Domain Exceptions</button>
      </div>
    </div>
</form>
<form action="" class="form-horizontal"  method="post" id="AddIPsForm">
    <div class="row mt-4 shadow p-3 mb-5 bg-white rounded p-4">
        <div class="col-lg-6">
            <label for="ListOfPersons" class="form-label">Select Campaign</label>
            <select name="ip_exceptions[]" multiple="multiple" id="ApprovalCampaign" title="Please select campaign." required style="width:400px">
                <?php $campsList = $viewData->get('campaignsList');
                echo getFormOptions($campsList);
                ?>
            </select>
        </div>
        <div class="col-lg-6  mt-3">   
          <button type="submit" class="filterbtn mx-3 mt-3" id="ApprovalCalcDaPr">Add Campaigns to Duplicate IP Exceptions</button>
        </div>
    </div>
</form>
<?php $viewData->scripts(array('js/approvals_add_link.js'), array('inline'=>false)) ?>