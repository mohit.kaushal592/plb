<h4 style="color:#DA542E;"><?php echo $viewData->getTitle() ?></h4>
<hr> 

<!--<div class="row-fluid">
    <div class="span8">
        <div class="widget-box">
            <div class="widget-title"> <span class="icon"> <i class="icon-align-justify"></i> </span>
                <h5>Bulk Add Influencers by Importing CSV</h5>
            </div>
            <div class="widget-content nopadding">
                <form action="?act=import" class="form-horizontal"  method="post" enctype="multipart/form-data" id="ImportInfluencerForm">
                    <div class="control-group">
                        <label class="control-label">Upload File :</label>
                        <div class="controls">
                            <input type="file" name="csv" id="InfluencerFile" placeholder="Upload Influencer's Import CSV" class="span11" required="required" accept="application/vnd.ms-excel"/>
                        </div><span style="float: right;"><a onclick="return confirm('Do you want to download the accepted sample format for import of Influencers?');" target="_blank" href="public/Sample_Influencers_Import_Sheet.csv">Download Sample CSV </a>&nbsp;</span>
                    </div>
                    <div class="form-actions">
            <button type="submit" class="btn btn-success">Import Influencers</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>-->

<div class="row-fluid">
    <div class="span8">
        <div class="widget-box">
            <div class="widget-title"> <span class="icon"> <i class="icon-align-justify"></i> </span>
                <h5>Add Influencer Information</h5>
            </div>
            <div class="widget-content nopadding">
                <form action="" class="form-horizontal"  method="post" id="AddDomainForm">
                    <div class="control-group">
                        <label class="control-label">Influencer Name :</label>
                        <div class="controls">
                            <input name="data[Influencer][name]" id="InfluencerName"   style="resize: none;" placeholder="Enter influencer name" class="span11" required="required" />
                        </div>
                    </div>
                    <?php //include(WWW_ROOT . DS . "layouts" . DS . "add_influencers_domain.php") ?> 

                    <div class="control-group">

                        <label class="control-label">Domain : </label>
                        <div class="controls">
                            <input name="data[Influencer][influencer_domain]" placeholder="Type to search domain" autocomplete="off" id="FilterDomain" type="text" class="span11" value="<?php echo!empty($filterVars['domain']) ? $filterVars['domain'] : '' ?>" class="typeahead" />
                            <input type="hidden" name="data[Influencer][domain_ids]" id="influencer_domain_id" value="" />
                        </div>
                    </div>
                   <div class="control-group">
                        <label class="control-label">Influencer Location :</label>
                        <div class="controls">
                            <input name="data[Influencer][location]" id="InfluencerLoc"   style="resize: none;" placeholder="Enter influencer Location" class="span11" required="required" />
                        </div>
                    </div>

                    <div class="control-group">
                        <label class="control-label" for="InfluencerCategory">Influencer Category :</label>
                        <div class="controls">
                            <select name="data[Influencer][category]" id="InfluencerCat" class="" required="required" >
                                <option value="0" disabled selected="">Please select</option>
                                <?php
                                $categories = $viewData->get('categories');
                                $i = 1;
                                foreach ($categories as $cat) {

                                    echo '<option value="' . $i . '">' . $cat . '</option>';
                                    $i++;
                                }
                                ?>

                            </select>
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label">Blog URL :</label>
                        <div class="controls">
                            <input name="data[Influencer][blog_url]" id="InfluencerUrl"   style="resize: none;" placeholder="Enter influencer Blog URL" class="span11" required="required" />
                        </div>
                    </div>

                    <div class="control-group">
                        <label class="control-label">Twitter Handle :</label>
                        <div class="controls">
                            <input name="data[Influencer][twitter_handle]" id="InfluencerHandle"   style="resize: none;" placeholder="Enter influencer twitter handle" class="span11" required="required" />
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label">Twitter Handle Influencer Price :</label>
                        <div class="controls">
                            <input name="data[Influencer][twitter_handle_price]" type="number" id="InfluencerHandlePrice"   style="resize: none;" placeholder="Enter influencer twitter handle Influencer Price" class="span11" required="required" />
                        </div>
                    </div>   
                    <div class="control-group">
                        <label class="control-label">Facebook ID :</label>
                        <div class="controls">
                            <input name="data[Influencer][facebook_id]" id="InfluencerFid"   style="resize: none;" placeholder="Enter influencer Facebook ID" class="span11" required="required" />
                        </div>
                    </div>
                   <div class="control-group">
                        <label class="control-label">Facebook ID Influencer Price:</label>
                        <div class="controls">
                            <input name="data[Influencer][facebook_id_price]" type="number" id="InfluencerFidPrice"   style="resize: none;" placeholder="Enter influencer Facebook ID Influencer Price" class="span11" required="required" />
                        </div>
                    </div>
                   

                    <div class="control-group">
                        <label class="control-label">Instagram ID :</label>
                        <div class="controls">
                            <input name="data[Influencer][instagram_id]" id="InfluencerIid"   style="resize: none;" placeholder="Enter influencer instagram ID" class="span11" required="required" />
                        </div>
                    </div>

                    <div class="control-group">
                        <label class="control-label">Instagram ID Influencer Price :</label>
                        <div class="controls">
                            <input name="data[Influencer][instagram_id_price]" type="number" id="InfluencerIidPrice"   style="resize: none;" placeholder="Enter influencer instagram ID influencer Price" class="span11" required="required" />
                        </div>
                    </div>

                    <div class="control-group">
                        <label class="control-label">Google Plus ID :</label>
                        <div class="controls">
                            <input name="data[Influencer][gplus_id]" id="InfluencerGid"   style="resize: none;" placeholder="Enter influencer G+ ID" class="span11" required="required" />
                        </div>
                    </div>


                    <div class="control-group">
                        <label class="control-label">Special Comments :</label>
                        <div class="controls">
                            <textarea name="data[Influencer][comment]" id="InfluencerComment" rows="6"  style="resize: none;" placeholder="Enter special comments" class="span11" ></textarea>
                        </div>
                    </div>     



            </div>
        </div>

        <div class="form-actions">
            <button type="submit" class="btn btn-success">Add Influencer</button>
        </div>
        </form>
    </div>
</div>


<?php //$viewData->scripts(array('js/domains_add.js'), array('inline' => false)) ?>

