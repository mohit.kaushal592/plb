<?php $filterVars = $viewData->get('filterVars') ?>
<form action="submitted_links.php" method="post" class="form-horizontal filter-form" id="FilterForm">
    <div class="row mt-4 shadow p-3 mb-5 bg-white rounded p-4">
        <?php $filterVars = $viewData->get('filterVars') ?>
        <input type="hidden" name="exportList" id="exportList" value="" />
        <input type="hidden" name="exportListBank" id="exportListBank" value="" />

        <div class="col-lg-3">
            <label class="form-label">User</label>
            <select name="users[]" id="FilterUser" multiple="multiple" class="form-control" style="width:300px">
                <?php echo getFormOptions($viewData->get('usersList'), $filterVars['users']); ?>
            </select>
        </div>
 
        <div class="col-lg-3">
            <label class="form-label">Campaign</label>
            <select name="campaigns[]" id="FilterCampaign" multiple="multiple" class="form-control" style="width:300px">
                <?php echo getFormOptions($viewData->get('campaignsList'), $filterVars['campaigns']); ?>
            </select>
        </div>
        <div class="col-lg-3">
            <label class="form-label">Domain</label>
            <input name="domain" autocomplete="off" id="FilterDomain" type="text" class="form-control"
                value="<?php echo !empty($filterVars['domain']) ? $filterVars['domain'] : '' ?>" class="typeahead" />
        </div>

        <div class="col-lg-3">
            <label class="form-label">Status</label>
            <select name="payment_status[]" id="FilterPaymentStatus" multiple="multiple" style="width:300px">
                <?php echo getFormOptions(array('1'=>'Paid', '0'=>'Rejected', '-1'=>'Underprocess'), $filterVars['status']); ?>
            </select>
        </div>

        <div class="col-lg-3">
            <label class="form-label">Month</label>
            <select name="month[]" id="FilterMonth" multiple="multiple" style="width:300px">
                <?php echo getFormOptions(monthsList(), $filterVars['month']); ?>
            </select>
        </div>
        <div class="col-lg-3">
            <label class="form-label">Year</label>
            <select name="year[]" id="FilterYear" multiple="multiple" style="width:300px">
                <?php echo getFormOptions(yearsList(array('min'=>'2012','max'=>date('Y'))), $filterVars['year']); ?>
            </select>
        </div>
        <div class="col-lg-3" id="WebmasterBlock">
            <label class="form-label">Webmaster</label>
            <input type="hidden" id="FilterClientMail"
                value="<?php echo !empty($filterVars['client_mail']) ? join(",", $filterVars['client_mail']) : '' ?>"
                style="width:300px" />
        </div>
        <div class="col-lg-3" id="PaypalBlock">
            <label class="form-label">Paypal</label>
            <input type="hidden" id="FilterPaypalEmail"
                value="<?php echo !empty($filterVars['paypal_email']) ? join(",", $filterVars['paypal_email']) : '' ?>"
                style="width:300px" />
        </div>

        <div class="col-lg-3">
            <label class="form-label">PR (operator)</label>
            <select name="pr_op" id="FilterPrOperator" class="form-control">
                <?php echo getFormOptions(array('eq'=>'= (Equal to)', 'lt'=>'< (Less than)', 'lte'=>'<= (Less than equal to)', 'gt'=>'> (Greater than)', 'gte'=>'>= (Greater than equal to)'), $filterVars['pr_op']); ?>
            </select>
        </div>
        <div class="col-lg-3">
            <label class="form-label">PR (value)</label>
            <select name="pr_value" id="FilterPrValue" style="width:300px">
                <?php $prValues[0] = 'Select Value'; for($i=1;$i<=10;$i++){$prValues[$i] = $i;} ?>
                <?php echo getFormOptions($prValues, $filterVars['pr_value']); ?>
            </select>
        </div>
        <div class="col-lg-3">
            <label class="form-label">DA (operator)</label>
            <select name="da_op" id="FilterDaOperator" style="width:300px">
                <?php echo getFormOptions(array('eq'=>'= (Equal to)', 'lt'=>'< (Less than)', 'lte'=>'<= (Less than equal to)', 'gt'=>'> (Greater than)', 'gte'=>'>= (Greater than equal to)'), $filterVars['da_op']); ?>
            </select>
        </div>
        <div class="col-lg-3">
            <label class="form-label">DA (value)</label>
            <select name="da_value" id="FilterDaValue" style="width:300px">
                <?php $daValues[0] = 'Select Value'; for($i=1;$i<=100;$i = ($i==1?$i+4 : $i+5)){$daValues[$i] = $i;} ?>
                <?php echo getFormOptions($daValues, $filterVars['da_value']); ?>
            </select>
        </div>

        <div class="col-lg-3">
            <label class="form-label">Payment Date</label>
            <div data-date="" class="input-append date datepicker">
                <input type="date" name="from"
                    value="<?php echo !empty($filterVars['dateFrom']) ? $filterVars['dateFrom'] : '' ?>"
                    id="FilterDateFrom" data-date-format="mm-dd-yyyy" class="form-control">
            </div>
            <div data-date="" class="input-append date datepicker">
                <input type="date" value="<?php echo !empty($filterVars['dateTo']) ? $filterVars['dateTo'] : '' ?>"
                    name="to" id="FilterDateTo" data-date-format="mm-dd-yyyy" class="form-control">
            </div>
        </div>
        <div class="col-lg-3">
            <label class="form-label">Submted Date</label>
            <div data-date="" class="input-append date datepicker">
                <input type="date" name="from"
                    value="<?php echo !empty($filterVars['dateFromSubmted']) ? $filterVars['dateFromSubmted'] : '' ?>"
                    id="FilterDateFromSubmted" data-date-format="mm-dd-yyyy" class="form-control">
            </div>
            <div data-date="" class="input-append date datepicker">
                <input type="date"
                    value="<?php echo !empty($filterVars['dateToSubmted']) ? $filterVars['dateToSubmted'] : '' ?>"
                    name="to" id="FilterDateToSubmted" data-date-format="mm-dd-yyyy" class="form-control">
            </div>
        </div>
        <!-- Start: Added by Jitendra: 28-Nov-2014 --->
        <div class="col-lg-3">
            <label class="form-label">Pay for month of</label>
            <select name="payment_month[]" id="FilterPaymentMonth" multiple="multiple">
                <?php echo getFormOptions(monthsList(), $filterVars['payment_month']); ?>
            </select>
        </div>
        <div class="col-lg-3">
            <label class="form-label">Pay for year of</label>
            <select name="payment_year[]" id="FilterPaymentYear" multiple="multiple" style="width:300px">
                <?php echo getFormOptions(yearsList(array('min'=>'2012','max'=>date('Y'))), $filterVars['payment_year']); ?>
            </select>
        </div>
        <div class="col-lg-3">
            <label class="form-label">Payment By</label>
            <select name="payment_type" id="Filterpayment_type" style="width:300px">
                <?php echo getFormOptions(array(''=>'Select','Paypal'=>'Paypal', 'NEFT'=>'NEFT'), $filterVars['payment_type']); ?>
            </select>
        </div>
        <div class="col-lg-3">
            <label class="form-label">Bencode</label>
            <input name="bencode" autocomplete="off" id="Filterbencode" type="text" class="form-control"
                value="<?php echo !empty($filterVars['bencode']) ? $filterVars['bencode'] : '' ?>" class="typeahead" />
        </div>

        <!-- End: Added by Jitendra: 28-Nov-2014 --->


        <div class="col-lg-12">
            <button type="reset" class="btn btn-primary">Reset</button>
            <button type="button" class="btn btn-success filterbtn" onclick="filterSubmit();">Filter</button>
        </div>

    </div>
</form>