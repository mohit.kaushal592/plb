<div class="row mt-4">
    <h4 class="darkBreadcrumb"><span class="lightBeradcrumb">Clients</span> / Assign Users</h4>
</div>
<div class="row mt-2">
    <h3 class="tableMainTitle"><?php echo $viewData->getTitle() ?></h3>
    <p class="subHeading">Please select user for clients (campaigns) to secondary <br>approval.</p>
</div>
<div class="row">
    <div class="table-responsive mt-3">
        <form action="" class="form-horizontal" method="post" id="AddDomainForm">
            <?php $clients = $viewData->get('clients'); ?>
            <?php $users = $viewData->get('users'); ?>
            <?php $campaignUsers = $viewData->get('campaignUsers') ? $viewData->get('campaignUsers') : array(); ?>
            <table class="table bg-white">
                <thead>
                    <tr>
                        <th class="tableHead">Client</th>
                        <th class="tableHead">Current User</th>
                        <th class="tableHead">Assign To</th>
                        <th class="tableHead">Action</th>
                    </tr>
                </thead>
                <tbody>
                    <?php if(!empty($clients)): foreach($clients as $client): ?>
                    <tr id="rw_<?php echo $client['Campaign']['id'] ?>">
                        <td class="col-lg-3"><?php echo $client['Campaign']['name'] ?>
                            <input type="hidden" id="rw_<?php echo $client['Campaign']['id'] ?>_id"
                                name="data[Campaign][id]" value="<?php echo $client['Campaign']['id'] ?>">
                        </td>
                        <td class="col-lg-3">
                            <?php echo $campaignUsers[$client['Campaign']['secondary_approval_user']] ?></td>
                        <td class="col-lg-3">
                            <select id="rw_<?php echo $client['Campaign']['id'] ?>_user"
                                name="data[Campaign][secondary_approval_user]">
                                <?php echo getFormOptions($users, $client['Campaign']['secondary_approval_user']) ?>
                            </select>
                        </td>
                        <td class="col-lg-3"><button type="button" class="filterbtn _updateRow"
                                data-tbl-row="rw_<?php echo $client['Campaign']['id'] ?>">Update</button></td>
                    </tr>
                    <?php endforeach; endif; ?>
                </tbody>
            </table>
        </form>

    </div>
</div>

<?php $viewData->scripts(array('js/clients_assign_user.js'), array('inline'=>false)) ?>