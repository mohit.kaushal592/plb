<h4 style="color:#DA542E;"><?php echo $viewData->getTitle() ?></h4>
  <hr> 
  
  <div class="row-fluid">
    <div class="span6">
      <div class="widget-box">
        <div class="widget-title"> <span class="icon"> <i class="icon-align-justify"></i> </span>
          <h5>View Link</h5>
        </div>
        <div class="widget-content nopadding">
	  <?php $payment = $viewData->get('payment') ?>
          <form action="submitted_links.php?act=edit_save" class="form-horizontal"  method="post" id="SubmitLinkForm">
	    <input type="hidden" name="data[Payment][id]" id="PaymentId" value="<?php echo $payment['Payment']['id'] ?>" />
	    <input type="hidden" name="data[Payment][child_table_id]" value="<?php echo $payment['Payment']['child_table_id'] ?>" />
            <div class="control-group">
			
              <label class="control-label">Domain Name :</label>
              <div class="controls">
              <?php echo $payment['Payment']['domain'] ?>  
              </div>
            </div>
	    <div class="control-group">
              <label class="control-label"> Campaign :</label>
              <div class="controls">
			   <?php

			   echo $payment['Payment']['campaign_name']; ?>  
		
              </div>
            </div>
	      <div class="control-group">
              <label class="control-label">Narration text :</label>
              <div class="controls">
               <?php echo $payment['Payment']['narration_text'] ?>
              </div>
            </div>
	      <div class="control-group">
              <label class="control-label">Page Url :</label>
              <div class="controls">
                <?php echo $payment['Payment']['url'] ?>
              </div>
            </div>
		 <div class="control-group">
              <label class="control-label">Webmaster Email :</label>
              <div class="controls">
               <?php echo $payment['Payment']['client_mail'] ?>
              </div>
            </div>	
			 <?php
			
          if($payment['Payment']['payment_type']!='NEFT')
		  {?>
	      <div class="control-group">
              <label class="control-label">Paypal Email :</label>
              <div class="controls">
              <?php echo $payment['Payment']['paypal_email'] ?>
              </div>
            </div>
			 <?php
          }
          elseif($payment['Payment']['amount']>0){
?>
          <div class="control-group">
              <label class="control-label">Beneficiary Code :</label>
              <div class="controls">
               <?php echo $payment['Payment']['bencode'] ?>
              </div>
            </div>
			<div class="form-actions">
              <button type="button" class="btn btn-danger"> Beneficiary Information</button>
            </div>
			 <div class="control-group">
              <label class="control-label">Beneficiary Name :</label>
              <div class="controls">
               <?php echo $payment['Payment']['benname'] ?>
              </div>
            </div>
			 <div class="control-group">
              <label class="control-label">Beneficiary Account :</label>
              <div class="controls">
             <?php echo $payment['Payment']['benaccount'] ?>
              </div>
            </div>
			 <div class="control-group">
              <label class="control-label">Beneficiary IFSC Code :</label>
              <div class="controls">
               <?php echo $payment['Payment']['ifsccode'] ?>
              </div>
            </div>
			 <div class="control-group">
              <label class="control-label">Beneficiary Address :</label>
              <div class="controls">
             <?php echo $payment['Payment']['address'] ?>
              </div>
            </div>
			 <div class="control-group">
              <label class="control-label">Beneficiary City :</label>
              <div class="controls">
               <?php echo $payment['Payment']['city'] ?>
              </div>
            </div>
			 <div class="control-group">
              <label class="control-label">Beneficiary State :</label>
              <div class="controls">
              <?php echo $payment['Payment']['state'] ?>
              </div>
            </div>
			 <div class="control-group">
              <label class="control-label">Beneficiary Zip Code :</label>
              <div class="controls">
               <?php echo $payment['Payment']['zip_code'] ?>
              </div>
            </div>
<?php
		  }?>		
	     
	    <div class="control-group">
              <label class="control-label">Site Geolocation :</label>
              <div class="controls">
                <?php echo $payment['Payment']['geo'];?>
              </div>
            </div>
	    <div class="control-group date-box">
              <label class="control-label">Start Date :</label>
              <div class="controls">
		<?php echo date('m/d/Y', strtotime($payment['Payment']['start_date'])) ?>
              </div>
            </div>
	    <div class="control-group date-box">
              <label class="control-label">End Date :</label>
              <div class="controls">
               <?php echo date('m/d/Y', strtotime($payment['Payment']['end_date'])) ?>
              </div>
            </div>
	    <div class="control-group">
              <label class="control-label">Anchor :</label>
              <div class="controls">
                <?php echo $payment['Payment']['anchor_text'] ?>
              </div>
            </div>
	    <div class="control-group">
              <label class="control-label">Amount :</label>
              <div class="controls">
                <?php echo $payment['Payment']['amount'] ?>
              </div>
            </div>
			<?php
          if($payment['Payment']['payment_type']=='NEFT' && $payment['Payment']['amount'] >0)
		  {?>
  <div class="form-actions">
              <button type="button" class="btn btn-danger">  TDS Detail</button>
  </div>
 <div class="control-group">
              <label class="control-label">TDS Amount :</label>
              <div class="controls">
               <?php echo $payment['Payment']['tdsamount'] ?>
              </div>
            </div>
 <div class="control-group">
              <label class="control-label">Gross Amount :</label>
              <div class="controls">
                <?php echo $payment['Payment']['grossamount'] ?>
              </div>
            </div>			
	<?php
		  }?>
			
	    <div class="control-group">
              <label class="control-label">Currency :</label>
              <div class="controls">
                <?php echo  $payment['Payment']['currency'] ?>
              </div>
            </div>
            
            <?php
            $monthArr = array(
                '1'=>'January',
                '2'=>'February',
                '3'=>'March',
                '4'=>'April',
                '5'=>'May',
                '6'=>'June',
                '7'=>'July',
                '8'=>'August',
                '9'=>'September',
                '10'=>'October',
                '11'=>'November',
                '12'=>'December'
                );
            ?>
            <div class="control-group">
              <label class="control-label">Payment For Month Of:</label>
              <div class="controls">
                <input type="hidden" name="data[Payment][payment_month]" id="PaymentPaymentMonth" value="<?php echo $payment['Payment']['payment_month']; ?>" />  
                <select name="data[Payment][payment_month_duplicate]" id="PaymentPaymentMonthDuplicate" title="Please select payment for the month of." required disabled="disabled">
                 <?php
                 foreach($monthArr as $key => $val)
                 {
                 ?> 
                    <option value="<?php echo $key;?>" <?php if($key == $payment['Payment']['payment_month']){echo " selected='selected'";}?>><?php echo $val;?></option>
                 <?php }?>
                </select>
              </div>
            </div>
            <div class="control-group">
              <label class="control-label">Payment For Year Of:</label>
              <div class="controls">
                <input type="hidden" name="data[Payment][payment_year]" id="PaymentPaymentYear" value="<?php echo $payment['Payment']['payment_year']; ?>" />
                <select name="data[Payment][payment_year_duplicate]"  id="PaymentPaymentYearDuplicate" title="Please select payment for the year of." required disabled="disabled">
                 <?php
                 for($i=-1; $i<3; $i++)
                 {
                 ?>
                     <option value="<?php echo (date('Y')+$i);?>" <?php if((date('Y')+$i) == $payment['Payment']['payment_year']){echo " selected='selected'";}?>><?php echo (date('Y')+$i);?></option>
                 <?php
                 }
                 ?>
                </select>
              </div>
            </div>
            
            
            
	    <div class="form-actions">
              <button type="button" class="btn btn-danger">  PR,DA and IP</button>
            </div>
			
            <div class="control-group">
			<!--<a href="#" class="history DomainIpHistory" id="IpHistory"><img src="img/iphistory.png"/></a>-->
              <label class="control-label">IP Address :</label>
              <div class="controls">
              <?php echo $payment['Payment']['ip'] ?>
              </div>
            </div>
            <div class="control-group">
              <label class="control-label">PR value :</label>
              <div class="controls">
               <?php echo $payment['Payment']['pr_value'] ?>
              </div>
            </div>
            <div class="control-group">
              <label class="control-label">DA value :</label>
              <div class="controls">
               <?php echo round($payment['Payment']['da_value'], 2) ?>
				</div>
            </div>
    
            
          </form>
        </div>
      </div>
    </div>
  </div>
  
    <?php $viewData->scripts(array('js/approvals_submit_link.js'), array('inline'=>false)) ?>
	 