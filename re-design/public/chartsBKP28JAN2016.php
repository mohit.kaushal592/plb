<?php
require_once("../includes/initialize.php");
require_once("../includes/Highcharts/Highchart.php");
 if (!$session->is_logged_in()) { redirect_to("login.php"); }
 
 
function charts_approval(){
    global $viewData, $session;
    $viewData->setTitle('Approval Links Chart');
    $Approval = new Approval;
    
    $filters = __chartsFilterVars();
    
    $approvalLinkFilter = array(
        'fields'=> array('id'),
        'contain'=>array(
            'User'=>array(
                'fields'=>array('id', 'first_name','last_name')
            ),
            'ApprovalLink'=>array(
                'fields'=>array('id', 'approve_status')
            )
        )
    );
    if($session->read('User.user_type')=='user'){
        $approvalLinkFilter['where'] = "Approval.user_id='".$session->read('User.id')."'";
    }
    
    if(!empty($filters['ApprovalLink'])){
        $approvalLinkFilter['where'] = !empty($approvalLinkFilter['where']) ? $approvalLinkFilter['where'].' AND '. $filters['ApprovalLink'] : $filters['ApprovalLink'];
        $dateStart = $viewData->get('dateFrom');
        $dateEnd = $viewData->get('dateTo');
    }else{
        $dateStart = date('Y-m-01');
        $dateEnd = date('Y-m-'. date('t'));
        $filterC = array();
        if(!empty($approvalLinkFilter['where'])){
            $filterC[] = $approvalLinkFilter['where'];
        }
        $approvalLinkFilter['where'] = array_merge($filterC, array(
                "DATE_FORMAT(ApprovalLink.added_on, '%Y-%m-%d') >= '". $dateStart."'",
                "DATE_FORMAT(ApprovalLink.added_on, '%Y-%m-%d') <= '".$dateEnd."'"
            )) ;
    }
      
    $_approvalLinks = $Approval->find_all($approvalLinkFilter);
    if(!empty($_approvalLinks)){
        $chartData=array();
        $userCount=0;
        foreach($_approvalLinks as $_approvls){
            $user = trim(ucwords($_approvls['User']['first_name'].' '.$_approvls['User']['last_name']));
            if(!isset($chartData['users']) || !in_array($user, $chartData['users'])){
                $userCount++;
                $chartData['users'][$userCount]=$user;
            }
            $userNo = isset($chartData['users']) ? array_search($user, $chartData['users']) : 0;
            foreach($_approvls['ApprovalLink'] as $_approvalLink){
                switch($_approvalLink['approve_status']){
                    case '-1':
                        $chartData['series']['Pending']['data'][$userNo] +=  1;               
                        break;
                    case '0':
                        $chartData['series']['Rejected']['data'][$userNo] +=  1;
                        break;
                    case '1':
                        $chartData['series']['Approved']['data'][$userNo] +=  1;
                        break;
                    default:
                        break;
                }
            }
        }
        
        foreach($chartData['users'] as $k=>$u){
            if(!isset($chartData['series']['Approved']['data']) || !key_exists($k, $chartData['series']['Approved']['data'])){
                $chartData['series']['Approved']['data'][$k] = 0;
            }
            if(!isset($chartData['series']['Rejected']['data']) || !key_exists($k, $chartData['series']['Rejected']['data'])){
                $chartData['series']['Rejected']['data'][$k] = 0;
            }
            if(!isset($chartData['series']['Pending']['data']) || !key_exists($k, $chartData['series']['Pending']['data'])){
                $chartData['series']['Pending']['data'][$k] = 0;
            }
        }
        
        ksort($chartData['series']['Approved']['data']);
        ksort($chartData['series']['Rejected']['data']);
        ksort($chartData['series']['Pending']['data']);
    }
    $chartData['title'] = 'Approval Links Chart';
    $chartData['subtitle'] = date('F j, Y', strtotime($dateStart)). (!empty($dateEnd) ? ' to '.date('F j, Y', strtotime($dateEnd)) : '');
    $chartData['yAxis']['title'] = 'Links';
    __setChartData($chartData, "approvals-chart");
}

function charts_payment(){
    global $viewData, $database;
    $viewData->setTitle('Payments Chart');
    $Payment = new Payment;
    $User = new User;
    
    $filters = __chartsFilterVars('Payment');
    
    $paymentFilters = "Payment.status IN ('Paid', 'Underprocess','Rejected')";    
    if(!empty($filters['Payment'])){
        $paymentFilters = !empty($paymentFilters) ? $paymentFilters.' AND '. $filters['Payment'] : $filters['Payment'];
        $dateStart = $viewData->get('dateFrom');
        $dateEnd = $viewData->get('dateTo');
    }else{
        $dateStart = date('Y-m-01');
        $dateEnd = date('Y-m-'. date('t'));
        $paymentFilters = "DATE_FORMAT(Payment.added_on, '%Y-%m-%d') >= '". $dateStart."'";
        $paymentFilters .= " AND ";
        $paymentFilters .= "DATE_FORMAT(Payment.added_on, '%Y-%m-%d') <= '".$dateEnd."'";
    }
    
    $paymentQuery = $database->query("SELECT COUNT(Payment.id) AS links, Payment.status AS status,  CONCAT(User.first_name, ' ', User.last_name) AS name 
FROM ".$Payment->table_name." Payment INNER JOIN ".$User->table_name." User ON(User.id=Payment.user_id)
 WHERE  ".$paymentFilters." GROUP BY Payment.status, Payment.user_id");
    $payments = $database->fetch_data_array($paymentQuery);
    $chartData=array();
    $userCount=0;
    if(!empty($payments)){
        foreach($payments as $payment){
            $user = trim(ucwords($payment['name']));
            if(!isset($chartData['users']) || !in_array($user, $chartData['users'])){
                $userCount++;
                $chartData['users'][$userCount]=$user;
            }
            $userNo = isset($chartData['users']) ? array_search($user, $chartData['users']) : 0;
            
            switch($payment['status']){
                case 'Underprocess':
                    $chartData['series']['Pending']['data'][$userNo] =  (int)$payment['links'];               
                    break;
                case 'Rejected':
                    $chartData['series']['Rejected']['data'][$userNo] =  (int)$payment['links'];
                    break;
                case 'Paid':
                    $chartData['series']['Done']['data'][$userNo] =  (int)$payment['links'];
                    break;
                default:
                    break;
            }
            
        }
        
        foreach($chartData['users'] as $k=>$u){
            if(!isset($chartData['series']['Done']['data']) || !key_exists($k, $chartData['series']['Done']['data'])){
                $chartData['series']['Done']['data'][$k] = 0;
            }
            if(!isset($chartData['series']['Rejected']['data']) || !key_exists($k, $chartData['series']['Rejected']['data'])){
                $chartData['series']['Rejected']['data'][$k] = 0;
            }
            if(!isset($chartData['series']['Pending']['data']) || !key_exists($k, $chartData['series']['Pending']['data'])){
                $chartData['series']['Pending']['data'][$k] = 0;
            }
        }
        
        ksort($chartData['series']['Done']['data']);
        ksort($chartData['series']['Rejected']['data']);
        ksort($chartData['series']['Pending']['data']);
    }
    $chartData['title'] = 'Payment Chart';
    $chartData['subtitle'] = date('F j, Y', strtotime($dateStart)). (!empty($dateEnd) ? ' to '.date('F j, Y', strtotime($dateEnd)) : '');
    $chartData['yAxis']['title'] = 'Links';
    __setChartData($chartData, "payments-chart");
}

function __setChartData($chartData, $where=''){
    global $viewData;
    $chart = new Highchart();
    $chart->chart->renderTo = $where;
    $chart->credits->enabled=false;
    $chart->chart->type = "column";
    $chart->title->text = $chartData['title'];
    $chart->subtitle->text = $chartData['subtitle'];
    
    $chart->xAxis->categories = !empty($chartData['users']) ? array_values($chartData['users']) : array();
        
    $chart->yAxis->min = 0;
    $chart->yAxis->title->text = $chartData['yAxis']['title'];
    $chart->tooltip->headerFormat = '<span style="font-size:10px">{point.key}</span><table>';
    $chart->tooltip->pointFormat = '<tr><td style="color:{series.color};padding:0">{series.name}: </td> <td style="padding:0"><b>{point.y}</b></td></tr>';
    $chart->tooltip->footerFormat = '</table>';
    $chart->tooltip->shared = true;
    $chart->tooltip->useHTML = true;
    
    $chart->plotOptions->column->pointPadding = 0.2;
    $chart->plotOptions->column->borderWidth = 0;
    if(!empty($chartData['series'])){
        foreach($chartData['series'] as $k=>$v){
            $chart->series[] = array( 
                        'name'=> $k,
                        'data'=> array_values($v['data'])
                       );
        }
    }        
    $viewData->set('chart', $chart);
}

function __chartsFilterVars($model='ApprovalLink'){
    global $viewData;
    $filter = array();
    $filterVars = array();
    if(!empty($_GET['_dt'])){
        list($dateFrom, $dateTo) = explode('_', base64_decode($_GET['_dt']));
        $field = "$model.";
        switch($model){
            case 'ApprovalLink':
                $field .= 'added_on';
                break;
            case 'Payment':
                $field .= 'added_on';
                break;
        }
        if(!empty($dateFrom) and !empty($dateTo)){
            $filterVars['dateFrom'] = $dateFrom;
            $filterVars['dateTo'] = $dateTo;
            $filter[$model][] = "DATE_FORMAT($field, '%Y-%m-%d') >= '". date('Y-m-d', strtotime($dateFrom))."'";
            $filter[$model][] = "DATE_FORMAT($field, '%Y-%m-%d') <= '".date('Y-m-d', strtotime($dateTo))."'"; 
        } else if(!empty($dateFrom) and empty($dateTo)){
            $filterVars['dateFrom'] = $dateFrom;
            $filter[$model][] = "DATE_FORMAT($field, '%Y-%m-%d') = '". date('Y-m-d', strtotime($dateFrom))."'"; 
        }
    }
    $filters[$model] = $filter[$model] ? implode(' AND ', $filter[$model]) : '';
    $viewData->set('filterVars', $filterVars);
    $viewData->set('dateFrom', $dateFrom);
    if(strtotime($dateFrom)!= strtotime($dateTo)){
        $viewData->set('dateTo', $dateTo);
    }
    return $filters;
}
 
 
// auto call function related to page if exists
$action = isset($_GET['act']) ? $_GET['act'] : 'approval';
if(function_exists('charts_'.$action)){ 
 call_user_func('charts_'.$action);
}
// include default template
$useLayout = isset($_GET['act']) ? 'charts_'.$_GET['act'].'.php' : 'charts_approval.php';
include "views/default.php";
 ?>