<?php
require_once("../includes/initialize.php");
 if (!$session->is_logged_in()) { redirect_to("login.php"); }

   function submitted_links_index(){
      global $viewData;
      $viewData->setTitle('Submitted Links Listing');
      $filters = __paymentFilterVars();
      
   }
   
   function submitted_links_data(){
      global $viewData;
      $Payment = new Payment;
      $filters = __paymentFilterVars();
      $isExport = base64_decode($_REQUEST['_export'])==1? true : false;
      $paymentFilter = array('contain'=>array('User'), 'order'=>array('Payment.id DESC'));
      if(!empty($filters['Payment'])){
         $paymentFilter['where'] = !empty($paymentFilter['where']) ? $paymentFilter['where'].' AND '. $filters['Payment'] : $filters['Payment'];
      }
      
      
      $submittedLinks = $Payment->find_all($paymentFilter, $isExport==false ? true : false);
      $viewData->set('submittedLinks', $submittedLinks);
      if($isExport===false){
         ob_start();
         include_once "views/submitted_links_data.php";
         $_html = ob_get_contents();
         ob_end_clean();
         echo json_encode(array('html'=>$_html, 'pagination'=>$viewData->get('pageLinks'), 'totalRecords'=> $viewData->get('totalRecords')));
      }else{
         include_once "views/submitted_links_export.php";
      }
      exit;      
   }
   
   function submitted_links_filter_form(){
      global $viewData;
      if($_POST['filterVars']){
         $viewData->set('filterVars', $_POST['filterVars']);
      }
      $Campaign = new Campaign;
      $User = new User;
      $viewData->set('campaignsList', $Campaign->getList());
      $viewData->set('usersList', $User->getList());
      include_once "views/submitted_links_filter_form.php";
      exit;
   }
      
   function submitted_links_do_action(){
      switch($_REQUEST['whatDo']){
       case 'link_delete':
        __action_link_delete($_REQUEST['data']);
        break;
       case 'link_payment':
        __action_link_payment($_REQUEST['data']);
        break;
      }
      exit;
   }
   
   function submitted_links_webmaster_list_json(){
      global $viewData;
      $Payment = new Payment;
      $options=array('group'=>'client_mail', 'fields'=>array('id','client_mail'));
      if(!empty($_GET['q'])){
         $options['where'] = "Payment.client_mail LIKE '%".$_GET['q']."%'";
         $options['limit'] = !empty($_GET['page_limit']) and (int)$_GET['page_limit']>0 ? '0, '.$_GET['page_limit'] : '0, 10';
         
         $clientsEmailFromPayment = $Payment->find_list($options);
         
         $clientEmailList = array();
         if(!empty($clientsEmailFromPayment)){
            foreach($clientsEmailFromPayment as $id=>$email){
               $clientEmailList[$email]=$email;
            }
         }
         echo json_encode(select2DataFormat($clientEmailList));
      }
      exit;
   }

   function submitted_links_edit(){
      global $viewData;
      $viewData->setTitle('Edit Submitted Link');
      $Payment = new Payment;
      $paymentData = $Payment->find_all(array('where'=>"Payment.id = '{$_GET['lid']}' and Payment.status ='Underprocess' "));
      if(!empty($paymentData)){
         $paymentData = array_shift($paymentData);
         $viewData->set('payment', $paymentData);
      }else{
         redirect_to('submitted_links.php');
      }
   }
   
   function submitted_links_edit_save(){
      global $database, $session;
      $msg = array('msg'=>'Link could not updated. Please try again later.', 'status'=>'error');
      if(!empty($_POST['data'])){
       if(!(__isPaypalEmailExistAndActive($_POST['data']['Payment']['paypal_email'], $msg))){
        echo json_encode($msg);
        exit;
       }
       $ApprovalLink = new ApprovalLink;
       $Payment = new Payment;
       $Paypal = new Paypal;
       
       $data = $_POST['data'];
       $anchorTxt = explode("\n", $data['Payment']['anchor_text']);
       $data['Payment']['anchor_text'] = implode('[', $anchorTxt);
       $data['Payment']['start_date'] = date('Y-m-d', strtotime($data['Payment']['start_date']));
       $data['Payment']['end_date'] = strtotime($data['Payment']['end_date'])<strtotime($data['Payment']['start_date']) ? date('Y-m-d', strtotime($data['Payment']['start_date'])) : date('Y-m-d', strtotime($data['Payment']['end_date']));
       
       $link = $ApprovalLink->find_all(array('contain'=>array('Approval'=>array('fields'=>array('id', 'domain_id'))), 'fields'=>array('id'), 'where'=>"ApprovalLink.id='". $data['Payment']['child_table_id']."'"));
       if($Payment->save($data)){
         $link = array_shift($link);
        // add domain id in paypal
        $paypal = $Paypal->find_all(array('where'=>"Paypal.name ='".$data['Payment']['paypal_email']."'"));
        if(!empty($paypal)){
         $paypal = array_shift($paypal);
         $_domainStr = array_filter(explode(',', $paypal['Paypal']['domain_id_string']));
         if(!in_array($link['Approval']['domain_id'], $_domainStr)){
          $_domainStr[] = $link['Approval']['domain_id'];
          $_paypalData['Paypal']['domain_id_string'] = join(',', $_domainStr);
          $_paypalData['Paypal']['id'] = $paypal['Paypal']['id'];
          $_paypalData['Paypal']['updated_on'] = date('Y-m-d H:i:s');
          $Paypal->save($_paypalData);
         }
        }else{
         $_paypalData['Paypal'] = array('name'=>$data['Payment']['paypal_email'], 'domain_id_string'=>$link['Approval']['domain_id'], 'status'=>1, 'added_on'=>date('Y-m-d H:i:s'), 'updated_on'=>date('Y-m-d H:i:s'));
         $Paypal->save($_paypalData);
        }
        
        
        $msg = array('msg'=>'Link has been updated.', 'status'=>'success');
       }
       
      }
      echo json_encode($msg);
      exit;
   }
   
   
   function __action_link_delete($data=array()){
      global $session, $database;
      $msg = array('msg'=>'Sorry, link could not be deleted. Please try again later.', 'status'=>'error');
      if(!empty($data)){
         $linkIds = $data['Payment']['id'];
         if(!empty($linkIds)){
            $Payment = new Payment;
            $ApprovalLink = new ApprovalLink;
            $deleteConds = array("id IN (". join(',', $linkIds). ") ");
            if($session->read('User.user_type')!= 'superadmin'){
               $deleteConds[] = "user_id='". $session->read('User.id'). "' ";
               $deleteConds[] = "status='Underprocess' ";
            }
            
            $paymentList = $Payment->find_list(array('where'=>join(' AND ', $deleteConds), 'fields'=>array('id','child_table_id')));
            if(!empty($paymentList)){
               foreach($paymentList as $paymentId=>$approvalLinkId){
                  if($Payment->delete(" id=$paymentId ")){
                     $ApprovalLink->save(array('ApprovalLink'=>array('id'=>$approvalLinkId, 'final_link_submit_status'=>0)));
                     $msg = array('msg'=>'link has been deleted.', 'status'=>'success');
                  }   
               }
            }
         }
      }
      echo json_encode($msg);
      exit;
   }
   
   function __action_link_payment($data=array()){
      global $session, $database;
      $msg = array('msg'=>'Sorry, link status could not be changed. Please try again later.', 'status'=>'error');
      if(!empty($data)){
         $Payment = new Payment;
         $linkAcceptIds = $data['Payment']['accept'];
         $linkRejectIds = $data['Payment']['reject'];
         if(!empty($linkAcceptIds)){
            foreach($linkAcceptIds as $paymentId){
               if($Payment->save(array('Payment'=>array('id'=>$paymentId, 'status'=>'Paid', 'payment_date'=>date('Y-m-d'))))){
                  __sendPaymentDoneMail($paymentId);
                  $msg = array('msg'=>'link status has been changed.', 'status'=>'success');
               }   
            }
         }
         if(!empty($linkRejectIds)){
            foreach($linkRejectIds as $paymentId){
               if($Payment->save(array('Payment'=>array('id'=>$paymentId, 'status'=>'Rejected')))){
                  __sendPaymentRejectMail($paymentId);
                  $msg = array('msg'=>'link status has been changed.', 'status'=>'success');
               }   
            }
         }
      }
      echo json_encode($msg);
      exit;
   }
   
   function __sendPaymentDoneMail($paymentId=null){
      global $session;
      if($paymentId){
         $Payment = new Payment;
         $link = $Payment->find_by_id($paymentId);
         if(!empty($link)){
         
            $mail = new PHPMail();
   
            $mail->setFrom($session->read('User.email_id'));
            $mail->setTo($link['Payment']['approached_by']);
            $username = array_shift(explode('@', $link['Payment']['approached_by']));
            $mail->setSubject("PLB - Payment done for submitted link.");
            $message = "Hello {$username},  <br />
                              <p>Payment done by client for submiited links, Please find details below- <br />
                              <b>Domain: </b> {$link['Payment']['domain']} <br />
                              <b>URL: </b> {$link['Payment']['url']} <br />
                              <b>Campaign: </b> {$link['Payment']['campaign_name']} <br />
                              <b>Ammount Paid: </b> ". currency_format($link['Payment']['amount'], $link['Payment']['currency']). " <br />
                              </p>
                              <p>
                              <b>Thanks</b> <br />
                              Adlift PLB
                              </p>
                              ";
            $mail->setBody($message);
            $mail->sendMail();
         }
      }
   }
   
   function __sendPaymentRejectMail($paymentId=null){
      global $session;
      if($paymentId){
         $Payment = new Payment;
         $link = $Payment->find_by_id($paymentId);
         if(!empty($link)){
         
            $mail = new PHPMail();
   
            $mail->setFrom($session->read('User.email_id'));
            $mail->setTo($link['Payment']['approached_by']);
            $username = array_shift(explode('@', $link['Payment']['approached_by']));
            $mail->setSubject("PLB - Payment rejected for submitted link.");
            $message = "Hello {$username},  <br />
                              <p>Payment rejected by client for submiited links, Please find details below- <br />
                              <b>Domain: </b> {$link['Payment']['domain']} <br />
                              <b>URL: </b> {$link['Payment']['url']} <br />
                              <b>Campaign: </b> {$link['Payment']['campaign_name']} <br />
                              <b>Ammount Paid: </b> ". currency_format($link['Payment']['amount'], $link['Payment']['currency']). " <br />
                              </p>
                              <p>
                              <b>Thanks</b> <br />
                              Adlift PLB
                              </p>
                              ";
            $mail->setBody($message);
            $mail->sendMail();
         }
      }
   }
      
   
   function __paymentFilterVars(){
      global $viewData;
      $filter = array();
      $filterVars = array();
      if(!empty($_POST['_uid'])){
       $users = base64_decode($_POST['_uid']);
       $filterVars['users'] = explode(',', $users);
       $filter['Payment'][] = ' Payment.user_id IN ('.$users.') ';
      }
      if(!empty($_POST['_camp'])){
       $campigns = base64_decode($_POST['_camp']);
       $filterVars['campaigns'] = explode(',', $campigns);
       $filter['Payment'][] = ' Payment.campaign_id IN ('.$campigns.') ';
      }
      
      if(!empty($_POST['_st'])){
         $paymentStatus = array('0'=>'Rejected','1'=>'Paid','-1'=>'Underprocess');
       $status = base64_decode($_POST['_st']);
       $filterVars['status'] = explode(',', $status);
       $stArr = array();
       foreach($filterVars['status'] as $_sts){
         $stArr[] = $paymentStatus[$_sts];
       }
       
       $filter['Payment'][] = " Payment.status IN ('".join("','", $stArr)."') ";
      }
      
      if(!empty($_POST['_m'])){
       $m = base64_decode($_POST['_m']);
       $filterVars['month'] = explode(',', $m);
       $filter['Payment'][] = ' Payment.month IN ('.$m.') ';
      }
      
      if(!empty($_POST['_y'])){
       $y = base64_decode($_POST['_y']);
       $filterVars['year'] = explode(',', $y);
       $filter['Payment'][] = ' Payment.year IN ('.$y.') ';
      }
      
      if(!empty($_POST['_ce'])){
       $ce = base64_decode($_POST['_ce']);
       $filterVars['client_mail'] = explode(',', $ce);
       $filter['Payment'][] = " Payment.client_mail IN ('".join("','", $filterVars['client_mail'])."') ";
      }
      
      if(!empty($_POST['_pe'])){
       $pe = base64_decode($_POST['_pe']);
       $filterVars['paypal_email'] = explode(',', $pe);
       $filter['Payment'][] = " Payment.paypal_email IN ('".join("','", $filterVars['paypal_email'])."') ";
      }
      
      $operatorsCollection = array('eq'=>'=', 'lt'=>'<', 'lte'=>'<=', 'gt'=>'>', 'gte'=>'>=');
      if(!empty($_POST['_prVl'])){
       $pr = base64_decode($_POST['_prVl']);
       $prOp = base64_decode($_POST['_prOp']);
       $filterVars['pr_value'] = $pr;
       $filterVars['pr_op'] = $prOp;
       $filter['Payment'][] = " Payment.pr_value ".$operatorsCollection[$prOp]." '".$pr."' ";
      }
      
      if(!empty($_POST['_daVl'])){
       $da = base64_decode($_POST['_daVl']);
       $daOp = base64_decode($_POST['_daOp']);
       $filterVars['da_value'] = $da;
       $filterVars['da_op'] = $daOp;
       $filter['Payment'][] = " Payment.da_value ".$operatorsCollection[$daOp]." '".$da."' ";
      }
      
      if(!empty($_POST['_dom'])){
       $domain = trim(base64_decode($_POST['_dom']));
       $filterVars['domain'] = $domain;
       $domain = rtrim($domain,"/");	
       $domain = str_replace(array("https://","http://","www."),"",$domain);	
       $domain = preg_replace('/^\./', '', get_domain_name($domain));
       $filter['Payment'][] = " (Payment.domain LIKE 'http://".$domain."%' OR Payment.domain LIKE 'http://www.".$domain."%') ";
      }
      
      if(!empty($_POST['_dt'])){
         list($dateFrom, $dateTo) = explode('_', base64_decode($_POST['_dt']));
         if(!empty($dateFrom) and !empty($dateTo)){
          $filterVars['dateFrom'] = $dateFrom;
          $filterVars['dateTo'] = $dateTo;
          $filter['Payment'][] = " DATE_FORMAT(Payment.payment_date, '%Y-%m-%d') BETWEEN '".date('Y-m-d', strtotime($dateFrom))."' AND '".date('Y-m-d', strtotime($dateTo))."' "; 
         } else if(!empty($dateFrom) and empty($dateTo)){
          $filterVars['dateFrom'] = $dateFrom;
          $filter['Payment'][] = " DATE_FORMAT(Payment.payment_date, '%Y-%m-%d') = '".date('Y-m-d', strtotime($dateFrom))."' "; 
         }
      }
      
      $filters['Payment'] = $filter['Payment'] ? implode(' AND ', $filter['Payment']) : '';
      $viewData->set('filterVars', $filterVars);
      return $filters;
   }
   
   function __isPaypalEmailExistAndActive($email=null, &$msg){ 
      if(!empty($email)){
       $Paypal = new Paypal;
       $paypalCount = $Paypal->count(array('where'=>"Paypal.name='".$email."' AND status=0"));
       if($paypalCount>0){
        $msg = array('msg'=>'Paypal email black listed.', 'status'=>'error');
        return false;
       }else{
        return true;
       }
      }else {
       $msg = array('msg'=>'Invalid paypal email.', 'status'=>'error');
      }
      return false;
   }
 
 // auto call function related to page if exists
$action = isset($_GET['act']) ? $_GET['act'] : 'index';
if(function_exists('submitted_links_'.$action)){ 
 call_user_func('submitted_links_'.$action);
}
// include default template
$useLayout = isset($_GET['act']) ? 'submitted_links_'.$_GET['act'].'.php' : 'submitted_links_index.php';
include "views/default.php";
 ?>