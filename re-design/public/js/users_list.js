$(function(){
    $('#RowPerPage').on('multiselectcreate', function(event, ui){
       $('#RowPerPage').multiselect('destroy');
    })
    $('#RowPerPage').on('change', function(){
		var curUrl = $.parseUrl();
		var queryString = curUrl.query;
		var _rp = $('#RowPerPage').val();
		var urlParams = [];
		queryString._rp=_rp;
                queryString.p=1;
		$.each(queryString, function(k,v){
			if(v.length>0){
				urlParams.push(k+'='+v);
			}
		});
		if(urlParams.length>=1){
			window.location = '?'+urlParams.join('&');
		}
	});
});