$(function(){
    $('._updateRow').on('click', function(){
       var tblRowId = $(this).data('tbl-row');
       var data = {};
       data.id = $('#'+tblRowId+'_id').val();
       data.user = $('#'+tblRowId+'_user').val();
       $.post('clients.php?act=update_assign_user',
              {data : data },
              function(res){
                if(res.status=='ok'){
                    $('#'+tblRowId+' td:eq(1)').html(res.user);
                }else{
                    alert('Unable to change user. Please try again later.');
                }
              }, 'json').fail(function(){
            alert('Unable to change user. Error in connection.');
       });
    });
})