$(function(){
    var _totalRecord = null;
    var _offset = 0;
    var _row = 5;
    var _totalHitCount = 0;
    var _hitCount=1;
    var progressBar = $('#updateProgress');
    var _timeInterval;
    var inItUpdatePR = function(){
        if (_hitCount<=_totalHitCount && _offset<=_totalRecord) { 
         _timeInterval = setTimeout(_updatePr(), 30000);   
        }else{
            window.location = 'approvals.php';
        }
    }
    var _updatePr = function(){
        var _limit = _offset+', '+ _row;
        if (_timeInterval) {
            clearTimeout(_timeInterval);
        }
        $.get('approvals.php?act=update_pr_for_all&rd='+ Math.floor(Math.random()*100101),
                {'limit': _limit},
                function (data) {
                  if (data.msg) {
                    alert(data.msg);
                  }else{
                    _hitCount++;
                    _offset = (_hitCount*_row) - _row ;
                    var recordsDone = Math.ceil((_offset/_totalRecord)*100);
                    recordsDone = recordsDone>100?100 : recordsDone;
                    progressBar.css({'width': recordsDone+'%'});
                    progressBar.attr('aria-valuenow', recordsDone);
                    progressBar.find('span.sr-only').html(recordsDone+'%');
                    inItUpdatePR();
                  }
                },
                'json'
                ).fail(function(){
              alert('request processing error.');
        });
    }
    
    $.get('approvals.php?act=update_pr_total&rd='+ Math.floor(Math.random()*101), {},
          function (data) {
            _totalRecord = data.totalRecord;
            _totalHitCount = Math.ceil(_totalRecord/_row);
            console.log(_totalRecord);
            inItUpdatePR();
          },
          'json'
          ).fail(function(){
        alert('request processing error.');
    });
    
    })