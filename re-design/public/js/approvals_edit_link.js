function checkAndSubmitApprovalForm(form) {
    $('.loader_box_overlay').show();
    var postData = $.post('approvals.php?act=edit_link_save',
                        $(form).serialize(),
                        function(msg){
                            $('.loader_box_overlay').hide();
                            if (msg.status=='success') {
                                refreshParentWindow();
                                window.location = window.location;
                                $(form).reset();
                            }else{
                                alert(msg.msg);
                            }
                        }, 'json');
    postData.fail(function(){
        $('.loader_box_overlay').hide();
        alert('error occurred in processing data.');
    });
}

function refreshParentWindow(){
    if (window.opener) {
        window.opener.location = window.opener.location;    
    }
    
}

jQuery(document).ready(function($){
    
    $('#add-campaign-submit').bind('click', function(){
       var form = $('#ApprovalEditLinkSave');
       var camps = $('#ApprovalLinkCampaign').multiselect("getChecked").map(function(){
            return this.value;    
         }).get();
       if (camps.length==0) {
        form.find('span.error').show();
       }else{
        form.find('span.error').hide();
        $('#modal-add-event').modal('hide');
        checkAndSubmitApprovalForm(form);
       }
       
    });
    
    $('.remove-campaign').bind('click', function(e){
        e.preventDefault();
        var linkId = $(this).attr('rel');
        if (confirm('Do you want to delete campaign from link?')) {
            $('.loader_box_overlay').show();
            var postData = $.post('approvals.php?act=remove_link',{'approvalLinkId' : linkId}, function(msg){
                $('.loader_box_overlay').hide();
                if (msg.status=='success') {
                    refreshParentWindow();
                    if (msg.window && msg.window=='close') {
                        window.close();
                    }else{
                        window.location = window.location;
                    }
                }else{
                    alert(msg.msg);
                }
            }, 'json').fail(function(){
                $('.loader_box_overlay').hide();
                alert('error occurred in processing request.');
            });
        }
    });
    
    $('#updateNarration').on('click', function(){
        $('.loader_box_overlay').show();
        var $data = {};
        $data.narration_text = $('#narrationText').val();
        $data.id = $('#_ApprovalLinkId').val();
        $.post('approvals.php?act=comment_save',
               $data,
               function(data){
                    $('.loader_box_overlay').hide();
                    data = $.parseJSON(data);
                    refreshParentWindow();
                    alert(data.msg);
               }).fail(function(){
            $('.loader_box_overlay').hide();
            alert('Error in request processing'); 
        });
    });
    
  });