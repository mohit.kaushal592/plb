   // submit Payment action form
   function __submitPaypalActForm($rtype){
      var $form = $('#PaypalListForm');
      var $act;
      switch ($rtype) {
         case 'paypal':
            $act='paypal.php?act=do_action';
            break;
         case 'domain':
            $act='domains.php?act=do_action';
            break;
      }
      if ($act) {
         $('.loader_box_overlay').show();
         var $postForm = $.post( $act, $form.serialize(), function(data) {
            if(data.status=='success'){
             window.location = window.location;
            }else{
                alert(data.msg);
            }
        }, "json");
        $postForm.done(function() {
            $('.loader_box_overlay').hide();
        }).fail(function(){
            $('.loader_box_overlay').hide();
            alert('error in form processing.');
        });
      }
      
   }
   function toggleAcceptRejectCheckbox(chkType) {
      // toggle accept or reject checkboxes
      $('._reject_'+chkType).bind('click', function(){
         var $this = $(this);
         if($this.is(':checked')===true){
             $('._accept_'+chkType+'[value='+$this.val()+']').attr('checked', false).parent('span').removeClass('checked');
         }
      });
      
      $('._accept_'+chkType).bind('click', function(){
         if($(this).is(':checked')== true){
             $('._reject_'+chkType+'[value='+$(this).val()+']').attr('checked', false).parent('span').removeClass('checked');
         }
      });
   }
 
 $(function(){
   toggleAcceptRejectCheckbox('paypal');
   toggleAcceptRejectCheckbox('domain');
   
   // action for approvals
   $('a.doAct').bind('click', function(e){
      e.preventDefault();
      var actType = $.trim($(this).attr('rel'));
      if(actType=='update_paypal'){
          var accpCheckedCount = $('._accept_paypal:checked').length;
          var rejctCheckedCount = $('._reject_paypal:checked').length;
          if(accpCheckedCount>0 || rejctCheckedCount>0){
           $('#PaypalWhatDo').val(actType);
           __submitPaypalActForm('paypal');
         }else{
            alert('select atleast 1 link to process');
         }
      } else if(actType=='update_domain'){
          var accpCheckedCount = $('._accept_domain:checked').length;
          var rejctCheckedCount = $('._reject_domain:checked').length;
          if(accpCheckedCount>0 || rejctCheckedCount>0){
           $('#PaypalWhatDo').val(actType);
           __submitPaypalActForm('domain');
         }else{
            alert('select atleast 1 link to process');
         }
      }
   });
 })
 
 $(function(){
    $('#RowPerPage').on('multiselectcreate', function(event, ui){
       $('#RowPerPage').multiselect('destroy');
    })
    $('#RowPerPage').on('change', function(){
		var curUrl = $.parseUrl();
		var queryString = curUrl.query;
		var _rp = $('#RowPerPage').val();
		var urlParams = [];
		queryString._rp=_rp;
		queryString.p=1;
		$.each(queryString, function(k,v){
			if(v.length>0){
				urlParams.push(k+'='+v);
			}
		});
		if(urlParams.length>=1){
			window.location = '?'+urlParams.join('&');
		}
	});
});