async function getTotalVisitsForLast12Months(apiKey, website) {
    // Calculate start date as 11 months ago from the end date
    const today = new Date();
    const end_date = new Date(today.getFullYear(), today.getMonth(), 0); // Set end date to the last day of the current month
    const start_date = new Date(end_date);
    start_date.setMonth(start_date.getMonth() - 11); // Subtract 11 months
  
    // Format start and end dates as strings
    const end_date_str = formatDate(end_date);
    const start_date_str = formatDate(start_date);
  
    function formatDate(date) {
      const year = date.getFullYear();
      const month = String(date.getMonth() + 1).padStart(2, '0');
      const day = String(date.getDate()).padStart(2, '0');
      return `${year}-${month}-${day}`;
    }
  
    // Define the URL and query parameters
    const url = `https://api.similarweb.com/v1/website/${website}/total-traffic-and-engagement/visits`;
    const params = {
      api_key: apiKey,
      granularity: 'monthly',
      main_domain_only: false,
      format: 'json',
      show_verified: false,
      mtd: false,
      engaged_only: false,
      start_date: start_date_str,
      end_date: end_date_str,
    };
  
    // Make an HTTP GET request and return a promise
    let end_data = $.get(url,params,
    function(response){   
        if (response.meta.status === 'Success'){ 
            const lastEntry = response.visits[response.visits.length - 1];
            const totalVisits = lastEntry.visits; 
            //console.log(`Total Visits for ${domain}: ${totalVisits}`); 
            $('#ApprovalTraffic').val(Math.ceil(totalVisits));
            return Math.ceil(totalVisits)
        }else{
            //$('#ApprovalTraffic').val(0);
            //return "0"
        }
        
    }, 'json').fail(function(){ 
        alert('error in request processing.');
        throw new Error(`Error: ${error.message}`);
    });     
}

function getDaPrIp(domain){  
    if(domain!=''){ 
        $('.loader_box_overlay').show();
        // Annual traffic api 

        // Set your SimilarWeb API key and website
        const apiKey = '7ec9e215afdc40d1b35aa2dd2e029cf9';
        const website = 'adlift.com';
        
        getTotalVisitsForLast12Months(apiKey, domain).then((result) => { 
            $('#ApprovalTraffic').val(result);
        }).catch((error) => {
            alert('error in request processing.');
        });
 
        $.get('approvals.php?act=calc_da_pr_ip',
            {'domain':domain},
            function(msg){
              $('.loader_box_overlay').hide(); 
              if(msg != "" && msg.ip){ 
                if(msg.ac ==""){
                  msg.ac = "Unknown";
                }
                  $('#ApprovalIp').val(msg.ip);
                  $('#ApprovalPr').val(msg.pr);
                  $('#ApprovalDa').val(msg.da);
                  $('#ApprovalSc').val(msg.sc);
                  $('#ApprovalAc').val(msg.ac);
              }else{
                  $('#pop-up-box').showModalBox();
              }
          }, 'json').fail(function(){
            $('.loader_box_overlay').hide();
            alert('error in request processing.');
        });

        
    }
}

function checkAndSubmitApprovalForm(form, e) {
    e.preventDefault();
    $('.loader_box_overlay').show();
    $.post('approvals.php?act=save_add_link',
        $(form).serialize(),
        function(msg){
            $('.loader_box_overlay').hide();
            alert(msg.status);
            if(msg.status=='success') {
                //console.log('success___',msg.msg) 
                alert(msg.msg, 'Success');
                $(form).reset();
                $('input[type=radio]').closest('span.checked').removeClass('checked');
            }else{
                //console.log('error___',msg.msg)
                alert(msg.msg); 
            }
      }, 'json').fail(function(){
        $('.loader_box_overlay').hide();
        alert('error in request processing.');
    });
    
}

 
jQuery(document).ready(function($){
    var AddLinkFormValidate = jQuery('#AddLinkForm').validate({errorElement : 'span', ignore: ':hidden:not("select")'});
    
    // calculate da, pr, ip
    $('#ApprovalCalcDaPr').bind('click', function(){
        if(AddLinkFormValidate.element('#ApprovalDomain')){
          getDaPrIp($('#ApprovalDomain').val());
        }
    });
    
    // check campaign and link duplicacy, submit add link form
    $('#AddLinkForm').bind('submit', function(e){
      if(AddLinkFormValidate.valid()){
        e.preventDefault();
        checkAndSubmitApprovalForm(this, e);

        // if(!$('input:radio[id^=ApprovalContentWriter]').is(':checked')){
        //     console.log('4__',this)
        //     $('span.content-writer').show();
        // }else{
        //     console.log('5__')
        //     $('span.content-writer').hide();
        //     checkAndSubmitApprovalForm(this, e);
        // }

      }else{
        console.log('3__')
      }
    });
    
    // domain history
    $('.DomainIpHistory').bind('click', function(e){
      e.preventDefault();
       if(AddLinkFormValidate.element('#ApprovalDomain')){ 
            if ($(this).attr('id')=='DomainHistory') {
                getDomainHistory($('#ApprovalDomain').val());
            } else if ($(this).attr('id')=='IpHistory') {
                if ($('#ApprovalIp').val()=='') {
                    alert('Please calculate ip for this domain.');
                }else{ 
                    getIpHistory($('#ApprovalIp').val());
                }
            }
          
        }
    });
    
  });
   /*         * ** Start: Added by Ajit: 15-DESC-2015  ***************** */
  //////Set up Currency////////
  
  function checkCurrency(curr)
  {
     
      if(curr=='No Payment')
      {          
          $("#PaymentAmount").val('0');
          $("#PaymentAmount").attr("readonly", "readonly");
      }
      else
      {
         if( $("#PaymentAmount").val()=='0')
         {
              $("#PaymentAmount").val('');
             
         }
          $("#PaymentAmount").removeAttr("readonly"); 
        
      }    
          
     /*         * ** End: Added by Ajit: 15-DESC-2015  ***************** */      
      
      
      
  }
  