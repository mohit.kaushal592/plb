<?php
/*ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
*/
require_once("../includes/initialize.php");
ini_set("memory_limit","59116M");
$responsecode = 401;
$responsemsg = "Invalid request.";
$action = "";
global $viewData;
require_once(LIB_PATH.DS.'database.php');
global $database;
$page = 0;
$limit = 10;


if(empty($_GET['action'])){

}else{
$action = $_GET['action'];
$responsecode = 200;
$responsemsg = "success";
}

if(!empty($_GET['page'])){
	$page=$_GET['page'];
}

if(!empty($_GET['limit'])){
	$limit=$_GET['limit'];
}



if($action=="listing"){
  	$time_start = microtime(true);
	$sqlq = 'SELECT DISTINCT Approval.id AS Approval_id FROM tbl_approval_links_main Approval INNER JOIN tbl_approval_links_child ApprovalLink ON (ApprovalLink.parent_id=Approval.id)   INNER JOIN tracking_login User ON (User.id = Approval.user_id)  WHERE  Approval.superadmin_status IN (0,1)  AND  ApprovalLink.final_link_submit_status IN (0,1)  ORDER BY ApprovalLink.added_on DESC';
	//ApprovalLink.approve_status IN (1)  and ApprovalLink.secondary_approval IN (1) and ApprovalLink.is_secondary_approval IN(1) AND 
	$resultset = $db->query($sqlq);
	$rescount = $db->num_rows($resultset);
	$resultar = $db->fetch_data_array($resultset);
	$list = array();
	foreach ($resultar as $rkey => $rvalue) {
		if($rkey>=$page){
			$list[] = $rvalue['Approval_id'];
		}
	if(!empty($list)){
		if(count($list)>$limit){
			break;
		}
	}
	}
	$aprvl_ids = implode(',',$list);
$fsql = "SELECT  Approval.id AS Approval_id, Approval.domain AS Approval_domain, Approval.domain_final AS Approval_domain_final, Approval.domain_id AS Approval_domain_id, Approval.ip AS Approval_ip, Approval.pr AS Approval_pr, Approval.da AS Approval_da, Approval.added_on AS Approval_added_on, Approval.child_update_date AS Approval_child_update_date, Approval.approached_by AS Approval_approached_by, Approval.user_id AS Approval_user_id, Approval.campaigns_count AS Approval_campaigns_count, Approval.camps_id AS Approval_camps_id, Approval.narration_text AS Approval_narration_text, Approval.narration_admin AS Approval_narration_admin, Approval.r_data AS Approval_r_data, Approval.ip_conflict_status AS Approval_ip_conflict_status, Approval.superadmin_status AS Approval_superadmin_status, Approval.status AS Approval_status, Approval.content_writer AS Approval_content_writer, Approval.payment_type AS Approval_payment_type, Approval.sc AS Approval_sc, Approval.ac AS Approval_ac, Approval.keyword AS Approval_keyword, Approval.sv AS Approval_sv, Approval.rankval AS Approval_rankval, Approval.title AS Approval_title, Approval.title_id AS Approval_title_id, Approval.target_url AS Approval_target_url, Approval.annual_traffic AS Approval_annual_traffic, Approval.dr AS Approval_dr, Approval.category AS Approval_category, Approval.backlinks AS Approval_backlinks, ApprovalLink.id AS ApprovalLink_id, ApprovalLink.approached_by AS ApprovalLink_approached_by, ApprovalLink.user_id AS ApprovalLink_user_id, ApprovalLink.campaign_name AS ApprovalLink_campaign_name, ApprovalLink.campaign_id AS ApprovalLink_campaign_id, ApprovalLink.added_on AS ApprovalLink_added_on, ApprovalLink.parent_id AS ApprovalLink_parent_id, ApprovalLink.approve_status AS ApprovalLink_approve_status, ApprovalLink.r_data_i AS ApprovalLink_r_data_i, ApprovalLink.secondary_approval AS ApprovalLink_secondary_approval, ApprovalLink.is_secondary_approval AS ApprovalLink_is_secondary_approval, ApprovalLink.secondary_app_user AS ApprovalLink_secondary_app_user, ApprovalLink.status_update_date AS ApprovalLink_status_update_date, ApprovalLink.secondary_status_update_date AS ApprovalLink_secondary_status_update_date, ApprovalLink.final_link_submit_status AS ApprovalLink_final_link_submit_status, ApprovalLink.Amount AS ApprovalLink_Amount, ApprovalLink.Currency AS ApprovalLink_Currency, ApprovalLink.link_submit_date AS ApprovalLink_link_submit_date, ApprovalLink.updated_on AS ApprovalLink_updated_on, ApprovalLink.cron_status AS ApprovalLink_cron_status, ApprovalLink.payment_type AS ApprovalLink_payment_type, User.id AS User_id, User.username AS User_username, User.password AS User_password, User.first_name AS User_first_name, User.last_name AS User_last_name, User.email_id AS User_email_id, User.status AS User_status, User.user_type AS User_user_type, User.supper_user AS User_supper_user, User.created_on AS User_created_on, User.designation AS User_designation, User.mobile_no AS User_mobile_no, User.employ_id AS User_employ_id, User.address AS User_address, User.alternate_email AS User_alternate_email, User.user_pic AS User_user_pic FROM tbl_approval_links_main Approval INNER JOIN tbl_approval_links_child ApprovalLink ON (ApprovalLink.parent_id=Approval.id)   INNER JOIN tracking_login User ON (User.id = Approval.user_id)  WHERE  Approval.superadmin_status IN (0,1)  AND Approval.id IN($aprvl_ids)  ORDER BY ApprovalLink.added_on DESC";


	$resultset = $db->query($fsql);
	$resultar = $db->fetch_data_array($resultset);

	$respar = array('code'=>$responsecode,'msg'=>$responsemsg,'data'=>$resultar);
	//var_dump($respar);
	$respstr = json_encode($respar);
	echo $respstr;
}

