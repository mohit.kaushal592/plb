<?php
require_once("../includes/initialize.php");
 if (!$session->is_logged_in()) { redirect_to("login.php"); }
 
 function blacklisted_campaign(){
    global $database, $viewData;
    $domain = base64_decode($_GET['d']);
    $msg = array('status'=>'success', 'msg'=>'Black listed campaigns not found.', 'title'=> 'Black listed campaigns for domain - '. $domain);
    if(!empty($domain)){
        $BlacklistDomain = new BlacklistDomain;
        $domain = str_replace(array('http://', 'https://', 'www.'), '', $domain);
        $campaigns = $BlacklistDomain->find_all(array('contain'=>array('Campaign'=>array('fields'=>array('id','name')), 'Domain'=>array('fields'=>array('id','name'), 'where'=>"(Domain.name = 'http://$domain' OR Domain.name = 'http://www.$domain')"))));
        $viewData->set('campaigns', $campaigns);
        ob_start();
        include_once "views/blacklisted_campaign.php";
        $_html = ob_get_contents();
        ob_end_clean();
        if(trim($_html)){
            $msg['msg'] = $_html;
        }
    }
    echo json_encode($msg);
    exit;
 }
 
 function blacklisted_add_campaign(){
    global $database, $viewData;
    $domain = base64_decode($_GET['d']);
    $domain = str_replace(array('http://', 'https://', 'www.'), '', $domain);
    $Campaign = new Campaign;
    $viewData->set('campaigns', $Campaign->getList());
    $viewData->set('campaignSelected', _selected_campaign($domain));
    include_once "views/blacklisted_add_campaign.php";
    exit;
 }
 
 function blacklisted_save_campaign(){
    if(!empty($_POST)){
        $campaigns = $_POST['campaigns'];
        $domain = base64_decode($_POST['domain']);
        $domain = str_replace(array('http://', 'https://', 'www.'), '', $domain);
        $BlacklistDomain = new BlacklistDomain;
        $Domain = new Domain;
        $domainData= $Domain->find_all(array('fields'=>array('id','name'), 'where'=>"(Domain.name = 'http://$domain' OR Domain.name = 'http://www.$domain')"));
        $domainData = !empty($domainData) ? array_shift($domainData) : $domainData;
        $blacklist = $BlacklistDomain->find_all(array('contain'=>array('Campaign'=>array('fields'=>array('id','name')), 'Domain'=>array('fields'=>array('id','name'), 'where'=>"(Domain.name = 'http://$domain' OR Domain.name = 'http://www.$domain')"))));
        if(count($blacklist)>0){
            foreach($blacklist as $_black){
                $BlacklistDomain->delete("id='{$_black['BlacklistDomain']['id']}'");
            }
        }
        if(!empty($campaigns)){
            foreach($campaigns as $campain){
                if(!empty($domainData)){
                    $BlacklistDomain->save(array('BlacklistDomain'=>array('domain_id'=>$domainData['Domain']['id'], 'campaign_id'=>$campain, 'created'=>date('Y-m-d H:i:s'))));
                }
            }
        }
    }else{
        echo "Sorry, campaigns could not be added. Please try again later.";
    }
    exit;
 }
 
 function _selected_campaign($domain){
    $campaigns = array();
    if(!empty($domain)){
        $BlacklistDomain = new BlacklistDomain;
        $blacklisted = $BlacklistDomain->find_all(array('contain'=>array('Campaign'=>array('fields'=>array('id','name')), 'Domain'=>array('fields'=>array('id','name'), 'where'=>"(Domain.name = 'http://$domain' OR Domain.name = 'http://www.$domain')"))));
        if(!empty($blacklisted)){
            foreach($blacklisted as $_black){
                $campaigns[] = $_black['Campaign']['id'];
            }
        }
    }
    return $campaigns;
 }
 
 // auto call function related to page if exists
$action = isset($_GET['act']) ? $_GET['act'] : 'index';
if(function_exists('blacklisted_'.$action)){ 
 call_user_func('blacklisted_'.$action);
}
// include default template
$useLayout = isset($_GET['act']) ? 'blacklisted_'.$_GET['act'].'.php' : 'blacklisted_index.php';
include "views/default.php";