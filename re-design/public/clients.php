<?php
require_once("../includes/initialize.php");
 if (!$session->is_logged_in()) { redirect_to("login.php"); }

 function clients_index(){
    global $viewData;
    $viewData->setTitle('Clients Listing');
    $Campaign = new Campaign;
    $filters = __campaignFilterVars(); 
      $campaignFilter = array('order'=>array('Campaign.id DESC'));
      if(!empty($filters['Campaign'])){
       $campaignFilter['where'] = !empty($campaignFilter['where']) ? $campaignFilter['where'].' AND '. $filters['Campaign'] : $filters['Campaign'];
      }
    $clients = $Campaign->find_all($campaignFilter, true);
    $viewData->set('clients', $clients);
 }
 
 function clients_do_action(){
  switch($_REQUEST['whatDo']){
   case 'update_client':
    __action_update_client($_REQUEST['data']);
    break;
   case 'update_client_approval':
    __action_update_client_approval($_REQUEST['data']);
    break;
  }
  exit;
 }
 
 function clients_edit(){
   global $viewData;
   $Client = new Campaign;
   if(isset($_REQUEST['cid']) and !isset($_REQUEST['data'])){
      $campaign = $Client->find_by_id($_GET['cid']);
      $viewData->set('campaign', $campaign);
      include_once "views/clients_edit.php";
   }else if(!empty($_POST['data']['Campaign'])){
      if(!$Client->save($_REQUEST['data'])){
         echo 'Error in updating DA/PR.';
      }
   }
   exit;
 }
 
 function clients_campaign_list_json(){
   global $viewData;
   $Campaign = new Campaign;
   $options=array('fields'=>array('id','name'));
   if(!empty($_GET['q'])){
      $options['where'] = "Campaign.name LIKE '%".$_GET['q']."%'";
      $options['limit'] = !empty($_GET['page_limit']) and (int)$_GET['page_limit']>0 ? '0, '.$_GET['page_limit'] : '0, 10';
      $campaignList = $Campaign->find_list($options);
      $campaignListFormated =array();
      if(!empty($campaignList)){
         foreach($campaignList as $id=>$name){
            $campaignListFormated[$name]=$name;
         }
      }
      echo json_encode(select2DataFormat($campaignListFormated));
   }
   exit;
 }
 
 function clients_assign_user(){
   global $viewData;
   $viewData->setTitle('Assign User');
   $Campaign = new Campaign;
   if(!empty($_POST['data']['Campaign'])){
      if(!$Client->save($_REQUEST['data'])){
         echo 'Error in updating Campaign.';
      }
   }
   $User = new User;
   $clients = $Campaign->find_all(array('where'=>'secondary_approval=1', 'fields'=>array('id', 'name', 'secondary_approval_user')));
   $users = $User->find_list(array('where'=>"user_type IN ('admin', 'superadmin') AND status=1"));
   if(!empty($clients)){
      $user_ids = array();
      foreach($clients as $client){
         $user_ids[] = $client['Campaign']['secondary_approval_user']>0 ? $client['Campaign']['secondary_approval_user'] : '';
      }
      
      if(!empty($user_ids)){
         array_filter($user_ids);
         $campaignUsers = $User->find_list(array('where'=>"id IN ('". implode("','", $user_ids)."')"));
      }
   }
   $viewData->set('clients', $clients);
   $viewData->set('users', $users);
   $viewData->set('campaignUsers', $campaignUsers);
 }
 
 function clients_update_assign_user(){
   global $database;
   $Campaign = new Campaign;
   $msg ['msg'] = 'Error in updating Campaign.';
   $msg ['status'] = 'error';
   if(!empty($_POST['data']) and !empty($_POST['data']['id'])){
      $data['Campaign']['id'] =  $_POST['data']['id'];
      $data['Campaign']['secondary_approval_user'] =  $_POST['data']['user'];
      if($Campaign->save($data)){
         $ApprovalLink = new ApprovalLink;
         $database->query('UPDATE '. $ApprovalLink->table_name." SET secondary_app_user='".$_POST['data']['user']."' WHERE is_secondary_approval=1 AND final_link_submit_status=0 AND campaign_id='".$_POST['data']['id']."'");
         $User = new User;
         $userData = $User->find_list(array('where'=>"id = '".$_POST['data']['user']."'"));
         $msg ['msg'] = 'Campaign updated.';
         $msg ['status'] = 'ok';
         $msg['user'] = $userData[$_POST['data']['user']];
      }
   }
   echo json_encode($msg);
   exit;
 }
 
 function __action_update_client($data=array()){
   global $database;
   $msg = array('msg'=>'Sorry, client status could not changed. Please try again later.', 'status'=>'error');
   if(!empty($data)){
      $Campaign= new Campaign;
    // client active
    if(!empty($data['Campaign']['accept'])){
     $_ids = $data['Campaign']['accept'];
     if(!empty($_ids)){
      $database->query('UPDATE '. $Campaign->table_name.' SET status_plb=1 WHERE id IN('. join(',', $_ids). ') AND status_plb!=1');
      $msg = array('msg'=>'Client status has been changed.', 'status'=>'success');
     }
    }
    // client block
    if(!empty($data['Campaign']['reject'])){
     $_ids = $data['Campaign']['reject'];
     if(!empty($_ids)){
      $database->query('UPDATE '. $Campaign->table_name.' SET status_plb=0 WHERE id IN('. join(',', $_ids). ') AND status_plb!=0');
      $msg = array('msg'=>'Client status has been changed.', 'status'=>'success');
     }
    }
   }
   echo json_encode($msg);
 }
 
 function __action_update_client_approval($data=array()){
   global $database;
   $msg = array('msg'=>'Sorry, client status could not changed. Please try again later.', 'status'=>'error');
   if(!empty($data)){
      $Campaign = new Campaign;
      $ApprovalLink = new ApprovalLink;
    // client active
    if(!empty($data['Campaign']['accept_sec'])){
     $_ids = $data['Campaign']['accept_sec'];
     if(!empty($_ids)){
      $database->query('UPDATE '. $Campaign->table_name.' SET secondary_approval=1 WHERE id IN('. join(',', $_ids). ') AND secondary_approval!=1');
      //$database->query('UPDATE '. $ApprovalLink->table_name.' SET is_secondary_approval=1, secondary_app_user=25 WHERE campaign_id IN('. join(',', $_ids). ') AND is_secondary_approval=0 AND final_link_submit_status=0');
      $msg = array('msg'=>'Client status has been changed.', 'status'=>'success');
     }
    }
    // client block
    if(!empty($data['Campaign']['reject_sec'])){
     $_ids = $data['Campaign']['reject_sec'];
     if(!empty($_ids)){
      $database->query('UPDATE '. $Campaign->table_name.' SET secondary_approval=0 WHERE id IN('. join(',', $_ids). ') AND secondary_approval!=0');
      //$database->query('UPDATE '. $ApprovalLink->table_name.' SET is_secondary_approval=0 WHERE campaign_id IN('. join(',', $_ids). ') AND is_secondary_approval=1  AND final_link_submit_status=0');
      $msg = array('msg'=>'Client status has been changed.', 'status'=>'success');
     }
    }
   }
   echo json_encode($msg);
 }
 
 function __campaignFilterVars(){
   global $viewData;
  $filter = array();
  $filterVars = array();
  if(!empty($_GET['_camp'])){
      $camp = base64_decode($_GET['_camp']);
      $filterVars['campaigns'] = explode(',', $camp);
      $filter['Campaign'][] = " Campaign.name IN ('".join("','", $filterVars['campaigns'])."') ";
   }
  if(!empty($_GET['_st'])){
      $st = base64_decode($_GET['_st']);
      $filterVars['status'] = explode(',', $st);
      $filter['Campaign'][] = " Campaign.status_plb IN ('".join("','", $filterVars['status'])."') ";
  }
  if(!empty($_GET['_st2'])){
      $st2 = base64_decode($_GET['_st2']);
      $filterVars['sec_status'] = explode(',', $st2);
      $filter['Campaign'][] = " Campaign.secondary_approval IN ('".join("','", $filterVars['sec_status'])."') ";
  }
  $filters['Campaign'] = $filter['Campaign'] ? implode(' AND ', $filter['Campaign']) : '';
   $viewData->set('filterVars', $filterVars);
   return $filters;
 }
 // auto call function related to page if exists
$action = isset($_GET['act']) ? $_GET['act'] : 'index';
if(function_exists('clients_'.$action)){ 
 call_user_func('clients_'.$action);
}
// include default template
$useLayout = isset($_GET['act']) ? 'clients_'.$_GET['act'].'.php' : 'clients_index.php';
include "views/default.php";
 ?>