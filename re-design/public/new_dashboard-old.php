<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- css-files -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <!-- <link rel="stylesheet" href="css/bootstrap.min.css" /> -->
    <link rel="stylesheet" href="css/bootstrap-responsive.min.css" />
    <link rel="stylesheet" href="css/uniform.css" />
    <link rel="stylesheet" href="css/datepicker.css" />
    <link rel="stylesheet" href="css/fullcalendar.css" />
    <!-- <link rel="stylesheet" href="css/style.css" /> -->
    <link rel="stylesheet" href="css/media.css" />
    <link rel="stylesheet" href="css/select2.css" />
    <link rel="stylesheet" href="css/jquery-ui-1.10.3.custom.min.css" />
    <link href="css/font-awesome.css" rel="stylesheet" />
    <link href="css/jquery.multiselect.css" rel="stylesheet" />
    <link href="css/jquery.multiselect.filter.css" rel="stylesheet" />
    <link rel="stylesheet" href="css/jquery.gritter.css" />
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,700,800' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="Assets/Css/Style.css">
    <link rel="stylesheet" href="Assets/Css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.13.7/css/dataTables.bootstrap5.min.css">
    <script src="https://code.jquery.com/jquery-3.7.0.js"></script>
    <script src="https://code.highcharts.com/highcharts.js"></script>
    <script src="https://code.highcharts.com/modules/exporting.js"></script>
    <script src="https://code.highcharts.com/modules/accessibility.js"></script>
    <script src="https://code.highcharts.com/highcharts-more.js"></script> 
</head>
 
<script>
    (function(){
        // get state data start
            fetch('https://api.adlift.ai/marketing/stats/').then(res=>{
                return res.json()
            }).then(response=>{ 
                $('.total_da').text(`${response.total_da}`)
                $('.total_traffic').text(`${response.total_traffic}`)
                $('.total_unique_sites').text(`${response.total_unique_sites}`)
                $('.total_websites').text(`${response.total_websites}`)
            }).catch(()=>{
                alert('some error to fetch stat data')
            })
        // get state data end

        // category bar chart start
            fetch('https://api.adlift.ai/marketing/category_month').then(res=>{
                return res.json()
            }).then(response=>{ 
                const categoryNameArr = [] 
                const categoryValueArr = [] 
                for(let categoryName in response){
                    categoryNameArr.push(categoryName)
                    categoryValueArr.push(response[categoryName])
                } 
                Highcharts.chart('categoryChartContainer', {
                    chart: {
                        type: 'column'
                    },
                    title: {
                        text: 'Category',
                        align: 'center'
                    }, 
                    xAxis: {
                        categories: categoryNameArr,
                        crosshair: true,
                        accessibility: {
                            description: 'Countries'
                        }
                    },  
                    series: [
                        {
                            name: 'Data',
                            data: categoryValueArr
                        }
                    ]
                })
            }).catch(()=>{
                alert('some error to fetch category data')
            })
        // category bar chart end

        // number of site by cost chart start
            fetch('https://api.adlift.ai/marketing/sites').then(res=>{
                return res.json()
            }).then(response=>{  
                const modifyResponse = response.data[0]
                const categoryNameArr = [] 
                const categoryValueArr = [] 
                for(let categoryName in modifyResponse){
                    categoryNameArr.push(categoryName)
                    categoryValueArr.push(modifyResponse[categoryName])
                } 
                Highcharts.chart('numberOfSiteByCostChartContainer', {
                    chart: {
                        type: 'column'
                    },
                    title: {
                        text: 'No. of Sites By Cost',
                        align: 'center'
                    }, 
                    xAxis: {
                        categories: categoryNameArr,
                        crosshair: true,
                        accessibility: {
                            description: 'Countries'
                        }
                    },  
                    series: [
                        {
                            name: 'Data',
                            data: categoryValueArr
                        }
                    ]
                })
            }).catch(()=>{
                alert('some error to fetch category data')
            })
        // number of site by cost chart end

        // top category chart start
            fetch('https://api.adlift.ai/marketing/category').then(res=>{
                return res.json()
            }).then(response=>{  
                var topCategoryObject = [] 
                for(let categoryName in response){
                    var obj = {
                        name: categoryName,
                        value: response[categoryName]
                    }
                    topCategoryObject.push(obj)
                }   
                Highcharts.chart('topCategoryChartContainer', {
                    chart: {
                        type: 'packedbubble',
                        height: '100%'
                    },
                    title: {
                        text: 'Top category',
                        align: 'center'
                    }, 
                    tooltip: {
                        useHTML: true,
                        pointFormat: '<b>{point.name}:</b> {point.value}'
                    },
                    plotOptions: {
                        packedbubble: {
                            minSize: '30%',
                            maxSize: '120%',
                            zMin: 0,
                            zMax: 1000,
                            layoutAlgorithm: {
                                splitSeries: false,
                                gravitationalConstant: 0.02
                            },
                            dataLabels: {
                                enabled: true,
                                format: '{point.name}',
                                filter: {
                                    property: 'y',
                                    operator: '>',
                                    value: 250
                                },
                                style: {
                                    color: 'black',
                                    textOutline: 'none',
                                    fontWeight: 'normal'
                                }
                            }
                        }
                    },
                    series: [{
                        name: 'Top Category',
                        data: topCategoryObject
                    }]
                })
            }).catch(()=>{
                alert('some error to fetch top category data')
            })
        // top category chart end
         
        // brandlift bar chart start
           fetch('https://api.adlift.ai/marketing/brandlift').then(res=>{
                return res.json()
            }).then(response=>{  
                const nameArr = [] 
                const idValueArr = [] 
                for(let id in response){ 
                    nameArr.push(response[id].name)
                    idValueArr.push(parseInt(response[id].total_ids))
                }  
                Highcharts.chart('brandLiftChartContainer', {
                    chart: {
                        type: 'column'
                    },
                    title: {
                        text: 'Brand',
                        align: 'center'
                    }, 
                    xAxis: {
                        categories: nameArr,
                        crosshair: true,
                        accessibility: {
                            description: 'Countries'
                        }
                    },  
                    series: [
                        {
                            name: 'Data',
                            data: idValueArr
                        }
                    ]
                })
            }).catch(()=>{
                alert('some error to fetch brand lift data')
            })
        // brandlift bar chart end 
        
        // Aps chart start
            fetch('https://api.adlift.ai//marketing/aps/?month=""&category=general').then(res=>{
                return res.json()
            }).then(response=>{    
                const nameArr = [] 
                const idValueArr = [] 
                for(let id in response){ 
                    nameArr.push(response[id].da_range)
                    idValueArr.push(parseInt(response[id].id_count))
                }  
                Highcharts.chart('apsChartContainer', {
                    chart: {
                        type: 'column'
                    },
                    title: {
                        text: 'APS',
                        align: 'center'
                    }, 
                    xAxis: {
                        categories: nameArr,
                        crosshair: true,
                        accessibility: {
                            description: 'Countries'
                        }
                    },  
                    series: [
                        {
                            name: 'Data',
                            data: idValueArr
                        }
                    ]
                })
            }).catch(()=>{
                alert('some error to fetch brand lift data')
            })
        // Aps chart end 

        // Unique blog chart start
            fetch('https://api.adlift.ai/marketing/unique_charts/?campaign_id=155').then(res=>{
                return res.json()
            }).then(response=>{  
                let updateResponse = response.data 
                const nameArr = [] 
                const totalDomainArr = [] 
                const uniqueDomainArr = [] 
                for(let id in updateResponse){ 
                    nameArr.push(updateResponse[id].Campaign_name)
                    totalDomainArr.push(parseInt(updateResponse[id].total_domain_count))
                    uniqueDomainArr.push(parseInt(updateResponse[id].unique_domain_count))
                }  
                Highcharts.chart('uniqueBlogChartContainer', {
                    chart: {
                        type: 'column'
                    },
                    title: {
                        text: 'Unique blog',
                        align: 'center'
                    }, 
                    xAxis: {
                        categories: nameArr,
                        crosshair: true,
                        accessibility: {
                            description: 'Countries'
                        }
                    },  
                    series: [
                        {
                            name: 'Total Domain',
                            data: totalDomainArr
                        },
                        {
                            name: 'Unique Domain',
                            data: uniqueDomainArr
                        }
                    ]
                    
                })
            }).catch(()=>{
                alert('some error to fetch unique blog data')
            })
        // Unique blog chart end 

        // Payment chart start
            fetch('https://api.adlift.ai/marketing/payment/').then(res=>{
                return res.json()
            }).then(response=>{  
                let updateResponse = response.data 
                const nameArr = [] 
                const paidArr = [] 
                const pendingArr = [] 
                for(let id in updateResponse){ 
                    nameArr.push(updateResponse[id].campaign_names)
                    paidArr.push(parseInt(updateResponse[id].Paid))
                    pendingArr.push(parseInt(updateResponse[id].Pending))
                }  
                Highcharts.chart('paymentChartContainer', {
                    chart: {
                        type: 'column'
                    },
                    title: {
                        text: 'Payment',
                        align: 'center'
                    }, 
                    xAxis: {
                        categories: nameArr,
                        crosshair: true,
                        accessibility: {
                            description: 'Payment'
                        }
                    },  
                    series: [
                        {
                            name: 'Paid',
                            data: paidArr
                        },
                        {
                            name: 'Pending',
                            data: pendingArr
                        }
                    ]
                    
                })
            }).catch(()=>{
                alert('some error to fetch payment data')
            })
        // Payment chart end 

        // No of sites chart start
            fetch('https://api.adlift.ai/marketing/links/').then(res=>{
                return res.json()
            }).then(response=>{  
                let updateResponse = response.data  
                const nameArr = [] 
                const durationArr = [] 
                const uniqueDomainsArr = [] 
                for(let id in updateResponse){ 
                    nameArr.push(updateResponse[id].campaign_name)
                    durationArr.push(parseInt(updateResponse[id].duration))
                    uniqueDomainsArr.push(parseInt(updateResponse[id].unique_domains))
                }  
                Highcharts.chart('noOfSiteChartContainer', {
                    chart: {
                        type: 'column'
                    },
                    title: {
                        text: 'No. of Sites',
                        align: 'center'
                    }, 
                    xAxis: {
                        categories: nameArr,
                        crosshair: true,
                        accessibility: {
                            description: 'Countries'
                        }
                    },  
                    series: [
                        {
                            name: 'Duration',
                            data: durationArr
                        },
                        {
                            name: 'Unique Domain',
                            data: uniqueDomainsArr
                        }
                    ]
                    
                })
            }).catch(()=>{
                alert('some error to fetch no of sites data')
            })
        // No of sites chart end 

        // Amount chart start
            fetch('https://api.adlift.ai/marketing/amount_chart/?month=2023-12').then(res=>{
                return res.json()
            }).then(response=>{  
                let updateResponse = response.data   
                const nameArr = [] 
                const domainArr = []  
                for(let id in updateResponse){ 
                    nameArr.push(updateResponse[id].name)
                    domainArr.push(parseInt(updateResponse[id].domain_count)) 
                }  
                Highcharts.chart('amountChartContainer', {
                    chart: {
                        type: 'column'
                    },
                    title: {
                        text: 'Amount',
                        align: 'center'
                    }, 
                    xAxis: {
                        categories: nameArr,
                        crosshair: true,
                        accessibility: {
                            description: 'Countries'
                        }
                    },  
                    series: [
                        {
                            name: 'Domain count',
                            data: domainArr
                        }
                    ]
                    
                })
            }).catch(()=>{
                alert('some error to fetch amount data')
            })
        // Amount chart end 

        // Approval chart start
            fetch('https://api.adlift.ai/marketing/pie_chart/').then(res=>{
                return res.json()
            }).then(response=>{  
                let updateResponse = response
                let data = {}
                const nameArr = []  
                for(let id in updateResponse){  
                    data = {
                        name : updateResponse[id].status_label,
                        y : updateResponse[id].count
                    }
                    nameArr.push(data) 
                }  
                Highcharts.chart('approvalChartContainer', {
                    chart: {
                        type: 'pie'
                    },
                    title: {
                        text: 'Approval',
                        align: 'center'
                    },    
                    series: [
                        {
                            name: 'Domain count',
                            data: nameArr
                        }
                    ]
                    
                })
            }).catch(()=>{
                console.log('some error to fetch approval data')
            })
        // Approval chart end 

        // Da chart start
            fetch('https://api.adlift.ai//marketing/da_cost').then(res=>{
                return res.json()
            }).then(response=>{   
                const updateResponse = response.data
                const amountArr = [] 
                const daArr = [] 
                for(let id in updateResponse){ 
                    amountArr.push(updateResponse[id].TotalAmount)
                    daArr.push(updateResponse[id].Da)
                }   
                Highcharts.chart('daChartContainer', {
                    chart: {
                        type: 'column'
                    },
                    title: {
                        text: 'DA',
                        align: 'center'
                    }, 
                    xAxis: {
                        categories: daArr,
                        crosshair: true,
                        accessibility: {
                            description: 'Countries'
                        }
                    },  
                    series: [
                        {
                            name: 'Data',
                            data: amountArr
                        }
                    ]
                })
            }).catch(()=>{
                alert('some error to fetch DA data')
            })
        // Da chart end 
         
    })()
</script>

<body>
    <div class="header_container">
        <div class="cards_Container">
            <div class="row">
                <div class="col-lg-6 my-2">
                    <a href="#" class="dash_card">
                        <div>
                            <h5 class="card_heading">No. of Sites</h5>
                            <h5 class="card_value total_websites">00</h5>
                            <h5 class="card_view">View Report ></h5>
                        </div>
                        <div>
                            <img src="Assets/Icons/NumSites.svg" width="32px" height="32px" alt="">
                        </div>

                    </a>
                </div>
                <div class="col-lg-6 my-2">
                    <a href="#" class="dash_card">
                        <div>
                            <h5 class="card_heading">Avearge DA</h5>
                            <h5 class="card_value total_da">00</h5>
                            <h5 class="card_view">View Report ></h5>
                        </div>
                        <div>
                            <img src="Assets/Icons/Avreg.svg" width="32px" height="32px" alt="">
                        </div>

                    </a>
                </div>
                <div class="col-lg-6 my-2">
                    <a href="#" class="dash_card">
                        <div>
                            <h5 class="card_heading">Total Traffic (All Sites)</h5>
                            <h5 class="card_value total_traffic">00</h5>
                            <h5 class="card_view">View Report ></h5>
                        </div>
                        <div>
                            <img src="Assets/Icons/TotalTrafic.svg" width="32px" height="32px" alt="">
                        </div>

                    </a>
                </div>
                <div class="col-lg-6 my-2">
                    <a href="#" class="dash_card">
                        <div>
                            <h5 class="card_heading">New Unique Sites</h5>
                            <h5 class="card_value total_unique_sites">00</h5>
                            <h5 class="card_view">View Report ></h5>
                        </div>
                        <div>
                            <img src="Assets/Icons/Avreg.svg" width="32px" height="32px" alt="">
                        </div>

                    </a>
                </div>
            </div>
        </div>
        <div class="chartContainer">
            <div class="chartCard my-2">
                <div class="chartHeading">
                    <h4 class="chart_title">
                        Total Category
                    </h4>
                    <h4 class="card_view">View Report ></h4>
                </div>
                <div class="pieChart_Card">
                    <div id="categoryChartContainer"></div>
                </div>
            </div>
        </div>
    </div>

    <div class="header_container">
        <div class="chartContainer">
            <div class="chartCard my-2">
                <div class="chartHeading">
                    <h4 class="tableMainTitle">
                        <span class="icon"><img src="Assets/Icons/Wallet.svg">  </span> Payments
                    </h4>

                </div>
                <div class="pieChart_Card"> 
                    <div id="paymentChartContainer"></div>
                </div>
            </div>
        </div>
        <!-- <div class="chartContainer">
            <div class="chartCard my-2">
                <div class="chartHeading">
                    <h4 class="tableMainTitle">
                        No. of Sites
                    </h4>

                </div>
                <div class="pieChart_Card">
                <h4>Chart Img</h4>
                    <div id="noOfSiteChartContainer"></div> 
                </div>
            </div>
        </div> -->
    </div>


    <div class="header_container">
        <div class="chartContainerTwo">
            <div class="chartCard my-2">
                <div class="chartHeading">
                    <h4 class="tableMainTitle">
                        No. of Sites By Cost
                    </h4>
                </div>
                <div class="pieChart_Card">
                    <div id="numberOfSiteByCostChartContainer"></div>
                </div>
            </div>
        </div>
        <div class="chartContainerThree">
            <div class="chartCard my-2">
                <div class="chartHeading">
                    <h4 class="tableMainTitle">
                        Top Category
                    </h4>
                    <h4 class="card_view">View Report ></h4>

                </div>
                <div class="pieChart_Card"> 
                    <div id="topCategoryChartContainer"></div>
                </div>
            </div>
        </div>
    </div>
    <div class="header_container">
        <div class="chartContainer">
            <div class="chartCard my-2">
                <div class="chartHeading">
                    <h4 class="tableMainTitle">
                        DA by Cost
                    </h4>
                    <h4 class="card_view">View Report ></h4>
                </div>
                <div class="pieChart_Card"> 
                    <div id="daChartContainer"></div>
                </div>
            </div>
        </div>
        <div class="chartContainer">
            <div class="chartCard my-2">
                <div class="chartHeading">
                    <h4 class="tableMainTitle">
                        APS
                    </h4>
                    <h4 class="card_view">View Report ></h4>
                </div>
                <div class="pieChart_Card"> 
                    <div id="apsChartContainer"></div>
                </div>
            </div>
        </div>
    </div>
    <div class="header_container">
        <div class="chartContainerThree">
            <div class="chartCard my-2">
                <div class="chartHeading">
                    <h4 class="tableMainTitle">
                        Approval Listing
                    </h4>
                </div>
                <div class="pieChart_Card">
                    <div id="approvalChartContainer"></div>
                </div>
            </div>
        </div>
        <div class="chartContainerTwo">
            <div class="chartCard my-2">
                <div class="chartHeading">
                    <h4 class="tableMainTitle">
                        BrandLift
                    </h4>
                    <h4 class="card_view">View Report ></h4>
                </div>
                <div class="pieChart_Card">
                    <!-- <h4>Chart Img</h4> -->
                    <div id="brandLiftChartContainer"></div>
                </div>
            </div>
        </div>
    </div>


    <div class="header_container">
        <div class="chartContainer">
            <div class="chartCard my-2">
                <div class="chartHeading">
                    <h4 class="tableMainTitle">
                        Unique Blog Chart
                    </h4>
                </div>
                <div class="pieChart_Card">
                    <!-- <h4>Chart Img</h4> -->
                    <div id="uniqueBlogChartContainer"></div>
                </div>
            </div>
        </div>
    </div>
    
    <div class="header_container">
        <div class="chartContainer">
            <div class="chartCard my-2">
                <div class="chartHeading">
                    <h4 class="tableMainTitle">
                        No. of Sites
                    </h4>
                </div>
                <div class="pieChart_Card"> 
                    <div id="noOfSiteChartContainer"></div> 
                </div>
            </div>
        </div>
    </div>

    <div class="header_container">
        <div class="chartContainer">
            <div class="chartCard my-2">
                <div class="chartHeading">
                    <h4 class="tableMainTitle">
                        Amount
                    </h4>
                </div>
                <div class="pieChart_Card"> 
                    <div id="amountChartContainer"></div> 
                </div>
            </div>
        </div>
    </div>


</body>

</html>