<!-- <div class="widget-box"> <?php $filterVars = $viewData->get('filterVars') ?>
        <div class="widget-title"> <span class="icon"> <i class="icon-search"></i> </span>
          <h5>Advance Filter</h5>
        </div>
      <div class="widget-content nopadding">
       <form action="#" method="post" class="form-horizontal filter-form" id="FilterForm">
	<div class="control-group">
	  <div class="span5">
		<label>Keywords</label>
		<input name="keyword" id="FilterKeyword" type="text" class="span12" value="<?php echo !empty($filterVars['keyword']) ? $filterVars['keyword'] : '' ?>" />
		<span class="help">Enter name, username or email</span>
	  </div>
	  <div class="span3">
	   <label>Status</label>
	    <select name="status" id="FilterStatus">
		<?php echo getFormOptions(array('-1'=>'All', '1'=>'Active', '0'=>'Deactive'), $filterVars['status']); ?>
	    </select>
	  </div>
	</div>
	<div class="form-actions">
	 <button type="reset" class="btn btn-primary">Reset</button>
	 <button type="submit" class="btn btn-success">Filter</button>
       </div>
    </form>
   </div>
  </div> -->
  
<?php $filterVars = $viewData->get('filterVars') ?>
<form action="#" method="post" class="form-horizontal filter-form" id="FilterForm">
	<div class="row mt-4">
		<div class="col-lg-4">
			<div class="mb-3">
				<label for="ListOfPersons" class="form-label">Keyword</label>
				<input type="text" class="form-control" placeholder="Enter name, username or email" name="keyword" id="FilterKeyword" value="<?php echo !empty($filterVars['keyword']) ? $filterVars['keyword'] : '' ?>"> 
			</div>
		</div>
		<div class="col-lg-3">
			<label for="ListOfPersons" class="form-label">Status</label>
			<!-- <select class="form-select form-control">
				<option disabled selected>Status</option>
				<option value="1">All</option>
			</select> -->
			<select name="status" id="FilterStatus" class="form-select1 form-control1">
				<?php echo getFormOptions(array('-1'=>'All', '1'=>'Active', '0'=>'Deactive'), $filterVars['status']); ?>
	    	</select>
		</div>
		<div class="col-lg-6">
			<button type="reset" class="clearbtn">Clear</button>
			<button type="submit" class="filterbtn mx-3">Filter</button>
		</div>
 	</div>
 </form>

<?php $viewData->scriptStart() ?>
$(document).ready(function(){
	$('#FilterForm').bind('submit', function(e){
		e.preventDefault();
		var keyword = $('#FilterKeyword').val();
		var status = $("#FilterStatus").multiselect("getChecked").map(function(){
			return this.value;    
		     }).get()[0];
			
		var curUrl = $.parseUrl();
		var queryString = curUrl.query;
		 
		queryString._kw = $.base64.encode(keyword); 
		queryString._st = $.base64.encode(status>-1?status:'');
		queryString.p = 1;
		var urlParams = [];
		
		$.each(queryString, function(k,v){
			if(v.length>0){
				urlParams.push(k+'='+v);
			}
		});
		
		window.location = '?'+urlParams.join('&');
	});
});
<?php $viewData->scriptEnd() ?>