 <?php $filterVars = $viewData->get('filterVars') ?>
 <div class="widget-title">
     <!-- <span class="icon"> <i class="icon-search"></i> </span> -->
     <h5 class="tableMainTitle my-2">Advance Filter</h5>
 </div>
 <form action="#" method="post" class="form-horizontal filter-form" id="FilterForm">
     <div class="row">
         <div class="col-lg-4">
             <label class="form-label">Domain</label>
             <input name="domain" autocomplete="off" id="FilterDomain" type="text" class="form-control"
                 value="<?php echo !empty($filterVars['domain']) ? $filterVars['domain'] : '' ?>" class="typeahead" />
         </div>

         <div class="col-lg-4">
             <label class="form-label">Status</label>
             <select name="status[]" id="FilterDomainsStatus" multiple="multiple" class="form-control"
                 style="width:400px">
                 <?php echo getFormOptions(array('1'=>'Allowed', '0'=>'Black Listed', '-1'=>'All'), $filterVars['status']); ?>
             </select>
         </div>

         <div class="col-lg-12 my-4">
             <button type="reset" class="clearbtn">Reset</button>
             <button type="submit" class="filterbtn mx-3">Filter</button>
         </div>
     </div>
 </form>


 <?php $viewData->scriptStart() ?>
 $(document).ready(function(){
 // autocomplete
 $('#FilterDomain').typeahead({
 ajax: 'domains.php?act=list_json',
 display: 'name',
 val: 'name'
 });
 $('#FilterForm').bind('submit', function(e){
 e.preventDefault();
 var domain = $('#FilterDomain').val();
 var status = $("#FilterDomainsStatus").multiselect("getChecked").map(function(){
 return this.value;
 }).get();
 var stAt = [];
 $.each(status, function(k,v){
 if(v>-1){
 stAt.push(v);
 }
 });
 var curUrl = $.parseUrl();
 var queryString = curUrl.query;
 queryString._dom = $.base64.encode(domain.toString());
 queryString._st = $.base64.encode(stAt.toString());
 queryString.p = 1;
 var urlParams = [];
 $.each(queryString, function(k,v){
 if(v.length>0){
 urlParams.push(k+'='+v);
 }
 });
 window.location = '?'+urlParams.join('&');
 });
 });
 <?php $viewData->scriptEnd() ?>