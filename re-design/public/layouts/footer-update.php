                  </div>
                </div>
            </div>
        </div>
        </div>
        <!-- jquery-files -->
        <script src="Assets/Javascript/jquery.js"></script>
        <script src="Assets/Javascript/Main.js"></script>
        <script src="Assets/Javascript/bootstrap.bundle.min.js"></script>
                  
        <script src="https://cdn.datatables.net/1.13.7/js/jquery.dataTables.min.js"></script>
        <script src="https://cdn.datatables.net/1.13.7/js/dataTables.bootstrap5.min.js"></script>

        <!--end-Footer-part-->
          
        <?php 
            $scriptArray = [
                'js/jquery.min.js',
                'js/jquery-ui_1_10_3.js',
                'js/jquery.validate.js',
                'js/jquery.uniform.js',
                'js/bootstrap.min.js',
                'js/bootstrap-datepicker.js',
                'js/bootstrap-typeahead.js',
                'js/jquery.tablesorter.min.js',
                'js/jquery.base64.js',
                'js/adlift.js',
                'js/colResizable-1.3.min.js',
                'js/jquery.parseurl.min.js',
                //'js/jquery.multiselect.js',
                'js/jquery.multiselect.filter.js',
                'js/select2.min.js'
            ];
             
              if(subStr($_SERVER['SCRIPT_NAME'],-17) != 'new_dashboard.php'){
                $scriptArray[] = 'js/jquery.multiselect.js';
              }   
              $viewData->scripts($scriptArray)
        ?>
        <?php $viewData->scriptStart() ?>
            jQuery(document).ready(function($){
              tblResizeInIt();
              multiSelectRender();
              uniformInIt();
              $('.datepicker').datepicker();
              $('.datepicker').on('changeDate', function(ev){
            $(this).datepicker('hide');
        });
            });
          <?php $viewData->scriptEnd() ?>
        <?php $viewData->script_for_layout() ?>

        <?php sqlDebugLogs() ?>

    </body>
</html>