 <?php $filterVars = $viewData->get('filterVars') ?>
 <div class="widget-title">
     <!-- <span class="icon"> <i class="icon-search"></i> </span> -->
     <h5 class="tableMainTitle my-2">Advance Filter</h5>
 </div>
 <form action="#" method="post" class="form-horizontal filter-form" id="FilterForm">
     <div class="row mt-4">
         <div class="col-lg-6">
             <p class="form-label">Paypal</p>
             <input type="hidden" id="FilterPaypalEmail"
                 value="<?php echo !empty($filterVars['paypal_email']) ? join(",", $filterVars['paypal_email']) : '' ?>"
                 class="form-control1" style="width:500px" />
         </div>

         <div class="col-lg-4">
             <label class="form-label">Status</label>
             <select name="status[]" id="FilterPaypalStatus" multiple="multiple" class="form-select1 form-control"
                 style="width:500px">
                 <?php echo getFormOptions(array('1'=>'Allowed', '0'=>'Black Listed', '-1'=>'All'), $filterVars['status']); ?>
             </select>
         </div>
         <div class="col-lg-12 mt-3">
             <button type="reset" class="clearbtn">Reset</button>
             <button type="submit" class="filterbtn mx-3">Filter</button>
         </div>
     </div>
 </form>



 <?php $viewData->scriptStart() ?>
 $(document).ready(function(){

 $('#FilterForm').bind('submit', function(e){
 e.preventDefault();
 var paypal_email = $("#FilterPaypalEmail").select2("val");
 var status = $("#FilterPaypalStatus").multiselect("getChecked").map(function(){
 return this.value;
 }).get();
 var stAt = [];
 $.each(status, function(k,v){
 if(v>-1){
 stAt.push(v);
 }
 });
 var curUrl = $.parseUrl();
 var queryString = curUrl.query;
 queryString._pe = $.base64.encode(paypal_email.toString());
 queryString._st = $.base64.encode(stAt.toString());
 queryString.p = 1;
 var urlParams = [];
 $.each(queryString, function(k,v){
 if(v.length>0){
 urlParams.push(k+'='+v);
 }
 });
 window.location = '?'+urlParams.join('&');
 });

 select2InIt('#FilterPaypalEmail', "paypal.php?act=email_list_json");
 });
 <?php $viewData->scriptEnd() ?>