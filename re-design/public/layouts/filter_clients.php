 
	<?php $filterVars = $viewData->get('filterVars') ?>
	<div class="mt-3">
		<h4 class="tableMainTitle">Advance Filter</h4>
	</div>
	
	<form action="#" method="post" class="form-horizontal filter-form" id="FilterForm">
      <div class="row">
	 
		<div class="col-md-4" id="CampaignBlock">
	   <label class="form-label">Campaigns</label>
	   <input type="hidden" id="FilterCampaign" value="<?php echo !empty($filterVars['campaigns']) ? join(",", $filterVars['campaigns']) : '' ?>" class="form-control1" style="width:300px"/>
	  </div>
	  <div class="col-md-4">
		<label class="form-label">Status</label>
	    <select name="status[]" id="FilterStatus" multiple="multiple" class="form-control" style="width:300px">
		<?php echo getFormOptions(array('1'=>'Active', '0'=>'Paused', '-1'=>'All'), $filterVars['status']); ?>
	    </select>
	  </div>
	  
		<div class="col-md-4">
	   <label class="form-label">Approval Phase II</label>
	    <select name="sec_status[]" id="FilterSecStatus" multiple="multiple" class="form-control" style="width:300px">
		<?php echo getFormOptions(array('1'=>'Required', '0'=>'Not Required', '-1'=>'All'), $filterVars['sec_status']); ?>
	    </select>
	  </div>
	   
       <div class="col-md-12 mt-3">
	 	<button type="reset" class="clearbtn">Reset</button>
	 	<button type="submit" class="filterbtn mx-3">Filter</button>
       </div>
 
   </div>
   </form>

<?php $viewData->scriptStart() ?>
function CampaignListRender(){
	select2InIt('#FilterCampaign', "clients.php?act=campaign_list_json");
}
$(document).ready(function(){
	CampaignListRender();
	$('#FilterForm').bind('submit', function(e){
		e.preventDefault();
		var campaign = $("#FilterCampaign").select2("val");
		var status = $("#FilterStatus").multiselect("getChecked").map(function(){
			return this.value;    
		     }).get();
		var secStatus = $("#FilterSecStatus").multiselect("getChecked").map(function(){
			return this.value;    
		     }).get();
		var stAt = [];
		var secStAt = [];
		$.each(status, function(k,v){
			if(v>-1){
				stAt.push(v);
			}
		});
		$.each(secStatus, function(k,v){
			if(v>-1){
				secStAt.push(v);
			}
		}); 
		var curUrl = $.parseUrl();
		var queryString = curUrl.query;
		queryString._st = $.base64.encode(stAt.toString());
		queryString._st2 = $.base64.encode(secStAt.toString());
		queryString._camp = $.base64.encode(campaign.toString());
		queryString.p = 1;
		var urlParams = [];
		$.each(queryString, function(k,v){
			if(v.length>0){
				urlParams.push(k+'='+v);
			}
		}); 
		window.location = '?'+urlParams.join('&');
	});
});
<?php $viewData->scriptEnd() ?>