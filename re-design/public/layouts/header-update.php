<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- css-files -->
    <title>PLB - <?php echo $viewData->getTitle() ?></title>

    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <!-- <link rel="stylesheet" href="css/bootstrap.min.css" /> -->
    <link rel="stylesheet" href="css/bootstrap-responsive.min.css" />
    <link rel="stylesheet" href="css/uniform.css" />
    <link rel="stylesheet" href="css/datepicker.css" />
    <link rel="stylesheet" href="css/fullcalendar.css" />
    <!-- <link rel="stylesheet" href="css/style.css" /> -->
    <link rel="stylesheet" href="css/media.css" />
    <link rel="stylesheet" href="css/select2.css" />
    <link rel="stylesheet" href="css/jquery-ui-1.10.3.custom.min.css" />
    <link href="css/font-awesome.css" rel="stylesheet" />
    <link href="css/jquery.multiselect.css" rel="stylesheet" />
    <link href="css/jquery.multiselect.filter.css" rel="stylesheet" />
    <link rel="stylesheet" href="css/jquery.gritter.css" />
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,700,800' rel='stylesheet' type='text/css'>

    <link rel="stylesheet" href="Assets/Css/Style.css">
    <link rel="stylesheet" href="Assets/Css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.13.7/css/dataTables.bootstrap5.min.css">
    <script src="https://code.jquery.com/jquery-3.7.0.js"></script>

    <script src="https://code.highcharts.com/highcharts.js"></script>
    <script src="https://code.highcharts.com/modules/exporting.js"></script>
    <script src="https://code.highcharts.com/modules/accessibility.js"></script>



    <script type="text/javascript">
    var __baseUrl = '<?php echo get_base_url(true) ?>';
    </script>

</head>

<body>
    <!-- loader -->
    <div class="loader_box_overlay hide" style="display:none">
        <div class="loader_box">
            <p><img src="img/spinner.gif"><span class="loader_box_txt">Please wait a while.</span></p>

        </div>
    </div>
    <!-- loader end -->
    <!-- model boxes -->
    <script>
        function closeModal(){
            $('#pop-up-box').hide()
            $('.modal-backdrop').hide()
        }
    </script>
    <div id="pop-up-box" class="modal hide fade1">
        <!-- <div class="modal-popup bg-white"> -->
            <div class="modal-header">
                <h3 class="tableMainTitle">Warning!</h3>
                <button data-dismiss="modal" class="close" type="button" onclick="closeModal()">&times;</button>
            </div>
            <div class="modal-body">
                <p>Sorry, some error occured in processing.</p>
            </div>
        <!-- </div> -->
    </div>
    <!-- end model boxes-->
    <div class="container-fluid">
        <div class="row">
            <!-- left-side -->
            <div class="col-lg p-0" id="left_side">
                <aside class="side_nav  mx-3 ">
                    <div class="brand_icon">
                        <img src="Assets/Icons/Close_btn.svg" width="32px" class="close position-absolute left-19">
                        <img src="Assets/Icons/Logo.svg" class="img-fluid" alt="">
                    </div>
                    <ul class="navbar-nav d-none">
                        <li class="nav-item active">
                            <a class="nav-link" href="#">
                                <img src="Assets/Icons/port_info.svg" class="me-2" alt="">Dashboard</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#">
                                <img src="Assets/Icons/Human_resources.svg" class="me-2" alt="">Charts </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#">
                                <img src="Assets/Icons/Form.svg" class="me-2" alt="">Approvals </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#">
                                <img src="Assets/Icons/Audits.svg" class="me-2" alt="">Users </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#">
                                <img src="Assets/Icons/help.svg" class="me-2" alt="">Link Listing </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#">
                                <img src="Assets/Icons/help.svg" class="me-2" alt="">Link Listing </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#">
                                <img src="Assets/Icons/help.svg" class="me-2" alt="">Beneficiary Listing </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#">
                                <img src="Assets/Icons/help.svg" class="me-2" alt="">Paypal Listing </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#">
                                <img src="Assets/Icons/dns.svg" class="me-2" alt="">Domains </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#">
                                <img src="Assets/Icons/help.svg" class="me-2" alt="">Clients </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#">
                                <img src="Assets/Icons/deployed_code_account.svg" class="me-2" alt="">Updated PR </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#">
                                <img src="Assets/Icons/help.svg" class="me-2" alt="">Influencers </a>
                        </li>
                    </ul>
                    <script>
                    $(document).ready(function() {
                        $(".submenu ul").hide(); 
                        $(".submenu").click(function(e) {  
                            const parentData = this 
                            const thisData = $(this).attr('class')
                            const getClassIndex = thisData.split(' ')[1].at(-1)-1 
                             
                            $('.submenu_back').each(function(index, element) {
                                if(index == getClassIndex){
                                    $(`.submenu_${index+1}`).addClass('open')
                                    $(this).show()
                                }else{
                                    $(this).hide()
                                    $(`.submenu_${index+1}`).removeClass('open')
                                }
                            })
                            //$(this).parent(".submenu").children("li").slideToggle("100");
                            //$(this).find(".right").toggleClass("fa-caret-up fa-caret-down");
                        });
                    });
                    </script>
                    <ul class="navbar-nav ">


                        <!-- New Dashboard -->
                        <?php if($_SESSION['User']['user_type'] == 'superadmin'){ ?>
                        <li class="nav-item"><a class="nav-link" href="new_dashboard.php">
                                <img src="Assets/Icons/Dashboard.svg" alt="" class="mx-1">
                                <span>Dashboard</span></a></li>
                        <?php }else{ ?>
                        <li class="<?php echo is_active_menu('index.php') ? 'active' : '' ?> nav-item"><a
                                class="nav-link" href="index.php">
                                <img src="Assets/Icons/Dashboard.svg" alt="" class="mx-1">
                                <span>Dashboard</span></a> </li>
                        <?php } ?>


                        <li class="submenu submenu_1"> <a class="nav-link" href="#" id="my-btn"> <img src="Assets/Icons/Chart.svg"
                                    alt="" class="mx-1">
                                <span class="menu_title">Reports</span> <span class="openMenu"><img
                                        src="Assets/Icons/menuIcon.svg"></span></a>
                            <ul class="submenu_back submenu_back_1">
                                <li class="nav-sub-item">
                                    <a class="nav-sub-link" href="reports.php?act=category">Category</a>
                                </li>
                                <li class="nav-sub-item">
                                    <a class="nav-sub-link" href="reports.php?act=payment">Payments</a>
                                </li>
                                <li class="nav-sub-item">
                                    <a class="nav-sub-link" href="reports.php?act=brandlift">BrandLift</a>
                                </li>
                                <li class="nav-sub-item">
                                    <a class="nav-sub-link" href="reports.php?act=unique">Unique Blogs</a>
                                </li>
                                <!-- <li class="nav-sub-item">
                                    <a class="nav-sub-link" href="uniqueBlogs_category.php">Unique Blogs</a>
                                </li>
                                <li class="nav-sub-item">
                                    <a class="nav-sub-link" href="#">Approval Listing</a>
                                </li>
                                <li class="nav-sub-item">
                                    <a class="nav-sub-link" href="siteByCost_category.php">No. of Sites</a>
                                </li> -->

                                <!-- <li class="nav-sub-item">
                                    <a class="nav-sub-link" href="#">APS</a>
                                </li> -->
                            </ul>
                        </li>


                        <!-- End SubMenu -->








                        <li class="submenu submenu_2">
                            <a class="nav-link" href="#" id="my-btn">
                                <img src="Assets/Icons/Chart.svg" alt="" class="mx-1">
                                <span class="menu_title">Charts &amp; graphs</span> <span class="openMenu">
                                    <img src="Assets/Icons/menuIcon.svg"></span></a>
                            <ul class="submenu_back submenu_back_2">
                                <li
                                    class="<?php echo is_active_menu('charts.php?act=approval') ? 'active' : '' ?> nav-sub-item">
                                    <a class="nav-sub-link" href="charts.php?act=approval">Approval</a>
                                </li>
                                <li
                                    class="<?php echo is_active_menu('charts.php?act=payment') ? 'active' : '' ?> nav-sub-item">
                                    <a class="nav-sub-link" href="charts.php?act=payment">Payment</a>
                                </li>
                                <?php if(in_array($session->read('User.user_type'), array('superadmin','admin'))): ?>
                                <!-- <li
                                    class="<?php echo is_active_menu('charts.php?act=unique_blogs') ? 'active' : '' ?>  nav-sub-item">
                                    <a class="nav-sub-link" href="charts.php?act=unique_blogs">Unique Blogs</a>
                                </li> -->
                                <li
                                    class="<?php echo is_active_menu('charts.php?act=spend') ? 'active' : '' ?>  nav-sub-item">
                                    <a class="nav-sub-link" href="charts.php?act=spend">Spend</a>
                                </li>
                                <li
                                    class="<?php echo is_active_menu('charts.php?act=spend_campaign') ? 'active' : '' ?>  nav-sub-item">
                                    <a class="nav-sub-link" href="charts.php?act=spend_campaign">Spend By Campaign
                                    </a>
                                </li>
                                <li
                                    class="<?php echo is_active_menu('charts.php?act=spend_user') ? 'active' : '' ?>  nav-sub-item">
                                    <a class="nav-sub-link" href="charts.php?act=spend_user">Spend By User </a>
                                </li>
                                <li
                                    class="<?php echo is_active_menu('charts.php?act=influencer') ? 'active' : '' ?>  nav-sub-item">
                                    <a class="nav-sub-link" href="charts.php?act=influencer">Influencers </a>
                                </li>

                                <?php endif ?>
                                <!--<li class="<?php echo is_active_menu('charts.php?act=link_submission_user') ? 'active' : '' ?> nav-item"><a class="nav-link" href="charts.php?act=link_submission_user">Link Submission User Wise</a></li>
                            <li class="<?php echo is_active_menu('charts.php?act=link_submission_clientent') ? 'active' : '' ?> nav-item"><a class="nav-link" href="charts.php?act=link_submission_client">Link Submission Client Wise</a></li>-->
                            </ul>
                        </li>
                        <li class="submenu submenu_3"> <a class="nav-link" href="#" id="my-btn"> <img
                                    src="Assets/Icons/Approvals.svg" alt="" class="mx-1">
                                <span class="menu_title">Approval</span> <span class="openMenu"><img
                                        src="Assets/Icons/menuIcon.svg"></span></a>
                            <ul class="submenu_back submenu_back_3">
                                <li
                                    class="<?php echo is_active_menu('approvals.php?act=add_link') ? 'active' : '' ?> nav-sub-item">
                                    <a class="nav-sub-link" href="approvals.php?act=add_link">Add Link</a>
                                </li>
                                <li class="<?php echo is_active_menu('approvals.php') ? 'active' : '' ?> nav-sub-item">
                                    <a class="nav-sub-link" href="approvals.php">Listing</a>
                                </li>
                                <?php if(in_array($session->read('User.user_type'), array('superadmin'))): ?>
                                <li
                                    class="<?php echo is_active_menu('approvals.php?act=add_exceptions') ? 'active' : '' ?>  nav-sub-item">
                                    <a class="nav-sub-link" href="approvals.php?act=add_exceptions">Add Duplicate
                                        Exceptions</a>
                                </li>

                                <li
                                    class="<?php echo is_active_menu('approvals.php?act=list_exceptions') ? 'active' : '' ?>  nav-sub-item">
                                    <a class="nav-sub-link" href="approvals.php?act=list_exceptions">List Duplicate
                                        Exceptions</a>
                                </li>
                                <?php endif ?>
                            </ul>
                        </li>
                        <?php if(in_array($session->read('User.user_type'), array('superadmin','admin'))): ?>
                        <li class="submenu submenu_4"> <a class="nav-link" href="#" id="my-btn">
                                <img src="Assets/Icons/UsersIcon.svg" alt="" class="mx-1">
                                <span class="menu_title">Users</span> <span class="openMenu"><img
                                        src="Assets/Icons/menuIcon.svg"></span></a>
                            <ul class="submenu_back submenu_back_4">
                                <li
                                    class="<?php echo is_active_menu('users.php?act=add') ? 'active' : '' ?> nav-sub-item">
                                    <a class="nav-sub-link" href="users.php?act=add">Add User</a>
                                </li>
                                <li class="<?php echo is_active_menu('users.php') ? 'active' : '' ?> nav-sub-item">
                                    <a class="nav-sub-link" href="users.php">User Listing</a>
                                </li>
                            </ul>
                        </li>
                        <?php endif ?>
                        <li class="<?php echo is_active_menu('submitted_links.php') ? 'active' : '' ?> nav-item"><a
                                class="nav-link" href="submitted_links.php">
                                <img src="Assets/Icons/data_check.svg" alt="" class="mx-1">
                                <span>Link
                                    Listing</span></a></li>
                        <li class="<?php echo is_active_menu('beneficiary.php') ? 'active' : '' ?> nav-item"><a
                                class="nav-link" href="beneficiary.php">
                                <img src="Assets/Icons/contact_page.svg" alt="" class="mx-1">
                                <span>Beneficiary
                                    Listing</span></a></li>

                        <?php //if(in_array($session->read('User.user_type'), array('superadmin','admin'))): ?>
                        <?php if(in_array($session->read('User.user_type'), array('superadmin'))): ?>
                        <li class="<?php echo is_active_menu('paypal.php') ? 'active' : '' ?> nav-item"> <a
                                class="nav-link" href="paypal.php"><img src="Assets/Icons/PayPal.svg" alt=""
                                    class="mx-1"> <span>Paypal
                                    Listing</span></a> </li>
                        <?php endif ?>
                        <?php if(in_array($session->read('User.user_type'), array('superadmin','admin'))): ?>
                        <li class="submenu submenu_5">
                            <a class="nav-link" href="#" id="my-btn"><img src="Assets/Icons/dns.svg" alt=""
                                    class="mx-1"> <span class="menu_title">Domains</span><span class="openMenu"><img
                                        src="Assets/Icons/menuIcon.svg"></span></a>
                            <ul class="submenu_back submenu_back_5">
                                <li
                                    class="<?php echo is_active_menu('domains.php?act=add') ? 'active' : '' ?> nav-sub-item">
                                    <a class="nav-sub-link" href="domains.php?act=add"><span>Add New</span></a>
                                </li>
                                <li class="<?php echo is_active_menu('domains.php') ? 'active' : '' ?> nav-sub-item">
                                    <a class="nav-sub-link" href="domains.php"><span>List</span></a>
                                </li>
                            </ul>
                        </li>
                        <li class="submenu submenu_6">
                            <a class="nav-link" href="#" id="my-btn"><img src="Assets/Icons/FolderUser.svg" alt=""
                                    class="mx-1"> <span class="menu_title">Clients</span><span class="openMenu"><img
                                        src="Assets/Icons/menuIcon.svg"></span></a>
                            <ul class="submenu_back submenu_back_6">
                                <li class="<?php echo is_active_menu('clients.php') ? 'active' : '' ?> nav-sub-item">
                                    <a class="nav-sub-link" href="clients.php"><span>List</span></a>
                                </li>
                                <li
                                    class="<?php echo is_active_menu('clients.php?act=assign_user') ? 'active' : '' ?> nav-sub-item">
                                    <a class="nav-sub-link" href="clients.php?act=assign_user"><span>Assign
                                            User</span></a>
                                </li>
                            </ul>
                        </li>

                        <li class="submenu submenu_7">
                            <a class="nav-link" href="#" id="my-btn"><img src="Assets/Icons/deployed_code_account.svg"
                                    alt="" class="mx-1"> <span class="menu_title">Update PR</span><span
                                    class="openMenu"><img src="Assets/Icons/menuIcon.svg"></span></a>
                            <ul class="submenu_back submenu_back_7">
                                <li class="nav-sub-item"><a class="nav-sub-link" href="javascript:void(0)"
                                        class="updateApprovalLinkPr">Domains (Approval Pending)</a></li>
                                <li
                                    class="<?php echo is_active_menu('approvals.php?act=update_all_pr') ? 'active' : '' ?> nav-sub-item">
                                    <a class="nav-sub-link" href="approvals.php?act=update_all_pr"><span>All
                                            Domains</span></a>
                                </li>
                            </ul>
                        </li>




                        <?php endif ?>
                        <!-- Influencers Section added by Nitin -->

                        <li class="submenu submenu_8">
                            <a class="nav-link" href="#" id="my-btn"><img src="Assets/Icons/influsencer.svg" alt=""
                                    class="mx-1">
                                <span class="menu_title">Influencers</span><span><img
                                        src="Assets/Icons/menuIcon.svg"></span></a>
                            <ul class="submenu_back submenu_back_8">
                                <li
                                    class="<?php echo is_active_menu('influencers.php?act=add') ? 'active' : '' ?> nav-sub-item">
                                    <a class="nav-sub-link" href="influencers.php?act=add"><span>Add New</span></a>
                                </li>
                                <li
                                    class="<?php echo is_active_menu('influencers.php') ? 'active' : '' ?> nav-sub-item">
                                    <a class="nav-sub-link" href="influencers.php"><span>List</span></a>
                                </li>
                            </ul>
                        </li>

                        <li class="submenu submenu_9">
                            <a class="nav-link" href="#" id="my-btn"><img src="Assets/Icons/Chart.svg" alt="" class="mx-1">
                                <span class="menu_title">Campaign</span><span><img
                                        src="Assets/Icons/menuIcon.svg"></span></a>
                            <ul class="submenu_back submenu_back_9">
                                <li class="<?php echo is_active_menu('campaign.php?act=add') ? 'active' : '' ?> nav-sub-item">
                                    <a class="nav-sub-link" href="campaign.php?act=add"><span>Add Campaign</span></a>
                                </li>
                                <li class="<?php echo is_active_menu('campaign.php') ? 'active' : '' ?> nav-sub-item">
                                    <a class="nav-sub-link" href="campaign.php"><span>Listings</span></a>
                                </li>
                            </ul>
                        </li>

                        <!-- <li class="<?php echo is_active_menu('approvals.php?act=content_listing') ? 'active' : '' ?> nav-item"> <a class="nav-link" href="approvals.php?act=content_listing"><i class="icon icon-link"></i> <span>Content Listing</span></a> </li> -->

                    </ul>

                </aside>
            </div>
            <!-- right-side -->
            <div class="col-lg" id="right_side">
                <div class="main">
                    <header class="top_nav bg-white d-flex justify-content-between">
                        <img src="Assets/Icons/menu_toggle.svg" width="32px" class="open_menu">
                        <div class="avatar col-lg-12">
                            <div class="user_details d-flex justify-content-between">
                                <h6 class="name">Welcome, <?php echo ucwords($session->read('User.first_name')) ?>
                                </h6>
                                <h6>
                                    <?php if($_SESSION['User']['id'] == 11){ ?>
                                    <a title="Change theme" href="index.php?change-theme=old"
                                        style="text-decoration:none">Theme <sub>(old)</sub></a> &nbsp;&nbsp;&nbsp;
                                    <?php } ?>
                                    <a title="" href="logout.php" class="logout">
                                        <i class="icon-signout"></i>
                                        Logout</a>
                                </h6>
                            </div>
                        </div>
                    </header>