<?php
/**
 *@project PLB Tool
 *@file Main DirectoryIndex file
 *@author   Akhtar Khan
 *@email    akhtar.khan@adlift.com
 **/ 
error_reporting(E_ALL);
ini_set('display_errors', '1');
require 'public' . DIRECTORY_SEPARATOR . 'index.php';